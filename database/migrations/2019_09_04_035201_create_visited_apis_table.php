<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitedApisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visited_apis', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('device_token');
            $table->string('vistited_api');
            $table->string('api_name');
            $table->string('ip_address');
            $table->string('visited_time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visited_apis');
    }
}
