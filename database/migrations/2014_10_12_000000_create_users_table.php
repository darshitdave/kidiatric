<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('relation')->nullable();
            $table->string('mobile_number')->nullable();
            $table->string('otp')->nullable();
            $table->string('email')->unique()->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->longText('address')->nullable();
            $table->integer('city_id')->nullable();
            $table->string('password')->nullable();
            $table->tinyInteger('is_otp_verified')->default(0);//0 for unverified 1 verified 
            $table->tinyInteger('parent_id')->nullable();
            $table->tinyInteger('type')->nullable();//0 for parent 1 other 
            $table->tinyInteger('is_delete')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
