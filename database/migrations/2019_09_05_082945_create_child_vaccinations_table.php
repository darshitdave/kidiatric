<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChildVaccinationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('child_vaccinations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_id')->nullable();
            $table->string('child_id');
            $table->integer('doctor_id')->nullable();
            $table->integer('vaccination_id');
            $table->string('vaccination_name');
            $table->integer('taken_at')->nullable();
            $table->date('vaccination_date')->nullable();
            $table->tinyInteger('vaccination_type');
            $table->tinyInteger('is_active')->default(0); //1 for active 0 deactive
            $table->tinyInteger('good_to_have')->default(0); //0 = not , 1 = has good to have
            $table->tinyInteger('taken_vaccination')->default(0); //0 = not taken, 1 = taken vaccination
            $table->tinyInteger('is_change_date')->default(0); //0 = not change, 1 = change
            $table->tinyInteger('is_good_to_have')->default(0); //0 = not , 1 = good to have
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('child_vaccinations');
    }
}
