<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserWisePriscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_wise_priscriptions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_id');
            $table->integer('child_id')->nullable(); 
            $table->string('title');  
            $table->string('prescription_attachment');
            $table->date('prescription_date');
            $table->double('size')->nullable();
            $table->string('image_token')->nullable();
            $table->tinyInteger('is_delete')->default(0);//0 for not delete
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_wise_priscriptions');
    }
}
