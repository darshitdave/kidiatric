<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVaccinationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vaccinations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('vaccination_name');
            $table->text('vaccination_des');
            $table->tinyInteger('taken_at');
            $table->text('prevents');
            $table->tinyInteger('vaccination_type');//0 for counsil and 1 for other
            $table->tinyInteger('is_active')->default(1); //1 for active 0 deactive
            $table->tinyInteger('is_delete')->default(0); //0 not delete 1 delete
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vaccinations');
    }
}
