<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorWiseVaccinationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctor_wise_vaccinations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('doctor_id');
            $table->tinyInteger('vaccination_id');
            $table->tinyInteger('taken_at');
            $table->tinyInteger('vaccination_type'); //0 for counsil and 1 for other    
            $table->tinyInteger('is_active')->default(1); //1 for active 0 deactive
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctor_wise_vaccinations');
    }
}
