<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChildrenDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('children_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('uuid');
            $table->string('user_id');
            $table->string('child_name');
            $table->date('date_of_birth');
            $table->string('gender');
            $table->string('profile_image')->nullable();
            $table->tinyInteger('is_delete')->default(0);//0 for not delete
            $table->integer('age')->->nullable();
            $table->integer('gender')->nullable;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('children_details');
    }
}
