<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('doctor_id')->nullable();
            $table->string('doctor_name')->nullable();
            $table->string('email')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('mobile_number');
            $table->string('otp')->nullable();
            $table->text('address')->nullable();
            $table->year('practicing_since')->nullable();
            $table->string('city')->nullable();
            $table->string('pincode')->nullable();
            $table->text('specialization')->nullable();
            $table->tinyInteger('otp_verified')->default(0); 
            $table->tinyInteger('is_approve')->default(0); //0 for pending 1 active 2 deactive 
            $table->tinyInteger('is_active')->default(1); //1 for active 0 deactive
            $table->tinyInteger('is_delete')->default(0); //0 not delete 1 delete
            $table->integer('is_complete')->default(1);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctors');
    }
}
