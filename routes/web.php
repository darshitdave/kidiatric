<?php

Route::get('/', function () {
    return view('welcome');
});

Route::get('/privacy-policy', 'HomeController@privacyPolicy')->name('privacyPolicy');
Route::get('/terms-and-conditions', 'HomeController@termsAndConditions')->name('termsAndConditions');
Route::get('/send-notification', 'HomeController@sendNotification')->name('sendNotification');

//admin routes
Route::group(['prefix' => 'administrator-panel', 'namespace' => 'Admin'], function () {

	// Authentication admin Login Routes
	Route::get('login', 'Auth\LoginController@showLoginForm')->name('admin.login');
	Route::post('login', 'Auth\LoginController@login')->name('admin.postlogin');
	Route::get('logout', 'Auth\LoginController@logout')->name('admin.logout');

	//forget and reset password
	Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('admin.auth.password.reset');
	Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('admin.passwordemail');
	Route::get('password/reset/{token?}', 'Auth\ResetPasswordController@showResetForm')->name('admin.auth.password.reset');
	Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('admin.resetpassword');

	//Dashboard Route....
	Route::get('/', 'AdminController@index')->name('admin.dashboard');
	Route::get('admin-profile', 'AdminController@adminProfile')->name('admin.profile');
	Route::post('admin-update-profile', 'AdminController@adminProfileUpdate')->name('admin.update.profile');

	//change password
	Route::get('/change-admin-password', 'AdminController@changeAdminPassword')->name('admin.changeAdminPassword');
	Route::post('/update-admin-password', 'AdminController@updateAdminPassword')->name('admin.updateAdminPassword');
	Route::get('/change-social-media-link', 'AdminController@changeSocialMediaLink')->name('admin.setSocialMediaLink');
	Route::post('/update-social-media-link', 'AdminController@updateSocialMediaLink')->name('admin.updateSocialMediaLink');

	//doctor
	Route::match(['get', 'post'],'admin-doctor-list/{id?}', 'DoctorController@doctorList')->name('admin.doctorList');
	Route::get('admin-add-doctor', 'DoctorController@addDoctor')->name('admin.addDoctor');
	Route::post('save-doctor', 'DoctorController@saveDoctorDetails')->name('admin.saveDoctor');
	Route::post('change-doctor-status', 'DoctorController@changeDoctorStatus')->name('admin.doctor.status');
	Route::get('admin-view-doctor-details/{id}', 'DoctorController@viewDoctorDetails')->name('admin.viewDoctorDetails');
	Route::post('approval-doctor-status', 'DoctorController@approvalDoctorStatus')->name('admin.doctor.approval');
	Route::get('admin-edit-doctor-details/{id}', 'DoctorController@editDoctorDetails')->name('admin.editDoctor');
	Route::get('admin-delete-doctor-details/{id}', 'DoctorController@deleteDoctorDetails')->name('admin.deleteDoctor');

	Route::get('admin-doctors-vaccination/{id}', 'DoctorController@doctorVaccination')->name('admin.doctor.vaccination');
	Route::post('admin-doctor-save-taken', 'DoctorController@saveDoctorTakenAt')->name('admin.doctor.saveTaken');
	Route::post('admin-doctor-get-taken', 'DoctorController@saveDoctorGetTaken')->name('admin.doctor.getTaken');
	Route::post('admin-doc-vac-status','DoctorController@changeDocVaccinationStatus')->name('admin.doctor.vaccination.status');

	//doctor csv
	Route::post('admin-doctor-csv', 'DoctorController@adminDoctorCsv')->name('admin.doctor_csv');

	//check doctor mobile number
	Route::post('doctor-check-mobile', 'DoctorController@checkDoctorMobileNumber')->name('admin.doctor.check_mobile');

	//Vaccination
	Route::match(['get', 'post'],'admin-vaccination-list', 'VaccinationController@vaccinationList')->name('admin.vaccinationList');
	Route::get('admin-add-vaccination', 'VaccinationController@addVaccination')->name('admin.addVaccination');
	Route::post('admin-save-taken', 'VaccinationController@saveAdminTakenAt')->name('admin.saveTaken');
	Route::post('admin-save-vaccination', 'VaccinationController@saveVaccinationDetails')->name('admin.saveVaccination');
	Route::get('admin-edit-vaccination/{id}', 'VaccinationController@editVaccination')->name('admin.editVaccination');
	Route::post('admin-update-vaccination', 'VaccinationController@updateVaccinationDetails')->name('admin.updateVaccination');
	Route::post('change-vaccination-status', 'VaccinationController@changeVaccinationStatus')->name('admin.vaccination.status');
	Route::get('admin-vaccination-details/{id}', 'VaccinationController@deleteVaccinationDetails')->name('admin.deleteVaccination');

	//vaccination csv
	Route::post('admin-vaccination-csv', 'VaccinationController@adminVaccinationCsv')->name('admin.vaccination_csv');
	
	//doctor Vaccination
	Route::match(['get', 'post'],'admin-doctor-vaccination-list', 'VaccinationController@doctorVaccinationList')->name('admin.doctor.vaccinationList');

	//parents details
	Route::match(['get', 'post'],'admin-parents-list/{id?}', 'UserController@ParentsList')->name('admin.ParentsList');
	Route::post('admin-parents-childdetails', 'UserController@parentsChildDetails')->name('admin.parentsChildDetails');
	
	//child
	Route::match(['get', 'post'],'admin-children-list/{id?}', 'UserController@ChildrenList')->name('admin.ChildrenList');
	Route::get('admin-children-vaccinations/{id}', 'UserController@ChildrenVaccination')->name('admin.ChildrenVaccination');
	Route::post('admin-children-parentdetails', 'UserController@ChildrenParentDetails')->name('admin.ChildrenParentDetails');
	Route::post('admin-vaccination-user', 'UserController@adminVaccinationNotificationUser')->name('admin.vaccination.notification_user');

	//notification
	Route::post('get-auto-suggest-doctor','UserController@getAutoSuggestDoctorName')->name('getAutoSuggestDoctorName');	
	Route::get('get-age-from-birthdate', 'UserController@getAgeFromBirthDate')->name('admin.getAgeFromBirthDate');
	Route::post('children-excel-data', 'UserController@childrenExcelData')->name('admin.childrenExcelData');
	Route::post('city-suggestions', 'UserController@citySuggestion')->name('admin.citySuggestion');
	Route::post('parent-excel-data', 'UserController@parentExcelData')->name('admin.parentExcelData');
	
	//Notification Routes
	Route::match(['get', 'post'],'create-notification-list', 'NotificationController@createNotificationList')->name('admin.createNotificationList');
	Route::post('admin-notification-excel-data', 'NotificationController@notificationExcelData')->name('admin.notificationExcelData');
	Route::match(['get', 'post'],'saved-notification-list', 'NotificationController@savedNotificationList')->name('admin.savedNotificationList');
	Route::match(['get', 'post'],'doc-wise-notification-list', 'NotificationController@docSavedNotificationList')->name('admin.doc.savedNotificationList');
	Route::post('send-notification-user', 'NotificationController@sendNotificationuser')->name('admin.sendNotificationuser');
	Route::post('export-doctor-notification-data', 'NotificationController@exportDoctorNotificationData')->name('admin.exportDoctorNotificationData');
	Route::get('view-notification/{id}', 'NotificationController@viewNotification')->name('admin.viewNotification');
	Route::get('view-doctor-notification/{id}', 'NotificationController@viewDoctorNotification')->name('admin.viewDoctorNotification');
	Route::post('send-notification-parent', 'NotificationController@sendLinkedParentNotification')->name('admin.sendLinkedParentNotification');
	Route::post('send-child-parent-notification', 'NotificationController@sendChildNotificationuser')->name('admin.sendChildNotificationuser');
	Route::post('send-user-notification', 'NotificationController@sendUserNotification')->name('admin.sendUserNotification');
	Route::post('admin-notification-image', 'NotificationController@adminNotificationImage')->name('admin.notification_image');
	Route::post('admin-notification-target-user', 'NotificationController@adminNotificationUser')->name('admin.notification_user');
	Route::post('admin-doctor-notification-user', 'NotificationController@adminDoctorNotificationUser')->name('admin.doctor.notification_user');

	//help and support route
	Route::get('help-support-request', 'AdminController@helpSupport')->name('admin.helpSupport');
	Route::get('attend-help-support-request/{id}', 'AdminController@attendSupportRequest')->name('admin.attendSupportRequest');
	Route::get('get-notification-count', 'AdminController@getNotificationCount')->name('admin.getNotificationCount');
	Route::post('get-message-details', 'AdminController@getMessageDetails')->name('admin.getMessageDetails');

	//child documents
	Route::get('child-documents/{id}', 'ChildDocumentController@childDocuments')->name('admin.childDocuments');
	Route::get('download-child-document/{id}', 'ChildDocumentController@downloadAllChildDocument')->name('admin.downloadAllChildDocument');
	Route::get('admin-auth-image/{id}', 'ChildDocumentController@adminAuthImage')->name('adminAuthImage');
	
});


//admin routes
Route::group(['prefix' => 'doctor-panel', 'namespace' => 'Doctor'], function () {

	// Registration Routes
	Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
	Route::post('register-mobile', 'Auth\RegisterController@registerMobileNumber')->name('doctor.register_mobile');
    Route::post('register', 'Auth\RegisterController@create')->name('doctor.postregister');

    Route::post('resend-web-otp', 'Auth\LoginController@resendWebOtp')->name('doctor.resendWebOtp');
    
    Route::post('register-check-otp', 'Auth\RegisterController@checkOTP')->name('doctor.check_otp');

	// Authentication admin Login Routes
	Route::get('login', 'Auth\LoginController@showLoginForm')->name('doctor.login');
	Route::post('login', 'Auth\LoginController@login')->name('doctor.postlogin');
	Route::get('logout', 'Auth\LoginController@logout')->name('doctor.logout');
    
    Route::post('check-mobile', 'Auth\LoginController@checkMobileNumber')->name('doctor.check_mobile');

    //validate otp
    Route::post('register-business', 'Auth\RegisterController@validateOtp')->name('business.validateotp');

	//Dashboard Route....
	Route::get('/', 'DoctorController@index')->name('doctor.dashboard');
	Route::get('doctor-profile', 'DoctorController@doctorProfile')->name('doctor.profile');
	Route::post('doctor-update-profile', 'DoctorController@doctorProfileUpdate')->name('doctor.update.profile');

	//update doctor details
	Route::post('doctor-update-details', 'DoctorController@updateDoctorDetails')->name('doctor.updateDoctor.details');

	//doctor child 
	Route::match(['get', 'post'],'doctor-wise-children/{id?}','DoctorController@doctorWiseChild')->name('doctor.wise.children');
	Route::match(['get','post'],'doctor-children-parentdetails', 'DoctorController@ChildrenParentDetails')->name('doctor.ChildrenParentDetails');
	Route::get('doctor-children-vaccinations/{id}', 'DoctorController@ChildrenVaccination')->name('doctor.ChildrenVaccination');

	//child csv 
	Route::post('doctor-children-excel-data', 'DoctorController@childrenExcelData')->name('doctor.childrenExcelData');

	//get city name of child
	Route::post('doc-city-suggestions', 'DoctorController@citySuggestion')->name('doctor.citySuggestion');

	//doctor child parents
	Route::match(['get', 'post'],'doctor-wise-Child-Parents/{id?}','DoctorController@doctorWiseChildParents')->name('doctor.ParentsList');
	Route::post('doctor-parents-childdetails','DoctorController@parentsChildDetails')->name('doctor.parentsChildDetails');

	//parents excel
	Route::post('doc-parent-excel-data', 'DoctorController@parentExcelData')->name('doctor.parentExcelData');

	//Vaccination
	Route::match(['get', 'post'],'doctor-vaccination-list', 'VaccinationController@vaccinationList')->name('doctor.vaccinationList');
	Route::post('doctor-save-taken', 'VaccinationController@saveDoctorTakenAt')->name('doctor.saveTaken');
	Route::post('doctor-change-vaccination-status','VaccinationController@changeVaccinationStatus')->name('doctor.vaccination.status');

	Route::post('disable-doctor-modal','VaccinationController@disableDoctorModel')->name('doctor.disable.model');

	//Child Document
	Route::get('doctor-child-documents/{id}', 'ChildDocumentController@doctorWiseChildDocument')->name('doc.doctorWiseChildDocument');
	Route::get('download-all-documents/{id}', 'ChildDocumentController@downloadAllDocument')->name('doc.downloadAllDocument');

	//Notification Routes
	Route::match(['get', 'post'],'doctor-create-notification-list', 'NotificationController@createNotificationList')->name('doctor.createNotificationList');
	Route::get('doc-create-notification','NotificationController@docCreateNotification')->name('doc.create_notification');
	Route::post('doc-send-user-notification', 'NotificationController@sendUserNotification')->name('doc.sendUserNotification');
	Route::post('doc-send-notification-user', 'NotificationController@sendNotificationuser')->name('doc.sendNotificationuser');
	Route::match(['get', 'post'],'doc-saved-notification-list', 'NotificationController@savedNotificationList')->name('doc.savedNotificationList');
	Route::post('doctor-notification-excel-data', 'NotificationController@notificationExcelData')->name('doctor.notificationExcelData');
	Route::post('doc-notification-image', 'NotificationController@docNotificationImage')->name('doc.notification_image');
	Route::post('doc-notification-target-user', 'NotificationController@docNotificationUser')->name('doc.notification_user');
	Route::get('doctor-notification/{id}', 'NotificationController@doctorNotification')->name('doc.doctorNotification');
	Route::post('doctor-notification-user', 'NotificationController@doctorNotificationUser')->name('doctor.notification_user');

	//Health and support request
	Route::post('doctor-help-support-request', 'NotificationController@saveHelpSupportRequest')->name('doc.saveHelpSupportRequest');

	Route::get('/auth-image/{secret}','ChildDocumentController@authImage')->name('doc.authImage');
	

});