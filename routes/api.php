<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//check app version
Route::post('check_version_app', 'ApiController@checkAppVersion')->name('checkAppVersion');

//about us link
Route::post('video_link', 'ApiController@videoLink')->name('videoLink');

//Api routes	
Route::post('store_logs','ApiController@LogInput')->name('store.logs');
Route::post('store_logoutput/{output}','ApiController@LogOutput')->name('store.logoutput');
Route::post('generate_device_token', 'ApiController@generateDeviceToken')->name('generateDeviceToken');

//register api
Route::post('user_register', 'ApiController@userRegister')->name('userRegister');
Route::post('user_register_otp', 'ApiController@userRegisterValidateOtp')->name('userRegisterValidateOtp');

//set doctor id
Route::post('set_doctor_id', 'ApiController@doctorId')->name('doctorId');

//login api
Route::post('user_login', 'ApiController@userLogin')->name('userLogin');
Route::post('user_login_otp', 'ApiController@UserLoginValidateOtp')->name('UserLoginValidateOtp');

//change mobile number
Route::post('reset_mobile_number', 'ApiController@resetMobileNumber')->name('resetMobileNumber');
Route::post('reset_mobile_number_otp', 'ApiController@resetMobileNumberOTP')->name('resetMobileNumberOTP');

//resend otp
Route::post('resend_otp', 'ApiController@resendOtp')->name('resendOtp');

//update parent details
Route::post('update_parent_details', 'ApiController@updateParentDetails')->name('updateParentDetails');

//link parent
Route::post('list_link_parent', 'ApiController@ListLinkParentDetail')->name('ListLinkParentDetail');
Route::post('add_link_parent', 'ApiController@addLinkParentDetail')->name('addParent');
Route::post('update_link_parent', 'ApiController@updateLinkParentDetail')->name('updateLinkParentDetail');
Route::post('delete_link_parent', 'ApiController@deleteLinkParent')->name('deleteLinkParent');


//child details
Route::post('children_list', 'ApiController@childrenList')->name('childrenList');
Route::post('add_child_details', 'ApiController@addChildDetails')->name('addChildDetails');
Route::post('add_child_doctor', 'ApiController@addchildDoctor')->name('addchildDoctor');
Route::post('change_child_doctor', 'ApiController@changechildDoctor')->name('changechildDoctor');
Route::post('delete_child', 'ApiController@deleteChild')->name('deleteChild');

//range of vaccination
Route::post('vaccination_range', 'ApiController@vaccinationRange')->name('vaccinationRange');

//doctor wise child vaccination
Route::post('child_wise_doctor_vacc', 'ApiController@childWiseDoctorVaccination')->name('childWiseDoctorVaccination');
Route::post('child_taken_vacc', 'ApiController@childVaccinationTaken')->name('childVaccinationTaken');
//remaining flag
Route::post('child_wise_vacc', 'ApiController@childWiseVaccination')->name('childWiseDoctorVaccination');
Route::post('child_remaining_vacc', 'ApiController@childWiseRemainingVaccination')->name('childWiseRemainingVaccination');

//change due date
Route::post('change_date_vacc', 'ApiController@changeDateVaccination')->name('changeDateVaccination');

//child wise vaccination
Route::post('select_good_to_have_vaccination', 'ApiController@selectGoodToHaveVaccination')->name('selectGoodToHaveVaccination');
Route::post('add_good_to_have_vaccination', 'ApiController@addGoodToHaveVaccination')->name('addGoodToHaveVaccination');

//add prescription
Route::post('List_of_prescription', 'ApiController@listOfPrescription')->name('listOfPrescription');
Route::post('add_prescription', 'ApiController@addPrescription')->name('addPrescription');
Route::post('delete_prescription', 'ApiController@deletePrescription')->name('deletePrescription');

//dashboard
Route::post('child_wise_Vaccine', 'ApiController@childWiseVaccine')->name('childWiseVaccine');

//calendar data
Route::post('calendar_data', 'ApiController@childWiseCalData')->name('childWiseCalData');

//calendar data
Route::post('graph_data', 'ApiController@childWiseGraphData')->name('childWiseGraphData');

Route::post('edit_child_details', 'ApiController@editChildDetails')->name('editChildDetails');

//notification
Route::post('notification_list', 'ApiController@userNotificationList')->name('userNotificationList');
Route::post('notification_update', 'ApiController@userNotificationUpdate')->name('userNotificationUpdate');
//unread notifications
Route::post('user_unread_notification', 'ApiController@UserUnreadNotification')->name('UserUnreadNotification');
//update status of notification
Route::post('update_notification_status', 'ApiController@notificationStatusChanage')->name('notificationStatusChanage');

//delete goodtohave
Route::post('remove_good_to_have', 'ApiController@removeGoodToHave')->name('removeGoodToHave');