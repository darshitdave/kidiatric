<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class DoctorAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$guard = 'doctor'){

        if(count(Auth::guard('doctor')->user()) > 0){
            if(Auth::guard('doctor')->user()->is_active != 1 || (Auth::guard('doctor')->user()->is_approve != 1 && Auth::guard('doctor')->user()->is_approve != 0 )|| Auth::guard('doctor')->user()->is_delete != 0){
                if(!Auth::guard($guard)->check()){
                    
                    return redirect(Route('doctor.login'));
                }return redirect(Route('doctor.logout'));
            }
        }else{
            return redirect(Route('doctor.login'));
        }
        return $next($request);
    }
}
