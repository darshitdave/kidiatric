<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class DoctorRedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $nex, $guard = 'doctor'){

        if (Auth::guard($guard)->check()) {
            
            return redirect(Route('doctor.dashboard'));
        }

        return $next($request);
    }
}
