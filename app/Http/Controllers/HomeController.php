<?php

namespace App\Http\Controllers;

use DateTime;
use App\Model\User;
use App\Model\UserToken;
use App\Model\Vaccination;
use App\Model\UserLinkParent;
use App\Model\ChildrenDetail;
use App\Model\Notification;
use Illuminate\Http\Request;
use App\Model\NotificationUser;
use App\Model\ChildVaccination;
use App\Model\UserChildWiseDoctor;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Model\DoctorWiseVaccination;
use App\Http\Controllers\GlobalController;
use App\Model\ChildAlreadyTakenVaccination;

class HomeController extends GlobalController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth',['except' => ['privacyPolicy','termsAndConditions','getAgeFromBirthDate','sendNotification']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
      
    }

    public function privacyPolicy(){
        return view('privacy_policy');
    }

    public function termsAndConditions(){
        return view('terms_and_conditions');   
    }

    public function getAgeFromBirthDate(){

        $getChildernList = ChildrenDetail::all();

        if(!is_null($getChildernList)){
            foreach($getChildernList as $k => $v){
                $from = new DateTime($v->date_of_birth);
                $to   = new DateTime('today');
                $year = $from->diff($to)->y;
                $months = $from->diff($to)->m;
                $days = $from->diff($to)->d;
                $year = $year * 365;
                $months = $months * 30;
                $days = $days + $year + $months;
                $updateChildrenDetails = ChildrenDetail::where('id',$v->id)->update(['age' => $days]);
            }
        }

        echo "Age Updated";
        exit;
    }

    public function sendNotification(Request $request){

        \Log::info('cron run successfully'.date('Y-m-d'));

        $get_users = User::where('is_otp_verified','1')->where('is_delete','0')->get();
       
        $clientId = array();
        $vv_ids = array();
        foreach ($get_users as $gk => $gv) {

            $get_link_parent = UserLinkParent::where('link_parent_id',$gv->id)->where('is_delete','0')->pluck('user_id');

            $get_link_parent[] = $gv->id;
            
            $get_child_details = ChildrenDetail::whereIn('user_id',$get_link_parent)->where('is_delete','0')->orderBy('date_of_birth','DESC')->get();

            if(!is_null($get_child_details)){
                
                foreach ($get_child_details as $gk => $cv) {

                    $current_date = date("Y-m-d");
                    $data_date = date('Y-m-d',strtotime($cv->date_of_birth));  
                    $datetime1 = new DateTime($data_date);
                    $datetime2 = new DateTime($current_date);
                    $interval = $datetime1->diff($datetime2);
                    //days is start range
                    $age_days = $interval->format('%a');

                    $age = (int)$age_days;
                   
                    //check doctor id
                    $check_doctor = UserChildWiseDoctor::whereIn('user_id',$get_child_details)->where('child_id',$cv->id)->first();

                    //get all vaccination id
                    $child_vaccination = ChildVaccination::whereIn('user_id',$get_link_parent)->where('child_id',$cv->id)->pluck('vaccination_id');

                    //get all vaccination id
                    $taken_vaccination = ChildAlreadyTakenVaccination::whereIn('user_id',$get_link_parent)->where('child_id',$cv->id)->pluck('vaccination_id');


                    if(isset($check_doctor->doctor_id) && $check_doctor->doctor_id != ''){

                        $query = DoctorWiseVaccination::query();
                        $query->with(['vaccination']);
                        $query->where('doctor_id',$check_doctor->doctor_id);
                        $query->where('is_active','1');
                        $query->whereNotIn('vaccination_id',$child_vaccination);
                        $query->whereNotIn('vaccination_id',$taken_vaccination);

                    }else{

                        $query = Vaccination::query();
                        $query->where('is_active','1');
                        $query->where('is_delete','0');
                        $query->whereNotIn('id',$child_vaccination);
                        $query->whereNotIn('id',$taken_vaccination);
                    }

                    //days is start range
                    $vacctinations = $query->orderBy('taken_at','ASC')->get();

                    if(count($vacctinations) > 0){
                        foreach ($vacctinations as $vk => $vv) {
                            if(!in_array($vv->id, $vv_ids)){
                                $vv_ids[] = $vv->id;

                                $vaccinaton_array = array();
                                $vv_taken = $vv->taken_at+1;

                                if($vv_taken >= $age_days){
                                   
                                    //last date of vaccination
                                    $last_date =  date('Y-m-d', strtotime($cv->date_of_birth. ' + '.$vv_taken.' days'));
                                    $first_date = date('Y-m-d', strtotime('-2 days', strtotime($last_date)));
                                    

                                    $Date = $this->getDatesFromRange($first_date,$last_date);
                                     
                                    $now = date('Y-m-d');

                                    if($now >= $first_date && $now <= $last_date){
                                    
                                        if(in_array($now,$Date)){
                                            $dateDiff = $this->dateDiffInDays($now, $last_date);
                                               
                                            if(isset($check_doctor->doctor_id) && $check_doctor->doctor_id != ''){
                                                if($dateDiff == 0){
                                                    /*$message = $cv->child_name.' '.$vv['vaccination']['vaccination_name'].' is due today.';*/
                                                    
                                                    $message = $vv['vaccination']['vaccination_name'].' for your child '.$cv->child_name.' is due today. Please Contact your doctor.';
                                                }elseif($dateDiff == 1){

                                                    
                                                    /*$message = $cv->child_name.' '.$vv['vaccination']['vaccination_name'].' will be due after '.$dateDiff.' day.';*/

                                                    /*$message = $vv['vaccination']['vaccination_name'].' for your child '.$cv->child_name.' will be due on '.date('d-m-Y',strtotime($Date[1])).','.$dateDiff.' Please contact your doctor.';*/
                                                    $message = $vv['vaccination']['vaccination_name'].' for your child '.$cv->child_name.' will be due on '.date('d-m-Y',strtotime($Date[1])).', Please contact your doctor.';
                                                }else{

                                                    /*$message = $vv['vaccination']['vaccination_name'].' for your child '.$cv->child_name.' will be due on '.date('d-m-Y',strtotime($Date[2])).','.$dateDiff.' Please contact your doctor.';*/
                                                    $message = $vv['vaccination']['vaccination_name'].' for your child '.$cv->child_name.' will be due on '.date('d-m-Y',strtotime($Date[2])).', Please contact your doctor.';
                                                }
                                            }else{
                                                if($dateDiff == 0){
                                                    
                                                    /*$message = $cv->child_name.' '.$vv['vaccination_name'].' is due today'; */
                                                    $message = $vv['vaccination_name'].' for your child '.$cv->child_name.' is due today. Please Contact your doctor.';   

                                                }elseif($dateDiff == 1){
                                                    
                                                    /*$message = $vv['vaccination_name'].' for your child '.$cv->child_name.' will be due on '.date('d-m-Y',strtotime($Date[1])).','.$dateDiff.' Please contact your doctor.';*/
                                                    /*$message = $cv->child_name.' '.$vv['vaccination_name'].' will be due after '.$dateDiff.' day.'; */   

                                                    $message = $vv['vaccination_name'].' for your child '.$cv->child_name.' will be due on '.date('d-m-Y',strtotime($Date[1])).', Please contact your doctor.';

                                                }else{

                                                    /*$message = $vv['vaccination_name'].' for your child '.$cv->child_name.' will be due on '.date('d-m-Y',strtotime($Date[2])).','.$dateDiff.' Please contact your doctor.';*/

                                                    $message = $vv['vaccination_name'].' for your child '.$cv->child_name.' will be due on '.date('d-m-Y',strtotime($Date[2])).', Please contact your doctor.';
                                                }
                                                
                                            }

                                            $title = 'Vaccination Due';
                                            $user_id = $get_link_parent;
                                            $user_id_count[] = $get_link_parent;
                                            $unique_id = array_unique($user_id_count);
                                            
                                            $imageUrl = '';
                                            $user_count = UserToken::whereIn('user_id',$unique_id)->where('fcm_token','!=','')->count();
                                            if($user_count != 0){
                                                $result = $this->saveNotificationPush($user_count,'PUSH_NOTIFICATION',$title,$message,$imageUrl);
                                            }
                                          
                                            if(isset($request->image) && count($request->image) > 0){
                                                $fileName = $this->uploadImage($request->image,'notification');
                                                $imageUrl = '';
                                            }
                                            foreach($user_id as $pk => $pv){
                                                $getUser = UserToken::where('user_id',$pv)->where('fcm_token','!=','')->get();
                                                if(!is_null($getUser)){
                                                    foreach($getUser as $gk => $uv){
                                                        if(isset($uv->fcm_token) && $uv->fcm_token != ''){
                                                            $type = 0;
                                                            $this->sendpushNotification($uv->device_type,$message,$uv->fcm_token,$title,$imageUrl,$type);

                                                            $notification = new NotificationUser;
                                                            $notification->user_id = $uv->user_id;
                                                            $notification->notification_id = $result;
                                                            $notification->child_id = $cv->id;
                                                            $notification->vaccination_id = $vv->id;
                                                            $notification->save();
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $change_date_vacc = ChildVaccination::with(['vaccination'])->where('child_id',$cv->id)->whereIn('user_id',$get_link_parent)->where('good_to_have','0')->where('taken_vaccination','0')->get();
                                       
                    $date_change = array();
                    if(count($change_date_vacc) >= 0){
                        foreach ($change_date_vacc as $ck => $dv) {
                            
                            if(!in_array($dv->id, $clientId)){
                                $clientId[] = $dv->id;

                                    //last date of vaccination
                                    $last_date =  date('Y-m-d', strtotime($dv->vaccination_date));
                                    $first_date = date('Y-m-d', strtotime('-2 days', strtotime($last_date)));

                                $Date = $this->getDatesFromRange($first_date,$last_date);
                                
                                $now = date('Y-m-d');

                                if($now >= $first_date && $now <= $last_date){
                                    
                                    if(in_array($now,$Date)){

                                        $dateDiff = $this->dateDiffInDays($now, $last_date);
                                    
                                        if(isset($check_doctor->doctor_id) && $check_doctor->doctor_id != ''){
                                            if($dateDiff == 0){

                                                /*$message = $cv->child_name.' '.$vv['vaccination']['vaccination_name'].'is due today.';*/
                                               
                                                $message = $dv['vaccination']['vaccination']['vaccination_name'].' for your child '.$cv->child_name.' is due today. Please Contact your doctor.';

                                            }elseif($dateDiff == 1){
                                                
                                                /*$message = $cv->child_name.' '.$vv['vaccination']['vaccination_name'].' will be due after '.$dateDiff.' day.';*/
                                                /*$message = $dv['vaccination']['vaccination']['vaccination_name'].' for your child '.$cv->child_name.' will be due on '.date('d-m-Y',strtotime($Date[1])).','.$dateDiff.' Please contact your doctor.';*/
                                                if(isset($dv->vaccination_date) && ($dv->vaccination_date != '')){
                                                    $message = $dv['vaccination']['vaccination']['vaccination_name'].' for your child '.$cv->child_name.' will be due on '.date('d-m-Y',strtotime($dv->vaccination_date)).', Please contact your doctor.';
                                                }else{
                                                    $message = $dv['vaccination']['vaccination']['vaccination_name'].' for your child '.$cv->child_name.' will be due on '.date('d-m-Y',strtotime($Date[1])).', Please contact your doctor.';
                                                }
                                                
                                            }else{
                                                /*$message = $dv['vaccination']['vaccination']['vaccination_name'].' for your child '.$cv->child_name.' will be due on '.date('d-m-Y',strtotime($Date[2])).','.$dateDiff.' Please contact your doctor.';*/
                                                if(isset($dv->vaccination_date) && ($dv->vaccination_date != '')){

                                                    $message = $dv['vaccination']['vaccination']['vaccination_name'].' for your child '.$cv->child_name.' will be due on '.date('d-m-Y',strtotime($dv->vaccination_date)).', Please contact your doctor.';
                                                }else{

                                                    $message = $dv['vaccination']['vaccination']['vaccination_name'].' for your child '.$cv->child_name.' will be due on '.date('d-m-Y',strtotime($Date[2])).', Please contact your doctor.';
                                                }
                                                
                                            }
                                        }else{
                                            if($dateDiff == 0){
                                                
                                                /*$message = $cv->child_name.' '.$vv['vaccination_name'].'is due today.';*/  
                                                $message = $dv['vaccination']['vaccination_name'].' for your child '.$cv->child_name.' is due today. Please Contact your doctor.';
                                                
                                            }elseif($dateDiff == 1){

                                               
                                                /*$message = $cv->child_name.' '.$vv['vaccination_name'].' will be due after '.$dateDiff.' day.'; */
                                                /*$message = $dv['vaccination']['vaccination_name'].' for your child '.$cv->child_name.' will be due on '.date('d-m-Y',strtotime($Date[1])).','.$dateDiff.' Please contact your doctor.';   */
                                                if(isset($dv->vaccination_date) && ($dv->vaccination_date != '')){

                                                    $message = $dv['vaccination']['vaccination_name'].' for your child '.$cv->child_name.' will be due on '.date('d-m-Y',strtotime($dv->vaccination_date)).', Please contact your doctor.';   

                                                }else{
                                                    
                                                    $message = $dv['vaccination']['vaccination_name'].' for your child '.$cv->child_name.' will be due on '.date('d-m-Y',strtotime($Date[1])).', Please contact your doctor.';   
                                                }
                                                
                                            }else{

                                                /*$message = $dv['vaccination']['vaccination_name'].' for your child '.$cv->child_name.' will be due on '.date('d-m-Y',strtotime($Date[2])).','.$dateDiff.' Please contact your doctor.';   */
                                                if(isset($dv->vaccination_date) && ($dv->vaccination_date != '')){

                                                    $message = $dv['vaccination']['vaccination_name'].' for your child '.$cv->child_name.' will be due on '.date('d-m-Y',strtotime($dv->vaccination_date)).', Please contact your doctor.';   

                                                }else{
                                                    $message = $dv['vaccination']['vaccination_name'].' for your child '.$cv->child_name.' will be due on '.date('d-m-Y',strtotime($Date[2])).', Please contact your doctor.';   
                                                }                                           
                                                    
                                                

                                            }
                                            
                                        }

                                        /*if(isset($check_doctor->doctor_id) && $check_doctor->doctor_id != ''){
                                            $message = $vv['vaccination']['vaccination_name'];
                                        }else{
                                            $message = $vv['vaccination_name'];
                                        }*/
                                        $title = 'Vaccination Due';
                                        $user_id = $get_link_parent;
                                        $user_id_count[] = $get_link_parent;
                                        $unique_id = array_unique($user_id_count);
                                        
                                        $imageUrl = '';
                                        $user_count = UserToken::whereIn('user_id',$unique_id)->where('fcm_token','!=','')->count();
                                        if($user_count != 0){
                                            $result = $this->saveNotificationPush($user_count,'PUSH_NOTIFICATION',$title,$message,$imageUrl);
                                        }
                                       
                                        if(isset($request->image) && count($request->image) > 0){
                                            $fileName = $this->uploadImage($request->image,'notification');
                                            $imageUrl = \URL::to('/')."/uploads/notification/".$fileName;
                                        }
                                        foreach($user_id as $pk => $pv){
                                            $getUser = UserToken::where('user_id',$pv)->where('fcm_token','!=','')->get();
                                            if(!is_null($getUser)){
                                                foreach($getUser as $gk => $uv){
                                                    if(isset($uv->fcm_token) && $uv->fcm_token != ''){
                                                        $type = 0;
                                                        $this->sendpushNotification($uv->device_type,$message,$uv->fcm_token,$title,$imageUrl,$type);

                                                        $notification = new NotificationUser;
                                                        $notification->user_id = $uv->user_id;
                                                        $notification->notification_id = $result;
                                                        $notification->child_id = $cv->id;
                                                        /*$notification->vaccination_id = $vv->id;*/
                                                        $notification->save();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
            }
        }
        echo "send successfully";
        exit;

    }  

    //save notification push
    public function saveNotificationPush($user_count,$type,$title,$message = null,$imageUrl = null){
        
        $notification = new Notification;
        $notification->user_count = $user_count;
        $notification->type = $type;
        $notification->date = date('d-m-Y');
        if($type == 'EMAIL'){
            $notification->title = $title;
        } elseif($type == "SMS"){
            $notification->title = $title;
        } elseif($type == "PUSH_NOTIFICATION") {
            $notification->title = $title;
            $notification->url = $imageUrl;
        }
        $notification->message = $message;
        $notification->notification_type = 0;
        $notification->save();

        return $notification->id;
    }
}
