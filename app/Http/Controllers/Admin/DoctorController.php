<?php

namespace App\Http\Controllers\Admin;

use App\Model\Doctor;
use App\Model\City;
use App\Model\Vaccination;
use Illuminate\Http\Request;
use App\Model\DoctorWiseVaccination;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\Model\DoctorClinicContactNumber;
use App\Http\Controllers\GlobalController;

class DoctorController extends GlobalController
{
    public function __construct(){
      $this->middleware('admin');
    }

    //doctor list
    public function doctorList(Request $request){
        
        //dashboard
        if(!isset($request->id) && $request->id == ''){

            $approval_id = 4;
            $city = '';

            $city_list = City::get();

            $query = Doctor::query();

            if(isset($request->approval_status) && $request->approval_status != 4){
                if($request->approval_status == '3'){
                        
                    $approval_id = $request->approval_status;
                    $query->orWhere('is_delete',1);
                    $query->orWhere('is_active',0);
                }else{
                    $approval_id = $request->approval_status;
                    $query->where('is_approve',$approval_id);
                    $query->where('is_delete',0);
                }
            }else{
                
                
                $query->where('is_delete',0);
            }

            if(isset($request->city_id) && $request->city_id != ''){
                $city = $request->city_id;
                $query->where('city',$city);
            }
            

            $doctor_list = $query->with(['city_name'])->orderBy('id','desc')->get();
            
        }else{

            //dashboard logic
            $city_list = City::get();
            $approval_id = 4;
            $city = '';

            if($request->id == '1'){
                $current_date = date("Y-m-d");
                $doctor_list = Doctor::Where('created_at','like','%'.$current_date.'%')->with(['city_name'])->where('is_delete',0)->orderBy('id','desc')->get();

            }elseif ($request->id == '2') {

                //weekly date
                $prev_sunday = date('Y-m-d',strtotime('last sunday'));
                $next_sunday = date('Y-m-d',strtotime('next saturday'));
                
                $doctor_list = Doctor::whereBetween('created_at',[$prev_sunday,$next_sunday])->with(['city_name'])->where('is_delete',0)->orderBy('id','desc')->get();

            }elseif ($request->id == '3') {

                //monthly date
                $first_day_this_month = date('Y-m-01'); // hard-coded '01' for first day
                $last_day_this_month  = date('Y-m-t');

                $doctor_list = Doctor::whereBetween('created_at',[$first_day_this_month,$last_day_this_month])->with(['city_name'])->where('is_delete',0)->orderBy('id','desc')->get();
            }
        }

        $id_data = array();
        foreach ($doctor_list as $dk => $dv) {
            $data = array();
            $id_data[] = $dv->id;
            
        }

        $id_json = json_encode($id_data);

    	return view('admin.doctor.doctor_list',compact('doctor_list','city_list','approval_id','city','id_json'));
    }

    //add doctor
    public function addDoctor(){
    	return view('admin.doctor.add_doctor');	
    }

    //save doctor details
    public function saveDoctorDetails(Request $request){
        
        $get_doctor_id = Doctor::orderBy('id', 'DESC')->first();
        
    	$add_doctor = new Doctor;
        $add_doctor->doctor_name = $request->doctor_name;
        $add_doctor->mobile_number = $request->mobile_number;
        $add_doctor->email = $request->email;
        $add_doctor->practicing_since = $request->practicing_since;

        if(isset($request->city_name) && !is_null($request->city_name)){
            //check in database
            $check_city = City::where('city_name',$request->city_name)->first();
            if($check_city == '' && is_null($check_city) && count($check_city) <= 0){
                $add_doctor_city = new City;
                $add_doctor_city->city_name = $request->city_name;
                $add_doctor_city->save();

                $add_doctor->city = $add_doctor_city->id;
            } else {
                $add_doctor->city = $check_city->id;
            }
        }
        $add_doctor->ref_city_name = $request->city;        
        $add_doctor->pincode = $request->pincode;
        $add_doctor->address = $request->address;
        $add_doctor->specialization = $request->specialization;
        if($get_doctor_id->doctor_id == ''){
              
            $add_doctor->doctor_id = 'DOC2729';
        }else{
            
            $explode_id = explode("C",$get_doctor_id->doctor_id);
            $new_id = $explode_id[1] + 1;
            $make_new_id = $explode_id[0].'C'.+$new_id;
            $add_doctor->doctor_id = $make_new_id;

        }
        $add_doctor->save();

        foreach ($request->clinic_phone_number as $ck => $cv) {
            if($cv != ''){
                $add_clinic_con = new DoctorClinicContactNumber;
                $add_clinic_con->doctor_id = $add_doctor->id;
                $add_clinic_con->clinic_phone_number = $cv;
                $add_clinic_con->save();    
            }
        }
        
        //all admin vaccination
        $vaccination_list = Vaccination::where('is_active',1)->where('is_delete',0)->orderBy('taken_at','ASC')->get();

        //check vaccination exists. if not add it.
        foreach ($vaccination_list as $vk => $vv) {
            $add_doc_vac = new DoctorWiseVaccination;
            $add_doc_vac->doctor_id = $add_doctor->id;
            $add_doc_vac->vaccination_id = $vv->id;
            $add_doc_vac->taken_at = $vv->taken_at;
            $add_doc_vac->vaccination_type = $vv->vaccination_type;
            if($vv->vaccination_type == '0'){
                $add_doc_vac->is_active = '1';
            }else{
                $add_doc_vac->is_active = '0';
            }
            $add_doc_vac->save();
        }

        return redirect(route('admin.doctorList'))->with('messages', [
                  [
                      'type' => 'success',
                      'title' => 'Doctor Detail',
                      'message' => 'Doctor Detail Successfully Added!',
                  ],
        ]);
    }

    //view doctor details
    public function viewDoctorDetails(Request $request){

        $doctor_list = Doctor::where('id',$request->id)->with(['clinic_number'])->first();

        return view('admin.doctor.view_doctor_detail',compact('doctor_list')); 
    }

    //edit doctor details
    public function editDoctorDetails(Request $request){
        
        $doctor_list = Doctor::where('id',$request->id)->with(['clinic_number'])->first();
        
        return view('admin.doctor.edit_doctor_detail',compact('doctor_list')); 
    }

    //for approval and update details
    public function approvalDoctorStatus(Request $request){
       
        $update_doctor = Doctor::findOrFail($request->id);
        $update_doctor->doctor_name = $request->doctor_name;
        $update_doctor->mobile_number = $request->mobile_number;
        $update_doctor->email = $request->email;
        $update_doctor->practicing_since = $request->practicing_since;
        
        $city = explode(",",$request->city); $value = reset($city);
        if(isset($request->city) && !is_null($request->city)){
            //check in database
            $check_city = City::where('city_name',$value)->first();
            if($check_city == '' && is_null($check_city) && count($check_city) <= 0){
                $add_doctor_city = new City;
                $add_doctor_city->city_name = $value;
                $add_doctor_city->save();

                $update_doctor->city = $add_doctor_city->id;
            }else{
                $update_doctor->city = $check_city->id;
            }
        }
        $update_doctor->ref_city_name = $request->city;
        $update_doctor->pincode = $request->pincode;
        $update_doctor->address = $request->address;
        $update_doctor->specialization = $request->specialization;
        $update_doctor->is_approve = $request->approval;
        $update_doctor->save();

            if(isset($request->clinic_phone_number)){
                $deleteclinic_no = DoctorClinicContactNumber::where('doctor_id',$request->id)->delete();
                foreach ($request->clinic_phone_number as $ck => $cv) { 
                    if($cv != ''){
                        $add_clinic_con = new DoctorClinicContactNumber;
                        $add_clinic_con->doctor_id = $request->id;
                        $add_clinic_con->clinic_phone_number = $cv;
                        $add_clinic_con->save();
                    }
                }   
            }

        if($request->approval != ''){
            return redirect(route('admin.doctorList'))->with('messages', [
                  [
                      'type' => 'success',
                      'title' => 'Doctor Detail',
                      'message' => 'Doctor Approval Successfully Updated!',
                  ],
            ]);
        }else{

            return redirect(route('admin.doctorList'))->with('messages', [
                  [
                      'type' => 'success',
                      'title' => 'Doctor Detail',
                      'message' => 'Doctor Detail Successfully Updated!',
                  ],
            ]);
        }

    }

    //delete doctor details
    public function deleteDoctorDetails(Request $request){

        $delete_doctor = Doctor::where('id',$request->id)->update(['is_delete' => 1]);

        $delete_vacc = DoctorWiseVaccination::where('doctor_id',$request->id)->delete();

        if($delete_doctor){

            return redirect(route('admin.doctorList'))->with('messages', [
                  [
                      'type' => 'success',
                      'title' => 'Doctor Detail',
                      'message' => 'Doctor Detail Successfully Deleted!',
                  ],
            ]); 
        }

    }

    //doctor vaccination
    public function doctorVaccination(Request $request){

        $doctor_hist = DoctorWiseVaccination::with(['doctors'])->with(['vaccination'])->where('doctor_id',$request->id)->orderBy('taken_at','ASC')->get();

        $colors = array('1'=>'#bbebfb','2'=>'#ffcdd2','3'=>'#ffecb3','4'=>'#d1c4e9','5'=>'#d0edc8');

        $days = array();

        $final_array = array();

        $i = 1;

        foreach ($doctor_hist as $dk => $dv) {

            if(!in_array($dv->taken_at, $days)){

                $days[] = $dv->taken_at;

                $final_array[$dv->taken_at] = $colors[$i];

                $i++;

                if($i > 5){

                    $i = 1;
                }
            }
        }

        $doctor_name = Doctor::where('id',$request->id)->select('doctor_name')->first();

        return view('admin.doctor.doctor_hist',compact('doctor_hist','final_array','doctor_name')); 
    }    

    //save doctor taken at
    public function saveDoctorTakenAt(Request $request){

        $update_taken_at = DoctorWiseVaccination::findOrFail($request->id);

        //convert to days
        $convert_days = explode(".",$request->taken_at);
        $month_days = 30*$convert_days[0];
        if(isset($convert_days[1])){
            $days = 3*$convert_days[1];
            //total days
            $total_days = $month_days + $days;
        }else{
            $total_days = $month_days;
        }

        $update_taken_at->taken_at = $total_days;
        $update_taken_at->save();

        if($update_taken_at){
            return 'true';
        } else {
            return 'false';
        }

    }

    //change doctor vaccination status
    public function changeDocVaccinationStatus(Request $request){
        $vaccination_active = DoctorWiseVaccination::where('id',$request->id)->update(['is_active'=> $request->status]);

        if($vaccination_active){
            return 'true';
        } else {
            return 'false';
        }
    }

    //change doctor status
    public function changeDoctorStatus(Request $request){
        $doctor_active = Doctor::where('id',$request->id)->update(['is_active'=> $request->status]);

        if($doctor_active){
            return 'true';
        } else {
            return 'false';
        }
    }

    //check doctor mobile number
    public function checkDoctorMobileNumber(Request $request){

        $doctor_check_mobile = Doctor::where('mobile_number',$request->mobile_number)->first();

        if($doctor_check_mobile){
            return 'true';
        } else {
            return 'false';
        }
    }

    //admin doctor csv
    public function adminDoctorCsv(Request $request){
        if($request->doctor_id != ''){

        
            $doctor_id = json_decode($request->doctor_id);
            $doctor_info = array();
            $i = 1;

            foreach ($doctor_id as $dk => $dv) {
                $doctor_data = array();
                $doctor = Doctor::where('id',$dv)->with(['city_name'])->first();


                $doctor_data['sr_no'] = $i;
                $doctor_data['Doctor_Name'] = $doctor->doctor_name;
                $doctor_data['address'] = $doctor->address;
                if(!is_null($doctor->city_name)){
                    $doctor_data['city'] = $doctor->city_name->city_name;
                } else {
                    $doctor_data['city'] = '----------';
                }
                $doctor_data['email'] = $doctor->email;
                $doctor_data['mobile_number'] = $doctor->mobile_number;
                $doctor_data['practicing_since'] = $doctor->practicing_since;
                $doctor_data['specialization'] = $doctor->specialization;

                $clinic_number = DoctorClinicContactNumber::where('doctor_id',$dv)->get();
                $c_phone_number = array();
                foreach($clinic_number as $cv){
                    
                    $c_phone_number[] = $cv->clinic_phone_number;
                    
                }
                $doctor_data['clinic_number'] = implode(',', $c_phone_number);

                if($doctor->is_approve == 0){
                    $doctor_data['approved'] = 'Pending';    
                }else if($doctor->is_approve == 1){
                    $doctor_data['approved'] = 'Approved';
                }else if($doctor->is_approve == 2){
                    $doctor_data['approved'] = 'Disapproved';
                }

                if($doctor->is_active == 0){
                    $doctor_data['active'] = 'Inactive';    
                }else if($doctor->is_active == 1){
                    $doctor_data['active'] = 'Active';
                }

                $doctor_info[] = $doctor_data;

                $i++;
                 
            }
            
          
           $doctorCsvArray[] = ['Sr no','Doctor Name','Address','City','Email','Mobile Number','Practicing Since','Specialization','Clinic Phone Number','Approval Status','Status'];

            foreach ($doctor_info as $ddata) {

                $doctorCsvArray[] = $ddata;

            }

            $current_date_time = date("d_m_Y_h_i_s");
            Excel::create('doctor_list_'.$current_date_time.'', function($excel) use ($doctorCsvArray) {

                // Set the spreadsheet title, creator, and description
                $excel->setTitle('Doctor List');
                $excel->setCreator('Kidiatric')->setCompany('Kidiatric');
                $excel->setDescription('Doctor List');

                // Build the spreadsheet, passing in the payments array
                $excel->sheet('sheet1', function($sheet) use ($doctorCsvArray) {
                    $sheet->fromArray($doctorCsvArray, null, 'A1', false, false);
                });

            })->download('xlsx');

        }else{

            return redirect(route('admin.doctorList'))->with('messages', [
                  [
                      'type' => 'danger',
                      'title' => 'Doctor CSV',
                      'message' => 'Doctor Detail Not Found!',
                  ],
            ]);
        }

    }
}
