<?php

namespace App\Http\Controllers\Admin;

use DateTime;
use Carbon;
use App\Model\City;
use App\Model\User;
use App\Model\Doctor;
use App\Model\UserToken;
use App\Model\Notification;
use App\Model\Vaccination;
use Illuminate\Http\Request;
use App\Model\ChildrenDetail;
use App\Model\ChildVaccination;
use App\Model\NotificationUser;
use App\Model\DoctorNotification;
use App\Model\UserChildWiseDoctor;
use App\Model\DoctorWiseVaccination;
use App\Http\Controllers\Controller;
use App\Http\Controllers\GlobalController;
use Maatwebsite\Excel\Facades\Excel;
use App\Model\ChildAlreadyTakenVaccination;

class NotificationController extends GlobalController
{
    public function __construct(){
      	$this->middleware('admin');
    }

    //create notification
    public function createNotificationList(Request $request){

        $realtion = array('0' => 'Mother','1' => 'Father','2' => 'Guardian');
        $id = '';
        $role = '';
        $gender = '';
        $city_id = '';
        $min_range = '';
        $max_range = '';
        $doctor_name = '';
        $doctor_name_id = '';
        $filter = 0;

        $query = ChildrenDetail::query();
        if(isset($request->gender) && $request->gender != ''){
            $query->where('gender',$request->gender);
            $gender = $request->gender;
            $filter = 1;
        }

        if(isset($request->min_age) && $request->min_age != ''){
            $min_range = $request->min_age;
            $filter_min_age = number_format($min_range * 30,0);
            $query->where('age','>=',$filter_min_age);
            $filter = 1;
        }

        if(isset($request->max_age) && $request->max_age != ''){
            $max_range = $request->max_age;
            $filter_max_age = number_format($max_range * 30,0);
            $query->where('age','<=',$filter_max_age);
            $filter = 1;
        }

        $query->where('is_delete','0');
        $children_list = $query->pluck('user_id');
          
        $query = User::query();
        if(isset($request->role) && $request->role != ''){
            $query->where('relation',$request->role);
            $role = $request->role;
            $filter = 1;
        }

        if(isset($request->city_id) && $request->city_id != ''){
            $query->where('city_id',$request->city_id);
            $city_id = $request->city_id;
            $filter = 1;
        }
        
        $query->whereIn('id',$children_list);
        $query->where('is_otp_verified','1');
        $query->where('is_delete','0');
        $query->with(['city']);
        $parent_list  = $query->get();

        $id_json = array();
        if(!is_null($parent_list)){
            foreach($parent_list as $ck => $cv){
                $id_json[] = $cv->id;
            }
        }

        $cityList = City::all();

        return view('admin.notification.create_notification',compact('parent_list','realtion','id_json','role','filter','id','city_id','cityList','gender','min_range','max_range','doctor_name','doctor_name_id'));
    }

    //export notification data
    public function notificationExcelData(Request $request){

        if($request->notification_id != ''){

            $parent_id = json_decode($request->notification_id);
            $childInfo = array();
            $realtion = array('0' => 'Mother','1' => 'Father','2' => 'Guardian');

            $i = 1;

            $parentDetails = array();
            foreach ($parent_id as $dk => $dv) {
                $parent_list = User::where('id',$dv)->where('is_otp_verified','1')->where('is_delete','0')->with(['city'])->first();
                $parentData['sr_no'] = $i;
                
                $parentData['Name'] = $parent_list->name;

                if($parent_list->mobile_number != ''){
                    $parentData['Mobile'] = $parent_list->mobile_number;
                } else {
                    $parentData['Mobile'] = '--------';
                }

                if($parent_list->email != ''){
                    $parentData['Email'] = $parent_list->email;
                } else {
                    $parentData['Email'] = '--------';
                }

                if(array_key_exists($parent_list->relation,$realtion)){
                    $parentData['realtion'] = $realtion[$parent_list->relation];
                } else {
                    $parentData['realtion'] = '--------';
                }
                
                if(!is_null($parent_list->city)){
                    $parentData['city'] = $parent_list->city->city_name;
                } else {
                    $parentData['city'] = '--------';
                }

                if($parent_list->type == 0){
                    $child_count = ChildrenDetail::where('user_id',$parent_list->id)->where('is_delete','0')->count();
                } else {
                    $child_count = ChildrenDetail::where('user_id',$parent_list->parent_id)->where('is_delete','0')->count();
                }
                if($child_count != '' && $child_count != 0){
                    $parentData['child_count'] = $child_count;
                } else {
                    $parentData['child_count'] = '-------';
                }

                $parentDetails[] = $parentData;
                $i++;
            }
          
            $parentExcelData[] = ['Sr no','Name','Mobile','Email','Relation','City','No of children'];

            foreach ($parentDetails as $child) {
                $parentExcelData[] = $child;
            }

            $current_date_time = date("d_m_Y_h_i_s");
            Excel::create('notification_list_'.$current_date_time.'', function($excel) use ($parentExcelData) {

                // Set the spreadsheet title, creator, and description
                $excel->setTitle('Doctor List');
                $excel->setCreator('Kidiatric')->setCompany('Kidiatric');
                $excel->setDescription('Doctor List');

                // Build the spreadsheet, passing in the payments array
                $excel->sheet('sheet1', function($sheet) use ($parentExcelData) {
                    $sheet->fromArray($parentExcelData, null, 'A1', false, false);
                });

            })->download('xlsx');

        } else {

            return redirect(route('admin.createNotificationList'))->with('messages', [
                  [
                      'type' => 'danger',
                      'title' => 'Notification CSV',
                      'message' => 'Notification Detail Not Found!',
                  ],
            ]);
        }
    }

    //save notification list
    public function savedNotificationList(Request $request){

        $notification = '';
        $filter = 0;
        $range = '';

        $notification_id = NotificationUser::where('doctor_id','!=','')->pluck('notification_id');
        
        $query = Notification::query();

        if(isset($request->date_range) && $request->date_range != ''){
            $date = explode(" - ",$request->date_range);

            $date1 = explode('/', $date[0]);
            $date1 = implode('-', $date1);

            $start_date = date('Y-m-d',strtotime($date1)); 

            $date2 = explode('/', $date[1]);
            $date2 = implode('-', $date2);  
            $end_date = date('Y-m-d',strtotime($date2));

            $query->whereBetween('created_at',[$start_date,$end_date]);
            $range = $request->date_range;
            $filter = 1;
        }

        if(isset($request->notification) && $request->notification != ''){
            $query->where('type',$request->notification);
            $notification = $request->notification;
            $filter = 1;
        }

        $query->whereNotIn('id',$notification_id);
        $query->orderBy('id','DESC');
        $notification_list = $query->get();
        
        return view('admin.notification.notification_list',compact('notification_list','notification','filter'));
    }

    //doc saved notification list
    public function docSavedNotificationList(Request $request){
        
        $filter = 0;
        $range = '';
        $doctor_id = '';
        $doctor_name_id = '';
        $doctor_name = '';
        $notification = "";

        $notification_id = NotificationUser::where('doctor_id','!=','')->pluck('notification_id');

        if(isset($request->doctor_name) && $request->doctor_name != ''){
            $doctor_name = $request->doctor_name;
            $doctor_name_id = $request->doctor_name_id;
            $notification_id = NotificationUser::where('doctor_id',$request->doctor_name_id)->pluck('notification_id');
            $filter = 1;
        } 

        if(isset($request->doctor_id) && $request->doctor_id != ''){
            $doctor_id = $request->doctor_id;
            $notification_id = NotificationUser::where('doctor_id',$request->doctor_id)->pluck('notification_id');
            $filter = 1;
        } 

        $query = Notification::query();
        if(isset($request->date_range) && $request->date_range != ''){
            $date = explode(" - ",$request->date_range);

            $date1 = explode('/', $date[0]);
            $date1 = implode('-', $date1);

            $start_date = date('d-m-Y',strtotime($date1));  

            $date2 = explode('/', $date[1]);
            $date2 = implode('-', $date2);
             
            $end_date = date('d-m-Y',strtotime($date2));
            $query->where('date','>=',trim($date[0]));
            $query->where('date','<=',trim($date[1]));
            $range = $request->date_range;
            $filter = 1;
        }

        if(isset($request->notification) && $request->notification != ''){
            $query->where('type',$request->notification);
            $notification = $request->notification;
            $filter = 1;
        }

        $query->whereIn('id',$notification_id);
        $query->with(['notificationuser' => function($q){ $q->with(['doctor']); }]);
        $query->orderBy('id','DESC');
        $notification_list = $query->get();

        $id_json = array();

        if(!is_null($notification_list)){
            foreach($notification_list as $ck => $cv){
                $id_json[] = $cv->id;
            }
        }
        
        return view('admin.notification.doc_notification_list',compact('notification_list','doctor_name','range','filter','doctor_name_id','doctor_id','notification','id_json'));   
    }

    //send notification and msg 
    public function sendNotificationuser(Request $request){

        if($request->sms != ''){
            $user_id = json_decode($request->user_id);
            $title = "MESSAGE_NOTIFICATION";
            $message_notification_id = $this->saveNotification(count($user_id),'SMS',$title,$request->sms);
            foreach($user_id as $mk => $mv){
                $getUser = User::where('id',$mv)->first();
                if(!is_null($getUser) && $getUser->mobile_number != ''){

                    $this->sendOtherSms($request->sms,$getUser->mobile_number);
                    /*$this->saveNotification($request->post_id,$mv,count($user_id),'SMS',$title,$request->sms);*/
                    $notificationUser = new NotificationUser;
                    $notificationUser->notification_id = $message_notification_id;
                    $notificationUser->user_id = $getUser->id;
                    $notificationUser->save();
                }
            }
            
        }

        if($request->push_notification_title != '' && $request->push_notification_description != ''){
            $message = $request->push_notification_description;
            $title = $request->push_notification_title;
            $user_id = json_decode($request->user_id);
            $all_user_id[] = $request->user_id;
            
            $imageUrl = '';
            if(isset($request->image) && count($request->image) > 0){
                $path = "admin_notification/notification_image";
                $fileName = $this->uploadImage($request->image,$path);
                $imageUrl = $fileName;
            }
            $user_count = UserToken::whereIn('id',$user_id)->where('fcm_token','!=','')->count();
            
            $result = $this->saveNotificationPush(count($user_id),'PUSH_NOTIFICATION',$title,$message,$imageUrl);
           
            foreach($user_id as $pk => $pv){
                $getUser = UserToken::where('user_id',$pv)->where('fcm_token','!=','')->get();
                if(!is_null($getUser)){
                    foreach($getUser as $gk => $gv){
                        if($gv->fcm_token != ''){
                            $type = 1;
                            $this->sendpushNotification($gv->device_type,$message,$gv->fcm_token,$title,$imageUrl,$type);

                            $notificationUser = new NotificationUser;
                            $notificationUser->notification_id = $result;
                            $notificationUser->user_id = $gv->user_id;
                            $notificationUser->save();
                        }
                    }
                }
            }

        }

        if($request->email != '' && $request->subject != ''){
            
            $user_id = json_decode($request->user_id);
            $message = $request->email;
            $subject = $request->subject;
            $user_count = User::whereIn('id',$user_id)->where('email','!=','')->count();
            $email_notification_id = $this->saveNotification($user_count,'EMAIL',$subject,$message);
            foreach($user_id as $ek => $ev){
                $getUser = User::where('id',$ev)->first();
                if(!is_null($getUser) && $getUser->email != ''){
                    $templete = 'null_temp';
                    $email = $getUser->email;
                    $sub = $request->subject;
                    $getemail = str_replace('/templateEditor/', 'http://localhost:8000/templateEditor/', $request->email);
                    $this->sendMail($templete,$getemail,$sub,$email);

                        $notificationUser = new NotificationUser;
                        $notificationUser->notification_id = $email_notification_id;
                        $notificationUser->user_id = $getUser->id;
                        $notificationUser->save();
                }   
            }
        }
       
        return redirect(route('admin.ParentsList'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Notification',
                  'message' => 'Notification Successfully Sent!',
              ],
        ]); 
    }

    //save notification
    public function saveNotification($user_count,$type,$title,$message = null,$imageUrl = null){

        $notification = new Notification;
        $notification->user_count = $user_count;
        $notification->type = $type;
        $notification->date = date('d-m-Y');
        if($type == 'EMAIL'){
            $notification->title = $title;
        } elseif($type == "SMS"){
            $notification->title = $title;
        } elseif($type == "PUSH_NOTIFICATION") {
            $notification->title = $title;
            $notification->url = $imageUrl;
        }
        $notification->message = $message;
        $notification->notification_type = 1;
        $notification->save();

        return $notification->id;
    }

    //save push notification 
    public function saveNotificationPush($user_count,$type,$title,$message = null,$imageUrl = null){

        $notification = new Notification;
        $notification->user_count = $user_count;
        $notification->type = $type;
        $notification->date = date('d-m-Y');
        if($type == 'EMAIL'){
            $notification->subject = $title;
        } elseif($type == "PUSH_NOTIFICATION") {
            $notification->title = $title;
            $notification->url = $imageUrl;
        }
        $notification->message = $message;
        $notification->notification_type = 1;
        $notification->save();

        return $notification->id;
    }

    //export doctor notification data
    public function exportDoctorNotificationData(Request $request){

        if($request->notification_id != ''){

        
            $notification_id = json_decode($request->notification_id);
            $notificationInfo = array();

            $i = 1;

            $notification = array();
            foreach ($notification_id as $dk => $dv) {
                
                $notification = Notification::where('id',$dv)->with(['notificationuser' => function($q){ $q->with(['doctor']); }])->first();

                $excelData['sr_no'] = $i;
                if(!is_null($notification->notificationuser)){
                    $excelData['doctor_id'] = $notification->notificationuser->doctor_id;
                } else {
                    $excelData['doctor_id'] = '----------';
                }

                if(!is_null($notification->notificationuser) && !is_null($notification->notificationuser->doctor)){
                    $excelData['doctor_name'] = $notification->notificationuser->doctor->doctor_name;
                } else {
                    $excelData['doctor_name'] = '----------';
                }
                $excelData['notification_type']  = $notification->type;
                $excelData['targeted_user'] = $notification->user_count;
                $excelData['interation'] = '----------';
                $excelData['date'] = date('d-m-Y',strtotime($notification->date));
                $notificationInfo[] = $excelData;
                $i++;
            }
          
            $notificationData[] = ['Sr no','Doctor Id','Doctor Name','Type','Targeted User','Interation','Date'];

            foreach ($notificationInfo as $child) {
                $notificationData[] = $child;
            }

            $current_date_time = date("d_m_Y_h_i_s");
            Excel::create('doctor_notification_'.$current_date_time.'', function($excel) use ($notificationData) {

                // Set the spreadsheet title, creator, and description
                $excel->setTitle('Doctor List');
                $excel->setCreator('Kidiatric')->setCompany('Kidiatric');
                $excel->setDescription('Doctor List');

                // Build the spreadsheet, passing in the payments array
                $excel->sheet('sheet1', function($sheet) use ($notificationData) {
                    $sheet->fromArray($notificationData, null, 'A1', false, false);
                });

            })->download('xlsx');

        } else {

            return redirect(route('admin.ParentsList'))->with('messages', [
                  [
                      'type' => 'danger',
                      'title' => 'Parents CSV',
                      'message' => 'Parents Detail Not Found!',
                  ],
            ]);
        }
    }

    //view notification
    public function viewNotification($id){

        $findNotificationData = Notification::where('id',$id)->first();
       
        $getNotificationUser = NotificationUser::where('notification_id',$findNotificationData->id)->with(['user'])->get();
       
        return view('admin.notification.view_notification',compact('findNotificationData','getNotificationUser'));
    }

    //view doctor notification
    public function viewDoctorNotification($id){

        $findNotificationData = Notification::where('id',$id)->first();

        $getNotificationUser = NotificationUser::where('notification_id',$findNotificationData->id)->with(['user'])->get();
        
        return view('admin.notification.view_doctor_notification',compact('findNotificationData','getNotificationUser'));
    }

    //send linked parent notification
    public function sendLinkedParentNotification(Request $request){

        $child_details = ChildrenDetail::whereIn('id',$request->checkbox)->pluck('user_id');  
       
        $parent_details = User::whereIn('id',$child_details)->where('is_delete','0')->where('type','0')->pluck('id');
        $link_parent = User::whereIn('parent_id',$parent_details)->where('type','1')->pluck('id');


        $user_id = $parent_details->merge($link_parent);
        $get_user = User::whereIn('id',$user_id)->where('is_delete','0')->get();
        
        $userData = array();
        $user_id = array();
        if(isset($get_user) && count($get_user) > 0){
            foreach($get_user as $gv){

                $user['name'] = $gv->name;                 
                $user['email'] = $gv->email;
                $user['mobile_number'] = $gv->mobile_number;
                $user_id[] = $gv->id;
                $userData[] = $user;
            }
        }

        return view('admin.user_details.send_notification_linked_parent',compact('userData','user_id'));
    }

    //send child notification user
    public function sendChildNotificationuser(Request $request){

        if($request->sms != ''){
            $user_id = json_decode($request->user_id);
            $title = "MESSAGE NOTIFICATION";
            $message_notification_id = $this->saveNotification(count($user_id),'SMS',$title,$request->sms);
            foreach($user_id as $mk => $mv){
                $getUser = User::where('id',$mv)->first();
                if(!is_null($getUser) && $getUser->mobile_number != ''){
                    $this->sendOtherSms($request->sms,$getUser->mobile_number);
                }
            }
        }

        if($request->push_notification_title != '' && $request->push_notification_description != ''){
            $message = $request->push_notification_description;
            $title = $request->push_notification_title;
            $user_id = json_decode($request->user_id);
            $imageUrl = '';
            if(isset($request->image) && count($request->image) > 0){                
                $path = "admin_notification/notification_image";
                $fileName = $this->uploadImage($request->image,$path);
                $imageUrl = $fileName;

            }
            $user_count = UserToken::whereIn('id',$user_id)->where('fcm_token','!=','')->count();
            $result = $this->saveNotificationPush($user_count,'PUSH_NOTIFICATION',$title,$message,$imageUrl);
            foreach($user_id as $pk => $pv){
                $getUser = UserToken::where('user_id',$pv)->where('fcm_token','!=','')->get();
                if(!is_null($getUser)){
                    foreach($getUser as $gk => $gv){
                        if($gv->fcm_token != ''){
                            $type = 1;
                            $this->sendpushNotification($gv->device_type,$message,$gv->fcm_token,$title,$imageUrl,$type);

                            $notificationUser = new NotificationUser;
                            $notificationUser->notification_id = $result;
                            $notificationUser->user_id = $gv->user_id;
                            $notificationUser->save();
                        }
                    }
                }
            }
        }

        if($request->email != '' && $request->subject != ''){
            $user_id = json_decode($request->user_id);
            $message = $request->email;
            $subject = $request->subject;
            $user_count = User::whereIn('id',$user_id)->where('email','!=','')->count();
            $email_notification_id = $this->saveNotification($user_count,'EMAIL',$subject,$message);
            foreach($user_id as $ek => $ev){
                $getUser = User::where('id',$ev)->first();
                if(!is_null($getUser) && $getUser->email != ''){
                    $templete = 'null_temp';
                    $email = $getUser->email;
                    $sub = $request->subject;
                    $getemail = str_replace('/templateEditor/', 'http://localhost:8000/templateEditor/', $request->email);
                    $this->sendMail($templete,$getemail,$sub,$email);
                }   
            }
        }
       
        return redirect(route('admin.ChildrenList'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Notification',
                  'message' => 'Notification Successfully Sent!',
              ],
        ]); 
    }

    //send user notification
    public function sendUserNotification(Request $request){

        $userData = array();
        $user_id = array();
        if(isset($request->checkbox) && count($request->checkbox) > 0){
            foreach($request->checkbox as $ck){
                $u = User::where('id',$ck)->first();
                $user['name'] = $u->name;                 
                $user['email'] = $u->email;
                $user['mobile_number'] = $u->mobile_number;
                $user_id[] = $ck;
                $userData[] = $user;
            }
        }

        return view('admin.user.user_data',compact('userData','user_id'));
    }

    //admin notification image
    public function adminNotificationImage(Request $request){
        
        $message = Notification::where('id',$request->id)->first();

        return view('admin.notification.notification_message',compact('message'));
    }

    //admin notification user
    public function adminNotificationUser(Request $request){

        $target_user = NotificationUser::where('notification_id',$request->id)->whereNull('doctor_id')->where('interaction','1')->get();
        
        return view('admin.notification.target_user',compact('target_user'));   
    }

    public function adminDoctorNotificationUser(Request $request){

        $target_user = NotificationUser::where('notification_id',$request->id)->where('doctor_id',$request->doctor_id)->where('interaction','1')->get();

        return view('admin.notification.target_user',compact('target_user'));      
    }

    //save child notification 
    public function saveChildNotification($user_count,$type,$title,$message = null,$imageUrl = null){
       
        $notification = new Notification;
        $notification->user_count = $user_count;
        $notification->type = $type;
        $notification->date = date('d-m-Y');
        if($type == "PUSH_NOTIFICATION") {
            $notification->title = $title;
            $notification->url = $imageUrl;
        }
        $notification->message = $message;
        $notification->notification_type = 1;
        $notification->save();

        return $notification->id;
    }
}	

