<?php

namespace App\Http\Controllers\Admin;

use Hash;
use Auth;
use Redirect;
use Validator;
use App\Model\User;
use App\Model\Admin;
use App\Model\Doctor;
use App\Model\HelpSupport;
use Illuminate\Http\Request;
use App\Model\ChildrenDetail;
use App\Model\SocialMediaLink;
use App\Http\Controllers\GlobalController;
use Illuminate\Support\Facades\Input;

class AdminController extends GlobalController
{
    public function __construct(){

      $this->middleware('admin');
      
    }

    //Dashboard 
    public function index(){

        $current_date = date("Y-m-d");
        
        $children_list = ChildrenDetail::pluck('user_id');

        $today_users = User::Where('created_at','like','%'.$current_date.'%')->where('is_otp_verified','1')->where('is_delete','0')->whereIn('id',$children_list)->count();
        $today_doctor = Doctor::Where('created_at','like','%'.$current_date.'%')->where('is_delete','0')->count();
        $today_children = ChildrenDetail::Where('created_at','like','%'.$current_date.'%')->where('is_delete','0')->count();
      
        //weekly date
        $prev_sunday = date('Y-m-d',strtotime('last sunday'));
        $next_sunday = date('Y-m-d',strtotime('next saturday'));

        //weekly data
        $weekly_user = User::whereBetween('created_at',[$prev_sunday,$next_sunday])->where('is_otp_verified','1')->where('is_delete','0')->whereIn('id',$children_list)->count();
        $weekly_doctor = Doctor::whereBetween('created_at',[$prev_sunday,$next_sunday])->where('is_delete','0')->count();
        $weekly_children = ChildrenDetail::whereBetween('created_at',[$prev_sunday,$next_sunday])->where('is_delete','0')->count();

        //monthly date
        $first_day_this_month = date('Y-m-01'); // hard-coded '01' for first day
        $last_day_this_month  = date('Y-m-t'); 
        
        //monthly data
        $monthly_user = User::whereBetween('created_at',[$first_day_this_month,$last_day_this_month])->where('is_otp_verified','1')->where('is_delete','0')->whereIn('id',$children_list)->count();
        $monthly_doctor = Doctor::whereBetween('created_at',[$first_day_this_month,$last_day_this_month])->where('is_delete','0')->count();
        $monthly_children = ChildrenDetail::whereBetween('created_at',[$first_day_this_month,$last_day_this_month])->where('is_delete','0')->count();      

        //All user data
        $all_user = User::where('is_otp_verified','1')->where('is_delete','0')->whereIn('id',$children_list)->count();
        $all_doctor = Doctor::where('is_delete','0')->count();
        $all_children = ChildrenDetail::where('is_delete','0')->count();  

        //dashboard data
        $doc_months = array();
        $doc_month = array();
        $monthly_doctor_data = array();

        for($m=1; $m<=12; ++$m){
            $doc_months[$m]['month'] = date('M', mktime(0, 0, 0, $m, 1));
            $doc_months[$m]['month_key'] = $m;
        }

        foreach($doc_months as $m){
            $doc_month[] = $m['month'];
            $currentMonth = date('m');
            $count = $data = Doctor::whereRaw('MONTH(created_at) = ?',[$m['month_key']])->where('is_delete','0')->count();
            $monthly_doctor_data[] = $count;
        }
      
        $user_months = array();
        $user_month = array();
        $monthly_user_data = array();  

        for($m=1; $m<=12; ++$m){
            $user_months[$m]['month'] = date('M', mktime(0, 0, 0, $m, 1));
            $user_months[$m]['month_key'] = $m;
        }

        foreach($user_months as $m){
            $user_month[] = $m['month'];
            $currentMonth = date('m');
            $count = $data = User::whereRaw('MONTH(created_at) = ?',[$m['month_key']])->where('is_otp_verified','1')->whereIn('id',$children_list)->where('is_delete','0')->count();
            $monthly_user_data[] = $count;
        }
        
        $child_months = array();
        $child_month = array();
        $monthly_child_data = array();  

        for($m=1; $m<=12; ++$m){
            $child_months[$m]['month'] = date('M', mktime(0, 0, 0, $m, 1));
            $child_months[$m]['month_key'] = $m;
        }

        foreach($child_months as $m){
            $child_month[] = $m['month'];
            $currentMonth = date('m');
            $count = $data = ChildrenDetail::whereRaw('MONTH(created_at) = ?',[$m['month_key']])->where('is_delete','0')->count();
            $monthly_child_data[] = $count;
        }

      return view('admin.dashboard.dashboard',compact('today_users','today_doctor','today_children','weekly_user','weekly_doctor','weekly_children','monthly_user','monthly_doctor','monthly_children','monthly_doctor_data','monthly_user_data','doc_month','month_user','user_month','child_month','monthly_child_data','all_user','all_children','all_doctor'));
    }
    
    //admin details
    public function adminProfile(){
        
        $admin_profile = Admin::where('id',Auth::guard('admin')->user()->id)->first();
        
        return view('admin.dashboard.admin_update_profile',compact('admin_profile'));
    }

    //update admin details
    public function adminProfileUpdate(Request $request){
        
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
        ]);

        $update_admin = Admin::findOrFail($request->id);
        $update_admin->name = $request->name;
        $update_admin->email = $request->email;
        $update_admin->save(); 

        return redirect()->route('admin.dashboard')->with(['message'=>'Profile successfully updated!','alert-type' => 'success']);
    }

    //change password
    public function changeAdminPassword(){

        return view('admin.dashboard.change_password');
    }

    //update password
    public function updateAdminPassword(Request $request){

        $this->validate($request, [
            'old_pass' => 'required',
            'new_pass' => 'required'
        ]);

        $user = Admin::where('id', '=', $request->id)->first();

        if(Hash::check($request->old_pass,$user->password)){

            $users = Admin::findOrFail($request->id);
            $users->password = Hash::make($request->new_pass);
            $users->save();

            return redirect(route('admin.dashboard'))->with('messages', [
                  [
                      'type' => 'success',
                      'title' => 'Password',
                      'message' => 'Password Successfully changed',
                  ],
            ]); 

        } else {
          
            return redirect()->back()->with('messages', [
                  [
                      'type' => 'danger',
                      'title' => 'Password',
                      'message' => 'Plese check your current password',
                  ],
            ]); 
        }
    }

    //Help Support Doctor List
    public function helpSupport(){

        $getRequest = HelpSupport::with(['doctor'])->orderBy('status','asc')->get();

        return view('admin.dashboard.help_support_request',compact('getRequest'));
    }

    //attend support request
    public function attendSupportRequest($id){

        $attendSuportRequest = HelpSupport::where('id',$id)->update(['status' => 2]);

        return redirect()->back()->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Password',
                  'message' => 'Support request successfully attened!',
              ],
        ]); 

    }

    //get notifation count
    public function getNotificationCount(){

        $getRequest = HelpSupport::where('status',1)->count();

        return $getRequest;
    }

    public function getMessageDetails(Request $request){

        $getMessage = HelpSupport::where('id',$request->id)->first();

        return $getMessage->message;
    }

    public function changeSocialMediaLink(){
        $updated_link = SocialMediaLink::first();

        return view('admin.dashboard.change_url',compact('updated_link'));   
    }

    public function updateSocialMediaLink(Request $request){
        $link = SocialMediaLink::where('user_id',$request->id)->delete();
            $users = new SocialMediaLink;
            $users->user_id = $request->id;
            $users->link = $request->link;
            $users->save();
        
        if($users){

            return redirect(route('admin.dashboard'))->with('messages', [
                [
                  'type' => 'success',
                  'title' => 'Link',
                  'message' => 'Link Successfully changed',
                ],
            ]);    
        }
    }
}