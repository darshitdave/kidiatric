<?php

namespace App\Http\Controllers\Admin;

use DateTime;
use Carbon;
use App\Model\City;
use App\Model\User;
use App\Model\Doctor;
use App\Model\UserToken;
use App\Model\Notification;
use App\Model\Vaccination;
use Illuminate\Http\Request;
use App\Model\ChildrenDetail;
use App\Model\UserLinkParent;
use App\Model\ChildVaccination;
use App\Model\NotificationUser;
use App\Model\DoctorNotification;
use App\Model\UserChildWiseDoctor;
use App\Model\DoctorWiseVaccination;
use App\Http\Controllers\Controller;
use App\Http\Controllers\GlobalController;
use Maatwebsite\Excel\Facades\Excel;
use App\Model\ChildAlreadyTakenVaccination;

class UserController extends GlobalController
{
    public function __construct(){
      $this->middleware('admin');
    }

    //Parents List
    public function ParentsList(Request $request){

        $realtion = array('0' => 'Mother','1' => 'Father','2' => 'Guardian');
        $gender = '';
        $min_range = '';
        $max_range = '';
        $role = '';
        $city_id = '';
        $filter = 0;
        $id = '';

        $query = ChildrenDetail::query();

        if(isset($request->gender) && $request->gender != ''){
            $query->where('gender',$request->gender);
            $gender = $request->gender;
            $filter = 1;
        }

        if(isset($request->min_age) && $request->min_age != ''){
            $min_range = $request->min_age;
            $filter_min_age = number_format($min_range * 30,0);
            $query->where('age','>=',$filter_min_age);
            $filter = 1;
        }

        if(isset($request->max_age) && $request->max_age != ''){
            $max_range = $request->max_age;
            $filter_max_age = number_format($max_range * 30,0);
            $total_days = $filter_max_age + 29;
            $query->where('age','<=',$total_days);
            $filter = 1;
        }
        $children_list = $query->pluck('user_id');
        
        $query = User::query();

        //dashboard
        if(isset($request->id) && $request->id != ''){
            
            $id = $request->id;
            if($request->id == '1'){
                $current_date = date("Y-m-d");
                $query->Where('created_at','like','%'.$current_date.'%');
                $query->orderBy('id','desc');
            } elseif ($request->id == '2') {
                //weekly date
                $prev_sunday = date('Y-m-d',strtotime('last sunday'));
                $next_sunday = date('Y-m-d',strtotime('next saturday'));

                $query->whereBetween('created_at',[$prev_sunday,$next_sunday]);
                $query->orderBy('id','desc');
              
            } elseif ($request->id == '3') {
                //monthly date
                $first_day_this_month = date('Y-m-01'); // hard-coded '01' for first day
                $last_day_this_month  = date('Y-m-t');
                $query->whereBetween('created_at',[$first_day_this_month,$last_day_this_month]);
                $query->orderBy('id','desc');
            }
        }

        if(isset($request->role) && $request->role != ''){
            $query->where('relation',$request->role);
            $role = $request->role;
            $filter = 1;
        }

        if(isset($request->city_id) && $request->city_id != ''){
            $query->where('city_id',$request->city_id);
            $city_id = $request->city_id;
            $filter = 1;
        }
        
        $query->where('is_otp_verified','1');
        $query->where('is_delete','0');
        $query->with(['city']);
            if(isset($children_list)){
                $query->whereIn('id',$children_list);
            }
        $parent_list  = $query->get();

        $id_json = array();
        if(!is_null($parent_list)){
            foreach($parent_list as $ck => $cv){
                $id_json[] = $cv->id;
            }
        }

        $cityList = City::all();

        return view('admin.user_details.parent_list',compact('parent_list','realtion','id_json','role','filter','id','city_id','cityList','gender','max_range','min_range'));
    }

    //Parents Child Details
    public function parentsChildDetails(Request $request){
        
        $child_Details = ChildrenDetail::where('user_id',$request->id)->where('is_delete','0')->with(['doctor' => function($q) { $q->with(['doctordetail']); }])->get();
        
        return view('admin.user_details.children_details',compact('child_Details'));
    }
    
    //Children List
    public function ChildrenList(Request $request){

        $gender = '';
        $min_range = '';
        $max_range = '';
        $city_id = '';
        $city_name = '';
        $filter = 0;

        $query = ChildrenDetail::query();

        //dashboard
        if(isset($request->id) && $request->id != ''){
           
            $id = $request->id;
            if($request->id == '1'){
                $current_date = date("Y-m-d");
                $query->Where('created_at','like','%'.$current_date.'%');
                $query->orderBy('id','desc');
            } elseif ($request->id == '2') {
                //weekly date
                $prev_sunday = date('Y-m-d',strtotime('last sunday'));
                $next_sunday = date('Y-m-d',strtotime('next saturday'));

                $query->whereBetween('created_at',[$prev_sunday,$next_sunday]);
                $query->orderBy('id','desc');
              
            } elseif ($request->id == '3') {
                //monthly date
                $first_day_this_month = date('Y-m-01'); // hard-coded '01' for first day
                $last_day_this_month  = date('Y-m-t');
                $query->whereBetween('created_at',[$first_day_this_month,$last_day_this_month]);
                $query->orderBy('id','desc');
            }

        }

        if(isset($request->gender) && $request->gender != ''){
            $query->where('gender',$request->gender);
            $gender = $request->gender;
            $filter = 1;
        }

        if(isset($request->min_age) && $request->min_age != ''){
            $min_range = $request->min_age;
            $filter_min_age = number_format($min_range * 30,0);
            $query->where('age','>=',$filter_min_age);
            $filter = 1;
        }

        if(isset($request->max_age) && $request->max_age != ''){
            $max_range = $request->max_age;
            $filter_max_age = number_format($max_range * 30,0);
            $total_days = $filter_max_age + 29;
            $query->where('age','<=',$total_days);
            $filter = 1;
        }

        if(isset($request->city_id) && $request->city_id != ''){
            $findCityChild = User::where('city_id',$request->city_id)->with(['children'])->where('type',0)->get();
            $getChildId = array();
            if(!is_null($findCityChild)){
                foreach($findCityChild as $fk => $fv){
                    if(!is_null($fv->children)){
                        foreach($fv->children as $ck => $cv){
                            $getChildId[] = $cv->child_id;
                        }
                    }
                }
            }
            $query->whereIn('id',$getChildId);
            $city_name = $request->city_name;
            $city_id = $request->city_id;
            $filter = 1;
        }

        $query->where('is_delete','0');
        $query->with(['location' => function($q) { $q->with(['city']);},'doctor' => function($q) { $q->with(['doctordetail']); }]);
        $children_list = $query->get();
        
        $id_json = array();
        if(!is_null($children_list)){
            foreach($children_list as $ck => $cv){
                $id_json[] = $cv->id;
            }
        }

        $cityList = City::all();

        return view('admin.user_details.children_list',compact('children_list','gender','min_range','max_range','filter','id_json','city_id','city_name','cityList'));
    }

    //children vaccination
    public function ChildrenVaccination(Request $request){

        $child_details = ChildrenDetail::with(['location' => function($q) { $q->with(['city']);},'doctor' => function($q) { $q->with(['doctordetail']); }])->where('id',$request->id)->first();
       
        //form birth date to current date difference
        $data_date = date('Y-m-d',strtotime($child_details->date_of_birth));  
        $currentDateTime = date('Y-m-d');
        $datetime1 = new DateTime($data_date);
        $datetime2 = new DateTime($currentDateTime);
        $interval = $datetime1->diff($datetime2);
        $days = $interval->format('%a');

        $check_doctor = UserChildWiseDoctor::with(['doctordetail'])->where('child_id',$child_details->id)->first();
        
        if(isset($check_doctor->doctor_id) && $check_doctor->doctor_id != ''){

            $children_vaccinations = ChildVaccination::with(['vaccination'])->where('child_id',$child_details->id)->pluck('vaccination_id');
            $taken_vaccination = ChildAlreadyTakenVaccination::where('child_id',$child_details->id)->pluck('vaccination_id');

            $query = DoctorWiseVaccination::query();
            $query->with(['vaccination']);
            $query->where('doctor_id',$check_doctor->doctor_id);
            $query->whereNotIn('vaccination_id',$children_vaccinations);
            $query->whereNotIn('vaccination_id',$taken_vaccination);

                //change date of vaccination and not takened vaccination
                $change_date = ChildVaccination::with(['vaccination'])->where('child_id',$child_details->id)->where('good_to_have','0')->where('taken_vaccination','0')->get();

                //good to have data
                $get_good_to_have = ChildVaccination::with(['vaccination'])->where('child_id',$child_details->id)->where('good_to_have','1')->where('taken_vaccination','0')->get();

                $child_taken = ChildAlreadyTakenVaccination::where('child_id',$child_details->id)->orderBy('taken_at','ASC')->get();

                //taken vaccination
                $takened_vaccination = ChildVaccination::with(['vaccination'])->where('child_id',$child_details->id)->where('good_to_have','0')->where('taken_vaccination','1')->get();

        }else{

            $children_vaccinations = ChildVaccination::with(['vaccination'])->where('child_id',$child_details->id)->pluck('vaccination_id');
            $taken_vaccination = ChildAlreadyTakenVaccination::where('child_id',$child_details->id)->pluck('vaccination_id');

            $query = Vaccination::query();
            $query->whereNotIn('id',$children_vaccinations);
            $query->whereNotIn('id',$taken_vaccination);
            $query->where('is_delete','0');

                //change date of vaccination and not takened vaccination
                $change_date = ChildVaccination::with(['vaccination'])->where('child_id',$child_details->id)->where('good_to_have','0')->where('taken_vaccination','0')->get();

                //good to have data
                $get_good_to_have = ChildVaccination::with(['vaccination'])->where('child_id',$child_details->id)->where('good_to_have','1')->where('taken_vaccination','0')->get();
                
                $child_taken = ChildAlreadyTakenVaccination::where('child_id',$child_details->id)->orderBy('taken_at','ASC')->get();

                //taken vaccination
                $takened_vaccination = ChildVaccination::with(['vaccination'])->where('child_id',$child_details->id)->where('good_to_have','0')->where('taken_vaccination','1')->get();
              
        } 

        $query->where('is_active','1');
        $query->where('vaccination_type','0');
        $vaccinations = $query->orderBy('taken_at','ASC')->get();

                $already_taken = array();
                if(!is_null($child_taken)){
                    foreach ($child_taken as $ck => $cv) {
                        $taken_vaccination = array();
                        if(isset($check_doctor->doctor_id) && $check_doctor->doctor_id != ''){
                            $taken_vaccination['id'] = $cv->vaccination_id;
                            $taken_vaccination['name'] = $cv->vaccination->vaccination_name;
                        }else{
                            $taken_vaccination['id'] = $cv->id;
                            $taken_vaccination['name'] = $cv->vaccination_name;
                        }
                        if($taken_vaccination['name'] == ''){
                            $taken_vaccination['name'] = $cv['vaccination']['vaccination_name'];   
                        } 
                        $taken_vaccination['taken_at'] = $cv->taken_at;
                        $taken_vaccination['status'] = 'Taken';
                        $taken_vaccination['due_date'] = $cv->vaccination_date;
                        $taken_vaccination['taken_date'] = $cv->vaccination_date;
                        $already_taken[] = $taken_vaccination;
                    }
                }    

                $not_taken = array();
                if(!is_null($change_date)){
                    foreach ($change_date as $dk => $dv) {
                        $remaining_vaccination = array();
                        if(isset($check_doctor->doctor_id) && $check_doctor->doctor_id != ''){
                            $remaining_vaccination['id'] = $dv->vaccination_id;
                            $remaining_vaccination['name'] = $dv->vaccination->vaccination_name;
                        }else{
                            $remaining_vaccination['id'] = $dv->id;
                            $remaining_vaccination['name'] = $dv->vaccination_name;
                        }
                        if($remaining_vaccination['name'] == ''){
                            $remaining_vaccination['name'] = $dv['vaccination']['vaccination_name'];   
                        } 
                        $remaining_vaccination['taken_at'] = $dv->taken_at;
                        $remaining_vaccination['status'] = 'Due';
                        $new_date =  date('d-m-Y', strtotime($dv->vaccination_date));
                        $remaining_vaccination['due_date'] = $new_date;
                        $remaining_vaccination['taken_date'] = '---------';
                        $not_taken[] = $remaining_vaccination;
                    }
                }

                $good_to_have = array();
                if(!is_null($get_good_to_have)){
                    foreach ($get_good_to_have as $gk => $gv) {
                        $good_to_have_vaccination = array();
                        if(isset($check_doctor->doctor_id) && $check_doctor->doctor_id != ''){
                            $good_to_have_vaccination['id'] = $gv->vaccination_id;
                            $good_to_have_vaccination['name'] = $gv->vaccination->vaccination_name;
                        }else{
                            $good_to_have_vaccination['id'] = $gv->id;
                            $good_to_have_vaccination['name'] = $gv->vaccination_name;
                        }
                        if($good_to_have_vaccination['name'] == ''){
                            $good_to_have_vaccination['name'] = $gv['vaccination']['vaccination_name'];   
                        }
                        $good_to_have_vaccination['taken_at'] = $gv->taken_at;
                        $good_to_have_vaccination['status'] = 'Due';
                        $new_date =  date('d-m-Y', strtotime($gv->vaccination_date));
                        $good_to_have_vaccination['due_date'] = $new_date;
                        $good_to_have_vaccination['taken_date'] = '---------';
                        $good_to_have[] = $good_to_have_vaccination;
                    }
                }

                $takened = array();
                if(!is_null($takened_vaccination)){
                    foreach ($takened_vaccination as $tk => $tv) {
                        $taken_detail = array();
                        if(isset($check_doctor->doctor_id) && $check_doctor->doctor_id != ''){
                            $taken_detail['id'] = $tv->vaccination_id;
                            $taken_detail['name'] = $tv->vaccination->vaccination_name;
                        }else{
                            $taken_detail['id'] = $tv->id;
                            $taken_detail['name'] = $tv->vaccination_name;
                        }
                        if($taken_detail['name'] == ''){
                            $taken_detail['name'] = $tv['vaccination']['vaccination_name'];   
                        }   
                        $taken_detail['taken_at'] = $tv->taken_at;
                        $taken_detail['status'] = 'Taken';
                          $new_date =  date('d-m-Y', strtotime($tv->vaccination_date));
                          $tv = $tv->taken_at+1;
                          $due_date =  date('d-m-Y', strtotime($child_details->date_of_birth. ' + '.$tv.' days'));
                        $taken_detail['due_date'] = $due_date;
                        $taken_detail['taken_date'] = $new_date;
                        $takened[] = $taken_detail;

                    }
                }

                $all_vaccinations = array();
                if(!is_null($vaccinations)){
                    foreach ($vaccinations as $vk => $vv) {
                        $vaccinations = array();
                        if(isset($check_doctor->doctor_id) && $check_doctor->doctor_id != ''){
                            $vaccinations['id'] = $vv->vaccination_id;
                            $vaccinations['name'] = $vv['vaccination']['vaccination_name'];
                        }else{
                            $vaccinations['id'] = $vv->id;
                            $vaccinations['name'] = $vv->vaccination_name;
                        }

                        $vaccinations['taken_at'] = $vv->taken_at;
                        $vv = $vv->taken_at+1;
                        if($vv <= $days){
                            $vaccinations['status'] = 'Taken';
                            $new_date =  date('d-m-Y', strtotime($child_details->date_of_birth. ' + '.$vv.' days'));
                            $vaccinations['due_date'] = $new_date;
                            $vaccinations['taken_date'] = $new_date;

                        }else{

                            $vaccinations['status'] = 'Due';
                            $new_date =  date('d-m-Y', strtotime($child_details->date_of_birth. ' + '.$vv.' days'));
                            $vaccinations['due_date'] = $new_date;
                            $vaccinations['taken_date'] = '---------';
                        }

                        
                        $all_vaccinations[] = $vaccinations;
                    }
                }
                

                $vaccination_data = array_merge($all_vaccinations,$already_taken,$not_taken,$good_to_have,$takened);
                
                $price = array_column($vaccination_data, 'taken_at');

                array_multisort($price, SORT_ASC, $vaccination_data);
                
            //notification 
            $notifications = NotificationUser::where('child_id',$child_details->id)->pluck('vaccination_id')->toArray();

        return view('admin.user_details.children_vaccinations',compact('child_details','days','vaccination_data','notifications'));
    }

    public function adminVaccinationNotificationUser(Request $request){

        $notification_details = NotificationUser::with(['user'])->where('vaccination_id',$request->vaccination_id)->where('child_id',$request->child_id)->where('interaction','1')->get();
        
        return view('admin.user_details.target_user',compact('notification_details'));
    }
    
    //children parent details
    public function ChildrenParentDetails(Request $request){

        $child_details = ChildrenDetail::where('id',$request->id)->where('is_delete','0')->first();
        
        $parent_list = User::where('id',$child_details->user_id)->where('is_delete','0')->first();
        
        $link_parent = UserLinkParent::with(['user'])->where('user_id',$parent_list->id)->where('is_delete','0')->get();
        
        return view('admin.user_details.parents_details',compact('parent_list','link_parent'));         
    }

    //get auto suggest doctor name
    public function getAutoSuggestDoctorName(Request $request){
        
        $doctorName = Doctor::where('doctor_name','LIKE','%'.$request->doctor_name.'%')->get();

        $doctorJson = array();

        if(!is_null($doctorName)){
            foreach($doctorName as $ck => $cv){
                $doctorJson[$ck]['label'] = $cv->doctor_name;
                $doctorJson[$ck]['value'] = $cv->id;
            }
        }

        return $doctorJson;   
    }

    //children excel data
    public function childrenExcelData(Request $request){

        if($request->children_id != ''){

        
            $children_id = json_decode($request->children_id);
            $childInfo = array();

            $i = 1;

            $childrenData = array();
            foreach ($children_id as $dk => $dv) {
                $child = ChildrenDetail::where('id',$dv)->with(['parent' => function($q){ $q->with(['linkedparent' => function($q) { $q->with(['user']); }]);},'location' => function($q) { $q->with(['city']);},'doctor' => function($q) { $q->with(['doctordetail']); }])->first();
                      $age = intval($child->age / 30,1);
                      
                if(!is_null($child->parent) && !is_null($child->parent->linkedparent)){
                    foreach($child->parent->linkedparent as $lk => $lv){
                        $childData['sr_no'] = $i;
                        $childData['Child Name'] = $child->child_name;
                        $childData['DOB'] = $child->date_of_birth;
                        $childData['Gender'] = $child->gender;
                        
                        $childData['Age'] = $age != 0 ? $age : '---';
                        if(!is_null($lv->user)){
                            $childData['parentsname'] = $lv->user->name;
                            $childData['parentscontact'] = $lv->user->mobile_number;
                        } else {
                            $childData['parentsname'] = '------';
                            $childData['parentscontact'] = '------';
                        }
                        if(!is_null($child->doctor) && $child->doctor->doctor_id != ''){
                            $childData['doctor'] = $child->doctor->doctordetail->doctor_name." (".$child->doctor->doctordetail->id.")";
                        } else {
                            $childData['doctor'] = 'Council';
                        }
                        if(!is_null($child->location) && !is_null($child->location->city)){
                            $childData['location'] = $child->location->city->city_name;
                        } else {
                            $childData['location'] = '------';
                        }
                        $childrenData[] = $childData;
                    }
                    $childData['sr_no'] = $i;
                    $childData['Child Name'] = $child->child_name;
                    $childData['DOB'] = $child->date_of_birth;
                    $childData['Gender'] = $child->gender;
                    $childData['Age'] = $age != 0 ? $age : '---';
                    $childData['parentsname'] = $child->parent->name;
                    $childData['parentscontact'] = $child->parent->mobile_number;
                    if(!is_null($child->doctor) && $child->doctor->doctor_id != ''){
                        $childData['doctor'] = $child->doctor->doctordetail->doctor_name." (".$child->doctor->doctordetail->id.")";
                    } else {
                        $childData['doctor'] = 'Council';
                    }
                    if(!is_null($child->location) && !is_null($child->location->city)){
                        $childData['location'] = $child->location->city->city_name;
                    } else {
                        $childData['location'] = '------';
                    }
                    $childrenData[] = $childData;
                } 
                $i++;
            }

            $childCsvArray[] = ['Sr no','Child Name','DOB','Gender','Age (months)','Parent Name','Parent Number','Doctor','Location'];

            foreach ($childrenData as $child) {
                $childCsvArray[] = $child;
            }

            $current_date_time = date("d_m_Y_h_i_s");
            Excel::create('children_details_'.$current_date_time.'', function($excel) use ($childCsvArray) {

                // Set the spreadsheet title, creator, and description
                $excel->setTitle('Doctor List');
                $excel->setCreator('Kidiatric')->setCompany('Kidiatric');
                $excel->setDescription('Doctor List');

                // Build the spreadsheet, passing in the payments array
                $excel->sheet('sheet1', function($sheet) use ($childCsvArray) {
                    $sheet->fromArray($childCsvArray, null, 'A1', false, false);
                });

            })->download('xlsx');

        } else {

            return redirect(route('admin.ChildrenList'))->with('messages', [
                  [
                      'type' => 'danger',
                      'title' => 'Children CSV',
                      'message' => 'Children Detail Not Found!',
                  ],
            ]);
        }
    }

    //city suggession
    public function citySuggestion(Request $request){

        $cityName = City::where('city_name','LIKE','%'.$request->city_name.'%')->get();

        $cityJson = array();

        if(!is_null($cityName)){
            foreach($cityName as $ck => $cv){
                $cityJson[$ck]['label'] = $cv->city_name;
                $cityJson[$ck]['value'] = $cv->id;
            }
        }

        return $cityJson;    
    }

    //parent excel data
    public function parentExcelData(Request $request){

        if($request->parent_id != ''){

            $parent_id = json_decode($request->parent_id);
            $childInfo = array();
            $realtion = array('0' => 'Mother','1' => 'Father','2' => 'Guardian');

            $i = 1;

            $parentDetails = array();
            foreach ($parent_id as $dk => $dv) {
                $parent_list = User::where('id',$dv)->where('is_otp_verified','1')->where('is_delete','0')->with(['city'])->first();
                $parentData['sr_no'] = $i;
                
                $parentData['Name'] = $parent_list->name;

                if($parent_list->mobile_number != ''){
                    $parentData['Mobile'] = $parent_list->mobile_number;
                } else {
                    $parentData['Mobile'] = '--------';
                }

                if($parent_list->email != ''){
                    $parentData['Email'] = $parent_list->email;
                } else {
                    $parentData['Email'] = '--------';
                }

                if(array_key_exists($parent_list->relation,$realtion)){
                    $parentData['realtion'] = $realtion[$parent_list->relation];
                } else {
                    $parentData['realtion'] = '--------';
                }
                
                if(!is_null($parent_list->city)){
                    $parentData['city'] = $parent_list->city->city_name;
                } else {
                    $parentData['city'] = '--------';
                }

                if($parent_list->type == 0){
                    $child_count = ChildrenDetail::where('user_id',$parent_list->id)->where('is_delete','0')->count();
                } else {
                    $child_count = ChildrenDetail::where('user_id',$parent_list->parent_id)->where('is_delete','0')->count();
                }
                if($child_count != '' && $child_count != 0){
                    $parentData['child_count'] = $child_count;
                } else {
                    $parentData['child_count'] = '-------';
                }

                $parentDetails[] = $parentData;
                $i++;
            }
          
            $parentExcelData[] = ['Sr no','Name','Mobile','Email','Relation','City','No of children'];

            foreach ($parentDetails as $child) {
                $parentExcelData[] = $child;
            }

            $current_date_time = date("d_m_Y_h_i_s");
            Excel::create('parent_'.$current_date_time.'', function($excel) use ($parentExcelData) {

                // Set the spreadsheet title, creator, and description
                $excel->setTitle('Doctor List');
                $excel->setCreator('Kidiatric')->setCompany('Kidiatric');
                $excel->setDescription('Doctor List');

                // Build the spreadsheet, passing in the payments array
                $excel->sheet('sheet1', function($sheet) use ($parentExcelData) {
                    $sheet->fromArray($parentExcelData, null, 'A1', false, false);
                });

            })->download('xlsx');

        } else {

            return redirect(route('admin.ParentsList'))->with('messages', [
                  [
                      'type' => 'danger',
                      'title' => 'Parents CSV',
                      'message' => 'Parents Detail Not Found!',
                  ],
            ]);
        }
    }

    

   

    
}
