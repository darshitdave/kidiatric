<?php

namespace App\Http\Controllers\Admin;

use App\Model\Doctor;
use App\Model\CommanCity;
use App\Model\Vaccination;
use Illuminate\Http\Request;
use App\Model\DoctorWiseVaccination;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use App\Http\Controllers\GlobalController;

class VaccinationController extends GlobalController
{
    public function __construct(){
      $this->middleware('admin');
    }

    //vaccination list
    public function vaccinationList(Request $request){

        $vaccination_list = Vaccination::where('is_delete',0)->orderBy('taken_at','ASC')->get();
        
        $colors = array('1'=>'#bbebfb','2'=>'#ffcdd2','3'=>'#ffecb3','4'=>'#d1c4e9','5'=>'#d0edc8');

        $days = array();

        $final_array = array();

        $i = 1;

        foreach ($vaccination_list as $vk => $vv) {

            if(!in_array($vv->taken_at, $days)){

                $days[] = $vv->taken_at;

                $final_array[$vv->taken_at] = $colors[$i];

                $i++;

                if($i > 5){

                    $i = 1;
                }
            }
        }
        
        $id_data = array();
        foreach ($vaccination_list as $vk => $vv) {
            $data = array();
            $id_data[] = $vv->id;
            
        }

        $id_json = json_encode($id_data);

        return view('admin.vaccination.vaccination_list',compact('vaccination_list','final_array','id_json'));
    }

    //add vaccination
    public function addVaccination(){

        return view('admin.vaccination.add_vaccination');   
    }

    //save vaccination
    public function saveVaccinationDetails(Request $request){
        
        $add_vaccination = new Vaccination;
        $add_vaccination->vaccination_name = $request->vaccination_name;
        $add_vaccination->vaccination_des = $request->vaccination_des;
        //convert to days
        $convert_days = explode(".",$request->taken_at);
        $month_days = 30*$convert_days[0];
        if(isset($convert_days[1])){
            $days = 3*$convert_days[1];
            //total days
            $total_days = $month_days + $days;
        }else{
            $total_days = $month_days;
        }
        $add_vaccination->taken_at = $total_days;
        $add_vaccination->prevents = $request->prevents;
        $add_vaccination->vaccination_type = $request->vacc_type;
        $add_vaccination->save();

        $doctor = $this->getDoctorId();

        if(!is_null($doctor)){
            foreach($doctor as $dk => $dv){
                $doctor_vaccination = new  DoctorWiseVaccination;
                $doctor_vaccination->doctor_id = $dv;
                $doctor_vaccination->vaccination_id = $add_vaccination->id;
                $doctor_vaccination->taken_at = $total_days;
                $doctor_vaccination->vaccination_type = $request->vacc_type;
                if($request->vacc_type == '0'){
                    $doctor_vaccination->is_active = '1';
                } else {
                    $doctor_vaccination->is_active = '0';
                }
                $doctor_vaccination->save();
            }           
        }

        return redirect(route('admin.vaccinationList'))->with('messages', [
                [
                    'type' => 'success',
                    'title' => 'Vaccination Detail',
                    'message' => 'Vaccination Detail Successfully Added!',
                ],
        ]);
    }
    
    //edit vaccination
    public function editVaccination(Request $request){
        
        $vaccination_list = Vaccination::where('id',$request->id)->first();

        $value = $vaccination_list->taken_at;
        $months = $value/30; 

        return view('admin.vaccination.edit_vaccination_detail',compact('vaccination_list','months')); 
    }

    //update vaccination details
    public function updateVaccinationDetails(Request $request){

        $update_vaccination = Vaccination::findOrFail($request->id);
        $update_vaccination->vaccination_name = $request->vaccination_name;
        $update_vaccination->vaccination_des = $request->vaccination_des;
        
        //convert to days
        $convert_days = explode(".",$request->taken_at);
        $month_days = 30*$convert_days[0];
        if(isset($convert_days[1])){
            $days = 3*$convert_days[1];
            //total days
            $total_days = $month_days + $days;
        }else{
            $total_days = $month_days;
        }

        $update_vaccination->taken_at = $total_days;
        $update_vaccination->prevents = $request->prevents;
        $update_vaccination->vaccination_type = $request->vacc_type;
        $update_vaccination->save();
        
        return redirect(route('admin.vaccinationList'))->with('messages', [
            [
                'type' => 'success',
                'title' => 'Vaccination Detail',
                'message' => 'Vaccination Detail Successfully Updated!',
            ],
        ]);

    }

    //delete vaccination details
    public function deleteVaccinationDetails(Request $request){

        $delete_vaccination = Vaccination::where('id',$request->id)->update(['is_delete' => 1]);

        if($delete_vaccination){

            return redirect(route('admin.vaccinationList'))->with('messages', [
                [
                    'type' => 'success',
                    'title' => 'Vaccination Detail',
                    'message' => 'Vaccination Detail Successfully Deleted!',
                ],
            ]); 
        }

    }

    //change vaccination status
    public function changeVaccinationStatus(Request $request){
        $vaccination_active = Vaccination::where('id',$request->id)->update(['is_active'=> $request->status]);

        if($request->status == 1){
            
            $check_vaccination = Doctor::where('is_delete','0')->get();

            foreach ($check_vaccination as $ck => $cv) {
                $doctor_vaccination = DoctorWiseVaccination::where('doctor_id',$cv->id)->where('vaccination_id',$request->id)->first();
                if(count($doctor_vaccination) == 0){
                    $vaccination = vaccination::where('id',$request->id)->first();

                    $doctor_vaccination = new DoctorWiseVaccination;
                    $doctor_vaccination->doctor_id = $cv->id;
                    $doctor_vaccination->vaccination_id = $request->id; 
                    $doctor_vaccination->taken_at = $vaccination->taken_at;
                    $doctor_vaccination->vaccination_type = $vaccination->vaccination_type;
                    $doctor_vaccination->is_active = $vaccination->is_active;
                    $doctor_vaccination->save();
                }
            }
        }

        if($vaccination_active){
            return 'true';
        } else {
            return 'false';
        }
    }

    //save admin taken at
    public function saveAdminTakenAt(Request $request){
        
        $update_taken_at = Vaccination::findOrFail($request->id);

        //convert to days
        $convert_days = explode(".",$request->taken_at);
        $month_days = 30*$convert_days[0];
        if(isset($convert_days[1])){
            $days = 3*$convert_days[1];
            //total days
            $total_days = $month_days + $days;
        }else{
            $total_days = $month_days;
        }

        $update_taken_at->taken_at = $total_days;
        $update_taken_at->save();

        if($update_taken_at){
            return 'true';
        }else{
            return 'false';
        }

    }

    //doctor vaccination list
    public function doctorVaccinationList(Request $request){
        
        $doctor_name = Doctor::where('is_active','1')->where('is_delete','0')->get();

        $vac_name = Vaccination::where('is_delete',0)->get();

        $doc_name = '';
        $vacc_name = '';

        $query = DoctorWiseVaccination::query();

        if(isset($request->doctor_id) && $request->doctor_id != ''){
            $doc_name = $request->doctor_id;
            $query->where('doctor_id',$doc_name);
        }

        if(isset($request->vacc_id) && $request->vacc_id != ''){
            $vacc_name = $request->vacc_id;
            $query->where('vaccination_id',$vacc_name);
        }

        //all doctor wise vaccination 
        $doc_vac = $query->with(['doctors'])->orderBy('taken_at','ASC')->get();
        
        return view('admin.doctor.vaccination.vaccination_list',compact('doc_vac','doctor_name','vac_name','doc_name','vacc_name'));
    }

    //admin vaccination csv
    public function adminVaccinationCsv(Request $request){

        if($request->vaccination != ''){
            $vaccination = json_decode($request->vaccination);

            $vaccination_info = array();
            $i = 1;

            foreach ($vaccination as $vk => $vv) {
                $vaccination_data = array();
                $vaccination = Vaccination::where('id',$vv)->first();

                $vaccination_data['sr_no'] = $i;
                $vaccination_data['vaccination_name'] = $vaccination->vaccination_name;
                if($vaccination->taken_at == '0'){
                    
                    $vaccination_data['taken_at'] = "0";    

                }else{
                    
                    $vaccination_data['taken_at'] = $vaccination->taken_at;    
                }
                
                $vaccination_data['prevent'] = $vaccination->prevents;
                

                if($vaccination->vaccination_type == 0){
                    $vaccination_data['vaccination_type'] = 'Counsil';    
                }else if($vaccination->vaccination_type == 1){
                    $vaccination_data['vaccination_type'] = 'Other';
                }

                if($vaccination->is_active == 0){
                    $vaccination_data['status'] = 'DeActive';    
                }else if($vaccination->is_active == 1){
                    $vaccination_data['status'] = 'Active';
                }

                $vaccination_info[] = $vaccination_data;

                $i++;
                 
            }
            
          
           $vaccinationCsvArray[] = ['Sr no','vaccination Name','Taken At','Prevents','Vaccination Type','Status'];

            foreach ($vaccination_info as $vdata) {

                $vaccinationCsvArray[] = $vdata;

            }
            $current_date_time = date("d-m-Y h:i:s");
            Excel::create('Vaccination '.$current_date_time.'', function($excel) use ($vaccinationCsvArray) {

                // Set the spreadsheet title, creator, and description
                $excel->setTitle('Vaccination List');
                $excel->setCreator('Kidiatric')->setCompany('Kidiatric');
                $excel->setDescription('Vaccination List');

                // Build the spreadsheet, passing in the payments array
                $excel->sheet('sheet1', function($sheet) use ($vaccinationCsvArray) {
                    $sheet->fromArray($vaccinationCsvArray, null, 'A1', false, false);
                });

            })->download('xlsx');

        }else{

            return redirect(route('admin.vaccinationList'))->with('messages', [
                [
                    'type' => 'danger',
                    'title' => 'Vaccination Detail',
                    'message' => 'Vaccination Detail Not Found!',
                ],
            ]); 
        }
    }

    //get doctor id
    public function getDoctorId(){

        $doctor = Doctor::pluck('id');

        return $doctor;
    }
}
