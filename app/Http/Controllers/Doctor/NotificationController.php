<?php

namespace App\Http\Controllers\Doctor;

use Hash;
use Auth;
use Carbon;
use DateTime;
use Redirect;
use Validator;
use App\Model\User;
use App\Model\Doctor;
use App\Model\HelpSupport;
use App\Model\City;
use App\Model\Vaccination;
use App\Model\Notification;
use App\Model\NotificationUser;
use App\Model\UserToken;
use Illuminate\Http\Request;
use App\Model\ChildrenDetail;
use App\Model\DoctorNotification;
use App\Model\UserChildWiseDoctor;
use App\Model\DoctorWiseVaccination;
use Maatwebsite\Excel\Facades\Excel;
use App\Model\DoctorClinicContactNumber;
use App\Http\Controllers\GlobalController;
use Illuminate\Support\Facades\Input;

class NotificationController extends GlobalController
{
    public function __construct(){
      	$this->middleware('doctor');
    }

    //create notification
    public function createNotificationList(Request $request){

        $realtion = array('0' => 'Mother','1' => 'Father','2' => 'Guardian');
        $id = '';
        $role = '';
        $gender = '';
        $city_id = '';
        $min_range = '';
        $max_range = '';
        $doctor_name = '';
        $doctor_name_id = '';
        $filter = 0;

        $get_child_info = UserChildWiseDoctor::where('doctor_id',Auth::guard('doctor')->user()->id)->pluck('child_id');

        $user_id = ChildrenDetail::whereIn('id',$get_child_info)->pluck('user_id');

        $query = ChildrenDetail::query();
        if(isset($request->gender) && $request->gender != ''){
            $query->where('gender',$request->gender);
            $gender = $request->gender;
            $filter = 1;
        }

        if(isset($request->min_age) && $request->min_age != ''){
            $min_range = $request->min_age;
            $filter_min_age = number_format($min_range * 30,0);
            $query->where('age','>=',$filter_min_age);
            $filter = 1;
        }

        if(isset($request->max_age) && $request->max_age != ''){
            $max_range = $request->max_age;
            $filter_max_age = number_format($max_range * 30,0);
            $query->where('age','<=',$filter_max_age);
            $filter = 1;
        }

        $query->where('is_delete','0');
        $query->whereIn('user_id',$user_id);
        $children_list = $query->pluck('user_id');
          
        $query = User::query();
        if(isset($request->role) && $request->role != ''){
            $query->where('relation',$request->role);
            $role = $request->role;
            $filter = 1;
        }

        if(isset($request->city_id) && $request->city_id != ''){
            $query->where('city_id',$request->city_id);
            $city_id = $request->city_id;
            $filter = 1;
        }
        
        $query->whereIn('id',$children_list);
        $query->where('is_otp_verified','1');
        $query->where('is_delete','0');
        $query->with(['city']);
        $parent_list  = $query->get();

        $id_json = array();
        if(!is_null($parent_list)){
            foreach($parent_list as $ck => $cv){
                $id_json[] = $cv->id;
            }
        }

        $cityList = City::all();

        return view('doctor.notification.create_notification',compact('parent_list','realtion','id_json','role','filter','id','city_id','cityList','gender','min_range','max_range','doctor_name','doctor_name_id'));
    }
    
    //send user notification
    public function sendUserNotification(Request $request){
        
        $userData = array();
        $user_id = array();
        if(isset($request->checkbox) && count($request->checkbox) > 0){
            foreach($request->checkbox as $ck){
                $u = User::where('id',$ck)->first();
                $user['name'] = $u->name;                 
                $user['email'] = $u->email;
                $user['mobile_number'] = $u->mobile_number;
                $user_id[] = $ck;
                $userData[] = $user;
            }
        }

      return view('doctor.doctorwisechild.user_data',compact('userData','user_id'));
    }

    //notification excel data
    public function notificationExcelData(Request $request){

      if($request->notification_id != ''){

            $parent_id = json_decode($request->notification_id);
            $childInfo = array();
            $realtion = array('0' => 'Mother','1' => 'Father','2' => 'Guardian');

            $i = 1;

            $parentDetails = array();
            foreach ($parent_id as $dk => $dv) {
                $parent_list = User::where('id',$dv)->where('is_otp_verified','1')->where('is_delete','0')->with(['city'])->first();
                $parentData['sr_no'] = $i;
                
                $parentData['Name'] = $parent_list->name;

                if($parent_list->mobile_number != ''){
                    $parentData['Mobile'] = $parent_list->mobile_number;
                } else {
                    $parentData['Mobile'] = '--------';
                }

                if($parent_list->email != ''){
                    $parentData['Email'] = $parent_list->email;
                } else {
                    $parentData['Email'] = '--------';
                }

                if(array_key_exists($parent_list->relation,$realtion)){
                    $parentData['realtion'] = $realtion[$parent_list->relation];
                } else {
                    $parentData['realtion'] = '--------';
                }
                
                if(!is_null($parent_list->city)){
                    $parentData['city'] = $parent_list->city->city_name;
                } else {
                    $parentData['city'] = '--------';
                }

                if($parent_list->type == 0){
                    $child_count = ChildrenDetail::where('user_id',$parent_list->id)->where('is_delete','0')->count();
                } else {
                    $child_count = ChildrenDetail::where('user_id',$parent_list->parent_id)->where('is_delete','0')->count();
                }
                if($child_count != '' && $child_count != 0){
                    $parentData['child_count'] = $child_count;
                } else {
                    $parentData['child_count'] = '-------';
                }

                $parentDetails[] = $parentData;
                $i++;
            }
          
            $parentExcelData[] = ['Sr no','Name','Mobile','Email','Relation','City','No of children'];

            foreach ($parentDetails as $child) {
                $parentExcelData[] = $child;
            }

            $current_date_time = date("d_m_Y_h_i_s");
            Excel::create('notification_list_'.$current_date_time.'', function($excel) use ($parentExcelData) {

                // Set the spreadsheet title, creator, and description
                $excel->setTitle('Doctor List');
                $excel->setCreator('Kidiatric')->setCompany('Kidiatric');
                $excel->setDescription('Doctor List');

                // Build the spreadsheet, passing in the payments array
                $excel->sheet('sheet1', function($sheet) use ($parentExcelData) {
                    $sheet->fromArray($parentExcelData, null, 'A1', false, false);
                });

            })->download('xlsx');

        } else {

            return redirect(route('doctor.createNotificationList'))->with('messages', [
                  [
                      'type' => 'danger',
                      'title' => 'Notification CSV',
                      'message' => 'Notification Detail Not Found!',
                  ],
            ]);
        }
    }

    //save notification list
    public function savedNotificationList(Request $request){
        
        $filter = 0;
        $range = '';
        $notification = "";

        $notification_id = NotificationUser::where('doctor_id','!=','')->where('doctor_id',Auth::guard('doctor')->user()->id)->pluck('notification_id');

        $query = Notification::query();

        if(isset($request->date_range) && $request->date_range != ''){
            $date = explode(" - ",$request->date_range);
            $start_date = date('d-m-Y',strtotime($date[0]));   
            $end_date = date('d-m-Y',strtotime($date[1]));
            $query->where('date','>=',trim($date[0]));
            $query->where('date','<=',trim($date[1]));
            $range = $request->date_range;
            $filter = 1;
        }

        if(isset($request->notification) && $request->notification != ''){
            $query->where('type',$request->notification);
            $notification = $request->notification;
            $filter = 1;
        }

        $query->whereIn('id',$notification_id);
        $query->orderBy('id','DESC');
        $notification_list = $query->get();
        
        return view('doctor.notification.notification_list',compact('notification_list','notification','filter','range'));
    }

    //doctor notification image
    public function docNotificationImage(Request $request){

        $message = Notification::where('id',$request->id)->first();

        return view('doctor.notification.notification_message',compact('message'));
    }

    //doc notification user
    public function docNotificationUser(Request $request){

        $target_user = NotificationUser::where('notification_id',$request->id)->get();

        return view('doctor.notification.target_user',compact('target_user'));
    }

    //send notification and msg 
    public function sendNotificationuser(Request $request){
       
        if($request->sms != ''){
            $user_id = json_decode($request->user_id);
            $title = "MESSAGE NOTIFICATION";
            $message_notification_id = $this->saveNotification(count($user_id),'SMS',$title,$request->sms);
            foreach($user_id as $mk => $mv){
                $getUser = User::where('id',$mv)->first();
                if(!is_null($getUser) && $getUser->mobile_number != ''){

                    $this->sendOtherSms($request->sms,$getUser->mobile_number);
                    $notificationUser = new NotificationUser;
                    $notificationUser->notification_id = $message_notification_id;
                    $notificationUser->user_id = $getUser->id;
                    $notificationUser->doctor_id = Auth::guard('doctor')->user()->id;
                    $notificationUser->save();
                }
            }
        }

        if($request->push_notification_title != '' && $request->push_notification_description != ''){
            $message = $request->push_notification_description;
            $title = $request->push_notification_title;
            $user_id = json_decode($request->user_id);
            $imageUrl = '';
            if(isset($request->image) && count($request->image) > 0){                
                $path = "doctor_notification/notification_image";
                $fileName = $this->uploadImage($request->image,$path);
                $imageUrl = $fileName;

            }
            $user_count = UserToken::whereIn('id',$user_id)->where('fcm_token','!=','')->count();
            $result = $this->saveNotification(count($user_id),'PUSH_NOTIFICATION',$title,$message,$imageUrl);
            foreach($user_id as $pk => $pv){
                $getUser = UserToken::where('user_id',$pv)->where('fcm_token','!=','')->get();
                if(!is_null($getUser)){
                    foreach($getUser as $gk => $gv){
                        if($gv->fcm_token != ''){
                            $type = 2;
                            $this->sendpushNotification($gv->device_type,$message,$gv->fcm_token,$title,$imageUrl,$type);

                            $notificationUser = new NotificationUser;
                            $notificationUser->notification_id = $result;
                            $notificationUser->user_id = $gv->user_id;
                            $notificationUser->doctor_id = Auth::guard('doctor')->user()->id;
                            $notificationUser->save();
                        }
                    }
                }
            }
        }

        if($request->email != '' && $request->subject != ''){
            $user_id = json_decode($request->user_id);
            $message = $request->email;
            $subject = $request->subject;
            $user_count = User::whereIn('id',$user_id)->where('email','!=','')->count();
            $email_notification_id = $this->saveNotification($user_count,'EMAIL',$subject,$message);
            foreach($user_id as $ek => $ev){
                $getUser = User::where('id',$ev)->first();
                if(!is_null($getUser) && $getUser->email != ''){
                    $templete = 'null_temp';
                    $email = $getUser->email;
                    $sub = $request->subject;
                    $url = \URL::to('/').'/templateEditor/';
                    $getemail = str_replace('/templateEditor/',$url, $request->email);
                    $this->sendMail($templete,$getemail,$sub,$email);

                    $notificationUser = new NotificationUser;
                    $notificationUser->notification_id = $email_notification_id;
                    $notificationUser->user_id = $getUser->id;
                    $notificationUser->doctor_id = Auth::guard('doctor')->user()->id;
                    $notificationUser->save();
                }   
            }
        }

        return redirect(route('doctor.wise.children'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Notification',
                  'message' => 'Notification Successfully Sent!',
              ],
        ]); 
    }

    //save notification
    public function saveNotification($user_count,$type,$title,$message = null,$imageUrl = null){
       
        $notification = new Notification;
        $notification->user_count = $user_count;
        $notification->type = $type;
        $notification->date = date('d-m-Y');
        if($type == 'EMAIL'){
            
        } elseif($type == "PUSH_NOTIFICATION") {
            
            $notification->title = $title;
            $notification->url = $imageUrl;
        }
        $notification->message = $message;
        $notification->notification_type = 2;
        $notification->save();


        return $notification->id;
    }

    //doctor notification
    public function doctorNotification($id){

        $findNotificationData = Notification::where('id',$id)->first();

        $getNotificationUser = NotificationUser::where('notification_id',$findNotificationData->id)->with(['user'])->get();

        return view('doctor.notification.view_doctor_notification',compact('findNotificationData','getNotificationUser'));
    }

    public function doctorNotificationUser(Request $request){

        $target_user = NotificationUser::where('notification_id',$request->id)->where('doctor_id',Auth::guard('doctor')->user()->id)->where('interaction','1')->get();

        return view('doctor.notification.target_user',compact('target_user'));      
    }

    //save health support request
    public function saveHelpSupportRequest(Request $request){

        $saveRequest = new HelpSupport;
        $saveRequest->doctor_id = $request->doctor_id;
        $saveRequest->message = $request->message;
        $saveRequest->save();

        return redirect(route('doctor.dashboard'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Notification',
                  'message' => 'Your request has been submitted to the Kidiatric team. They\'ll contact you within some time. ',
              ],
        ]); 
    }
}
