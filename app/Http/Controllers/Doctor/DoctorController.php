<?php

namespace App\Http\Controllers\Doctor;

use Hash;
use Auth;
use Carbon;
use DateTime;
use Redirect;
use Validator;
use App\Model\User;
use App\Model\Doctor;
use App\Model\HelpSupport;
use App\Model\City;
use App\Model\Vaccination;
use App\Model\Notification;
use App\Model\NotificationUser;
use App\Model\UserToken;
use Illuminate\Http\Request;
use App\Model\ChildrenDetail;
use App\Model\UserLinkParent;
use App\Model\ChildVaccination;
use App\Model\DoctorNotification;
use App\Model\UserChildWiseDoctor;
use App\Model\DoctorWiseVaccination;
use Maatwebsite\Excel\Facades\Excel;
use App\Model\DoctorClinicContactNumber;
use App\Http\Controllers\GlobalController;
use App\Model\ChildAlreadyTakenVaccination;
use Illuminate\Support\Facades\Input;

class DoctorController extends GlobalController
{
    public function __construct(){
        $this->middleware('doctor');
    }

    //Dashboard 
    public function index(){

        $doctor_profile = Doctor::with(['clinic_number'])->where('mobile_number',Auth::guard('doctor')->user()->mobile_number)->first();

        if($doctor_profile->is_complete == 0){
        

            return view('doctor.update_profile.doctor_details',compact('doctor_profile'));
            
        } else {

          $current_date = date("Y-m-d");
              
          //parent id
          $get_parents = UserChildWiseDoctor::where('doctor_id',Auth::guard('doctor')->user()->id)->pluck('user_id');

          //child id
          $get_child = UserChildWiseDoctor::where('doctor_id',Auth::guard('doctor')->user()->id)->pluck('child_id');

          $today_users = User::whereIn('id',$get_parents)->Where('created_at','like','%'.$current_date.'%')->where('is_delete','0')->count();

        $today_children = ChildrenDetail::whereIn('id',$get_child)->Where('created_at','like','%'.$current_date.'%')->where('is_delete','0')->count();

        
          //weekly date
          $prev_sunday = date('Y-m-d',strtotime('last sunday'));
          $next_sunday = date('Y-m-d',strtotime('next saturday'));

        //weekly data
        $weekly_user = User::whereIn('id',$get_parents)->whereBetween('created_at',[$prev_sunday,$next_sunday])->where('is_delete','0')->count();
        $weekly_children = ChildrenDetail::whereIn('id',$get_child)->whereBetween('created_at',[$prev_sunday,$next_sunday])->where('is_delete','0')->count();

          //monthly date
          $first_day_this_month = date('Y-m-01'); // hard-coded '01' for first day
          $last_day_this_month  = date('Y-m-t'); 

        //monthly data
        $monthly_user = User::whereIn('id',$get_parents)->whereBetween('created_at',[$first_day_this_month,$last_day_this_month])->where('is_delete','0')->count();
        $monthly_children = ChildrenDetail::whereIn('id',$get_child)->whereBetween('created_at',[$first_day_this_month,$last_day_this_month])->where('is_delete','0')->count();

        //all time data
        $all_user = User::whereIn('id',$get_parents)->where('is_delete','0')->count();
   
        $all_children = ChildrenDetail::whereIn('id',$get_child)->where('is_delete','0')->count();

      $user_months = array();
      $user_month = array();
      $monthly_user_data = array();  

        for($m=1; $m<=12; ++$m){
            $user_months[$m]['month'] = date('M', mktime(0, 0, 0, $m, 1));
            $user_months[$m]['month_key'] = $m;
        }

        foreach($user_months as $m){
            $user_month[] = $m['month'];
            $currentMonth = date('m');
            $count = $data = User::whereIn('id',$get_parents)->whereRaw('MONTH(created_at) = ?',[$m['month_key']])->where('is_otp_verified','1')->where('is_delete','0')->count();
            $monthly_user_data[] = $count;
        }


      $child_months = array();
      $child_month = array();
      $monthly_child_data = array();  

        for($m=1; $m<=12; ++$m){
            $child_months[$m]['month'] = date('M', mktime(0, 0, 0, $m, 1));
            $child_months[$m]['month_key'] = $m;
        }

        foreach($child_months as $m){
            $child_month[] = $m['month'];
            $currentMonth = date('m');
            $count = $data = ChildrenDetail::whereIn('id',$get_child)->whereRaw('MONTH(created_at) = ?',[$m['month_key']])->where('is_delete','0')->count();
            $monthly_child_data[] = $count;
        }

          return view('doctor.dashboard.dashboard',compact('today_users','today_children','weekly_user','weekly_children','monthly_user','monthly_children','user_month','monthly_user_data','child_month','monthly_child_data','all_user','all_children'));
      }
    }   

    //doctor details
    public function doctorProfile(){
        
        $doctor_profile = Doctor::with(['clinic_number'])->where('id',Auth::guard('doctor')->user()->id)->first();
  
        return view('doctor.update_profile.doctor_details',compact('doctor_profile'));
    }

    public function updateDoctorDetails(Request $request){

      $update_doctor = Doctor::findOrFail($request->id);
      $update_doctor->doctor_name = $request->doctor_name;
      $update_doctor->mobile_number = $request->mobile_number;
      $update_doctor->email = $request->email;
      $update_doctor->practicing_since = $request->practicing_since;
        
        $city = explode(",",$request->city); $value = reset($city);
        if(isset($request->city) && !is_null($request->city)){
            //check in database
            $check_city = City::where('city_name',$value)->first();
            if($check_city == '' && is_null($check_city) && count($check_city) <= 0){
                $add_doctor_city = new City;
                $add_doctor_city->city_name = $value;
                $add_doctor_city->save();

                $update_doctor->city = $add_doctor_city->id;
            }else{
                $update_doctor->city = $check_city->id;
            }
        }
      $update_doctor->ref_city_name = $request->city;
      $update_doctor->pincode = $request->pincode;
      $update_doctor->address = $request->address;
      $update_doctor->specialization = $request->specialization;
      $update_doctor->is_complete = '1';
      $update_doctor->save();

        if(isset($request->clinic_phone_number)){
            $deleteclinic_no = DoctorClinicContactNumber::where('doctor_id',$request->id)->delete();
            foreach ($request->clinic_phone_number as $ck => $cv) {
              if($cv != ''){
                $add_clinic_con = new DoctorClinicContactNumber;
                $add_clinic_con->doctor_id = $request->id;
                $add_clinic_con->clinic_phone_number = $cv;
                $add_clinic_con->save();  
              }  
            }
        }
        

      return redirect(route('doctor.dashboard'))->with('messages', [
            [
                'type' => 'success',
                'title' => 'Doctor Detail',
                'message' => 'Profile Successfully Updated!',
            ],
      ]);
        
    }

    public function doctorWiseChild(Request $request){
     
      $gender = '';
      $min_range = '';
      $max_range = '';
      $city = '';
      $city_id = '';
      $filter = 0;


      $get_child_info = UserChildWiseDoctor::where('doctor_id',Auth::guard('doctor')->user()->id)->pluck('child_id');

      $query = ChildrenDetail::query();
      
      if(isset($request->id) && $request->id != ''){

        if($request->id == '1'){
          $current_date = date("Y-m-d");
          $query->Where('created_at','like','%'.$current_date.'%');

        }elseif ($request->id == '2') {
          //weekly date
          $prev_sunday = date('Y-m-d',strtotime('last sunday'));
          $next_sunday = date('Y-m-d',strtotime('next saturday'));
          $query->whereBetween('created_at',[$prev_sunday,$next_sunday]);
              
        }elseif ($request->id == '3') {
          //monthly date
          $first_day_this_month = date('Y-m-01'); // hard-coded '01' for first day
          $last_day_this_month  = date('Y-m-t');
          $query->whereBetween('created_at',[$first_day_this_month,$last_day_this_month]);
              
        }

      }

      if(isset($request->gender) && $request->gender != ''){
          $query->where('gender',$request->gender);
          $gender = $request->gender;
          $filter = 1;
      }

      if(isset($request->min_age) && $request->min_age != ''){
          $min_range = $request->min_age;
          $filter_min_age = number_format($min_range * 30,0);
          $query->where('age','>=',$filter_min_age);
          $filter = 1;
      }

      if(isset($request->max_age) && $request->max_age != ''){
          $max_range = $request->max_age;
          $filter_max_age = number_format($max_range * 30,0);
          $total_days = $filter_max_age + 29;
          $query->where('age','<=',$total_days);
          $filter = 1;
      }

      if(isset($request->city_id) && $request->city_id != ''){

          $findCityChild = User::where('city_id',$request->city_id)->with(['children'])->where('type',0)->get();

          $getChildId = array();
          if(!is_null($findCityChild)){
              foreach($findCityChild as $fk => $fv){
                  if(!is_null($fv->children)){
                      foreach($fv->children as $ck => $cv){
                          $getChildId[] = $cv->child_id;
                      }
                  }
              }
          }
          $query->whereIn('id',$getChildId);
          $city_id = $request->city_id;
          $filter = 1;
      }

      $query->whereIn('id',$get_child_info);
      $query->where('is_delete','0');
      $query->with(['ParentDetails']);
      $query->with(['location' => function($q) { $q->with(['city']);},'doctor' => function($q) { $q->with(['doctordetail']); }]);
      $child_details = $query->get(); 

      
      $id_json = array();
      if(!is_null($child_details)){
          foreach($child_details as $ck => $cv){
            $id_json[] = $cv->id;
        }
      }

      $cityList = City::all();

      return view('doctor.doctorwisechild.doctorwisechildlist',compact('get_child_info','child_details','id_json','gender','min_range','max_range','filter','city_id','city','cityList'));
    }


    public function citySuggestion(Request $request){

        $cityName = City::where('city_name','LIKE','%'.$request->city_name.'%')->get();

        $cityJson = array();

        if(!is_null($cityName)){
            foreach($cityName as $ck => $cv){
                $cityJson[$ck]['label'] = $cv->city_name;
                $cityJson[$ck]['value'] = $cv->id;
            }
        }
      return $cityJson; 
    }

    public function ChildrenVaccination($uuid){

      $child_details = ChildrenDetail::with(['location' => function($q) { $q->with(['city']);},'doctor' => function($q) { $q->with(['doctordetail']); }])->where('uuid',$uuid)->first();

        //form birth date to current date difference
        $data_date = date('Y-m-d',strtotime($child_details->date_of_birth));  
        $currentDateTime = date('Y-m-d');
        $datetime1 = new DateTime($data_date);
        $datetime2 = new DateTime($currentDateTime);
        $interval = $datetime1->diff($datetime2);
        $days = $interval->format('%a');

        $children_vaccinations = ChildVaccination::where('child_id',$child_details->id)->pluck('vaccination_id');
        $taken_vaccination = ChildAlreadyTakenVaccination::where('child_id',$child_details->id)->pluck('vaccination_id');

        $query = DoctorWiseVaccination::query();
        $query->with(['vaccination']);
        $query->where('doctor_id',Auth::guard('doctor')->user()->id);
        $query->whereNotIn('vaccination_id',$children_vaccinations);
        $query->whereNotIn('vaccination_id',$taken_vaccination);
        $query->where('is_active','1');
        $query->where('vaccination_type','0');
        $vaccinations = $query->orderBy('taken_at','ASC')->get();

            //change date of vaccination and not takened vaccination
            $change_date = ChildVaccination::where('child_id',$child_details->id)->where('good_to_have','0')->where('taken_vaccination','0')->get();

            //good to have data
            $get_good_to_have = ChildVaccination::where('child_id',$child_details->id)->where('good_to_have','1')->where('taken_vaccination','0')->get();
            
            //taken vaccination
            $takened_vaccination = ChildVaccination::where('child_id',$child_details->id)->where('good_to_have','0')->where('taken_vaccination','1')->get();

            $child_taken = ChildAlreadyTakenVaccination::where('child_id',$child_details->id)->orderBy('taken_at','ASC')->get();


        $already_taken = array();
        if(!is_null($child_taken)){
            foreach ($child_taken as $ck => $cv) {
                $taken_vaccination = array();
                $taken_vaccination['name'] = $cv->vaccination->vaccination_name;
                $taken_vaccination['taken_at'] = $cv->taken_at;
                $taken_vaccination['status'] = 'Taken';
                $taken_vaccination['due_date'] = $cv->vaccination_date;
                $taken_vaccination['taken_date'] = $cv->vaccination_date;
                $already_taken[] = $taken_vaccination;
            }
        }    

        $not_taken = array();
        if(!is_null($change_date)){
            foreach ($change_date as $dk => $dv) {
                $remaining_vaccination = array();
                $remaining_vaccination['name'] = $dv->vaccination->vaccination_name;
                $remaining_vaccination['taken_at'] = $dv->taken_at;
                $remaining_vaccination['status'] = 'Due';
                  $new_date =  date('d-m-Y', strtotime($dv->vaccination_date));
                $remaining_vaccination['due_date'] = $new_date;
                $remaining_vaccination['taken_date'] = '---------';
                $not_taken[] = $remaining_vaccination;
            }
        }

        $good_to_have = array();
        if(!is_null($get_good_to_have)){
            foreach ($get_good_to_have as $gk => $gv) {
                $good_to_have_vaccination = array();
                $good_to_have_vaccination['name'] = $gv->vaccination->vaccination_name;
                $good_to_have_vaccination['taken_at'] = $gv->taken_at;
                $good_to_have_vaccination['status'] = 'Due';
                  $new_date =  date('d-m-Y', strtotime($gv->vaccination_date));
                $good_to_have_vaccination['due_date'] = $new_date;
                $good_to_have_vaccination['taken_date'] = '---------';
                $good_to_have[] = $good_to_have_vaccination;
            }
        }

        $takened = array();
        if(!is_null($takened_vaccination)){
            foreach ($takened_vaccination as $tk => $tv) {
                $taken_detail = array();
                $taken_detail['name'] = $tv->vaccination->vaccination_name;
                $taken_detail['taken_at'] = $tv->taken_at;
                $taken_detail['status'] = 'Taken';
                  $new_date =  date('d-m-Y', strtotime($tv->vaccination_date));
                  $tv = $tv->taken_at+1;
                  $due_date =  date('d-m-Y', strtotime($child_details->date_of_birth. ' + '.$tv.' days'));
                $taken_detail['due_date'] = $due_date;
                $taken_detail['taken_date'] = $new_date;
                $takened[] = $taken_detail;
            }
        }

        $all_vaccinations = array();
        if(!is_null($vaccinations)){
            foreach ($vaccinations as $vk => $vv) {
                $vaccinations = array();
                $vaccinations['name'] = $vv['vaccination']['vaccination_name'];
                $vaccinations['taken_at'] = $vv->taken_at;
                if($vv->taken_at <= $days){
                    $vaccinations['status'] = 'Taken';
                      $vv = $vv->taken_at+1;
                    $new_date =  date('d-m-Y', strtotime($child_details->date_of_birth. ' + '.$vv.' days'));
                    $vaccinations['due_date'] = $new_date;
                    $vaccinations['taken_date'] = $new_date;

                }else{

                    $vaccinations['status'] = 'Due';
                    $vv = $vv->taken_at+1;
                    $new_date =  date('d-m-Y', strtotime($child_details->date_of_birth. ' + '.$vv.' days'));
                    $vaccinations['due_date'] = $new_date;
                    $vaccinations['taken_date'] = '---------';
                }

                
                $all_vaccinations[] = $vaccinations;
            }
        }

        $vaccination_data = array_merge($all_vaccinations,$already_taken,$not_taken,$good_to_have,$takened);
                
        $price = array_column($vaccination_data, 'taken_at');

        array_multisort($price, SORT_ASC, $vaccination_data);
        
      return view('doctor.doctorwisechild.children_vaccinations',compact('child_details','days','vaccination_data'));                    
    }

    public function ChildrenParentDetails(Request $request){

      $child_details = ChildrenDetail::where('id',$request->id)->first();
      
      $parent_list = User::where('id',$child_details->user_id)->where('is_delete','0')->first();

      $link_parent = UserLinkParent::with(['user'])->where('user_id',$parent_list->id)->where('is_delete','0')->get();
          
      return view('doctor.doctorwisechild.parents_details',compact('parent_list','link_parent'));       
    }

    //doctor child wise parents details
    public function doctorWiseChildParents(Request $request){
        
        $get_child_info = UserChildWiseDoctor::where('doctor_id',Auth::guard('doctor')->user()->id)->pluck('child_id');

        $user_id = ChildrenDetail::whereIn('id',$get_child_info)->pluck('user_id');
        
        $relation = array('0' => 'Mother','1' => 'Father','2' => 'Guardian');
        
        $gender = '';
        $min_range = '';
        $max_range = '';
        $role = '';
        $filter = 0;
        $city_id = '';
        $city_name = '';
        $id = '';

        $query = ChildrenDetail::query();

        if(isset($request->gender) && $request->gender != ''){
            $query->where('gender',$request->gender);
            $gender = $request->gender;
            $filter = 1;
        }

        if(isset($request->min_age) && $request->min_age != ''){
            $min_range = $request->min_age;
            $filter_min_age = number_format($min_range * 30,0);
            $query->where('age','>=',$filter_min_age);
            $filter = 1;
        }

        if(isset($request->max_age) && $request->max_age != ''){
            $max_range = $request->max_age;
            $filter_max_age = number_format($max_range * 30,0);
            $total_days = $filter_max_age + 29;
            $query->where('age','<=',$total_days);
            $filter = 1;
        }
        $children_list = $query->pluck('user_id');
        
        $query = User::query();

        if(isset($request->id) && $request->id != ''){
            
            $id = $request->id;
            if($request->id == '1'){
                $current_date = date("Y-m-d");
                $query->Where('created_at','like','%'.$current_date.'%');
                $query->orderBy('id','desc');
            } elseif ($request->id == '2') {
                //weekly date
                $prev_sunday = date('Y-m-d',strtotime('last sunday'));
                $next_sunday = date('Y-m-d',strtotime('next saturday'));

                $query->whereBetween('created_at',[$prev_sunday,$next_sunday]);
                $query->orderBy('id','desc');
              
            } elseif ($request->id == '3') {
                //monthly date
                $first_day_this_month = date('Y-m-01'); // hard-coded '01' for first day
                $last_day_this_month  = date('Y-m-t');
                $query->whereBetween('created_at',[$first_day_this_month,$last_day_this_month]);
                $query->orderBy('id','desc');
            }
        }

        if(isset($request->role) && $request->role != ''){
            $query->where('relation',$request->role);
            $role = $request->role;
            $filter = 1;
        }

        if(isset($request->city_id) && $request->city_id != ''){
            $query->where('city_id',$request->city_id);
            $city_id = $request->city_id;
            $filter = 1;
        }

        $query->whereIn('id',$user_id);
        $query->where('is_delete','0');
        $query->where('is_otp_verified','1');
        $query->with(['city']);
        if(isset($children_list))
            {
                $query->whereIn('id',$children_list);
            }
        $parent_list  = $query->get();
   
        $id_json = array();

        if(!is_null($parent_list)){
            foreach($parent_list as $ck => $cv){
                $id_json[] = $cv->id;
            }
        }

        $cityList = City::all();

        return view('doctor.parents.parents_list',compact('parent_list','relation','id_json','role','filter','id','city_id','cityList','min_range','max_range','gender'));
    }

    public function parentsChildDetails(Request $request){

      $child = UserChildWiseDoctor::where('user_id',$request->id)->where('doctor_id',Auth::guard('doctor')->user()->id)->pluck('child_id');

      $child_Details = ChildrenDetail::whereIn('id',$child)->where('is_delete','0')->get();
        
      return view('doctor.parents.children_details',compact('child_Details'));
    }

    public function childrenExcelData(Request $request){

      if($request->children_id != ''){
        
            $children_id = json_decode($request->children_id);
            $childInfo = array();

            $i = 1;

            $childrenData = array();
            foreach ($children_id as $dk => $dv) {
                $child = ChildrenDetail::where('id',$dv)->with(['parent' => function($q){ $q->with(['linkedparent' => function($q) { $q->with(['user']); }]);},'location' => function($q) { $q->with(['city']);},'doctor' => function($q) { $q->with(['doctordetail']); }])->first();
                      $age = intval($child->age / 30,1);
                      
                if(!is_null($child->parent) && count($child->parent) > 0 && !is_null($child->parent->linkedparent) && count($child->parent->linkedparent) > 0){
                    foreach($child->parent->linkedparent as $lk => $lv){
                        $childData['sr_no'] = $i;
                        $childData['Child Name'] = $child->child_name;
                        $childData['DOB'] = $child->date_of_birth;
                        $childData['Gender'] = $child->gender;
                        
                        $childData['Age'] = $age != 0 ? $age : '---';
                        if(!is_null($lv->user)){
                            $childData['parentsname'] = $lv->user->name;
                            $childData['parentscontact'] = $lv->user->mobile_number;
                        } else {
                            $childData['parentsname'] = '------';
                            $childData['parentscontact'] = '------';
                        }
                        if(!is_null($child->location) && !is_null($child->location->city)){
                            $childData['location'] = $child->location->city->city_name;
                        } else {
                            $childData['location'] = '------';
                        }
                        $childrenData[] = $childData;
                    }
                    $childData['sr_no'] = $i;
                    $childData['Child Name'] = $child->child_name;
                    $childData['DOB'] = $child->date_of_birth;
                    $childData['Gender'] = $child->gender;
                    $childData['Age'] = $age != 0 ? $age : '---';
                    $childData['parentsname'] = $child->parent->name;
                    $childData['parentscontact'] = $child->parent->mobile_number;
                    if(!is_null($child->location) && !is_null($child->location->city)){
                        $childData['location'] = $child->location->city->city_name;
                    } else {
                        $childData['location'] = '------';
                    }
                    $childrenData[] = $childData;
                } 
                $i++;
            }
          
            $childCsvArray[] = ['Sr no','Child Name','DOB','Gender','Age (months)','Parent Name','Parent Number','Location'];

            foreach ($childrenData as $child) {
                $childCsvArray[] = $child;
            }

            $current_date_time = date("d_m_Y");
            Excel::create('children_details_'.$current_date_time.'', function($excel) use ($childCsvArray) {

                // Set the spreadsheet title, creator, and description
                $excel->setTitle('Doctor List');
                $excel->setCreator('Kidiatric')->setCompany('Kidiatric');
                $excel->setDescription('Doctor List');

                // Build the spreadsheet, passing in the payments array
                $excel->sheet('sheet1', function($sheet) use ($childCsvArray) {
                    $sheet->fromArray($childCsvArray, null, 'A1', false, false);
                });

            })->download('xlsx');

        } else {

            return redirect(route('doctor.wise.children'))->with('messages', [
                  [
                      'type' => 'danger',
                      'title' => 'Children CSV',
                      'message' => 'Children Detail Not Found!',
                  ],
            ]);
        }
    }

    public function parentExcelData(Request $request){

       if($request->parent_id != ''){

        
            $parent_id = json_decode($request->parent_id);
            $childInfo = array();
            $realtion = array('0' => 'Mother','1' => 'Father','2' => 'Guardian');

            $i = 1;

            $parentDetails = array();
            foreach ($parent_id as $dk => $dv) {
                $parent_list = User::where('id',$dv)->where('is_otp_verified','1')->where('is_delete','0')->with(['city'])->first();
                $parentData['sr_no'] = $i;
                
                $parentData['Name'] = $parent_list->name;

                if($parent_list->mobile_number != ''){
                    $parentData['Mobile'] = $parent_list->mobile_number;
                } else {
                    $parentData['Mobile'] = '--------';
                }

                if($parent_list->email != ''){
                    $parentData['Email'] = $parent_list->email;
                } else {
                    $parentData['Email'] = '--------';
                }

                if(array_key_exists($parent_list->relation,$realtion)){
                    $parentData['realtion'] = $realtion[$parent_list->relation];
                } else {
                    $parentData['realtion'] = '--------';
                }
                
                if(!is_null($parent_list->city)){
                    $parentData['city'] = $parent_list->city->city_name;
                } else {
                    $parentData['city'] = '--------';
                }

                if($parent_list->type == 0){
                    $child_count = ChildrenDetail::where('user_id',$parent_list->id)->where('is_delete','0')->count();
                } else {
                    $child_count = ChildrenDetail::where('user_id',$parent_list->parent_id)->where('is_delete','0')->count();
                }
                if($child_count != '' && $child_count != 0){
                    $parentData['child_count'] = $child_count;
                } else {
                    $parentData['child_count'] = '-------';
                }

                $parentDetails[] = $parentData;
                $i++;
            }
          
            $parentExcelData[] = ['Sr no','Name','Mobile','Email','Relation','City','No of children'];

            foreach ($parentDetails as $child) {
                $parentExcelData[] = $child;
            }

            $current_date_time = date("d_m_Y_h_i_s");
            Excel::create('parent_'.$current_date_time.'', function($excel) use ($parentExcelData) {

                // Set the spreadsheet title, creator, and description
                $excel->setTitle('Doctor List');
                $excel->setCreator('Kidiatric')->setCompany('Kidiatric');
                $excel->setDescription('Doctor List');

                // Build the spreadsheet, passing in the payments array
                $excel->sheet('sheet1', function($sheet) use ($parentExcelData) {
                    $sheet->fromArray($parentExcelData, null, 'A1', false, false);
                });

            })->download('xlsx');

        } else {

            return redirect(route('doctor.ParentsList'))->with('messages', [
                  [
                      'type' => 'danger',
                      'title' => 'Parents CSV',
                      'message' => 'Parents Detail Not Found!',
                  ],
            ]);
        }
    }

}