<?php

namespace App\Http\Controllers\Doctor;

use Auth;
use App\Model\Doctor;
use App\Model\CommanCity;
use App\Model\Vaccination;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\DoctorWiseVaccination;
use App\Http\Controllers\GlobalController;

class VaccinationController extends GlobalController
{
    public function __construct(){
      $this->middleware('doctor');
    }

    public function vaccinationList(Request $request){

        $vaccination_list = Vaccination::where('is_active',1)->where('is_delete',0)->orderBy('taken_at','ASC')->get();
        
        //all doctor wise vaccination 
        $doc_vac = DoctorWiseVaccination::with(['vaccination'])->where('doctor_id',Auth::guard('doctor')->user()->id)->orderBy('taken_at','ASC')->get();

        $colors = array('1'=>'#bbebfb','2'=>'#ffcdd2','3'=>'#ffecb3','4'=>'#d1c4e9','5'=>'#d0edc8');

        $days = array();

        $final_array = array();

        $i = 1;

        foreach ($doc_vac as $dk => $dv) {

            if(!in_array($dv->taken_at, $days)){

                $days[] = $dv->taken_at;

                $final_array[$dv->taken_at] = $colors[$i];

                $i++;

                if($i > 5){

                    $i = 1;
                }
            }
        }

        $doctor_check = Doctor::where('id',Auth::guard('doctor')->user()->id)->where('is_approve','1')->where('is_active','1')->where('is_delete','0')->first();

        return view('doctor.vaccination.vaccination_list',compact('vaccination_list','doc_vac','final_array','doctor_check'));
       
    }

    public function changeVaccinationStatus(Request $request){
        $vaccination_active = DoctorWiseVaccination::where('id',$request->id)->update(['is_active'=> $request->status]);

        if($vaccination_active){
            return 'true';
        } else {
            return 'false';
        }
    }

    public function saveDoctorTakenAt(Request $request){
        
        $update_taken_at = DoctorWiseVaccination::findOrFail($request->id);

        //convert to days
        $convert_days = explode(".",$request->taken_at);
        $month_days = 30*$convert_days[0];
        if(isset($convert_days[1])){
            $days = 3*$convert_days[1];
            //total days
            $total_days = $month_days + $days;
        }else{
            $total_days = $month_days;
        }

        $update_taken_at->taken_at = $total_days;
        $update_taken_at->save();

        if($update_taken_at){
            return 'true';
        }else{
            return 'false';
        }

    }

    public function disableDoctorModel(Request $request){

        if($request->type == 'type'){
          
           return view('doctor.dashboard.doctor_model'); 
        }
    }
}
