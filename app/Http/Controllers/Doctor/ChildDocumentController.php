<?php

namespace App\Http\Controllers\Doctor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\UserWisePriscription;
use App\Model\ChildrenDetail;
use App\Model\UserChildWiseDoctor;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\Filesystem;
use League\Flysystem\ZipArchive\ZipArchiveAdapter;
use Response;

class ChildDocumentController extends Controller
{
    public function __construct(){
      	
      	$this->middleware('doctor');

    }

    public function doctorWiseChildDocument($id){

    	$child_details = ChildrenDetail::with(['location' => function($q) { $q->with(['city']);},'doctor' => function($q) { $q->with(['doctordetail']); }])->where('uuid',$id)->first();

        if(!is_null($child_details)){

        	$getPrescription = UserWisePriscription::where('child_id',$child_details->id)->where('is_delete',0)->get();

        	return view('doctor.child.documemt_list',compact('getPrescription','child_details'));
            
        } else {

            abort('404');
        }
    }

    public function downloadAllDocument($id){
        
    	$child_details = ChildrenDetail::where('id',$id)->first();

        $getPrescription = UserWisePriscription::where('child_id',$child_details->id)->where('is_delete',0)->get();

        if(count($getPrescription) > 0){

        	$imgArray = array();

        	$source_disk = 's3';
            $source_path = '/uploads/child_image/'.$child_details->uuid.'/prescription/';

            $child = "child/".$child_details->child_name.".zip";

            if(is_file($child)){
                unlink(public_path($child));
            }

            $file_names = Storage::disk($source_disk)->files($source_path);

            $zip = new Filesystem(new ZipArchiveAdapter(public_path($child)));

            foreach($file_names as $file_name){
                $file = explode('/',$file_name);
                $file_content = Storage::disk($source_disk)->get($file_name);
                $zip->put(end($file), $file_content);
            }

            $zip->getAdapter()->getArchive()->close();

            return response()->download($child);

        } else {

            return redirect()->back()->with('messages', [
                    [
                        'type' => 'success',
                        'title' => 'Doctor Detail',
                        'message' => 'Documents Not Found',
                    ],
            ]);
        }
    }

    public function authImage($secret){

        $getPrescription = UserWisePriscription::where('image_token',$secret)->where('is_delete',0)->first();

        if(!is_null($getPrescription)){

            $child_details = ChildrenDetail::where('id',$getPrescription->child_id)->first();

            $getDoctorId = UserChildWiseDoctor::where('child_id',$getPrescription->child_id)->first();

            if(!is_null($getDoctorId) && $getDoctorId->doctor_id == \Auth::guard('doctor')->user()->id){

                $pathToFile = "https://kidiatric.s3.ap-south-1.amazonaws.com/uploads/child_image/".$child_details->uuid."/prescription/".$getPrescription->prescription_attachment;

                $source_path = '/uploads/child_image/'.$child_details->uuid.'/prescription/'.$getPrescription->prescription_attachment;

                if(Storage::disk('s3')->has($source_path)){
                    $data = Storage::disk('s3')->get($source_path);
                    $getMimeType = Storage::disk('s3')->getMimetype($source_path);
                    $headers = [
                    'Content-type' => $getMimeType, 
                    'Content-Disposition'=>sprintf('attachment; filename="%s"', $getPrescription->prescription_attachment)
                    ];
                    return Response::make($data, 200, $headers);
                }

                return response()->file($pathToFile);

            } else {

                abort('403');
            }

        } else {

            abort('404');
        }
    }

    
}
