<?php

namespace App\Http\Controllers\Doctor\Auth;

use Hash;
use App\Model\Doctor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers {
    	logout as performLogout;
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    protected $redirectPath = '/doctor-panel';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        //$this->middleware('guest')->except('logout');
    }

    public function showLoginForm(){
        return view('doctor.auth.login');
    }

    protected function validateLogin(Request $request){

        $request->validate([
            'mobile_number' => 'required|string',
            'otp' => 'required|string',
        ]);
    }

    protected function login(Request $request)
    {    
      
        // Get user record
        $doctor = Doctor::where('mobile_number', $request->get('mobile_number'))->first();

        if(count($doctor) > 0){

            if($doctor->is_active == 0){

                return redirect()->back()->with('messages', [
                    [
                        'type' => 'danger',
                        'title' => 'Doctor',
                        'message' => 'Your account is inactive, please contact to admin',
                    ],
                ]);
            }

            if($doctor->is_delete == 1){

                return redirect()->back()->with('messages', [
                    [
                        'type' => 'danger',
                        'title' => 'Doctor',
                        'message' => 'Your account is deleted, please contact to admin',
                    ],
                ]);
            }
            // Check Condition Mobile No. Found or Not
            if($request->get('mobile_number') != $doctor->mobile_number || $request->get('otp') != $doctor->otp) {
                

                return redirect()->back()->with('messages', [
                    [
                        'type' => 'danger',
                        'title' => 'Doctor',
                        'message' => 'Mobile number or OTP not valid',
                    ],
                ]);
                
            } else {
                
                $insert_creden = Doctor::Where('mobile_number',$request->get('mobile_number'))->update(['otp_verified'=>'1']);

                \Auth::guard('doctor')->login($doctor);
                // Set Auth Details
                return redirect(route('doctor.dashboard'));    

            } 

        } else {

            return redirect()->back()->with('messages', [
                [
                    'type' => 'danger',
                    'title' => 'Doctor',
                    'message' => 'Mobile Number Or OTP is wrong',
                ],
            ]);
        }
    }

    public function logout(Request $request){
        
        $this->performLogout($request);
        return redirect()->route('doctor.login');
    }

    //defining guard for admins
    protected function guard(){
        return Auth::guard('doctor');
    }


    public function sendSms($message,$number,$otp){
        
        // Account details
        $authkey = '299160AjdGSQyXfsx25da6ec52';
        $senderid = 'KIDRIC';
        $message = urlencode($message);
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://control.msg91.com/api/sendotp.php?otp=".$otp."&sender=".$senderid."&message=".$message."&mobile=".$number."&authkey=".$authkey,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } 

        return "true";
    }
    
    public function checkMobileNumber(Request $request){

        $check_mobile = Doctor::Where('mobile_number',$request->mobile_number)->first();

        if(count($check_mobile)>0 && $check_mobile->is_active == '1' && $check_mobile->is_delete == '0' && $check_mobile->is_approve != '2') {
            $otp = mt_rand(100000, 999999);
            $message = "<#> Please enter it and proceed! Your OTP is: ".$otp."\n";
            $this->sendSms($message,$request->mobile_number,$otp);
            sleep(5);
            $insert_creden = Doctor::Where('mobile_number',$request->mobile_number)->update(['otp'=>$otp]);

            $mobile_number = $request->mobile_number;
            return view('doctor.auth.otp',compact('mobile_number'));
            
        }else if(count($check_mobile)>0 && $check_mobile->is_approve == '2'){

            return 'disapprove';

        } elseif(count($check_mobile)>0 && $check_mobile->is_delete == '1'){

            return 'delete';

        } elseif(count($check_mobile)>0 && $check_mobile->is_active == '0'){
            
            return 'deactivate';

        } else {
            return 'false';
        }
    }

    public function resendWebOtp(Request $request){

        $otp = mt_rand(100000, 999999);
        $message = "<#> Please enter it and proceed! Your OTP is: ".$otp."\n";
        $this->sendSms($message,$request->mobile_number,$otp);
        sleep(5);
        $updateOtp = Doctor::Where('mobile_number',$request->mobile_number)->update(['otp'=>$otp]);
        return 'true';
    }
    
}
