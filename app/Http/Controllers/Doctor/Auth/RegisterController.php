<?php

namespace App\Http\Controllers\Doctor\Auth;

use App\Model\Doctor;
use App\Model\Vaccination;
use Illuminate\Http\Request;
use App\Model\DoctorWiseVaccination;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/doctor-panel';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /*public function __construct()
    {
        $this->middleware('guest');
    }*/

    //show register page
    public function showRegistrationForm(){
        return view('doctor.auth.registration');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    /*protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }*/

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(Request $request)
    {   

        // Get user record
        $doctor = Doctor::where('mobile_number', $request->get('mobile_number'))->first();
        
        // Check Condition Mobile No. Found or Not
        if($request->get('mobile_number') != $doctor->mobile_number || $request->get('otp') != $doctor->otp) {
            
            return redirect()->back()->with('messages', [
                  [
                      'type' => 'danger',
                      'title' => 'Doctor',
                      'message' => 'Entered OTP is wrong',
                  ],
            ]);
            
        } else {   

            $insert_creden = Doctor::Where('mobile_number',$request->get('mobile_number'))->update(['otp_verified'=>'1']);
            
            \Auth::guard('doctor')->login($doctor);

                //all admin vaccination
                $vaccination_list = Vaccination::where('is_active',1)->where('is_delete',0)->orderBy('taken_at','ASC')->get();

                //check vaccination exists. if not add it.
                foreach ($vaccination_list as $vk => $vv) {
                    
                    $add_doc_vac = new DoctorWiseVaccination;
                    $add_doc_vac->doctor_id = $doctor->id;
                    $add_doc_vac->vaccination_id = $vv->id;
                    $add_doc_vac->taken_at = $vv->taken_at;
                    $add_doc_vac->vaccination_type = $vv->vaccination_type;
                    if($vv->vaccination_type == '0'){
                        $add_doc_vac->is_active = '1';
                    }else{
                        $add_doc_vac->is_active = '0';
                    }
                    $add_doc_vac->save();
                }
                
            // Set Auth Details
            return redirect(route('doctor.dashboard'));    
        }
    }

    public function checkOTP(Request $request){
        
        // Get user record
        $doctor = Doctor::where('mobile_number', $request->get('mobile_number'))->first();
        
        // Check Condition Mobile No. Found or Not
        if($request->get('mobile_number') != $doctor->mobile_number || $request->get('otp') != $doctor->otp) {
            return 'false';
        }else{

            return 'true';
        }
    }

    public function sendSms($message,$number,$otp){
        
        // Account details
        $authkey = '299160AjdGSQyXfsx25da6ec52';
        $senderid = 'KIDRIC';
        $message = urlencode($message);
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://control.msg91.com/api/sendotp.php?otp=".$otp."&sender=".$senderid."&message=".$message."&mobile=".$number."&authkey=".$authkey,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } 

        return "true";
    }

    public function registerMobileNumber(Request $request){

        $check_mobile = Doctor::Where('mobile_number',$request->mobile_number)->first();
        $get_doctor_id = Doctor::orderBy('id', 'DESC')->first();
        
        if(count($check_mobile)<= 0 && $check_mobile == ''){

            $otp = mt_rand(100000, 999999);
            
            $token = $otp;
            $message = "<#> Please enter it and proceed! Your OTP is: ".$otp."\n";
            $this->sendSms($message,$request->mobile_number,$token);

            $insert_creden = new Doctor;
            $insert_creden->mobile_number = $request->mobile_number;
            $insert_creden->doctor_name = $request->doctor_name;
            $insert_creden->otp = $otp;
            $insert_creden->is_complete = 0;
            if(is_null($get_doctor_id)){
                
                $insert_creden->doctor_id = 'DOC2729';
            }else{
                
                $explode_id = explode("C",$get_doctor_id->doctor_id);
                $new_id = $explode_id[1] + 1;
                $make_new_id = $explode_id[0].'C'.+$new_id;
                $insert_creden->doctor_id = $make_new_id;

            }
            $insert_creden->save();
          
            $mobile_number = $request->mobile_number;
            $doctor_name = $request->doctor_name;

            return view('doctor.auth.register_otp',compact('mobile_number','doctor_name'));
            
        } else {

            return 'false';
        }
    }
}
