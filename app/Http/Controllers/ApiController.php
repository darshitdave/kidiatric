<?php

namespace App\Http\Controllers;

use URL;
use Auth;
use File;
use Hash;
use Request;
use Response;
use DateTime;
use App\Model\XApi;
use App\Model\User;
use App\Model\Doctor;
use App\Model\City;
use App\Model\UserToken;
use App\Model\AppVersion;
use App\Model\Vaccination;
use App\Model\Notification;
use App\Model\ChildrenDetail;
use App\Model\SocialMediaLink;
use App\Model\UserWiseDoctor;
use App\Model\UserLinkParent;
use App\Model\ChildVaccination;
use App\Model\UserChildWiseDoctor;
use App\Model\UserWisePriscription;
use App\Model\DoctorWiseVaccination;
use App\Model\NotificationUser;
use App\Model\ChildAlreadyTakenVaccination;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;

class ApiController extends GlobalController
{
    public function __construct(){

        $headers = apache_request_headers();
        //check X-API
        if (isset($headers['x-api-key']) && $headers['x-api-key'] != '') {
            if (isset($headers['device-type']) && $headers['device-type'] != '') {
               if(!XApi::checkXAPI($headers['x-api-key'],$headers['device-type'])){
                   echo json_encode(array('status'=>500,'message'=>'Invalid X-API'));exit;
               }
            } else {
               echo json_encode(array('status'=>500,'message'=>'Device type not found!'));exit;
            }
        } else {
          echo json_encode(array('status'=>500,'message'=>'X-API key not found!'));exit;
        }

        //check version
        if (isset($headers['device-token']) && isset($headers['version'])) {
            $updateVersion = UserToken::where('device_token',$headers['device-token'])->update(['Version' => $headers['version']]);
        }

         if (isset($headers['device-token']) && isset($headers['fcm-token'])) {
            $updateVersion = UserToken::where('device_token',$headers['device-token'])->update(['fcm_token' => $headers['fcm-token']]);
        }

        if (isset($headers['device-type']) && isset($headers['version'])) {
            $getUserAppVersion = AppVersion::where('platform',$headers['device-type'])->where('version',$headers['version'])->first();
            if (!empty($getUserAppVersion->expireddate)) {
                if ($getUserAppVersion->expireddate < date('Y-m-d H:i:s')) {
                    //CHECK IF THE USER VERSION IS EXPIRED OR NOT
                    $res['status'] = "700";
                    $res['message'] = "App version expired!";
                    echo json_encode($res);
                    exit;
                }
            }
        }

    }

    public function Append($log_file, $value)
    {
        \File::append($log_file, $value . "\r\n");
    }

    public function LogInput()
    {
        $log_file = storage_path() . '/logs/api' . date('Ymd') . '.log';
        $headers = apache_request_headers();

        $this->Append($log_file,'----------------' . debug_backtrace()[1]['function'] . ' --------------');
        $this->Append($log_file,'Request Info : ');
        $this->Append($log_file,'Date: ' . date('Y-m-d H:i:s') . '    IP: ' .  Request::ip());
        $this->Append($log_file,'User-Agent: ' .  Request::header('User-Agent'));
        $this->Append($log_file,'URL: ' .  Request::url());
        $this->Append($log_file,'Input Parameters: ' .  json_encode(Input::all()));
        $this->Append($log_file,'Headers Parameters: ' .  json_encode($headers));
        $this->Append($log_file,'-----------');
        return;
    }

    public function LogOutput($output)
    {
        $log_file = storage_path() . '/logs/api' . date('Ymd') . '.log';
        $this->Append($log_file, 'Output: ');
        $this->Append($log_file,$output);
        $this->Append($log_file,'--------------------END------------------------');
        $this->Append($log_file,'');
        return;
    }

    public function nulltoblank($data) {

        return !$data ? $data = "" : $data;
    }

    public function checkUser($id){
        $check_user = User::where('id',$id)->where('is_otp_verified','1')->where('is_delete','0')->first();
        return $check_user;
    }

    //get uuid
    public function getUUID($id){
        $childDetails = ChildrenDetail::where('id',$id)->first();
        return $childDetails;
    }

    //GENERATE DEVICE TOKEN
    public function generateDeviceToken(){

        $this->LogInput();
        $errors_array = array();
        $headers = apache_request_headers();

        if (!isset($headers['device-type']) && $headers['device-type'])
            $errors_array['device-type'] = 'Please enter device type!';

        if(count($errors_array) == 0){
            $user = new User;
            $token = $user->generateToken($headers);
            $response['device_token'] = $token;

            $this->LogOutput(Response::json(array('status'=>200,'data' => $response)));
            return Response::json(array('status'=>200,'data' => $response),200);

        } else {
            $this->LogOutput(Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array)));
            return Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array),500);
        }
    }

    public function checkAppVersion(){

        $headers = apache_request_headers();
        if (isset($headers['device-type']) && $headers['device-type'] && isset($headers['version']) && $headers['version']) {
            $getUserAppVersion = AppVersion::where('platform',$headers['device-type'])->orderBy('id','DESC')->first();

            $a = str_replace(".", "",$getUserAppVersion['version']);
            $b = str_replace(".", "",$headers['version']);
            
            if($a > $b){
                //CHECK IF THE NEW VERSION LAUNCHED
                $response['status'] = 600;
                $response['message'] = "New app version launched!";
            }elseif($a == $b){
                $response['status'] = 200;
                $response['message'] = "User application is up to date!";
            } else {
                $response['status'] = 700;
                $response['message'] = "App version expired!";
            }
            echo json_encode($response);
            exit();
        }else{
            $response['status'] = 500;
            $response['message'] = "Provide all parameters!";
            echo json_encode($response);
            exit();
        }
    }

    public function userRegister(){

    	$this->LogInput();
        $errors_array = array();
        $headers = apache_request_headers();

        if (!Input::has('mobile_number') || Input::get('mobile_number') == "")
            $errors_array['mobile_number'] = 'Please send mobile number!';

        if (!Input::has('name') || Input::get('name') == "")
            $errors_array['name'] = 'Please enter name!';

        if(count($errors_array) == 0){
        	$userData = array();
        	$data = Input::all();

                //check otp verifier
                $otp_check = User::where('mobile_number',$data['mobile_number'])->where('is_otp_verified','0')->first();

            	//check link parent exists
            	$link_check = User::where('mobile_number',$data['mobile_number'])->where('is_delete','1')->first();
                //check user exists
                $user_check = User::where('mobile_number',$data['mobile_number'])->where('is_delete','0')->first();
            if($otp_check == '' && count($otp_check) == 0){

            	if($user_check == '' && count($user_check) == 0){

                    if($link_check == '' && count($link_check) == 0){
                		$otp = mt_rand(100000,999999);

                        $add_user = new User;
                        $add_user->name = $data['name'];
                        $add_user->mobile_number = $data['mobile_number'];
                        $add_user->type = '0';
                        $add_user->otp = $otp;
                        $add_user->is_otp_verified = '0';
                        $add_user->save();

                        if($add_user){

                                $token = $otp;

                                /*$message = "<#> Your OTP is: ".$otp."\n".$data['msg_key'];*/
                                $message = "<#> Please enter it and proceed! Your OTP is: ".$otp."\n".$data['msg_key'];
                                $this->sendSms($message,$data['mobile_number'],$token);

        	                	//update otp verification
        	                	$update_user = User::findOrFail($add_user->id);
        	                	$update_user->parent_id = '0';
        	                   	$update_user->save();

                        	$userData['user_id'] = $this->nulltoblank($add_user->id);
                            $userData['parent_name'] = $this->nulltoblank($add_user->name);
                            $userData['mobile_number'] = $this->nulltoblank($add_user->mobile_number);
                            $userData['is_otp_verified'] = false;

        					$this->LogOutput(Response::json(array('status'=>200,'data' => $userData)));
        					return Response::json(array('status'=>200,'data' => $userData),200);
                        }else{

        					$this->LogOutput(Response::json(array('status'=>500,'message' => 'Something went wrong!')));
                        	return Response::json(array('status'=>500,'message' => 'Something went wrong!'),500);
                        }
                    }else{

                        $this->LogOutput(Response::json(array('status'=>500,'message' => 'User is already registered!')));
                        return Response::json(array('status'=>500,'message' => 'User is already registered!'),500);
                    }
            	}else{

            		$this->LogOutput(Response::json(array('status'=>500,'message' => 'User is already registered!')));
                    return Response::json(array('status'=>500,'message' => 'User is already registered!'),500);
            	}
            }else{

                $this->LogOutput(Response::json(array('status'=>500,'message' => 'OTP not verified yet!')));
                return Response::json(array('status'=>500,'message' => 'OTP not verified yet!'),500);
            }

        }else{

        	$this->LogOutput(Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array)));
            return Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array),500);
        }

    }

    public function userRegisterValidateOtp(){

    	$this->LogInput();
        $errors_array = array();
        $headers = apache_request_headers();

        if (!Input::has('mobile_number') || Input::get('mobile_number') == "")
            $errors_array['mobile_number'] = 'Please enter mobile number!';

        if (!Input::has('otp') || Input::get('otp') == "")
            $errors_array['otp'] = 'Please enter otp';

        if (!isset($headers['user-id']) || $headers['user-id'] == "")
            $errors_array['user-id'] = 'Please enter user id!';

        if(count($errors_array) == 0){
        	$data = Input::all();

        	$userData = array();

        	$get_user = User::where('id',$headers['user-id'])->where('otp',$data['otp'])->first();

        	if(count($get_user) > 0 && $get_user != ''){

        		if($get_user->otp == $data['otp']){

        			$userData['user_id'] = $this->nulltoblank($get_user->id);
                    $userData['parent_name'] = $this->nulltoblank($get_user->name);
                    $userData['mobile_number'] = $this->nulltoblank($data['mobile_number']);

                    $check_child = ChildrenDetail::where('user_id',$get_user->id)->get();
                    if(count($check_child) == 0){
                        $userData['is_child_added'] = false;
                    }else{
                        $userData['is_child_added'] = true;
                    }

                    $check_doctor = UserWiseDoctor::where('user_id',$get_user->id)->get();
                    if(count($check_doctor) == 0){
                        $userData['is_doctor_added'] = false;
                    }else{
                        $userData['is_doctor_added'] = true;
                    }

                    $check_doctor = UserLinkParent::where('user_id',$get_user->id)->get();
                    if(count($check_doctor) == 0){
                        $userData['is_parent_link'] = false;
                    }else{
                        $userData['is_parent_link'] = true;
                    }

                    if($get_user->relation == ''){
                        $userData['is_relation'] = false;
                    } else {
                        $userData['is_relation'] = true;
                    }

                    $userData['is_otp_verified'] = true;

                    $type = (int)$get_user->type;
                    $userData['type'] = $type;
                    $app_token = $this->updateUserAppToken($headers,$get_user->id);
                    $userData['user_token'] = $app_token;

                    	//update otp verification
                    	$update_user = User::findOrFail($get_user->id);
                        $update_user->mobile_number = $data['mobile_number'];
                    	$update_user->is_otp_verified = '1';
                       	$update_user->save();

                        //update fcm token
                        if(isset($headers['fcm-token']) && !is_null($headers['fcm-token'])){
                            $updateToken = UserToken::where('device_token',$headers['device-token'])
                                        ->update(['fcm_token' => $headers['fcm-token']]);
                        }

                    $this->LogOutput(Response::json(array('status'=>200,'message' => 'Registration successful!','data' => $userData)));
                    return Response::json(array('status'=>200,'message' => 'Registration successful!','data' => $userData),200);

        		}else{

        			$this->LogOutput(Response::json(array('status'=>500,'message' => 'Invalid OTP!')));
                	return Response::json(array('status'=>500,'message' => 'Invalid OTP!'),500);
        		}
        	}else{

        		$this->LogOutput(Response::json(array('status'=>500,'message' => 'Invalid OTP!')));
                return Response::json(array('status'=>500,'message' => 'Invalid OTP!'),500);
        	}
        }else{

        	$this->LogOutput(Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array)));
            return Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array),500);
        }

    }

    public function doctorId(){

        $this->LogInput();
        $errors_array = array();
        $headers = apache_request_headers();

        if (!isset($headers['user-id']) || $headers['user-id'] == "")
            $errors_array['user-id'] = 'Please enter user id!';

        if (!Input::has('doctor_id') || Input::get('doctor_id') == "")
            $errors_array['doctor_id'] = 'Please enter doctor id!';

        if(count($errors_array) == 0){
            $child_data = array();
            $data = Input::all();

            $user = User::where('id',$headers['user-id'])->where('is_otp_verified','1')->first();

            if(count($user) > 0){

                $check_doctor = UserWiseDoctor::where('user_id',$headers['user-id'])->where('doctor_id',$data['doctor_id'])->first();

                if(count($check_doctor) == 0){

                    $doctor_name = Doctor::where('doctor_id',$data['doctor_id'])->where('is_delete','0')->first();

                    if(count($doctor_name) > 0){
                        $new_doctor = new UserWiseDoctor;
                        $new_doctor->user_id = $headers['user-id'];
                        $new_doctor->doctor_id = $doctor_name->id;
                        $new_doctor->save();

                        if($new_doctor){

                                $child_data['id'] = $this->nulltoblank($user->id);
                                $child_data['parent_name'] = $this->nulltoblank($user->name);
                                $child_data['mobile_number'] = $this->nulltoblank($user->mobile_number);
                                $child_data['doctor_id'] = $this->nulltoblank($data['doctor_id']);

                                //for child screen data
                                $child_data['doctor_name'] = $this->nulltoblank($doctor_name->doctor_name);
                                $child_data['doctor_email'] = $this->nulltoblank($doctor_name->email);
                                $child_data['doctor_mobile_number'] = $this->nulltoblank($doctor_name->mobile_number);
                                $child_data['is_otp_verified'] = 'true';

                                $this->LogOutput(Response::json(array('status'=>200,'message' => 'Doctor is successfully linked!','data' => $child_data)));
                                return Response::json(array('status'=>200,'message' => 'Doctor is successfully linked!','data' => $child_data),200);

                            }else{

                                $this->LogOutput(Response::json(array('status'=>500,'message' => 'Something went wrong!')));
                                return Response::json(array('status'=>500,'message' => 'Something went wrong!'),500);
                            }
                    }else{

                            $this->LogOutput(Response::json(array('status'=>500,'message' => 'Doctor not found!')));
                            return Response::json(array('status'=>500,'message' => 'Doctor not found!'),500);
                    }
                }else{

                    $this->LogOutput(Response::json(array('status'=>500,'message' => 'Doctor is already linked!')));
                    return Response::json(array('status'=>500,'message' => 'Doctor is already linked!'),500);
                }

            }else{

                $this->LogOutput(Response::json(array('status'=>401,'message' => 'User not found!')));
                return Response::json(array('status'=>401,'message' => 'User not found!'),401);
            }
        }else{

            $this->LogOutput(Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array)));
            return Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array),500);
        }

    }

    public function userLogin(){

    	$this->LogInput();
        $errors_array = array();
        $headers = apache_request_headers();

		if (!Input::has('mobile_number') || Input::get('mobile_number') == "")
            $errors_array['mobile_number'] = 'Please enter mobile number!';

        if(count($errors_array) == 0){
            $data = Input::all();

            if($data['mobile_number'] == '1234512345'){
                $otp = 123456;
            } else {
                $otp = mt_rand(100000,999999);
            }
            //check member
            $user = User::where('mobile_number',$data['mobile_number'])->where('is_delete','0')->first();

            if($user != '' && count($user)>0){
            	$parent_data = array();

            	$update_user = User::findOrFail($user->id);
            	$update_user->otp = $otp;
                $update_user->is_otp_verified = '0';
                $update_user->save();

                if($update_user){

                    $token = $otp;

                	/*$message =    "<#> Please enter OTP and proceed! Your OTP is: ".$otp."\n".$data['msg_key'];*/
                    $message = "<#> Please enter it and proceed! Your OTP is: ".$otp."\n".$data['msg_key'];
                    $this->sendSms($message,$user->mobile_number,$token);

                    $parent_data['user_id'] = $this->nulltoblank($user->id);
                    $parent_data['type'] = $this->nulltoblank($user->type);
                    $parent_data['parent_name'] = $this->nulltoblank($user->name);
                    $parent_data['mobile_number'] = $this->nulltoblank($user->mobile_number);
                    $parent_data['is_otp_verified'] = false;

                    $this->LogOutput(Response::json(array('status'=>200,'data' => $parent_data)));
                    return Response::json(array('status'=>200,'data' => $parent_data),200);
                }else{

                	$this->LogOutput(Response::json(array('status'=>500,'message' => 'Something went wrong!')));
                	return Response::json(array('status'=>500,'message' => 'Something went wrong!'),500);
                }
            }else{

				$this->LogOutput(Response::json(array('status'=>401,'message' => 'User not found, please signup!')));
                return Response::json(array('status'=>401,'message' => 'User not found, please signup!'),401);
            }

        } else {

        	$this->LogOutput(Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array)));
            return Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array),500);
        }

    }

    public function UserLoginValidateOtp(){

    	$this->LogInput();
        $errors_array = array();
        $headers = apache_request_headers();


        if (!Input::has('mobile_number') || Input::get('mobile_number') == "")
            $errors_array['mobile_number'] = 'Please enter mobile number!';

        if (!Input::has('otp') || Input::get('otp') == "")
            $errors_array['otp'] = 'Please enter OTP!';

        if(count($errors_array) == 0){
            $userData = array();

        	$data = Input::all();
        	$user_validate = User::where('mobile_number',$data['mobile_number'])->where('is_delete','0')->first();

        	if(count($user_validate) > 0 && $user_validate != ''){

        		if($user_validate->otp == $data['otp']){

        			$userData['user_id'] = $this->nulltoblank($user_validate->id);
                    $userData['type'] = $this->nulltoblank($user_validate->type);
                    $userData['parent_name'] = $this->nulltoblank($user_validate->name);
                    $userData['mobile_number'] = $this->nulltoblank($user_validate->mobile_number);

                    $check_child = ChildrenDetail::where('user_id',$user_validate->id)->get();
                    if(count($check_child) == 0){
                        $userData['is_child_added'] = false;
                    }else{
                        $userData['is_child_added'] = true;
                    }

                    $check_doctor = UserWiseDoctor::where('user_id',$user_validate->id)->get();
                    if(count($check_doctor) == 0){
                        $userData['is_doctor_added'] = false;
                    }else{
                        $userData['is_doctor_added'] = true;
                    }

                    $check_doctor = UserLinkParent::where('link_parent_id',$user_validate->id)->get();
                    if(count($check_doctor) == 0){
                        $userData['is_parent_link'] = false;
                    }else{
                        $userData['is_parent_link'] = true;
                    }

                    $userData['is_otp_verified'] = true;

                    if($user_validate->relation == ''){

                        $userData['is_relation'] = false;

                    } else {

                        $userData['is_relation'] = true;
                    }

                    $type = (int)$user_validate->type;
                    $userData['type'] = $type;

                        $user_doctor = UserWiseDoctor::with(['doctordetail'])->where('user_id',$user_validate->id)->first();
                    if(isset($user_doctor->doctor_id) && $user_doctor->doctor_id != ''){
                        $userData['doctor_id'] = $user_doctor['doctordetail']['doctor_id'];
                    }else{
                        $userData['doctor_id'] = "";
                    }
                    $app_token = $this->updateUserAppToken($headers,$user_validate->id);
                    $userData['user_token'] = $app_token;

                    if($userData){

                        $update_member = User::findOrFail($user_validate->id);
                        $update_member->is_otp_verified = '1';
                        $update_member->save();
                    }

                    //update fcm token
                    if(isset($headers['fcm-token']) && !is_null($headers['fcm-token'])){
                        $updateToken = UserToken::where('device_token',$headers['device-token'])
                                    ->update(['fcm_token' => $headers['fcm-token']]);
                    }

                    $this->LogOutput(Response::json(array('status'=>200,'message' => 'Login successful!','data' => $userData)));
                    return Response::json(array('status'=>200,'message' => 'Login successful!','data' => $userData),200);

        		}else{

        			$this->LogOutput(Response::json(array('status'=>500,'message' => 'Invalid OTP!')));
                	return Response::json(array('status'=>500,'message' => 'Invalid OTP!'),500);
        		}
        	}else{

				$this->LogOutput(Response::json(array('status'=>500,'message' => 'Invalid mobile number!')));
                return Response::json(array('status'=>500,'message' => 'Invalid mobile number!'),500);
        	}

        }else{

            $this->LogOutput(Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array)));
            return Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array),500);
        }
    }


    //update mobile number
    public function resetMobileNumber(){

        $this->LogInput();
        $errors_array = array();
        $headers = apache_request_headers();

        if (!isset($headers['user-id']) || $headers['user-id'] == "")
            $errors_array['user-id'] = 'Please enter user id!';

        if (!Input::has('mobile_number') || Input::get('mobile_number') == "")
            $errors_array['mobile_number'] = 'Please enter mobile number!';

        if(count($errors_array) == 0){

            $data = Input::all();

            $user = User::where('mobile_number',$data['mobile_number'])->first();

            if(!is_null($user) && count($user) > 0){

                $this->LogOutput(Response::json(array('status'=>500,'message' => 'User is already registered!')));
                return Response::json(array('status'=>500,'message' => 'User is already registered!'),500);

            } else {

                $token = mt_rand(100000,999999);

                $reset_mobile = User::where('id',$headers['user-id'])
                                ->update(['otp' => $token]);

                if($reset_mobile){

                    $otp = $token;
                    /*$message =    "<#> Please enter OTP and proceed! Your OTP is: ".$otp."\n".$data['msg_key'];*/
                    $message = "<#> Please enter it and proceed! Your OTP is: ".$otp."\n".$data['msg_key'];
                    $this->sendSms($message,$data['mobile_number'],$token);

                    $this->LogOutput(Response::json(array('status'=>200,'message' => 'OTP is sent successfully!')));
                    return Response::json(array('status'=>200,'message' => 'OTP is sent successfully!'));
                } else {

                    $this->LogOutput(Response::json(array('status'=>500,'message' => 'User not found!')));
                    return Response::json(array('status'=>500,'message' => 'User not found!'),500);
                }
            }

        } else {
            $this->LogOutput(Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array)));
            return Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array),500);
        }
    }


    //reset mobile number otp verification
    public function resetMobileNumberOTP(){

        $this->LogInput();
        $errors_array = array();
        $headers = apache_request_headers();

        if (!isset($headers['user-id']) || $headers['user-id'] == "")
            $errors_array['user-id'] = 'Please enter user id!';

        if (!Input::has('mobile_number') || Input::get('mobile_number') == "")
            $errors_array['mobile_number'] = 'Please enter mobile number!';

        if (!Input::has('otp') || Input::get('otp') == "")
            $errors_array['otp'] = 'Please enter OTP!';

        if(count($errors_array) == 0){
            $data = Input::all();

            $userData = array();

            //check member
            $user = User::where('id',$headers['user-id'])->where('otp',$data['otp'])->first();

            if($user != '' && count($user) > 0){

                $otp_verifierd = User::where('id',$headers['user-id'])
                                    ->update(['mobile_number' => $data['mobile_number'],'is_otp_verified'=> '1']);

                if($otp_verifierd){

                    $userData['user_id'] = $this->nulltoblank($user->id);
                    $userData['first_name'] = $this->nulltoblank($user->first_name);
                    $userData['last_name'] = $this->nulltoblank($user->last_name);
                    $userData['card_number'] = $this->nulltoblank($user->card_number);
                    $userData['mobile_number'] = $this->nulltoblank($data['mobile_number']);
                    $userData['otp_verified'] = $this->nulltoblank($user->is_otp_verified == 1 ? 'true' : 'false');
                    $app_token = $this->updateUserAppToken($headers,$user->id);
                    $userData['user_token'] = $app_token;


                    $this->LogOutput(Response::json(array('status'=>200,'data' => $userData,'message' => 'OTP verified successfully!')));
                    return Response::json(array('status'=>200,'data' => $userData,'message' => 'OTP verified successfully!'),200);
                }
            }else{

                $this->LogOutput(Response::json(array('status'=>500,'message' => 'Invalid mobile number or OTP!')));
                return Response::json(array('status'=>500,'message' => 'Invalid mobile number or OTP!'),500);
            }

        } else {

            $this->LogOutput(Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array)));
            return Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array),500);

        }

    }

    //resend otp
    public function resendOtp(){

        $this->LogInput();
        $errors_array = array();
        $headers = apache_request_headers();

        if (!isset($headers['user-id']) || $headers['user-id'] == "")
            $errors_array['user-id'] = 'Please enter user id!';

        if (!Input::has('mobile_number') || Input::get('mobile_number') == "")
            $errors_array['mobile_number'] = 'Please enter mobile number!';

        if(count($errors_array) == 0){

            $data = Input::all();


            $token = mt_rand(100000,999999);

            $user = User::findOrFail($headers['user-id']);
            $user->otp = $token;
            $user->save();

            $otp = $token;
            /*$message =    "<#> Please enter OTP and proceed! Your OTP is: ".$otp."\n".$data['msg_key'];*/
            $message = "<#> Please enter it and proceed! Your OTP is: ".$otp."\n".$data['msg_key'];
            $this->sendSms($message,$data['mobile_number'],$token);

            if($user){

                $this->LogOutput(Response::json(array('status'=>200,'message' => 'OTP sent successfully!')));
                return Response::json(array('status'=>200,'message' => 'OTP sent successfully!'),200);

            } else {

                $this->LogOutput(Response::json(array('status'=>500,'message' => 'Something went wrong!')));
                return Response::json(array('status'=>500,'message' => 'Something went wrong!'),500);
            }

        } else {

            $this->LogOutput(Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array)));
            return Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array),500);
        }
    }


    public function updateParentDetails(){

    	$this->LogInput();
        $errors_array = array();
        $headers = apache_request_headers();

        if (!isset($headers['user-id']) || $headers['user-id'] == "")
            $errors_array['user-id'] = 'Please enter user id!';

        if (!Input::has('relation') || Input::get('relation') == "")
            $errors_array['relation'] = 'Please enter relation!';

        if (!Input::has('name') || Input::get('name') == "")
            $errors_array['name'] = 'Please enter name!';

        if(count($errors_array) == 0){

        	$data = Input::all();

        	$getUser = User::where('id',$headers['user-id'])->where('is_otp_verified','1')->where('is_delete','0')->first();

        	if(!is_null($getUser)){

        		$userData = User::findOrFail($headers['user-id']);

                if(isset($data['email']) && $data['email'] != ''){
                    if($getUser->email != $data['email']){

                        if($data['email'] == null){
                            $userData->email = '';
                        }else{
                            $userData->email = $data['email'];
                        }

                    }else{

                        $this->LogOutput(Response::json(array('status'=>500,'message' => 'Email ID already exists!')));
                        return Response::json(array('status'=>500,'message' => 'Email ID already exists!'),500);
                    }
                }

                if(isset($data['name']) && $data['name'] != ''){
                    if($data['name'] == null){

                    }else{

                        $userData->name = $data['name'];
                    }
                }

                //0 for mother,1 = father,2 = guardian
                if(isset($data['relation']) && $data['relation'] != ''){
                    if($data['relation'] == null){
                        $userData->relation = '';
                    }else{

                        $relation = (int)$data['relation'];
                        $userData->relation = $relation;
                    }
                }


                if(isset($data['location']) && $data['location'] != ''){
                    if($data['location'] == null){

                    }else{

                        $check_city = City::where('city_name',$data['location'])->first();
                        if($check_city == '' && is_null($check_city) && count($check_city) <= 0){

                            $add_city = new City;
                            $add_city->city_name = $data['location'];
                            $add_city->save();

                            $userData->city_id = $add_city->id;
                        }else{

                            $userData->city_id = $check_city->id;
                        }
                    }
                }

                $userData->save();

                if($userData){

                	$this->LogOutput(Response::json(array('status'=>200,'messages' => 'Details updated successfully!')));
                	return Response::json(array('status'=>200,'messages' => 'Details updated successfully!'),200);
                }

        	}else{

        		$this->LogOutput(Response::json(array('status'=>401,'message' => 'User not found!')));
                return Response::json(array('status'=>401,'message' => 'User not found!'),401);
        	}
        }else{

        	$this->LogOutput(Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array)));
            return Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array),500);
        }
    }

    public function ListLinkParentDetail(){

        $this->LogInput();
        $errors_array = array();
        $headers = apache_request_headers();

        if (!isset($headers['user-id']) || $headers['user-id'] == "")
            $errors_array['user-id'] = 'Please enter user id!';

        if(count($errors_array) == 0){
            $data = Input::all();
            $link_parent_list = array();

            $get_link_parent = UserLinkParent::where('user_id',$headers['user-id'])->with(['user'])->where('is_delete','0')->get();

            if(count($get_link_parent) == 0){

                $this->LogOutput(Response::json(array('status'=>500,'message' => 'No data found!')));
                return Response::json(array('status'=>500,'message' => 'No data found!'),500);
            }else{

                $link_parent = array();
                foreach ($get_link_parent as $gk => $gv) {
                    $get_all = array();
                    $get_all['user_id'] = $gv->link_parent_id;
                    $get_all['parent_id'] = (int)$gv->parent_id;
                    $get_all['type'] = 1;
                    $get_all['name'] = $gv->user->name;
                    $get_all['mobile_number'] = $gv->user->mobile_number;
                    $link_parent[] = $get_all;
                }
                $link_parent_list[] = $link_parent;

                if($link_parent){

                    $this->LogOutput(Response::json(array('status'=>200,'data' => $link_parent)));
                    return Response::json(array('status'=>200,'data' => $link_parent),200);

                }else{

                    $this->LogOutput(Response::json(array('status'=>500,'message' => 'No data found!')));
                    return Response::json(array('status'=>500,'message' => 'No data found!'),500);
                }
            }

        } else {

            $this->LogOutput(Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array)));
            return Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array),500);
        }

    }

    public function addLinkParentDetail(){

        $this->LogInput();
        $errors_array = array();
        $headers = apache_request_headers();

        if (!isset($headers['user-id']) || $headers['user-id'] == "")
            $errors_array['user-id'] = 'Please enter user id!';

        if (!Input::has('parent_name') || Input::get('parent_name') == "")
            $errors_array['parent_name'] = 'Please enter parent name!';

        if (!Input::has('mobile_number') || Input::get('mobile_number') == "")
            $errors_array['mobile_number'] = 'Please enter mobile number!';

        if(count($errors_array) == 0){
            $data = Input::all();

            $check_user = User::where('id',$headers['user-id'])->where('is_otp_verified','1')->first();

            if(count($check_user) > 0){

                $check_child = ChildrenDetail::where('user_id',$headers['user-id'])->first();

                if(count($check_child) > 0){


                    $getmobile_num = User::where('mobile_number',$data['mobile_number'])->first();

                    if(count($getmobile_num) == 0){

                        $userData = new User;
                        $userData->name = $data['parent_name'];
                        $userData->mobile_number = $data['mobile_number'];
                        $userData->is_otp_verified = '0';
                        $userData->parent_id = $headers['user-id'];
                        $userData->type = '1';
                        $userData->save();

                        if($userData){

                            $user_link = new UserLinkParent;
                            $user_link->user_id = $headers['user-id'];
                            $user_link->link_parent_id = $userData->id;
                            $user_link->save();

                            $parent_data['link_parent_id'] = $this->nulltoblank($userData->id);
                            $parent_data['user_id'] = $this->nulltoblank($headers['user-id']);
                            $parent_data['type'] = '1';
                            $parent_data['parent_name'] = $data['parent_name'];
                            $parent_data['mobile_number'] = $data['mobile_number'];
                            $parent_data['is_otp_verified'] = 'false';

                            $message = 'Greetings '.$data['parent_name'].', '.$check_user->name.' has give you the access of his/her child, please check the application for more details!';
                            $mobileNumber = $data['mobile_number'];
                            $this->sendInvitationSms($mobileNumber,$message);

                            $this->LogOutput(Response::json(array('status'=>200,'message' => 'Linked successfully!','data' => $parent_data)));
                            return Response::json(array('status'=>200,'message' => 'Linked successfully!','data' => $parent_data),200);

                        } else {

                            $this->LogOutput(Response::json(array('status'=>500,'message' => 'Something went wrong!')));
                            return Response::json(array('status'=>500,'message' => 'Something went wrong!'),500);
                        }

                    } else {

                        $get_link = UserLinkParent::where('user_id',$headers['user-id'])->where('link_parent_id',$getmobile_num->id)->where('is_delete','1')->first();


                        if(count($get_link) == 0){

                            $user_link = new UserLinkParent;
                            $user_link->user_id = $headers['user-id'];
                            $user_link->link_parent_id = $getmobile_num->id;
                            $user_link->save();

                            $parent_data['link_parent_id'] = $this->nulltoblank($getmobile_num->id);
                            $parent_data['user_id'] = $headers['user-id'];
                            $parent_data['type'] = '1';
                            $parent_data['parent_name'] = $this->nulltoblank($getmobile_num->name);
                            $parent_data['mobile_number'] = $this->nulltoblank($getmobile_num->mobile_number);
                            $parent_data['type'] = $this->nulltoblank($getmobile_num->type);
                            $parent_data['is_otp_verified'] = $getmobile_num->is_otp_verified ? 'true' : 'false';

                            $message = 'Greetings '.$getmobile_num->name.', '.$check_user->name.' has give you the access of his/her child, please check the application for more details!';
                            $mobileNumber = $data['mobile_number'];
                            $this->sendInvitationSms($mobileNumber,$message);

                            $this->LogOutput(Response::json(array('status'=>200,'message' => 'Linked successfully!','data' => $parent_data)));
                            return Response::json(array('status'=>200,'message' => 'Linked successfully!','data' => $parent_data),200);

                        } else {

                            $this->LogOutput(Response::json(array('status'=>500,'message' => 'Already linked!')));
                            return Response::json(array('status'=>500,'message' => 'Already linked!'),500);

                        }

                        $this->LogOutput(Response::json(array('status'=>200,'message' => 'Linked successfully!','data' => $parent_data)));
                        return Response::json(array('status'=>200,'message' => 'Linked successfully!','data' => $parent_data),200);
                    }
                }else{

                    $this->LogOutput(Response::json(array('status'=>500,'message' => 'Please add at least one child!')));
                    return Response::json(array('status'=>500,'message' => 'Please add at least one child!'),500);
                }
            } else {

                $this->LogOutput(Response::json(array('status'=>500,'message' => 'Unauthorised request!')));
                return Response::json(array('status'=>500,'message' => 'Unauthorised request!'),500);
            }

        }else{

            $this->LogOutput(Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array)));
            return Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array),500);
        }

    }

    public function deleteLinkParent(){

        $this->LogInput();
        $errors_array = array();
        $headers = apache_request_headers();

        if (!isset($headers['user-id']) || $headers['user-id'] == "")
            $errors_array['user-id'] = 'Please enter user id!';

        if (!Input::has('link_parent_id') || Input::get('link_parent_id') == "")
            $errors_array['link_parent_id'] = 'Please enter link parent id!';

        if(count($errors_array) == 0){

            $data = Input::all();

            $check_user = User::where('id',$headers['user-id'])->where('is_otp_verified','1')->first();

            if(count($check_user) > 0){

                $delete_link_parent = UserLinkParent::where('user_id',$headers['user-id'])->where('link_parent_id',$data['link_parent_id'])->delete();

                if($delete_link_parent){

                    $this->LogOutput(Response::json(array('status'=>200,'message' => 'Successfully removed!')));
                    return Response::json(array('status'=>200,'message' => 'Successfully removed!'),200);

                } else {

                    $this->LogOutput(Response::json(array('status'=>500,'message' => 'Something went wrong!')));
                    return Response::json(array('status'=>500,'message' => 'Something went wrong!'),500);
                }
            } else {

                $this->LogOutput(Response::json(array('status'=>401,'message' => 'Unauthorised user!')));
                return Response::json(array('status'=>401,'message' => 'Unauthorised user!'),401);
            }
        }else{

            $this->LogOutput(Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array)));
            return Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array),500);
        }

    }

   	public function addChildDetails(){

    	$this->LogInput();
        $errors_array = array();
        $headers = apache_request_headers();

        if (!isset($headers['user-id']) || $headers['user-id'] == "")
            $errors_array['user-id'] = 'Please enter user id!';

        if(count($errors_array) == 0){

        	$data = Input::all();
            $doctor_data = array();

        	$check_user = $this->checkUser($headers['user-id']);

        	if(isset($check_user) && count($check_user) > 0){

                if(isset($data['doctor_id']) && $data['doctor_id'] != ''){
                $doctor_id = strtoupper($data['doctor_id']);
                $doctor_check = Doctor::where('doctor_id',$doctor_id)->where('is_approve','1')->where('is_active','1')->where('is_delete','0')->first();

                }
                if(isset($data['doctor_id']) && is_null($doctor_check)){

                    $this->LogOutput(Response::json(array('status'=>500,'message' => 'Doctor not found!')));
                    return Response::json(array('status'=>500,'message' => 'Doctor not found!'),500);

                }else{

                    $uuid = (string) Str::uuid();
            		$childData = new ChildrenDetail;
                    $childData->uuid = $uuid;
            		$childData->user_id = $headers['user-id'];

            		if(isset($data['child_name']) && $data['child_name'] != ''){
                        if($data['child_name'] == null){
                            $childData->child_name = '';
                        }else{
                            $childData->child_name = $this->nulltoblank($data['child_name']);
                        }
                    }

                    if(isset($data['doctor_id']) && $data['doctor_id'] != ''){
                        if($data['doctor_id'] == null){

                        }else{
                            $check_doctor = UserWiseDoctor::where('user_id',$headers['user-id'])->where('doctor_id',$doctor_check->id)->first();
                            //insert user doctor
                            if(count($check_doctor) <= 0){
                                $doctor_id = new UserWiseDoctor;
                                $doctor_id->user_id = $headers['user-id'];
                                $doctor_id->doctor_id = $doctor_check->id;
                                $doctor_id->save();
                            }
                        }
                    }

                    if(isset($data['date_of_birth']) && $data['date_of_birth'] != ''){
                        if($data['date_of_birth'] == null){
                            $childData->date_of_birth = '';
                        }else{

                        	$data_date = date('Y-m-d',strtotime($data['date_of_birth']));
                            $childData->date_of_birth = $this->nulltoblank($data_date);
                            $currentDateTime = date('Y-m-d');
                            $datetime1 = new DateTime($data_date);
                            $datetime2 = new DateTime($currentDateTime);
                            $interval = $datetime1->diff($datetime2);
                            $days = $interval->format('%a');
                            $childData->age = $days;
                        }
                    }


                    if(isset($data['gender']) && $data['gender'] != ''){
                        if($data['gender'] == null){
                            $childData->gender = '';
                        }else{
                            $childData->gender = $this->nulltoblank($data['gender']);
                        }
                    }

                    if(isset($data['profile_image']) && $data['profile_image'] != ''){
                        if($data['profile_image'] == null){
                            $childData->profile_image = '';
                        }else{
                            $path = "child_image/".$uuid."/profile_image";
                            $fileName = $this->uploadImage($data['profile_image'],$path);
                            $childData->profile_image = $fileName;
                        }
                    }

                    $childData->save();

                    //insert all id in one table
                    $user_mst = new UserChildWiseDoctor;
                    $user_mst->user_id = $headers['user-id'];
                    if(isset($data['doctor_id']) && !is_null($doctor_check)){

                        $user_mst->doctor_id = $doctor_check->id;
                        $doctor_data['id'] = $doctor_id;
                        $doctor_data['doctor_name'] = $doctor_check->doctor_name;

                    }else{

                        $user_mst->doctor_id = '';
                    }

                    $user_mst->child_id = $childData->id;
                    $user_mst->save();

                    if($user_mst){

                    	$this->LogOutput(Response::json(array('status'=>200,'messages' => 'Details added successfully!','data' => $doctor_data)));
                    	return Response::json(array('status'=>200,'messages' => 'Details added successfully!','data' => $doctor_data),200);
                    }
                }
        	}else{

        		$this->LogOutput(Response::json(array('status'=>500,'message' => 'Unauthorised user!')));
                return Response::json(array('status'=>500,'message' => 'Unauthorised user!'),500);
        	}
        }else{

        	$this->LogOutput(Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array)));
            return Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array),500);
        }
    }


    public function childrenList(){

        $this->LogInput();
        $errors_array = array();
        $headers = apache_request_headers();

        if (!isset($headers['user-id']) || $headers['user-id'] == "")
            $errors_array['user-id'] = 'Please enter user id';

        if(count($errors_array) == 0){

            $child_data = array();
            $data = Input::all();
            $base_path = URL::to('https://kidiatric.s3.ap-south-1.amazonaws.com/uploads/child_image/');

            $check_user = $this->checkUser($headers['user-id']);

            if(isset($check_user) && count($check_user) > 0){

                $get_users = UserLinkParent::where('link_parent_id',$headers['user-id'])->where('is_delete','0')->pluck('user_id');

                $get_users[] = $headers['user-id'];

                $query = ChildrenDetail::query();
                if(count($get_users) == 0){
                    $query->where('user_id',$headers['user-id']);
                }else{
                    $query->whereIn('user_id',$get_users);
                }
                $query->where('is_delete','0');
                $query->orderBy('date_of_birth','DESC');
                $get_child_list = $query->get();

                if(count($get_child_list) > 0){

                    $child_details = array();
                    foreach ($get_child_list as $ck => $cv) {

                        $child_info = array();
                        $doctor = array();
                        $child_info['id'] = $this->nulltoblank($cv->id);
                        $child_info['name'] = $this->nulltoblank($cv->child_name);

                            //month calculation
                            $dob = date('Y-m-d',strtotime($cv->date_of_birth));
                            $currentDateTime = date('Y-m-d');
                            $diff = abs(strtotime($currentDateTime) - strtotime($dob));
                            $years = floor($diff / (365*60*60*24));
                            $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));

                            $child_dob = date('d-m-Y',strtotime($cv->date_of_birth));

                        $child_info['dob'] = $this->nulltoblank($child_dob);
                        $child_info['gender'] = $this->nulltoblank($cv->gender);

                        if($years != ''){

                            $child_info['age'] = $years.' Years '.$months.' Months';

                        }else{

                            $child_info['age'] = $months.' Months';

                        }
                        $child_info['profile_image'] = $cv->profile_image ? $base_path.$cv->uuid."/profile_image/".$cv->profile_image : "";

                            //doctor information
                            if(count($get_users) == 0){
                                $child_doctor = UserChildWiseDoctor::where('user_id',$headers['user-id'])->where('child_id',$cv->id)->first();
                            }else{

                                $child_doctor = UserChildWiseDoctor::whereIn('user_id',$get_users)->where('child_id',$cv->id)->first();
                            }

                            if(isset($child_doctor->doctor_id) && $child_doctor->doctor_id != ''){
                                $doctor_details = Doctor::where('id',$child_doctor->doctor_id)->first();
                                $doctor['doctor_id'] = $this->nulltoblank($doctor_details->doctor_id);

                                $doctor['doctor_name'] = $this->nulltoblank($doctor_details->doctor_name);
                                $doctor['mobile_number'] = $this->nulltoblank($doctor_details->mobile_number);
                                $doctor['email'] = $this->nulltoblank($doctor_details->email);

                            }else{

                                $doctor = array();
                            }

                        if($headers['user-id'] == $cv->user_id){
                            $child_info['is_delete'] = true;
                        } else {
                            $child_info['is_delete'] = false;
                        }
                        $child_info['doctor'] = $doctor;

                        $child_details[] = $child_info;
                    }

                    $child_data = $child_details;

                    $this->LogOutput(Response::json(array('status'=>200,'data' => $child_data)));
                    return Response::json(array('status'=>200,'data' => $child_data),200);
                }else{

                    $this->LogOutput(Response::json(array('status'=>500,'message' => 'Please add details!')));
                    return Response::json(array('status'=>500,'message' => 'Please add details!'),500);
                }
            }else{

                $this->LogOutput(Response::json(array('status'=>401,'message' => 'User not found!')));
                return Response::json(array('status'=>401,'message' => 'User not found!'),401);
            }
        }else{

            $this->LogOutput(Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array)));
            return Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array),500);
        }

    }

    public function editChildDetails(){

        $this->LogInput();
        $errors_array = array();
        $headers = apache_request_headers();

        if (!isset($headers['user-id']) || $headers['user-id'] == "")
            $errors_array['user-id'] = 'Please enter user id!';

        if (!Input::has('child_id') || Input::get('child_id') == "")
            $errors_array['child_id'] = 'Please enter child id!';

        if(count($errors_array) == 0){
            $data = Input::all();
            $doctor_data = array();
            $doctor_info = array();

            $check_user = $this->checkUser($headers['user-id']);

            if(isset($check_user) && count($check_user) > 0){

                $get_link_parent = array();
                $get_link_parent = UserLinkParent::where('link_parent_id',$headers['user-id'])->where('is_delete','0')->pluck('user_id');
                //get parent id
                $parent_id = ChildrenDetail::where('id',$data['child_id'])->first();
                $get_link_parent[] = $headers['user-id'];

                $check_child = ChildrenDetail::whereIn('user_id',$get_link_parent)->where('id',$data['child_id'])->first();

                if(isset($data['doctor_id']) && $data['doctor_id'] != ''){
                    $convert_doctor_id = strtoupper($data['doctor_id']);
                    $doctor_check = Doctor::where('doctor_id',$convert_doctor_id)->where('is_approve','1')->where('is_active','1')->where('is_delete','0')->first();

                }
                if(isset($data['doctor_id']) && is_null($doctor_check)){

                    $this->LogOutput(Response::json(array('status'=>500,'message' => 'Doctor not found!')));
                    return Response::json(array('status'=>500,'message' => 'Doctor not found!'),500);

                }else{

                    if(isset($check_child) && !is_null($check_child)){

                        $childData = ChildrenDetail::findOrFail($data['child_id']);

                        if(isset($data['child_name']) && $data['child_name'] != ''){
                            if($data['child_name'] == null){
                                $childData->child_name = '';
                            } else {
                                $childData->child_name = $this->nulltoblank($data['child_name']);
                            }
                        }

                        if(isset($data['doctor_id']) && !is_null($doctor_check)){
                            if($data['doctor_id'] != null){

                                $check_doctor = UserChildWiseDoctor::with(['doctordetail'])->where('user_id',$headers['user-id'])->where('doctor_id',$convert_doctor_id)->first();

                                //insert user doctor
                                if(count($check_doctor) == 0){
                                    $doctor_id = new UserChildWiseDoctor;
                                    $doctor_id->user_id = $parent_id->user_id;
                                    $doctor_id->doctor_id = $doctor_check->id;
                                    $doctor_id->child_id = $data['child_id'];
                                    $doctor_id->save();

                                    $doctor_info['doctor_id'] = $convert_doctor_id;
                                    $doctor_info['doctor_name'] = $doctor_check->doctor_name;
                                }else{
                                    $doctor_info['doctor_id'] = $convert_doctor_id;
                                    $doctor_info['doctor_name'] = $doctor_check->doctor_name;
                                }
                            }
                        }

                        if(isset($data['date_of_birth']) && $data['date_of_birth'] != ''){
                            if($data['date_of_birth'] == null){
                                $childData->date_of_birth = '';
                            } else {
                                $data_date = date('Y-m-d',strtotime($data['date_of_birth']));
                                $childData->date_of_birth = $this->nulltoblank($data_date);
                                $currentDateTime = date('Y-m-d');
                                $datetime1 = new DateTime($data_date);
                                $datetime2 = new DateTime($currentDateTime);
                                $interval = $datetime1->diff($datetime2);
                                $days = $interval->format('%a');
                                $childData->age = $days;
                            }
                        }

                        if(isset($data['gender']) && $data['gender'] != ''){
                            if($data['gender'] == null){
                                $childData->gender = '';
                            }else{
                                $childData->gender = $this->nulltoblank($data['gender']);
                            }
                        }

                        if(isset($data['profile_image']) && $data['profile_image'] != ''){
                            if($data['profile_image'] == null){
                                $childData->profile_image = '';
                            }else{
                                $path = "child_image/".$check_child->uuid."/profile_image";
                                $fileName = $this->uploadImage($data['profile_image'],$path);
                                $childData->profile_image = $fileName;
                            }
                        }

                        $childData->save();

                        $check_child_doctor = UserChildWiseDoctor::where('user_id',$headers['user-id'])->where('child_id',$data['child_id'])->first();

                        if(!is_null($check_child_doctor)){
                            if(isset($data['doctor_id'])){
                                $user_mst = UserChildWiseDoctor::where('user_id',$headers['user-id'])->where('child_id',$data['child_id'])->update(['doctor_id' => $doctor_check->id]);
                            } else {
                                $user_mst = UserChildWiseDoctor::where('user_id',$headers['user-id'])->where('child_id',$data['child_id'])->update(['doctor_id' => '']);
                            }
                        }

                        if(isset($data['doctor_id']) && !is_null($doctor_check)){

                            $this->LogOutput(Response::json(array('status'=>200,'message' => 'Details successfully updated!','data' => $doctor_info)));
                            return Response::json(array('status'=>200,'message' => 'Details successfully updated!','data' => $doctor_info),200);
                        }else{

                            $this->LogOutput(Response::json(array('status'=>200,'message' => 'Details successfully updated!')));
                            return Response::json(array('status'=>200,'message' => 'Details successfully updated!'),200);
                        }

                    } else {

                        $this->LogOutput(Response::json(array('status'=>500,'message' => 'Child not found!')));
                        return Response::json(array('status'=>500,'message' => 'Child not found!'),500);
                    }
                }

            } else {

                $this->LogOutput(Response::json(array('status'=>401,'message' => 'Unauthorised user!')));
                return Response::json(array('status'=>401,'message' => 'Unauthorised user!'),401);

            }
        } else {

            $this->LogOutput(Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array)));
            return Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array),500);

        }
    }

    //discuss
    public function deleteChild(){

        $this->LogInput();
        $errors_array = array();
        $headers = apache_request_headers();

        if (!isset($headers['user-id']) || $headers['user-id'] == "")
            $errors_array['user-id'] = 'Please enter user id!';

        if (!Input::has('child_id') || Input::get('child_id') == "")
            $errors_array['child_id'] = 'Please enter child id!';

        if(count($errors_array) == 0){

            $data = Input::all();

            $check_user = $this->checkUser($headers['user-id']);

            if(isset($check_user) && count($check_user) > 0){

                if($check_user->type == '1'){
                    $get_link_parent = UserLinkParent::where('link_parent_id',$headers['user-id'])
                                                     ->where('user_id',$check_user->parent_id)
                                                     ->where('is_delete','0')->first();
                }

                if(isset($check_user) && count($check_user) > 0){

                    $check_child = ChildrenDetail::where('user_id',$headers['user-id'])
                                                 ->where('id',$data['child_id'])
                                                 ->first();

                    if(isset($check_child) && !is_null($check_child)){

                        $check_child_doctor = ChildrenDetail::where('user_id',$headers['user-id'])
                                                            ->where('id',$data['child_id'])
                                                            ->delete();

                        if($check_child_doctor){

                            if(isset($get_link_parent) && count($get_link_parent) >= 1){
                                $child = UserChildWiseDoctor::where('user_id',$get_link_parent)->where('child_id',$data['child_id'])->delete();
                            } else{
                                $child = UserChildWiseDoctor::where('user_id',$headers['user-id'])->where('child_id',$data['child_id'])->delete();
                            }

                            $this->LogOutput(Response::json(array('status'=>200,'message' => 'Successfully removed!')));
                            return Response::json(array('status'=>200,'message' => 'Successfully removed!'),200);

                        } else {

                            $this->LogOutput(Response::json(array('status'=>500,'message' => 'Successfully removed!')));
                            return Response::json(array('status'=>500,'message' => 'Successfully removed!'),500);
                        }

                    } else {

                        $this->LogOutput(Response::json(array('status'=>500,'message' => 'Please add child details!')));
                        return Response::json(array('status'=>500,'message' => 'Please add child details!'),500);

                    }

                } else {

                    $this->LogOutput(Response::json(array('status'=>401,'message' => 'Unauthorised user!')));
                    return Response::json(array('status'=>401,'message' => 'Unauthorised user!'),401);
                }

            } else {

                $this->LogOutput(Response::json(array('status'=>401,'message' => 'Unauthorised user!')));
                return Response::json(array('status'=>401,'message' => 'Unauthorised user!'),401);
            }

        } else {

            $this->LogOutput(Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array)));
            return Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array),500);
        }
    }

    public function childWiseVaccination(){

        $this->LogInput();
        $errors_array = array();
        $headers = apache_request_headers();

        if (!isset($headers['user-id']) || $headers['user-id'] == "")
            $errors_array['user-id'] = 'Please enter user id!';

        if (!Input::has('child_id') || Input::get('child_id') == "")
            $errors_array['child_id'] = 'Please enter child id!';

        if (!Input::has('range') || Input::get('range') == "")
            $errors_array['range'] = 'Please enter range!';

        if(count($errors_array) == 0){

            $vaccination_data = array();
            $data = Input::all();
            $base_path = URL::to('https://kidiatric.s3.ap-south-1.amazonaws.com/uploads/child_image/');

            $check_user = $this->checkUser($headers['user-id']);

            if(isset($check_user) && count($check_user) > 0){
                $get_link_parent = array();
                $get_link_parent = UserLinkParent::where('link_parent_id',$headers['user-id'])->where('is_delete','0')->pluck('user_id');
                $get_link_parent[] = $headers['user-id'];

                    $get_child_details = ChildrenDetail::whereIn('user_id',$get_link_parent)->where('id',$data['child_id'])->where('is_delete','0')->first();

                    if(count($get_child_details) > 0){

                        //start range
                        $convert_days = explode("-",$data['range']);
                        $convert_days = explode(".",$convert_days[0]);
                        $first = 30 * $convert_days[0];
                        if(isset($convert_days[1])){
                            $second = 3*$convert_days[1];
                            $start_range = $first + $second;
                        } else {
                            $start_range = $first;
                        }

                        $convert_days = explode("-",$data['range']);
                        $number2 = $convert_days[1].'.0';
                        $convert_days = explode(".",$number2);
                        $end_range = 30*$convert_days[0] + 3*$convert_days[1];

                        //change date or not taken vaccinations
                        $child_vaccination = ChildVaccination::where('child_id',$data['child_id'])->whereIn('user_id',$get_link_parent)->where('good_to_have','0')->where('taken_vaccination','0')->pluck('vaccination_id');

                        //child taken vaccinations
                        $taken_vacc = ChildVaccination::where('child_id',$data['child_id'])->whereIn('user_id',$get_link_parent)->where('good_to_have','0')->where('taken_vaccination','1')->pluck('vaccination_id');

                        //get doctor
                        $get_doctor_id = UserChildWiseDoctor::where('child_id',$data['child_id'])->whereIn('user_id',$get_link_parent)->first();


                        if(isset($get_doctor_id->doctor_id) && $get_doctor_id->doctor_id != ''){

                            $query = DoctorWiseVaccination::query();
                            $query->with(['vaccination']);
                            $query->where('doctor_id',$get_doctor_id->doctor_id);
                            $query->whereNotIn('vaccination_id',$child_vaccination);
                            $query->whereNotIn('vaccination_id',$taken_vacc);
                        } else {
                            $query = Vaccination::query();
                            $query->whereNotIn('id',$child_vaccination);
                            $query->whereNotIn('id',$taken_vacc);
                            $query->where('is_delete','0');
                        }
                        $query->where('is_active','1');
                        $query->where('vaccination_type','0');
                        $query->where('taken_at','>=',$start_range);
                        if($end_range != 4320){
                            $query->where('taken_at','<',$end_range);
                        }
                        $vacctinations = $query->orderBy('taken_at','ASC')->get();

                        //form birth date to current date difference
                        $data_date = date('Y-m-d',strtotime($get_child_details->date_of_birth));
                        $currentDateTime = date('Y-m-d');
                        $datetime1 = new DateTime($data_date);
                        $datetime2 = new DateTime($currentDateTime);
                        $interval = $datetime1->diff($datetime2);
                        $days = $interval->format('%a');

                        $taken = array();
                        $taken_to_be = array();
                        $i = 0;

                        $child_details = array();
                        $child_details['child_id'] = $get_child_details->id;
                        $child_details['name'] = $get_child_details->child_name;
                        $child_details['gender'] = $get_child_details->gender;
                        if($get_child_details->user_id == $headers['user-id']){
                            $child_details['is_edit'] = true;
                        } else {
                            $child_details['is_edit'] = false;
                        }
                        $child_details['profile_image'] = $get_child_details->profile_image ? $base_path.$get_child_details->uuid."/profile_image/".$get_child_details->profile_image : "";

                            //month calculation
                            $dob = date('Y-m-d',strtotime($get_child_details->date_of_birth));
                            $currentDateTime = date('Y-m-d');
                            $diff = abs(strtotime($currentDateTime) - strtotime($dob));
                            $years = floor($diff / (365*60*60*24));
                            $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));

                            $child_dob = date('d-m-Y',strtotime($get_child_details->date_of_birth));

                        $child_details['dob'] = $child_dob;

                        if($years != ''){
                            $child_details['age'] = $years.' Years '.$months.' Months';
                        } else {
                            $child_details['age'] = $months.' Months';
                        }

                        if(isset($get_doctor_id->doctor_id) && $get_doctor_id->doctor_id != ''){

                            $doctor_details = array();
                            $doctor_info = Doctor::where('id',$get_doctor_id->doctor_id)->first();
                            $doctor_details['doctor_id'] = $doctor_info->doctor_id;
                            $doctor_details['doctor_name'] = $doctor_info->doctor_name;
                            $doctor_details['mobile_number'] = $doctor_info->mobile_number;
                            $doctor_details['email'] = $doctor_info->email;

                        } else {

                            $doctor_details[] = array();
                        }

                        $child_details['doctor'] = $doctor_details;

                        //not taken vaccination array
                        $child_vaccination = ChildVaccination::whereIn('user_id',$get_link_parent)->where('good_to_have','0')->where('taken_vaccination','0')->where('child_id',$get_child_details->id)->get();

                        $not_taken_vaccination = array();
                        if(count($child_vaccination) > 0){
                            foreach ($child_vaccination as $ck => $cv) {

                                $vaccinaton_info = array();
                                if($cv->taken_at <= $days){

                                    $vaccinaton_info['id'] = $cv->vaccination_id;
                                    $vaccinaton_info['name'] = $cv['vaccination']['vaccination_name'];
                                    $vaccinaton_info['taken_at'] = $cv->taken_at;

                                    $not_taken_vaccination[] = $vaccinaton_info;
                                }
                            }
                        }

                        $good_to_have_vaccination = ChildVaccination::whereIn('user_id',$get_link_parent)->where('good_to_have','1')->where('taken_vaccination','0')->where('child_id',$get_child_details->id)->get();

                        $all_good_to_have_vaccination = array();
                        if(count($good_to_have_vaccination) > 0){
                            foreach ($good_to_have_vaccination as $gk => $gv) {

                                $vaccinaton_details = array();
                                if($gv->taken_at <= $days){

                                    $vaccinaton_details['id'] = $gv->vaccination_id;
                                    $vaccinaton_details['name'] = $gv['vaccination']['vaccination_name'];
                                    $vaccinaton_details['taken_at'] = $gv->taken_at;

                                    $all_good_to_have_vaccination[] = $vaccinaton_details;
                                }
                            }
                        }
                        $remaining_vaccination = array_merge($not_taken_vaccination,$all_good_to_have_vaccination);
                        $child_details['remaining_vaccination'] = $remaining_vaccination;

                        if(count($vacctinations) > 0){
                            foreach ($vacctinations as $vk => $vv) {

                                $already_taken = array();
                                $taken_be = array();

                                $vv_taken = $vv->taken_at+1;

                                if($vv_taken == '' || $vv_taken == 0){

                                    $new_date =  date('d-m-Y', strtotime($get_child_details->date_of_birth. ' + 1 days'));
                                }else{
                                    $new_date =  date('d-m-Y', strtotime($get_child_details->date_of_birth. ' + '.$vv_taken.' days'));
                                }

                                if($vv_taken <= $days){

                                    if(isset($get_doctor_id->doctor_id) && $get_doctor_id->doctor_id != ''){

                                        $already_taken['vaccination_id'] = $this->nulltoblank($vv->vaccination_id);
                                        $already_taken['vaccination_name'] = $this->nulltoblank($vv['vaccination']['vaccination_name']);
                                        $taken_be['vaccination_type'] = 0;
                                        $already_taken['prevents'] = $vv['vaccination']['prevents'];
                                        $already_taken['description'] = $vv['vaccination']['vaccination_des'];

                                    }else{

                                        $already_taken['vaccination_id'] = $this->nulltoblank($vv->id);
                                        $already_taken['vaccination_name'] = $this->nulltoblank($vv->vaccination_name);
                                        $taken_be['vaccination_type'] = 0;
                                        $already_taken['prevents'] = $vv->prevents;
                                        $already_taken['description'] = $vv->vaccination_des;
                                    }

                                    $already_taken['taken_at'] = date('d M Y',strtotime($this->nulltoblank($new_date)));
                                    $already_taken['checked'] = true;
                                    $taken[] = $already_taken;

                                    $query = ChildAlreadyTakenVaccination::query();
                                    $query->where('user_id',$headers['user-id']);
                                    $query->where('child_id',$get_child_details->id);
                                    if(isset($get_doctor_id->doctor_id) && $get_doctor_id->doctor_id != ''){
                                        $query->where('vaccination_id',$vv->vaccination_id);
                                    } else {
                                        $query->where('vaccination_id',$vv->id);
                                    }
                                    $check_vaccination = $query->first();

                                    if(count($check_vaccination) == 0){
                                        //add vaccination in already have vaccination
                                        $already_have = new ChildAlreadyTakenVaccination;
                                        $already_have->user_id = $headers['user-id'];
                                        $already_have->child_id = $get_child_details->id;
                                        if(isset($get_doctor_id->doctor_id) && $get_doctor_id->doctor_id != ''){
                                            $already_have->doctor_id = $get_doctor_id->doctor_id;
                                            $already_have->vaccination_id = $vv->vaccination_id;
                                        } else {
                                            $already_have->doctor_id = '';
                                            $already_have->vaccination_id = $vv->id;
                                        }

                                        $already_have->taken_at = $vv_taken;
                                        $already_have->vaccination_date = $new_date;
                                        $already_have->vaccination_type = $vv->vaccination_type;
                                        $already_have->save();
                                    }

                                } else {

                                    if(isset($get_doctor_id->doctor_id) && $get_doctor_id->doctor_id != ''){

                                        $taken_be['vaccination_id'] = $this->nulltoblank($vv->vaccination_id);
                                        $taken_be['vaccination_name'] = $this->nulltoblank($vv->vaccination->vaccination_name);
                                        $taken_be['vaccination_name'] = $this->nulltoblank($vv['vaccination']['vaccination_name']);
                                        $taken_be['vaccination_type'] = 0;
                                        $taken_be['prevents'] = $this->nulltoblank($vv['vaccination']['prevents']);
                                        $taken_be['description'] = $this->nulltoblank($vv['vaccination']['vaccination_des']);

                                    }else{

                                        $taken_be['vaccination_id'] = $this->nulltoblank($vv->id);
                                        $taken_be['vaccination_name'] = $this->nulltoblank($vv->vaccination_name);
                                        $taken_be['vaccination_type'] = 0;
                                        $taken_be['prevents'] = $this->nulltoblank($vv->prevents);
                                        $taken_be['description'] = $this->nulltoblank($vv->vaccination_des);

                                    }

                                    $taken_be['taken_at'] = date('d M Y',strtotime($this->nulltoblank($new_date)));
                                    $taken_be['checked'] = false;
                                    $taken_to_be[] = $taken_be;

                                }
                            }
                        }

                        //change date of vaccination
                        $query = ChildVaccination::query();
                        $query->with(['vaccination']);
                        $query->where('child_id',$data['child_id']);
                        $query->whereIn('user_id',$get_link_parent);
                        $query->where('good_to_have','0');
                        $query->where('taken_vaccination','0');
                        $query->where('taken_at','>=',$start_range);
                        if($end_range != 4320){
                            $query->where('taken_at','<',$end_range);
                        }
                        $change_date = $query->orderBy('taken_at','ASC')->get();

                            $date_change = array();
                            if(count($change_date) >= 0){
                                foreach ($change_date as $ck => $cv) {
                                    $vacc = array();
                                    $vacc['vaccination_id'] = $cv->vaccination_id;
                                    $vacc['vaccination_name'] = $cv['vaccination']['vaccination_name'];
                                        $taken_date =  date('d M Y', strtotime($cv->vaccination_date));
                                    $vacc['taken_at'] = $taken_date;
                                    if($cv->is_good_to_have == 1){
                                        $vacc['vaccination_type'] = 1;
                                    }else{
                                        $vacc['vaccination_type'] = 0;
                                    }
                                    $vacc['prevents'] = $cv['vaccination']['prevents'];
                                    $vacc['description'] = $cv['vaccination']['vaccination_des'];
                                    $vacc['checked'] = false;
                                    $date_change[] = $vacc;
                                }
                            }

                        //good to have data
                        $query = ChildVaccination::query();
                        $query->where('child_id',$data['child_id']);
                        $query->whereIn('user_id',$get_link_parent);
                        $query->where('good_to_have','1');
                        $query->where('taken_vaccination','0');
                        $query->where('taken_at','>=',$start_range);
                        if($end_range != 4320){
                            $query->where('taken_at','<',$end_range);
                        }
                        $get_good_to_have = $query->orderBy('taken_at','ASC')->get();

                        $good_to_have_vacc = array();
                        if(count($get_good_to_have) >= 0){
                            foreach ($get_good_to_have as $gk => $gv) {

                                $vacc = array();
                                $vacc['vaccination_id'] = $gv->vaccination_id;
                                $vacc['vaccination_name'] = $gv['vaccination']['vaccination_name'];
                                    $data_date = date('d M Y',strtotime($gv->vaccination_date));
                                $vacc['taken_at'] = $data_date;
                                $vacc['vaccination_type'] = 1;
                                $vacc['prevents'] = $gv['vaccination']['prevents'];
                                $vacc['description'] = $gv['vaccination']['vaccination_des'];
                                $vacc['checked'] = false;
                                $good_to_have_vacc[] = $vacc;
                            }
                        }

                        $query = ChildVaccination::query();
                        $query->where('child_id',$data['child_id']);
                        $query->whereIn('user_id',$get_link_parent);
                        $query->where('good_to_have','0');
                        $query->where('taken_vaccination','1');
                        $query->where('taken_at','>=',$start_range);
                        if($end_range != 4320){
                            $query->where('taken_at','<',$end_range);
                        }
                        $child_taken = $query->orderBy('taken_at','ASC')->get();

                        $taken_vacc = array();
                        if(count($child_taken) >= 0){
                            foreach ($child_taken as $tk => $tv) {
                                $vacc = array();
                                $vacc['vaccination_id'] = $tv->vaccination_id;
                                $vacc['vaccination_name'] = $tv['vaccination']['vaccination_name'];
                                    $data_date = date('d M Y',strtotime($tv->vaccination_date));
                                $vacc['taken_at'] = $data_date;
                                if($tv->is_good_to_have == 1){
                                    $vacc['vaccination_type'] = 1;
                                }else{
                                    $vacc['vaccination_type'] = 0;
                                }
                                $vacc['prevents'] = $tv['vaccination']['prevents'];
                                $vacc['description'] = $tv['vaccination']['vaccination_des'];
                                $vacc['checked'] = true;
                                $taken_vacc[] = $vacc;
                            }
                        }

                        $already_taken = array_merge($taken_to_be,$date_change,$good_to_have_vacc,$taken,$taken_vacc);
                        if(count($already_taken) > 0){
                            //id sorting
                            foreach ($already_taken as $key => $value) {
                                $edition[$key] = $value['vaccination_id'];
                            }
                            array_multisort($edition, SORT_ASC, $already_taken);
                        }

                        usort($already_taken, function($a, $b) {
                            return new DateTime($a['taken_at']) <=> new DateTime($b['taken_at']);
                        });

                        $vaccination_data['child_details'] = $child_details;
                        $vaccination_data['vaccination'] = $already_taken;

                        $this->LogOutput(Response::json(array('status'=>200,'data' => $vaccination_data)));
                        return Response::json(array('status'=>200,'data' => $vaccination_data),200);

                    } else {

                        $this->LogOutput(Response::json(array('status'=>500,'message' => 'Please add child!')));
                        return Response::json(array('status'=>500,'message' => 'Please add child!'),500);

                    }

            } else {

                $this->LogOutput(Response::json(array('status'=>401,'message' => 'Unauthorised user!')));
                return Response::json(array('status'=>401,'message' => 'Unauthorised user!'),401);
            }


        } else {

            $this->LogOutput(Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array)));
            return Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array),500);

        }

    }

    public function childVaccinationTaken(){

        $this->LogInput();
        $errors_array = array();
        $headers = apache_request_headers();

        if (!isset($headers['user-id']) || $headers['user-id'] == "")
            $errors_array['user-id'] = 'Please enter user id!';

        if (!Input::has('child_id') || Input::get('child_id') == "")
            $errors_array['child_id'] = 'Please enter child id!';

        if (!Input::has('vaccination_id') || Input::get('vaccination_id') == "")
            $errors_array['vaccination_id'] = 'Please enter vaccination id!';

        if(count($errors_array) == 0){

            $data = Input::all();

            $check_user = User::where('id',$headers['user-id'])->where('is_otp_verified','1')->where('is_delete','0')->first();

            if(isset($check_user) && count($check_user) > 0){

                $get_link_parent = array();
                $get_link_parent = UserLinkParent::where('link_parent_id',$headers['user-id'])->where('is_delete','0')->pluck('user_id');
                //get parent id
                $child_detail = ChildrenDetail::where('id',$data['child_id'])->first();
                $get_link_parent[] = $headers['user-id'];

                //get doctor
                $get_doctor_id = UserChildWiseDoctor::where('child_id',$data['child_id'])->whereIn('user_id',$get_link_parent)->first();

                if(isset($get_doctor_id->doctor_id) && $get_doctor_id->doctor_id != ''){
                    $query = DoctorWiseVaccination::query();
                    $query->where('vaccination_id',$data['vaccination_id']);
                    $query->where('doctor_id',$get_doctor_id->doctor_id);

                } else {
                    $query = Vaccination::query();
                    $query->where('id',$data['vaccination_id']);
                    $query->where('is_delete','0');
                }
                $vacctinations = $query->first();

                $query = ChildAlreadyTakenVaccination::query();
                $query->where('child_id',$data['child_id']);
                $query->where('vaccination_id',$data['vaccination_id']);
                if(isset($get_doctor_id->doctor_id) && $get_doctor_id->doctor_id != ''){
                    $query->where('doctor_id',$get_doctor_id->doctor_id);
                }
                $get_already_taken = $query->first();

                $taken_vacc = new ChildVaccination;
                $taken_vacc->user_id = $child_detail->user_id;
                $taken_vacc->child_id = $data['child_id'];
                if(isset($get_doctor_id->doctor_id) && $get_doctor_id->doctor_id != ''){
                    $taken_vacc->doctor_id = $get_doctor_id->doctor_id;
                }
                $taken_vacc->vaccination_id = $data['vaccination_id'];

                $taken_vacc->vaccination_type = $vacctinations->vaccination_type;
                $taken_vacc->is_active = $vacctinations->is_active;

                if(count($get_already_taken) == 0){

                    $child_vaccination = ChildVaccination::where('child_id',$data['child_id'])->where('vaccination_id',$data['vaccination_id'])->first();
                    if(!is_null($child_vaccination) && $child_vaccination->is_good_to_have == 1){
                        $taken_vacc->is_good_to_have = 1;
                    }
                    if(!is_null($child_vaccination) && $child_vaccination->taken_vaccination == 1){

                        $child_vaccination = ChildVaccination::where('child_id',$data['child_id'])->where('vaccination_id',$data['vaccination_id'])->delete();
                        $taken_vacc->taken_vaccination = 0;
                       /* $taken_date =  date('Y-m-d');*/
                        $birth_date = ChildrenDetail::where('id',$data['child_id'])->first();
                        $vv_taken = $vacctinations->taken_at+1;
                        $new_date =  date('Y-m-d', strtotime($birth_date->date_of_birth. ' + '.$vv_taken.' days'));
                        $taken_vacc->vaccination_date = $new_date;

                    } else {

                        $child_vaccination = ChildVaccination::where('child_id',$data['child_id'])->where('vaccination_id',$data['vaccination_id'])->delete();
                        $taken_vacc->taken_vaccination = 1;

                        if(isset($data['vaccination_date'])){
                            $change_date = date('Y-m-d', strtotime($data['vaccination_date']));

                            $datetime1 = new DateTime($child_detail->date_of_birth);
                            $datetime2 = new DateTime($change_date);
                            $interval = $datetime1->diff($datetime2);
                            $vaccination_days = $interval->format('%a');
                            $vaccination_days = $vaccination_days+1;

                            $taken_vacc->vaccination_date = $change_date;
                        }else{

                            $taken_date =  date('Y-m-d');
                            $taken_vacc->vaccination_date = $taken_date;
                        }
                    }

                } else {

                    $child_vaccination = ChildVaccination::where('child_id',$data['child_id'])->where('vaccination_id',$data['vaccination_id'])->delete();
                    $taken_vacc->taken_vaccination = 0;
                    $birth_date = ChildrenDetail::where('id',$data['child_id'])->first();
                    $vv_taken = $vacctinations->taken_at+1;
                    $new_date =  date('Y-m-d', strtotime($birth_date->date_of_birth. ' + '.$vv_taken.' days'));
                    $taken_vacc->vaccination_date = $new_date;

                }
                if(isset($vaccination_days)){
                    $taken_vacc->taken_at = $vaccination_days;
                }else{
                    $taken_vacc->taken_at = $vacctinations->taken_at;
                }
                $taken_vacc->good_to_have = '0';
                $taken_vacc->save();

                if($taken_vacc){

                    if(isset($get_doctor_id->doctor_id) && $get_doctor_id->doctor_id != ''){
                        $get_already_taken = ChildAlreadyTakenVaccination::whereIn('user_id',$get_link_parent)->where('vaccination_id',$data['vaccination_id'])->where('doctor_id',$get_doctor_id->doctor_id)->delete();
                    } else {
                        //get already taken
                        $get_already_taken = ChildAlreadyTakenVaccination::whereIn('user_id',$get_link_parent)->where('vaccination_id',$data['vaccination_id'])->delete();
                    }

                    $this->LogOutput(Response::json(array('status'=>200,'message' => 'Vaccination taken successfully!')));
                    return Response::json(array('status'=>200,'message' => 'Vaccination taken successfully!'),200);

                } else {

                    $this->LogOutput(Response::json(array('status'=>500,'message' => 'Something went wrong!')));
                    return Response::json(array('status'=>500,'message' => 'Something went wrong!'),500);
                }

            } else {

                $this->LogOutput(Response::json(array('status'=>401,'message' => 'Unauthorised user!')));
                return Response::json(array('status'=>401,'message' => 'Unauthorised user!'),401);
            }

        } else {

            $this->LogOutput(Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array)));
            return Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array),500);
        }

    }

    public function changeDateVaccination(){

        $this->LogInput();
        $errors_array = array();
        $headers = apache_request_headers();

        if (!isset($headers['user-id']) || $headers['user-id'] == "")
            $errors_array['user-id'] = 'Please enter user id!';

        if (!Input::has('child_id') || Input::get('child_id') == "")
            $errors_array['child_id'] = 'Please enter child id!';

        if (!Input::has('vaccination_id') || Input::get('vaccination_id') == "")
            $errors_array['vaccination_id'] = 'Please enter vaccination id!';

        if (!Input::has('change_vaccination_date') || Input::get('change_vaccination_date') == "")
            $errors_array['change_vaccination_date'] = 'Please enter vaccination date!';

        if(count($errors_array) == 0){

            $vaccination_data = array();
            $data = Input::all();

            $check_user = $this->checkUser($headers['user-id']);

            if(isset($check_user)  && count($check_user) > 0){

                $get_link_parent = array();
                $get_link_parent = UserLinkParent::where('link_parent_id',$headers['user-id'])->where('is_delete','0')->pluck('user_id');

                //get parent id
                $parent_id = ChildrenDetail::where('id',$data['child_id'])->where('is_delete','0')->first();
                $get_link_parent[] = $headers['user-id'];

                //get doctor
                $get_doctor_id = UserChildWiseDoctor::where('child_id',$data['child_id'])->whereIn('user_id',$get_link_parent)->first();

                if(isset($get_doctor_id->doctor_id) && $get_doctor_id->doctor_id != ''){

                    $query = DoctorWiseVaccination::query();
                    $query->where('vaccination_id',$data['vaccination_id']);
                    $query->where('doctor_id',$get_doctor_id->doctor_id);

                }else{

                    $query = Vaccination::query();
                    $query->where('id',$data['vaccination_id']);
                    $query->where('is_delete','0');
                }
                $query->where('is_active','1');
                $vacctinations = $query->first();

                $check_good_to_have = ChildVaccination::where('child_id',$data['child_id'])->where('vaccination_id',$data['vaccination_id'])->whereIn('user_id',$get_link_parent)->first();

                if(isset($check_good_to_have) && $check_good_to_have->is_good_to_have != '' && $check_good_to_have->is_good_to_have == 1){
                    $good_to_have = 1;
                }
                //change date and not taken and taken vaccinations delete
                $child_vaccination = ChildVaccination::where('child_id',$data['child_id'])->where('vaccination_id',$data['vaccination_id'])->whereIn('user_id',$get_link_parent)->delete();

                $change_date = date('Y-m-d', strtotime($data['change_vaccination_date']));

                $datetime1 = new DateTime($parent_id->date_of_birth);
                $datetime2 = new DateTime($change_date);
                $interval = $datetime1->diff($datetime2);
                $days = $interval->format('%a');
                $days = $days+1;

                $remaining_vacc = new ChildVaccination;
                $remaining_vacc->user_id = $parent_id->user_id;
                $remaining_vacc->child_id = $data['child_id'];
                $remaining_vacc->vaccination_id = $data['vaccination_id'];
                $remaining_vacc->taken_at = $days;
                $remaining_vacc->vaccination_date = $change_date;
                $remaining_vacc->vaccination_type = $vacctinations->vaccination_type;
                $remaining_vacc->is_active = $vacctinations->is_active;
                $remaining_vacc->good_to_have = '0';
                $remaining_vacc->taken_vaccination = '0';
                $remaining_vacc->is_change_date = 1;
                if(isset($good_to_have) && $good_to_have == 1){
                    $remaining_vacc->is_good_to_have = 1;
                }
                $remaining_vacc->save();

                if($remaining_vacc){

                    $this->LogOutput(Response::json(array('status'=>200,'message' => 'Due date successfully changed!')));
                    return Response::json(array('status'=>200,'message' => 'Due date successfully changed!'),200);

                }else{

                    $this->LogOutput(Response::json(array('status'=>500,'message' => 'Something went wrong')));
                    return Response::json(array('status'=>500,'message' => 'Something went wrong'),500);
                }

            } else {

                $this->LogOutput(Response::json(array('status'=>401,'message' => 'Unauthorised user!')));
                return Response::json(array('status'=>401,'message' => 'Unauthorised user!'),401);
            }

        }else{

            $this->LogOutput(Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array)));
            return Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array),500);
        }
    }

    public function selectGoodToHaveVaccination(){

        $this->LogInput();
        $errors_array = array();
        $headers = apache_request_headers();
        $base_path = URL::to('/');

        if (!isset($headers['user-id']) || $headers['user-id'] == "")
            $errors_array['user-id'] = 'Please enter user id!';

        if (!Input::has('child_id') || Input::get('child_id') == "")
            $errors_array['child_id'] = 'Please enter child id!';

        if(count($errors_array) == 0){

            $good_to_have = array();
            $data = Input::all();

            $check_user = $this->checkUser($headers['user-id']);

            if(isset($check_user) && count($check_user) > 0){

                $get_link_parent = array();
                $get_link_parent = UserLinkParent::where('link_parent_id',$headers['user-id'])->where('is_delete','0')->pluck('user_id');
                $get_link_parent[] = $headers['user-id'];

                //change date vaccinaton
                $child_vacc = ChildVaccination::whereIn('user_id',$get_link_parent)->where('child_id',$data['child_id'])->where('good_to_have','0')->where('taken_vaccination','0')->pluck('vaccination_id')->toArray();

                //good to have vaccination
                $getAllGoodTohaveVaccine = ChildVaccination::whereIn('user_id',$get_link_parent)->where('child_id',$data['child_id'])->where('good_to_have','1')->pluck('vaccination_id')->toArray();

                //already taken
                $taken_vaccination = ChildVaccination::whereIn('user_id',$get_link_parent)->where('child_id',$data['child_id'])->where('good_to_have','0')->where('taken_vaccination','1')->pluck('vaccination_id')->toArray();

                $notInVaccine = array_merge($child_vacc,$getAllGoodTohaveVaccine,$taken_vaccination);

                $child_detail = ChildrenDetail::where('id',$data['child_id'])->first();

                if(isset($child_detail) && count($child_detail) > 0){

                    //days from child birth
                    $current_date = date("Y-m-d");
                    $data_date = date('Y-m-d',strtotime($child_detail->date_of_birth));
                    $datetime1 = new DateTime($data_date);
                    $datetime2 = new DateTime($current_date);
                    $interval = $datetime1->diff($datetime2);
                    //days is start range
                    $days = $interval->format('%a');

                    $check_doctor = UserChildWiseDoctor::where('child_id',$data['child_id'])->first();

                    if(!is_null($check_doctor) && $check_doctor->doctor_id != ''){

                        $all_vaccination = DoctorWiseVaccination::where('doctor_id',$check_doctor->doctor_id)->whereNotIn('vaccination_id',$notInVaccine)->where('vaccination_type','1')->orderBy('taken_at','ASC')->where('is_active','1')->get();

                    } else {

                        $all_vaccination = Vaccination::where('vaccination_type','1')->whereNotIn('id',$notInVaccine)->where('is_active','1')->where('is_delete','0')->orderBy('taken_at','ASC')->get();
                    }

                    if(count($all_vaccination) > 0){
                        $vacctinations = array();
                        $good_to_have = array();
                        foreach ($all_vaccination as $ak => $av) {
                            $vacctinations_data = array();
                            if($av->taken_at >= $days){
                                if(count($getAllGoodTohaveVaccine) == 0 || !in_array($av->vaccination_id,$getAllGoodTohaveVaccine)){
                                    if(!is_null($check_doctor) && $check_doctor->doctor_id != ''){
                                        $vacctinations_data['vacctination_id'] = $av->vaccination_id;
                                        $get_prevent = Vaccination::where('id',$av->vaccination_id)->first();
                                        $vacctinations_data['description'] = $get_prevent->vaccination_name;
                                        $vacctinations_data['days'] = $av->taken_at;
                                    } else {
                                        $vacctinations_data['vacctination_id'] = $av->id;
                                        $vacctinations_data['description'] = $av->vaccination_name;
                                        $vacctinations_data['days'] = $av->taken_at;
                                    }

                                    $good_to_have[] = $vacctinations_data;
                                }
                            }
                        }

                        if(count($good_to_have) > 0){

                            $this->LogOutput(Response::json(array('status'=>200,'data' => $good_to_have)));
                            return Response::json(array('status'=>200,'data' => $good_to_have),200);

                        } else {

                            $this->LogOutput(Response::json(array('status'=>500,'message' => 'No vaccination found!')));
                            return Response::json(array('status'=>500,'message' => 'No vaccination found!'));
                        }

                    }else{

                        $this->LogOutput(Response::json(array('status'=>500,'message' => 'No vaccination found!')));
                        return Response::json(array('status'=>500,'message' => 'No vaccination found!'));
                    }

                }else{

                    $this->LogOutput(Response::json(array('status'=>500,'message' => 'Child not found!')));
                    return Response::json(array('status'=>500,'message' => 'Child not found!'));
                }

            }else{

                $this->LogOutput(Response::json(array('status'=>401,'message' => 'Unauthorised user!')));
                return Response::json(array('status'=>401,'message' => 'Unauthorised user!'),401);
            }

        }else{

            $this->LogOutput(Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array)));
            return Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array),500);
        }
    }

    public function addGoodToHaveVaccination(){

        $this->LogInput();
        $errors_array = array();
        $headers = apache_request_headers();
        $base_path = URL::to('/');

        if (!isset($headers['user-id']) || $headers['user-id'] == "")
            $errors_array['user-id'] = 'Please enter user id!';

        if (!Input::has('vaccination_id') || Input::get('vaccination_id') == "")
            $errors_array['vaccination_id'] = 'Please enter vaccination id!';

        if (!Input::has('child_id') || Input::get('child_id') == "")
            $errors_array['child_id'] = 'Please enter child id!';

        if(count($errors_array) == 0){

            $data = Input::all();

            $check_user = $this->checkUser($headers['user-id']);

            if(isset($check_user)  && count($check_user) > 0){

                //get parent id
                $parent_id = ChildrenDetail::where('id',$data['child_id'])->where('is_delete','0')->first();

                $check_doctor = UserChildWiseDoctor::where('child_id',$data['child_id'])->first();

                if($check_doctor->doctor_id != ''){
                    $all_vacc = DoctorWiseVaccination::where('vaccination_id',$data['vaccination_id'])->first();
                } else {
                    $all_vacc = Vaccination::where('id',$data['vaccination_id'])->first();
                }

                if(count($all_vacc) > 0){

                    $checkIfVaccExists = ChildVaccination::where('child_id',$data['child_id'])->where('vaccination_id',$data['vaccination_id'])->first();

                    if(is_null($checkIfVaccExists)){

                        $add_good_to_have = new ChildVaccination;
                        if($check_doctor->doctor_id != ''){

                            $add_good_to_have->vaccination_id = $all_vacc->vaccination_id;
                            $add_good_to_have->doctor_id = $check_doctor->doctor_id;

                        } else {

                            $add_good_to_have->vaccination_id = $all_vacc->id;
                        }

                        $add_good_to_have->user_id = $parent_id->user_id;
                        $add_good_to_have->child_id = $data['child_id'];

                        $child_dob = ChildrenDetail::where('id',$data['child_id'])->first();
                        $new_date =  date('Y-m-d', strtotime($child_dob->date_of_birth. ' + '.$all_vacc->taken_at.' days'));

                        $add_good_to_have->taken_at = $all_vacc->taken_at;
                        $add_good_to_have->vaccination_date = $new_date;
                        $add_good_to_have->good_to_have = '1';
                        $add_good_to_have->is_good_to_have = '1';
                        $add_good_to_have->save();

                        if($add_good_to_have){

                            $this->LogOutput(Response::json(array('status'=>200,'message' => 'Vaccination added successfully!','days' => $all_vacc->taken_at)));
                            return Response::json(array('status'=>200,'message' => 'Vaccination added successfully!','days' => $all_vacc->taken_at),200);

                        } else {

                            $this->LogOutput(Response::json(array('status'=>500,'message' => 'Something went wrong!')));
                            return Response::json(array('status'=>500,'message' => 'Something went wrong!'),500);
                        }

                    } else {

                        $this->LogOutput(Response::json(array('status'=>500,'message' => 'This Good To Have vaccine is already added!')));
                        return Response::json(array('status'=>500,'message' => 'This Good To Have vaccine is already added!'),500);
                    }

                } else {

                    $this->LogOutput(Response::json(array('status'=>500,'message' => 'Please select atleast one vaccination!')));
                    return Response::json(array('status'=>500,'message' => 'Please select atleast one vaccination!'),500);
                }

            } else {

                $this->LogOutput(Response::json(array('status'=>401,'message' => 'Unauthorised user!')));
                return Response::json(array('status'=>401,'message' => 'Unauthorised user!'),401);
            }

        }else{

            $this->LogOutput(Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array)));
            return Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array),500);
        }

    }

    public function listOfPrescription(){

        $this->LogInput();
        $errors_array = array();
        $headers = apache_request_headers();
        $base_path = URL::to('https://kidiatric.s3.ap-south-1.amazonaws.com/uploads/child_image/');

        if (!isset($headers['user-id']) || $headers['user-id'] == "")
            $errors_array['user-id'] = 'Please enter user id!';

        if(count($errors_array) == 0){
            $data = Input::all();

            $check_user = $this->checkUser($headers['user-id']);

            if(isset($check_user) && count($check_user) > 0){

                $get_link_parent = array();
                $get_link_parent = UserLinkParent::where('link_parent_id',$headers['user-id'])->where('is_delete','0')->pluck('user_id');
                $get_link_parent[] = $headers['user-id'];

                $query = ChildrenDetail::query();
                $query->whereIn('user_id',$get_link_parent);
                $query->with(['prescription']);
                if(isset($data['child_id'])){
                    $query->where('id',$data['child_id']);
                }
                $getChildData = $query->get();

                $childPrescriptionData = array();

                if(count($getChildData) > 0){
                    foreach($getChildData as $ak => $av){
                        $prescriptions = array();
                        $prescriptions['child_id'] = $av->id;
                        $prescriptions['child_name'] = $av->child_name;
                        $prescriptions['date_of_birth'] = $av->date_of_birth;
                        $prescriptions['gender'] = $av->gender;
                        $prescriptions['profile_image'] = $av->profile_image ? $base_path.$av->uuid."/profile_image/".$av->profile_image : "";
                        if(isset($av->prescription) && count($av->prescription) > 0){
                            foreach($av->prescription as $pk => $pv){
                                $prescriptions['prescription_data'][$pk]['id'] = $pv->id;
                                $prescriptions['prescription_data'][$pk]['name'] = $pv->title;
                                $prescriptions['prescription_data'][$pk]['date'] = date('d M Y',strtotime($pv->prescription_date));

                                $prescriptions['prescription_data'][$pk]['attachment'] = $pv->prescription_attachment ? $base_path.$av->uuid."/prescription/".$pv->prescription_attachment : "";
                            }
                        } else {
                            $prescriptions['prescription_data'] = array();
                        }

                        $childPrescriptionData[] = $prescriptions;
                    }

                    $this->LogOutput(Response::json(array('status'=>200,'data' => $childPrescriptionData)));
                    return Response::json(array('status'=>200,'data' => $childPrescriptionData),200);

                } else {

                    $this->LogOutput(Response::json(array('status'=>500,'message' => 'No medical document(s) found!')));
                    return Response::json(array('status'=>500,'message' => 'No medical document(s) found!'),500);
                }
            }else{

                $this->LogOutput(Response::json(array('status'=>401,'message' => 'Unauthorised user!')));
                return Response::json(array('status'=>401,'message' => 'Unauthorised user!'),401);
            }

        }else{

            $this->LogOutput(Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array)));
            return Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array),500);
        }

    }

    public function getUserUploadSize($child_id){

        $getUserId = ChildrenDetail::where('id',$child_id)->first();
        $getUser = ChildrenDetail::where('user_id',$getUserId->user_id)->pluck('id');
        $findTotalSize = UserWisePriscription::whereIn('child_id',$getUser)->where('is_delete',0)->sum('size');

        return $findTotalSize;

    }

    public function addPrescription(){

        $this->LogInput();
        $errors_array = array();
        $headers = apache_request_headers();

        if (!isset($headers['user-id']) || $headers['user-id'] == "")
            $errors_array['user-id'] = 'Please enter user id!';

        if (!Input::has('child_id') || Input::get('child_id') == "")
            $errors_array['child_id'] = 'Please enter child id!';

        if (!Input::has('title') || Input::get('title') == "")
            $errors_array['title'] = 'Please enter title!';

        if (!Input::has('attachment'))
            $errors_array['image'] = 'Please enter attachment!';

        if (!Input::has('selecte_date') || Input::get('selecte_date') == "")
            $errors_array['selecte_date'] = 'Please enter select date!';

        if(count($errors_array) == 0){

            $check_user = $this->checkUser($headers['user-id']);

            if(isset($check_user) && count($check_user) > 0){

                $data = Input::all();

                //get parent id
                $parent_id = ChildrenDetail::where('id',$data['child_id'])->where('is_delete','0')->first();
                $totalSize = $this->getUserUploadSize($data['child_id']);

                $size = 500;

                if($size >= $totalSize){

                    $add_presciption = new UserWisePriscription;
                    $add_presciption->user_id = $parent_id->user_id;
                    $add_presciption->child_id = $data['child_id'];
                    $add_presciption->title = $data['title'];

                        //date conversion
                        $pres_date = date('Y-m-d', strtotime($data['selecte_date']));
                    $add_presciption->prescription_date = $pres_date;

                    //add uploader image
                    if(isset($data['attachment'])){
                      $imagedata = $data['attachment'];
                      $size = $imagedata->getSize();
                      $size = $size / 1048576;
                      $getUUID = $this->getUUID($data['child_id']);
                      $path = "child_image/".$getUUID->uuid."/prescription";
                      $fileName = $this->uploadImage($data['attachment'],$path);
                      $add_presciption->prescription_attachment = $fileName;
                      $add_presciption->size = $size;
                    }else{
                        if($data['attachment'] == ''){
                            $add_presciption->prescription_attachment = '';
                        }
                    }
                    $image_token = $this->randomStringGenerater(68);
                    $add_presciption->image_token = $image_token;
                    $add_presciption->save();

                    $this->LogOutput(Response::json(array('status'=>200,'message' => 'Medical document added successfully!')));
                    return Response::json(array('status'=>200,'message' => 'Medical document added successfully!'),200);

                } else {

                    $this->LogOutput(Response::json(array('status'=>500,'message' => 'You have exceeded maximum amount of free storage!')));
                    return Response::json(array('status'=>500,'message' => 'You have exceeded maximum amount of free storage!'),500);
                }

            } else {

                $this->LogOutput(Response::json(array('status'=>401,'message' => 'Unauthorised user!')));
                return Response::json(array('status'=>401,'message' => 'Unauthorised user!'),401);
            }
        }else{

            $this->LogOutput(Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array)));
            return Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array),500);
        }
    }

    public function deletePrescription(){

        $this->LogInput();
        $errors_array = array();
        $headers = apache_request_headers();

        if (!isset($headers['user-id']) || $headers['user-id'] == "")
            $errors_array['user-id'] = 'Please enter user id!';

        if (!Input::has('id') || Input::get('id') == "")
            $errors_array['id'] = 'Please enter id!';

        if(count($errors_array) == 0){

            $check_user = $this->checkUser($headers['user-id']);

            if(isset($check_user) && count($check_user) > 0){
                $get_link_parent = array();
                $get_link_parent = UserLinkParent::where('link_parent_id',$headers['user-id'])->where('is_delete','0')->pluck('user_id');
                $get_link_parent[] = $headers['user-id'];

                $data = Input::all();

                $check_attachment = UserWisePriscription::where('id',$data['id'])->whereIn('user_id',$get_link_parent)->first();

                if(count($check_attachment) > 0){

                    $getUUID = $this->getUUID($check_attachment->child_id);
                    $path = 'child_image/'.$getUUID->uuid."/prescription/".$check_attachment->prescription_attachment;
                    $this->deleteUploadedFile($path);

                    $delete_prescription = UserWisePriscription::where('id',$data['id'])->whereIn('user_id',$get_link_parent)->update(['is_delete' => '1']);

                    if($delete_prescription){

                        $this->LogOutput(Response::json(array('status'=>200,'message' => 'Medical document successfully removed!')));
                        return Response::json(array('status'=>200,'message' => 'Medical document successfully removed!'),200);

                    }else{

                        $this->LogOutput(Response::json(array('status'=>500,'message' => 'Something went wrong!')));
                        return Response::json(array('status'=>500,'message' => 'Something went wrong!'),500);
                    }
                }else{

                    $this->LogOutput(Response::json(array('status'=>500,'message' => 'No medical document(s) found!')));
                    return Response::json(array('status'=>500,'message' => 'No medical document(s) found!'),500);
                }
            }else{

                $this->LogOutput(Response::json(array('status'=>401,'message' => 'Unauthorised user!')));
                return Response::json(array('status'=>401,'message' => 'Unauthorised user!'),401);
            }
        }else{

            $this->LogOutput(Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array)));
            return Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array),500);
        }
    }

    //dashboard
    public function childWiseVaccine(){

        $this->LogInput();
        $errors_array = array();
        $headers = apache_request_headers();
        $base_path = URL::to('https://kidiatric.s3.ap-south-1.amazonaws.com/uploads/child_image/');

        if (!isset($headers['user-id']) || $headers['user-id'] == "")
            $errors_array['user-id'] = 'Please enter user id';

        if(count($errors_array) == 0){
            $child_data = array();
            $data = Input::all();
            $check_user = $this->checkUser($headers['user-id']);

            if(isset($check_user) && count($check_user) > 0){
                $get_link_parent = array();
                $get_link_parent = UserLinkParent::where('link_parent_id',$headers['user-id'])->where('is_delete','0')->pluck('user_id');
                $get_link_parent[] = $headers['user-id'];


                $get_child_details = ChildrenDetail::whereIn('user_id',$get_link_parent)->orderBy('date_of_birth','DESC')->where('is_delete','0')->get();

                if(isset($get_child_details) && count($get_child_details) > 0){

                    $child = array();
                    $vaccination_info = array();

                    foreach ($get_child_details as $gk => $gv) {
                        $child_info = array();
                        $doctor = array();

                        $child_info['child_id'] = $gv->id;
                        $child_info['name'] = $gv->child_name;

                            $date_birth = date('d-m-Y',strtotime($gv->date_of_birth));
                            $child_info['dob'] = $date_birth;

                            //age calculation
                            $currentDateTime = date('Y-m-d');
                            $diff = abs(strtotime($currentDateTime) - strtotime($date_birth));
                            $years = floor($diff / (365*60*60*24));
                            $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));

                        if($years != ''){
                            $child_info['age'] = $years.' Years '.$months.' Months';
                        }else{
                            $child_info['age'] = $months.' Months';
                        }
                        $child_info['gender'] = $gv->gender;
                        $child_info['profile_image'] = $gv->profile_image ? $base_path.$gv->uuid."/profile_image/".$gv->profile_image : "";

                        $check_doctor = UserChildWiseDoctor::whereIn('user_id',$get_link_parent)->where('child_id',$gv->id)->first();

                            $child_vaccination = ChildVaccination::whereIn('user_id',$get_link_parent)->where('child_id',$gv->id)->pluck('vaccination_id');

                            //change date vaccinaton
                            $already_taken = ChildVaccination::whereIn('user_id',$get_link_parent)->where('child_id',$gv->id)->where('good_to_have','0')->where('taken_vaccination','1')->pluck('vaccination_id');

                            //get days of birthdate
                            $data_date = date('Y-m-d',strtotime($gv->date_of_birth));
                            $currentDateTime = date('Y-m-d');
                            $datetime1 = new DateTime($data_date);
                            $datetime2 = new DateTime($currentDateTime);
                            $interval = $datetime1->diff($datetime2);
                            $days = $interval->format('%a');

                        if(isset($check_doctor->doctor_id) && $check_doctor->doctor_id != ''){

                            //doctor vaccination
                            $query = DoctorWiseVaccination::query();
                            $query->with(['vaccination']);
                            $query->where('doctor_id',$check_doctor->doctor_id);
                            $query->whereNotIn('vaccination_id',$already_taken);

                        }else{

                            $query = Vaccination::query();
                            $query->where('vaccination_type','0');
                            $query->where('is_delete','0');
                            $query->whereNotIn('id',$already_taken);
                        }
                        $query->where('is_active','1');
                        $query->where('taken_at','>=',$days);
                        $query->orderBy('taken_at','ASC');
                        $vacctinations = $query->first();

                        $check_vaccination = ChildVaccination::with(['vaccination'])->where('child_id',$gv->id)->where('taken_vaccination','0')->where('taken_at','>=',$days)->orderBy('taken_at','ASC')->first();

                        $count = $query->where('taken_at','>=',$days)->orderBy('taken_at','ASC')->count();

                             //change date vaccinaton
                            $already_taken_count = ChildVaccination::whereIn('user_id',$get_link_parent)->where('child_id',$gv->id)->where('good_to_have','0')->where('taken_vaccination','0')->where('is_change_date','0')->count();

                            //good to have vaccination
                            $getAllGoodTohaveVaccine_count = ChildVaccination::whereIn('user_id',$get_link_parent)->where('child_id',$gv->id)->where('good_to_have','1')->count();

                            //date of vaccination
                            $vv_taken = $vacctinations['taken_at']+1;
                            $new_date =  date('d-m-Y', strtotime($gv->date_of_birth. ' + '.$vv_taken.' days'));
                            //for compare
                            $date =  date('Y-m-d', strtotime($gv->date_of_birth. ' + '.$vacctinations['taken_at'].' days'));
                        if(!is_null($vacctinations['taken_at'])){
                            if(count($check_vaccination) > 0){

                                if($check_vaccination['vaccination_date'] <= $date){
                                    $child_info['vaccination_date'] = date('d M Y',strtotime($check_vaccination['vaccination_date']));
                                }else{
                                    $child_info['vaccination_date'] = date('d M Y',strtotime($new_date));
                                }
                            }else{
                                $child_info['vaccination_date'] = date('d M Y',strtotime($new_date));
                            }

                        }else{

                            $child_info['vaccination_date'] = '';
                        }

                        $total_count = $count+$already_taken_count+$getAllGoodTohaveVaccine_count;

                        $child_info['due_vaccinations'] = $this->nulltoblank($total_count);

                            if(isset($check_doctor) && $check_doctor->doctor_id != ''){
                                    //doctor info
                                    $doctor = Doctor::where('id',$check_doctor->doctor_id)->first();
                                $doctor['doctor_id'] = $this->nulltoblank($doctor->doctor_id);
                                $doctor['doctor_name'] = $this->nulltoblank($doctor->doctor_name);
                                if(isset($vacctinations->vaccination) && $vacctinations->vaccination != ''){
                                    if(count($check_vaccination) > 0){
                                        if($check_vaccination['vaccination_date'] <=  $date){

                                            if($check_vaccination['good_to_have'] == 1){
                                                $child_info['vaccination_type'] = 1;
                                            }else{
                                                $child_info['vaccination_type'] = 0;
                                            }

                                            $child_info['vaccination_name'] = $this->nulltoblank($check_vaccination['vaccination']['vaccination_name']);
                                        }else{

                                            if($vacctinations->vaccination_type == 1){
                                                $child_info['vaccination_type'] = 1;
                                            }else{
                                                $child_info['vaccination_type'] = 0;
                                            }
                                            $child_info['vaccination_name'] = $this->nulltoblank($vacctinations->vaccination->vaccination_name);
                                        }
                                    }else{

                                        if($vacctinations->vaccination_type == 1){
                                            $child_info['vaccination_type'] = 1;
                                        }else{
                                            $child_info['vaccination_type'] = 0;
                                        }
                                        $child_info['vaccination_name'] = $this->nulltoblank($vacctinations->vaccination->vaccination_name);
                                    }

                                }else{
                                    $child_info['vaccination_name'] = '';
                                }

                            }else{

                                $doctor['doctor_id'] = '';
                                $doctor['doctor_name'] = '';
                                if($vacctinations->vaccination_type == 1){
                                    $child_info['vaccination_type'] = 1;
                                }else{
                                    $child_info['vaccination_type'] = 0;
                                }
                                $child_info['vaccination_name'] = $this->nulltoblank($vacctinations->vaccination_name);
                            }

                        if(isset($check_doctor->doctor_id) && $check_doctor->doctor_id != ''){

                            //doctor vaccination
                            $query = DoctorWiseVaccination::query();
                            $query->where('doctor_id',$check_doctor->doctor_id);
                            $query->whereNotIn('vaccination_id',$child_vaccination);

                        }else{

                            $query = Vaccination::query();
                            $query->where('is_delete','0');
                            $query->whereNotIn('id',$child_vaccination);
                        }
                        $query->where('vaccination_type','1');
                        $query->where('is_active','1');
                        $query->where('taken_at','>=',$days);
                        $query->orderBy('taken_at','ASC');
                        $good_to_have = $query->limit(3)->get();

                        //good to have vaccination array
                        if(count($good_to_have) >= 0){
                            $vacctination_data = array();
                            foreach ($good_to_have as $gk => $gv) {

                                if($gv->taken_at >= $days){

                                    if(isset($check_doctor->doctor_id) && $check_doctor->doctor_id != ''){

                                        $vacctinations_info['vacctination_id'] = $gv->vaccination_id;
                                        $vacctinations_info['vaccination_name'] = $gv['vaccination']['vaccination_name'];
                                            $get_prevent = Vaccination::where('id',$gv->vaccination_id)->first();
                                        $vacctinations_info['days'] = $get_prevent->taken_at;
                                        $vacctinations_info['prevents'] = $get_prevent->prevents;
                                        $vacctinations_info['description'] = $get_prevent->vaccination_des;

                                    }else{

                                        $vacctinations_info['vacctination_id'] = $gv->id;
                                        $vacctinations_info['vaccination_name'] = $gv->vaccination_name;
                                        $vacctinations_info['days'] = $gv->taken_at;
                                        $vacctinations_info['prevents'] = $gv->prevents;
                                        $vacctinations_info['description'] = $gv->vaccination_des;

                                    }

                                    $vacctination_data[] = $vacctinations_info;
                                }
                            }
                        }

                        $child_info['doctor'] = $doctor;
                        $child_info['good_to_have'] = $vacctination_data;
                        $child[] = $child_info;

                    }
                    $child_data =$child;

                    $this->LogOutput(Response::json(array('status'=>200,'data' => $child_data)));
                    return Response::json(array('status'=>200,'data' => $child_data),200);
                }else{

                    $this->LogOutput(Response::json(array('status'=>500,'message' => 'Record not found!')));
                    return Response::json(array('status'=>500,'message' => 'Record not found!'),500);
                }
            }else{

                $this->LogOutput(Response::json(array('status'=>401,'message' => 'Unauthorised user!')));
                return Response::json(array('status'=>401,'message' => 'Unauthorised user!'),401);
            }
        }else{

            $this->LogOutput(Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array)));
            return Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array),500);
        }

    }

    public function childWiseCalData(){

        $this->LogInput();
        $errors_array = array();
        $headers = apache_request_headers();
        $base_path = URL::to('/');

        if (!isset($headers['user-id']) || $headers['user-id'] == "")
            $errors_array['user-id'] = 'Please enter user id!';

        if (!Input::has('child_id') || Input::get('child_id') == "")
            $errors_array['child_id'] = 'Please enter child id!';

        if (!Input::has('month') || Input::get('month') == "")
            $errors_array['month'] = 'Please enter month!';

        if(count($errors_array) == 0){
            $vaccination_data = array();
            $data = Input::all();

            $check_user = $this->checkUser($headers['user-id']);

            if(isset($check_user) && count($check_user) > 0){

                $get_link_parent = array();
                $get_link_parent = UserLinkParent::where('link_parent_id',$headers['user-id'])->where('is_delete','0')->pluck('user_id');
                $get_link_parent[] = $headers['user-id'];

                $get_child_details = ChildrenDetail::whereIn('user_id',$get_link_parent)->where('id',$data['child_id'])->where('is_delete','0')->first();

                if(isset($get_child_details) && count($get_child_details) >= 0){
                    $complete_given_date = '01-'.$data['month'];

                    $formate_date = date('Y-m-d',strtotime($complete_given_date));

                    //days from child birth
                    $current_date = date("Y-m-d");
                    $data_date = date('Y-m-d',strtotime($get_child_details->date_of_birth));
                    $datetime1 = new DateTime($data_date);
                    $datetime2 = new DateTime($current_date);
                    $interval = $datetime1->diff($datetime2);
                    //days is start range
                    $days = $interval->format('%a');


                    $data_date = date('Y-m-d',strtotime($get_child_details->date_of_birth));
                    $datetime1 = new DateTime($data_date);
                    $datetime2 = new DateTime($formate_date);
                    $interval = $datetime1->diff($datetime2);
                    //days is start range
                    $start_days = $interval->format('%a');

                    //end range is next month days
                    $formating = strtotime($formate_date);
                    $end_date = date("Y-m-d", strtotime("30 days", $formating));

                    $datetime3 = new DateTime($end_date);
                    $interval1 = $datetime1->diff($datetime3);
                    $end_days = $interval1->format('%a');


                    //check doctor id
                    $check_doctor = UserChildWiseDoctor::whereIn('user_id',$get_link_parent)->where('child_id',$data['child_id'])->first();

                    //change date vaccinaton
                    $child_vaccination = ChildVaccination::whereIn('user_id',$get_link_parent)->where('child_id',$data['child_id'])->pluck('vaccination_id');

                    //already taken
                    $already_taken = ChildAlreadyTakenVaccination::whereIn('user_id',$get_link_parent)->where('child_id',$data['child_id'])->pluck('vaccination_id');

                    if(isset($check_doctor->doctor_id) && $check_doctor->doctor_id != ''){

                        $query = DoctorWiseVaccination::query();
                        $query->with(['vaccination']);
                        $query->where('doctor_id',$check_doctor->doctor_id);
                        $query->whereNotIn('vaccination_id',$child_vaccination);
                        $query->whereNotIn('vaccination_id',$already_taken);
                        $query->where('vaccination_type','0');

                    }else{

                        $query = Vaccination::query();
                        $query->where('is_delete','0');
                        $query->whereNotIn('id',$child_vaccination);
                        $query->whereNotIn('id',$already_taken);
                        $query->where('vaccination_type','0');
                    }

                    if($start_days > $end_days){
                        $start_range = $end_days-1;
                        $end_range = $start_days;
                    }else{

                        $start_range = $start_days-1;
                        $end_range = $end_days;
                    }


                    if($start_range <= 31 || $start_range == 0){
                        $start_range = 0;
                    }

                    $query->where('is_active','1');
                    $query->where('taken_at','>=',$start_range);
                    $query->where('taken_at','<',$end_range);

                    //days is start range
                    $vacctinations = $query->orderBy('taken_at','ASC')->get();

                        $taken = array();
                        $taken_to_be = array();
                        $i = 0;
                        if(count($vacctinations) > 0){
                            foreach ($vacctinations as $vk => $vv) {
                                $already_taken = array();
                                $taken_be = array();

                                $vv_taken = $vv->taken_at+1;
                                //date calculation
                                $new_date =  date('Y-m-d', strtotime($get_child_details->date_of_birth. ' + '.$vv_taken.' days'));

                                if($vv_taken <= $days){

                                    if($check_doctor->doctor_id != ''){

                                        $already_taken['vaccination_id'] = $this->nulltoblank($vv->vaccination_id);
                                        $already_taken['vaccination_name'] = $this->nulltoblank($vv['vaccination']['vaccination_name']);

                                    }else{

                                        $already_taken['vaccination_id'] = $this->nulltoblank($vv->id);
                                        $already_taken['vaccination_name'] = $this->nulltoblank($vv->vaccination_name);
                                    }

                                    $already_taken['taken_at'] = $this->nulltoblank($new_date);
                                    $already_taken['already_taken'] = true;
                                    $taken[] = $already_taken;

                                }else{

                                    if(isset($check_doctor) && !is_null($check_doctor)){

                                        $taken_be['vaccination_id'] = $this->nulltoblank($vv->vaccination_id);
                                        $taken_be['vaccination_name'] = $this->nulltoblank($vv['vaccination']['vaccination_name']);

                                    }else{

                                        $taken_be['vaccination_id'] = $this->nulltoblank($vv->id);
                                        $taken_be['vaccination_name'] = $this->nulltoblank($vv->vaccination_name);
                                    }

                                    $taken_be['taken_at'] = $this->nulltoblank($new_date);
                                    $taken_be['already_taken'] = false;
                                    $taken_to_be[] = $taken_be;
                                }
                            }
                        }

                         // First day of the month.
                        $start_date = date('Y-m-01', strtotime($formate_date));

                        // Last day of the month.
                        $end_date = date('Y-m-t', strtotime($formate_date));

                        $child_vaccination = ChildVaccination::whereIn('user_id',$get_link_parent)->where('good_to_have','0')->where('taken_vaccination','0')->where('child_id',$data['child_id'])->where('vaccination_date','>=',$start_date)->where('vaccination_date','<=',$end_date)->get();

                        $not_taken_vaccination = array();
                        if(count($child_vaccination) > 0){
                            foreach ($child_vaccination as $ck => $cv) {

                                $vaccinaton_info = array();

                                $vaccinaton_info['id'] = $cv->vaccination_id;
                                $vaccinaton_info['name'] = $cv['vaccination']['vaccination_name'];
                                $vaccinaton_info['taken_at'] = $cv->vaccination_date;
                                $vaccinaton_info['already_taken'] = false;
                                $not_taken_vaccination[] = $vaccinaton_info;

                            }
                        }

                        $taken_vaccination = ChildVaccination::whereIn('user_id',$get_link_parent)->where('good_to_have','0')->where('taken_vaccination','1')->where('child_id',$data['child_id'])->where('vaccination_date','>=',$start_date)->where('vaccination_date','<=',$end_date)->get();

                        $taken_by_user = array();
                        if(count($taken_vaccination) > 0){
                            foreach ($taken_vaccination as $ck => $cv) {

                                $taken_info = array();

                                $taken_info['id'] = $cv->vaccination_id;
                                $taken_info['name'] = $cv['vaccination']['vaccination_name'];
                                $taken_info['taken_at'] = $cv->vaccination_date;
                                $taken_info['already_taken'] = true;
                                $taken_by_user[] = $taken_info;

                            }
                        }

                        $good_to_have_vaccination = ChildVaccination::whereIn('user_id',$get_link_parent)->where('good_to_have','1')->where('taken_vaccination','0')->where('child_id',$data['child_id'])->where('vaccination_date','>=',$start_date)->where('vaccination_date','<=',$end_date)->get();

                        $all_good_to_have_vaccination = array();
                        if(count($good_to_have_vaccination) > 0){
                            foreach ($good_to_have_vaccination as $gk => $gv) {

                                $vaccinaton_details = array();

                                $vaccinaton_details['id'] = $gv->vaccination_id;
                                $vaccinaton_details['name'] = $gv['vaccination']['vaccination_name'];
                                $vaccinaton_details['taken_at'] = $gv->vaccination_date;
                                $vaccinaton_details['already_taken'] = false;
                                $all_good_to_have_vaccination[] = $vaccinaton_details;

                            }
                        }

                        $already_taken = ChildAlreadyTakenVaccination::whereIn('user_id',$get_link_parent)->where('child_id',$data['child_id'])->whereBetween('taken_at',[$start_range,$end_range])->get();

                        $already_have = array();
                        if(count($already_taken) > 0){
                            foreach ($already_taken as $ak => $av) {

                                $taken_vaccinaton = array();

                                $taken_vaccinaton['id'] = $av->vaccination_id;
                                $taken_vaccinaton['name'] = $av['vaccination']['vaccination_name'];
                                $new_date =  date('Y-m-d', strtotime($av->vaccination_date));
                                $taken_vaccinaton['taken_at'] = $new_date;
                                $taken_vaccinaton['already_taken'] = true;
                                $already_have[] = $taken_vaccinaton;

                            }
                        }

                        $vaccinaton = array_merge($taken,$taken_to_be,$not_taken_vaccination,$all_good_to_have_vaccination,$already_have,$taken_by_user);
                        $vaccination_data['vacctination'] = $vaccinaton;

                    if($vaccination_data){

                        $this->LogOutput(Response::json(array('status'=>200,'data' => $vaccination_data)));
                        return Response::json(array('status'=>200,'data' => $vaccination_data),200);
                    }else{

                        $this->LogOutput(Response::json(array('status'=>500,'message' => 'Something went wrong!')));
                        return Response::json(array('status'=>500,'message' => 'Something went wrong!'),500);
                    }
                }else{

                    $this->LogOutput(Response::json(array('status'=>500,'message' => 'Child record not found!')));
                    return Response::json(array('status'=>500,'message' => 'Child record not Found!'),500);
                }
            }else{

                $this->LogOutput(Response::json(array('status'=>401,'message' => 'Unauthorised user!')));
                return Response::json(array('status'=>401,'message' => 'Unauthorised user!'),401);
            }
        }else{

            $this->LogOutput(Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array)));
            return Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array),500);
        }

    }

    public function childWiseGraphData(){

        $this->LogInput();
        $errors_array = array();
        $headers = apache_request_headers();

        if (!isset($headers['user-id']) || $headers['user-id'] == "")
            $errors_array['user-id'] = 'Please enter user id';

        if(count($errors_array) == 0){
            $child_array = array();

            $check_user = $this->checkUser($headers['user-id']);

            if(isset($check_user) && count($check_user) > 0){

                $get_link_parent = array();
                $get_link_parent = UserLinkParent::where('link_parent_id',$headers['user-id'])->where('is_delete','0')->pluck('user_id');
                $get_link_parent[] = $headers['user-id'];

                $get_child_details = ChildrenDetail::whereIn('user_id',$get_link_parent)->where('is_delete','0')->orderBy('date_of_birth','DESC')->get();

                if(count($get_child_details) == 0){

                    $this->LogOutput(Response::json(array('status'=>500,'message' => 'Child record not found!')));
                    return Response::json(array('status'=>500,'message' => 'Child record not found!'),500);

                }else{

                    foreach ($get_child_details as $gk => $gv) {
                        $child_info = array();
                        $child_info['id'] = $this->nulltoblank($gv->id);
                        $child_info['name'] = $this->nulltoblank($gv->child_name);


                        $current_date = date("Y-m-d");
                        $data_date = date('Y-m-d',strtotime($gv->date_of_birth));
                        $datetime1 = new DateTime($data_date);
                        $datetime2 = new DateTime($current_date);
                        $interval = $datetime1->diff($datetime2);
                        //days is start range
                        $age_days = $interval->format('%a');

                        $age = (int)$age_days;
                        $child_info['age'] = $age;

                        //change date vaccinaton and not takened
                        $child_vaccination = ChildVaccination::whereIn('user_id',$get_link_parent)->where('child_id',$gv->id)->pluck('vaccination_id');

                        //check doctor id
                        $check_doctor = UserChildWiseDoctor::whereIn('user_id',$get_link_parent)->where('child_id',$gv->id)->first();

                        if(isset($check_doctor->doctor_id) && $check_doctor->doctor_id != ''){

                            $query = DoctorWiseVaccination::query();
                            $query->with(['vaccination']);
                            $query->where('doctor_id',$check_doctor->doctor_id);
                            $query->whereNotIn('vaccination_id',$child_vaccination);

                        }else{

                            $query = Vaccination::query();
                            $query->where('is_delete','0');
                            $query->whereNotIn('id',$child_vaccination);

                        }
                        $query->where('is_active','1');
                        //days is start range
                        $vacctinations = $query->orderBy('taken_at','ASC')->get();

                        $taken_to_be = array();
                        if(count($vacctinations) > 0){
                            foreach ($vacctinations as $vk => $vv) {

                                $vaccinaton_array = array();
                                $vv_taken = $vv->taken_at+1;
                                if($vv_taken <= $age_days){

                                    if(isset($check_doctor->doctor_id) && $check_doctor->doctor_id != ''){
                                        $vaccinaton_array['id'] = $vv->vaccination_id;
                                        $vaccinaton_array['name'] = $vv['vaccination']['vaccination_name'];
                                    }else{

                                        $vaccinaton_array['id'] = $vv->id;
                                        $vaccinaton_array['name'] = $vv['vaccination_name'];
                                    }
                                    $vaccinaton_array['taken_at'] = $vv->taken_at;

                                    $taken_to_be[] = $vaccinaton_array;
                                }
                            }
                        }

                        $child_vaccination = ChildVaccination::whereIn('user_id',$get_link_parent)->where('good_to_have','0')->where('taken_vaccination','0')->where('child_id',$gv->id)->get();

                        $not_taken_vaccination = array();
                        if(count($child_vaccination) > 0){
                            foreach ($child_vaccination as $ck => $cv) {

                                $vaccinaton_info = array();
                                if($cv->taken_at <= $age_days){

                                    $vaccinaton_info['id'] = $cv->vaccination_id;
                                    $vaccinaton_info['name'] = $cv['vaccination']['vaccination_name'];
                                    $vaccinaton_info['taken_at'] = $cv->taken_at;

                                    $not_taken_vaccination[] = $vaccinaton_info;
                                }
                            }
                        }


                        $good_to_have_vaccination = ChildVaccination::whereIn('user_id',$get_link_parent)->where('good_to_have','1')->where('taken_vaccination','0')->where('child_id',$gv->id)->get();

                        $all_good_to_have_vaccination = array();
                        if(count($good_to_have_vaccination) > 0){
                            foreach ($good_to_have_vaccination as $gk => $gv) {

                                $vaccinaton_details = array();
                                if($gv->taken_at <= $age_days){

                                    $vaccinaton_details['id'] = $gv->vaccination_id;
                                    $vaccinaton_details['name'] = $gv['vaccination']['vaccination_name'];
                                    $vaccinaton_details['taken_at'] = $gv->taken_at;

                                    $all_good_to_have_vaccination[] = $vaccinaton_details;
                                }
                            }
                        }

                        $all_vaccination = array_merge($not_taken_vaccination,$all_good_to_have_vaccination);
                        $child_info['vacctination'] = $all_vaccination;
                        $child_array[] = $child_info;
                    }

                    $this->LogOutput(Response::json(array('status'=>200,'data' => $child_array)));
                    return Response::json(array('status'=>200,'data' => $child_array),200);
                }

            }else{

                $this->LogOutput(Response::json(array('status'=>401,'message' => 'Unauthorised user!')));
                return Response::json(array('status'=>401,'message' => 'Unauthorised user!'),401);
            }

        }else{

            $this->LogOutput(Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array)));
            return Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array),500);

        }
    }

    public function userNotificationList(){

        $this->LogInput();
        $errors_array = array();
        $headers = apache_request_headers();

        if (!isset($headers['user-id']) || $headers['user-id'] == "")
            $errors_array['user-id'] = 'Please enter user id!';


        if(count($errors_array) == 0){


            $not_interect = NotificationUser::where('user_id',$headers['user-id'])->where('interaction','0')->orderBy('created_at','ASC')->pluck('notification_id');

            $interaction = NotificationUser::where('user_id',$headers['user-id'])->where('interaction','1')->orderBy('created_at','ASC')->pluck('notification_id');

            $get_notifications = Notification::whereIn('id',$not_interect)->where('type','PUSH_NOTIFICATION')->orderBy('created_at','DESC')->paginate(10);

            $count_get_notification = Notification::whereIn('id',$not_interect)->where('type','PUSH_NOTIFICATION')->orderBy('created_at','DESC')->count();

            $all_notifications = Notification::whereIn('id',$interaction)->where('type','PUSH_NOTIFICATION')->orderBy('created_at','DESC')->paginate(10);
            $count_all_notifications = Notification::whereIn('id',$interaction)->where('type','PUSH_NOTIFICATION')->orderBy('created_at','DESC')->count();

            $total_notification = $count_get_notification + $count_all_notifications;

            $notification_array = array();
            if(count($get_notifications) > 0 || count($all_notifications) > 0){
                foreach ($get_notifications as $nk => $nv) {

                    $notification_info = array();
                    $notification_info['id'] = $nv->id;
                    $notification_info['title'] = $nv->title;
                    $notification_info['message'] = $nv->message;
                    if($nv->notification_type == 0)
                        $notification_info['type'] = 0;
                    elseif($nv->notification_type == 1)
                        $notification_info['type'] = 1;
                    else
                        $notification_info['type'] = 2;

                    $notification_info['url'] = $this->nulltoblank($nv->url);
                    if($nv->notification_type == '1' || $nv->notification_type == '0'){
                        if($nv->url != ''){
                            $notification_info['url'] = $this->nulltoblank("https://kidiatric.s3.ap-south-1.amazonaws.com/uploads/admin_notification/notification_image/".$nv->url);
                        }
                        else {
                            $notification_info['url'] = '';
                        }
                    }else{
                        if($nv->url != ''){
                            $notification_info['url'] = $this->nulltoblank("https://kidiatric.s3.ap-south-1.amazonaws.com/uploads/doctor_notification/notification_image/".$nv->url);
                        }
                        else{
                            $notification_info['url'] = '';
                        }
                    }
                    $notification_info['date'] = $nv->date;
                    $notification_info['interect'] = false;
                    $notification_array[] = $notification_info;
                }

                if(count($all_notifications) > 0){
                    foreach ($all_notifications as $ak => $av) {

                        $not_seen_notification = array();
                        $not_seen_notification['id'] = $av->id;
                        $not_seen_notification['title'] = $av->title;
                        $not_seen_notification['message'] = $av->message;
                        if($av->notification_type == 0)
                            $not_seen_notification['type'] = 0;
                        elseif($av->notification_type == 1)
                            $not_seen_notification['type'] = 1;
                        else
                            $not_seen_notification['type'] = 2;
                        $not_seen_notification['url'] = $this->nulltoblank($av->url);
                        if($av->notification_type == '1' || $av->notification_type == '0'){
                            if($av->url != ''){
                                $not_seen_notification['url'] = $this->nulltoblank("https://kidiatric.s3.ap-south-1.amazonaws.com/uploads/admin_notification/notification_image/".$av->url);
                            }
                            else{
                                $not_seen_notification['url'] = '';
                            }
                        }else{
                            if($av->url != ''){
                                $not_seen_notification['url'] = $this->nulltoblank("https://kidiatric.s3.ap-south-1.amazonaws.com/uploads/doctor_notification/notification_image/".$av->url);
                            }else{

                                $not_seen_notification['url'] = '';
                            }
                        }
                        $not_seen_notification['date'] = $av->date;
                        $not_seen_notification['interect'] = true;
                        $notification_array[] = $not_seen_notification;
                    }
                }

                $notification_detail['total_notification'] = $total_notification;
                $notification_detail['notification'] = $notification_array;

                $this->LogOutput(Response::json(array('status'=>200,'data' => $notification_detail)));
                return Response::json(array('status'=>200,'data' => $notification_detail),200);

            } else {

                $this->LogOutput(Response::json(array('status'=>500,'message' => 'No notification(s) found!')));
                return Response::json(array('status'=>500,'message' => 'No notification(s) found!'),500);
            }

        } else {

            $this->LogOutput(Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array)));
            return Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array),500);
        }
    }

    public function userNotificationUpdate(){

        $this->LogInput();
        $errors_array = array();
        $headers = apache_request_headers();

        if (!isset($headers['user-id']) || $headers['user-id'] == "")
            $errors_array['user-id'] = 'Please enter user id!';

        if (!Input::has('notification_id') || Input::get('notification_id') == "")
            $errors_array['notification_id'] = 'Please enter notification id!';

        if(count($errors_array) == 0){
            $data = Input::all();

            $notification_update = NotificationUser::where('user_id',$headers['user-id'])->where('notification_id',$data['notification_id'])->update(['interaction' => 1]);

            if($notification_update){

                $this->LogOutput(Response::json(array('status'=>200,'message' => 'Notification successfully viewed!')));
                return Response::json(array('status'=>200,'message' => 'Notification successfully viewed!'),200);

            } else {

                $this->LogOutput(Response::json(array('status'=>500,'message' => 'Something went wrong!')));
                return Response::json(array('status'=>500,'message' => 'Something went wrong!'),500);
            }

        } else {

            $this->LogOutput(Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array)));
            return Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array),500);
        }
    }

    public function videoLink(){

        $this->LogInput();
        $errors_array = array();
        $headers = apache_request_headers();
        $get_url = SocialMediaLink::first();

        $data['link'] = $get_url->link;
        $this->LogOutput(Response::json(array('status'=>200,'data' => $data)));
        return Response::json(array('status'=>200,'data' => $data),200);
    }

    public function UserUnreadNotification(){

        $this->LogInput();
        $errors_array = array();
        $headers = apache_request_headers();

        if (!isset($headers['user-id']) || $headers['user-id'] == "")
            $errors_array['user-id'] = 'Please enter user id!';

        if(count($errors_array) == 0){
            $data = Input::all();

           $interaction = NotificationUser::where('user_id',$headers['user-id'])->where('interaction','0')->orderBy('created_at','ASC')->pluck('notification_id');

            $count_all_notifications = Notification::whereIn('id',$interaction)->where('type','PUSH_NOTIFICATION')->orderBy('created_at','DESC')->count();


            $unread_notification['unread_notification'] = $count_all_notifications;

            $this->LogOutput(Response::json(array('status'=>200,'data' => $unread_notification)));
            return Response::json(array('status'=>200,'data' => $unread_notification),200);

        } else {

            $this->LogOutput(Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array)));
            return Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array),500);
        }
    }

    public function notificationStatusChanage(){

        $this->LogInput();
        $errors_array = array();
        $headers = apache_request_headers();

        if (!isset($headers['user-id']) || $headers['user-id'] == "")
            $errors_array['user-id'] = 'Please enter user id!';

        if(count($errors_array) == 0){

            $data = Input::all();

            $getNotificationCount = NotificationUser::where('user_id',$headers['user-id'])->update(['interaction' => 1]);

            $this->LogOutput(Response::json(array('status'=>200,'message' => 'Notification status changed!')));
            return Response::json(array('status'=>200,'message' => 'Notification status changed!'),200);

        } else {

            $this->LogOutput(Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array)));
            return Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array),500);
        }
    }

    public function removeGoodToHave(){

        $this->LogInput();
        $errors_array = array();
        $headers = apache_request_headers();

        if (!isset($headers['user-id']) || $headers['user-id'] == "")
            $errors_array['user-id'] = 'Please enter user id!';

        if (!Input::has('vaccination_id') || Input::get('vaccination_id') == "")
            $errors_array['vaccination_id'] = 'Please enter vaccination id!';

        if (!Input::has('child_id') || Input::get('child_id') == "")
            $errors_array['child_id'] = 'Please enter child id!';

        if(count($errors_array) == 0){
            $data = Input::all();
            $check_user = $this->checkUser($headers['user-id']);

            if(isset($check_user) && count($check_user) > 0){

                $get_link_parent = array();
                $get_link_parent = UserLinkParent::where('link_parent_id',$headers['user-id'])->where('is_delete','0')->pluck('user_id');
                $get_link_parent[] = $headers['user-id'];

                $child_vaccination = ChildVaccination::whereIn('user_id',$get_link_parent)->where('child_id',$data['child_id'])->where('vaccination_id',$data['vaccination_id'])->delete();

                if($child_vaccination){
                    $this->LogOutput(Response::json(array('status'=>200,'message' => 'Vaccination successfully deleted!')));
                    return Response::json(array('status'=>200,'message' => 'Vaccination successfully deleted!'),200);
                }else{
                    $this->LogOutput(Response::json(array('status'=>500,'message' => 'Something went wrong!')));
                    return Response::json(array('status'=>500,'message' => 'Something went wrong!'),500);
                }
            }else{

                $this->LogOutput(Response::json(array('status'=>401,'message' => 'User not found!')));
                return Response::json(array('status'=>401,'message' => 'User not found!'),401);
            }

        }else{

            $this->LogOutput(Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array)));
            return Response::json(array('status'=>500,'message' => 'errors','errors' => $errors_array),500);
        }
    }

}
