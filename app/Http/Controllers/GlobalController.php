<?php
namespace App\Http\Controllers;

use Mail;
use DateInterval;
use DateTime;
use DatePeriod;
use App\Model\User;
use App\Model\UserToken;
use Illuminate\Http\Request;

class GlobalController extends Controller
{   
    //Upload Image 
    public function uploadImage($image,$path){
        
        $imagedata = $image;
        $destinationPath = 'uploads/'.$path;
        $extension = $imagedata->getClientOriginalExtension(); 
        $fileName = rand(11111,99999).'.'.$extension;
        $s3 = \Storage::disk('s3');
        $destinationPath = $destinationPath."/".$fileName;
        $s3->put($destinationPath, file_get_contents($imagedata), 'public');
        return $fileName;
    }

    public function deleteUploadedFile($path){
        
        $s3 = \Storage::disk('s3');
        $existingImagePath = 'uploads/'.$path;
        $s3->delete($existingImagePath);

        return 'true';
    }
    
    //Upload Image 
    public function uploadImageData($image,$path){

        $imagedata = $image;
        $destinationPath = 'uploads/'.$path; 
        $extension = $imagedata->getClientOriginalExtension(); 
        $fileName = rand(11111,99999).'.'.$extension;
        $imagedata->move($destinationPath, $fileName);
        return $fileName;
    }

    //password genrate
    public function password($name,$mobile_number){
    	
    	$user_name = strtolower($name);
    	$user_mobilenumber = $mobile_number;
	    //4 character of name and 4 digit of mobilenumber
        $string = preg_replace('/\s+/', '', $user_name);//remove all white spaces
        $p_name = substr($string,0, 1);
        $p_number = substr($mobile_number,0,5);
        $new_password = $p_name.$p_number;
        return $new_password;
    }

    //qr-code genrate
    public function qr_code_genrate($user_list){

    	//random number genrate for qr-code
        $generator = "7463849"; 
        $total = count($user_list);
        $new_total = $total + 1;
        $result = $generator.$new_total;
        return $result;
    }

    //otp genrate
    public function otp_genrate($generator){
        $n = 4; 
        $result = ""; 
          
        for ($i = 1; $i <= $n; $i++) { 
            $result .= substr($generator, (rand()%(strlen($generator))), 1); 
        }
        return $result;
    }

    //mail function
    public function sendMail($templete,$mail_message,$sub,$email = null){
        
       $maildata = array('bodymessage' => $mail_message);

        Mail::send('mail.'.$templete, $maildata, function($msg) use ($email,$sub) {
           $msg->to($email, 'Site Admin');
           $msg->subject($sub);
        }); 
   }

   public function randomStringGenerater($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }


    public function updateUserAppToken($headers,$user_id){

        $randToken = $this->randomStringGenerater(32);
        $usertoken = UserToken::where('device_token',$headers['device-token'])
                              ->update(['app_token' => $randToken,'user_id' => $user_id]);
        if($usertoken){
            return $randToken;
        } else {
            return false;
        }
    }

    public function sendSms($message,$number,$otp){
        
        // Account details
        $authkey = '299160AjdGSQyXfsx25da6ec52';

        $senderid = 'KIDRIC';

        $message = urlencode($message);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://control.msg91.com/api/sendotp.php?otp=".$otp."&sender=".$senderid."&message=".$message."&mobile=".$number."&authkey=".$authkey,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } 

        return "true";
    }

    public function sendOtherSms($message,$number){

        $authKey = "299160AjdGSQyXfsx25da6ec52";

        //Multiple mobiles numbers separated by comma
        $mobileNumber = $number;
        //Sender ID,While using route4 sender id should be 6 characters long.
        $senderId = "KIDRIC";
        //Your message to send, Add URL encoding here.
        $message = urlencode($message);
        //Define route 
        $route = "4";

        //post parameters
        $postData = array(
            'authkey' => $authKey,
            'mobiles' => $mobileNumber,
            'message' => $message,
            'sender' => $senderId,
            'route' => $route
        );

        //API URL
        $url="http://api.msg91.com/api/v2/sendsms";

        // init the resource
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
        ));

        //Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        //get response
        $output = curl_exec($ch);

        //Print error if any
        if(curl_errno($ch))
        {
            //echo 'error:' . curl_error($ch);
        }
        curl_close($ch);
        return true;
    }
    
    public function sendInvitationSms($mobileNumber,$message){

        $postData = array(
            'authkey' => '299160AjdGSQyXfsx25da6ec52',
            'mobiles' => $mobileNumber,
            'message' => $message,
            'sender' => 'KIDRIC',
            'route' => 4
        );

        //API URL
        $url="http://api.msg91.com/api/v2/sendsms";

        // init the resource
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
        ));

        //Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        //get response
        $output = curl_exec($ch);
        
        //Print error if any
        if(curl_errno($ch))
        {
            //echo 'error:' . curl_error($ch);
        }
        curl_close($ch);
        return true;
    }

  /*  public function sendInvitationSms($message,$number){
        
        // Account details
        $apiKey = urlencode('S9JscOwBbiA-Pqg6hQ4YZFEi4RxiPSBPB2QixjcMs0');
        
        // Message details
        $numbers = $number;
        $sender = urlencode('TXTLCL');
        $message = rawurlencode($message);
     
        //$numbers = implode(',', $numbers);
     
        // Prepare data for POST request
        $data = array('apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message);
     
        // Send the POST request with cURL
        $ch = curl_init('https://api.textlocal.in/send/');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        // Process your response here
        return 'true';
    }
*/
    public function checkUser($user_id){

        $check_user = User::where('id',$user_id)->where('is_otp_verified','1')->where('is_delete','0')->first();

        return $check_user;
    }

    public function sendpushNotification($device_type,$msg,$devicetoken,$title,$url,$type,$notification_id = null){
        
        $API_ACCESS_KEY = 'AIzaSyCh5CGLFdyVFYbhM9khIPj1xFFbHRHxO20';
        $token = $devicetoken;
        
        if($url != ''){
            if($type == 1){
               $url =  'https://kidiatric.s3.ap-south-1.amazonaws.com/uploads/admin_notification/notification_image/'.$url;
            }elseif ($type == 2) {
                $url = 'https://kidiatric.s3.ap-south-1.amazonaws.com/uploads/doctor_notification/notification_image/'.$url;
            }else{
                $url = '';
            }
        }else{
            if($type == 0){
                $url = '';
            }else{

                $url = '';
            }
        }

        if($token != ''){

            $notification = array(
            'title' => $title,
            'body' => $msg, //for ios
            "sound" => "default",
            "priority" => "high",
            "badge" => 0,
            'notification_id' => $notification_id,
            'show_in_foreground' => true,
            'image' => $url,
            );
                       
            $data = array(
                $custom_notification = array(
                    'title' => $title,
                    'body' => $msg, //for ios
                    "sound" => "default",
                    "priority" => "high",
                    "badge" => 0,
                    'notification_id' => $notification_id,
                    'show_in_foreground' => true,
                    'image' => 'https://invesun.com/img/cake.png',
                )
            );
            
            $apns = array(
                'payload' => array(
                    'aps' => array(
                        "mutable-content" => 1,
                        "category" => "CustomSamplePush"
                    )
                ),
                'fcm_options' => array(
                    'image' => 'https://invesun.com/img/cake.png'
                )
            );

            $fields = array
            (
                'to'    => $token,
                "badge" => 0,
                "content_available" => true,
                "priority" => "high",
                'notification' => $notification,
                'apns' => $apns//for ios
            );
                
            $headers = array
            (
                'Authorization: key=' . $API_ACCESS_KEY,
                'Content-Type: application/json'
            );

            try{

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                $result = curl_exec($ch);
                curl_close($ch);
                
            } catch(Exception $e){
                print_r($e);
                exit;
            }
        }  
    }
    public function getDatesFromRange($start, $end, $format = 'Y-m-d') { 

        // Declare an empty array 
        $array = array(); 
          
        // Variable that store the date interval 
        // of period 1 day 
        $interval = new DateInterval('P1D'); 
      
        $realEnd = new DateTime($end); 
        $realEnd->add($interval); 
      
        $period = new DatePeriod(new DateTime($start), $interval, $realEnd); 
      
        // Use loop to store date into array 
        foreach($period as $date) {                  
            $array[] = $date->format($format);  
        } 
      
        // Return the array elements 
        return $array; 
    }

    public function dateDiffInDays($date1, $date2){
     
        // Calulating the difference in timestamps 
        $diff = strtotime($date2) - strtotime($date1); 
          
        // 1 day = 24 hours 
        // 24 * 60 * 60 = 86400 seconds 
        return abs(round($diff / 86400)); 
    } 
}
