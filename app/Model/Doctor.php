<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\DoctorResetPasswordNotification;

class Doctor extends Authenticatable
{
    use Notifiable;

    /**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	*/
    protected $fillable = [
        'mobile_number','otp','email','password'
    ];

     /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new DoctorResetPasswordNotification($token));
    }

    public function city_name(){
        return $this->hasOne('App\Model\City','id','city');
    }

    public function clinic_number(){
        return $this->hasMany('App\Model\DoctorClinicContactNumber','doctor_id','id');
    }
}
