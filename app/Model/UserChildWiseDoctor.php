<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserChildWiseDoctor extends Model
{
    public function doctordetail(){
    	return $this->hasOne('App\Model\Doctor','id','doctor_id');	
    }
}
