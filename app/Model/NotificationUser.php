<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class NotificationUser extends Model
{
    public function doctor(){
        return $this->hasOne('App\Model\Doctor','id','doctor_id');
    }
    public function user(){
        return $this->hasOne('App\Model\User','id','user_id');
    }
}
