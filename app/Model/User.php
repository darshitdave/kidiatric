<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
/*use Illuminate\Foundation\Auth\User as Authenticatable;*/
use Illuminate\Database\Eloquent\Model;
use App\Model\City;

class User extends Model
{
    /*use Notifiable;*/

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    /*protected $fillable = [
        'name', 'email', 'password',
    ];*/

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    /*protected $hidden = [
        'password', 'remember_token',
    ];*/

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    /*protected $casts = [
        'email_verified_at' => 'datetime',
    ];*/


    public function generateToken($headers){
        $randToken = $this->randomString(16);
        $usertoken = new UserToken;
        $usertoken->device_token = $randToken;
        $usertoken->device_type = $headers['device-type'];
        if(isset($headers['fcm-token']) && $headers['fcm-token'] != ''){
            $usertoken->fcm_token = $headers['fcm-token'];
        }
        $usertoken->version = $headers['version'];
        $usertoken->save();
        if($usertoken){
            return $randToken;
        } else {
            return false;
        }
    }

    public function randomString($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function userToken(){
        return $this->hasMany('App\Models\UserToken','user_id','id');
    }

    public function city(){
        return $this->hasOne('App\Model\City','id','city_id');
    }

    public function children(){
        return $this->hasMany('App\Model\UserChildWiseDoctor','user_id','id');
    }

    public function linkedparent(){
        return $this->hasMany('App\Model\UserLinkParent','user_id','id');   
    }
}
