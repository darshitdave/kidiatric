<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ChildVaccination extends Model
{
    public function vaccination(){
        return $this->hasOne('App\Model\Vaccination','id','vaccination_id');
    }
}
