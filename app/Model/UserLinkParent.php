<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserLinkParent extends Model
{
    public function user(){
        return $this->hasOne('App\Model\User','id','link_parent_id');
    }
}
