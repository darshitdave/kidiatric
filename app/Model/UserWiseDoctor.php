<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserWiseDoctor extends Model
{
    public function doctordetail(){
    	return $this->hasOne('App\Model\Doctor','id','doctor_id');	
    }
}
