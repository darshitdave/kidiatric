<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ChildAlreadyTakenVaccination extends Model
{
    public function doctors(){
        return $this->hasOne('App\Model\Doctor','id','doctor_id');
    }
    
    public function vaccination(){
        return $this->hasOne('App\Model\Vaccination','id','vaccination_id');
    }
}
