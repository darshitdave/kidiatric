<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    public function notificationuser(){
        return $this->hasOne('App\Model\NotificationUser','notification_id','id');
    }
}
