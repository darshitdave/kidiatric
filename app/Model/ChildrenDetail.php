<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ChildrenDetail extends Model
{
    public function ParentDetails(){
        return $this->hasOne('App\Model\User','id','user_id');
    }

    public function doctor(){
    	return $this->hasOne('App\Model\UserChildWiseDoctor','child_id','id');	
    }

    public function location(){
        return $this->hasOne('App\Model\User','id','user_id');
    }

    public function prescription(){
    	return $this->hasMany('App\Model\UserWisePriscription','child_id','id')->where('is_delete',0);	
    }

    public function parent(){
        return $this->hasOne('App\Model\User','id','user_id');
    }
}
