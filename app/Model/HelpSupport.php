<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class HelpSupport extends Model
{
    public function doctor(){
    	return $this->hasOne('App\Model\Doctor','id','doctor_id');	
    }
}
