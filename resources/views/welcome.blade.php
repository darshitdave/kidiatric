<!DOCTYPE HTML>
<html>
    <head>
        <title>Kidiatric - Pocket Paediatric. Vaccine and Health Data Recording App.</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="icon" href="https://kidiatric.app/img/kiditric-logo.png" type="image/png" sizes="16x16">
        <link rel="stylesheet" href="{{ asset('css/front/main.css') }}" />
    </head>
    <body class="is-preload">

        <!-- Header -->
            <header id="header" class="alt">

                <!-- Logo -->
                    <div class="logo">
                        <a href="/">Kidiatric</a><span> the Pocket Paediatric</span>
                    </div>

                <!-- Nav -->
                    <nav id="nav">
                        <ul>
                            <li><a href="https://kidiatric.app/doctor-panel/login">Doctor Login</a></li>
                            <!--<li>
                                <a href="#" class="icon solid fa-angle-down">Dropdown</a>
                                <ul>
                                    <li><a href="#">Option One</a></li>
                                    <li><a href="#">Option Two</a></li>
                                    <li><a href="#">Option Three</a></li>
                                    <li>
                                        <a href="#">Submenu</a>
                                        <ul>
                                            <li><a href="#">Option One</a></li>
                                            <li><a href="#">Option Two</a></li>
                                            <li><a href="#">Option Three</a></li>
                                            <li><a href="#">Option Four</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="generic.html">Generic</a></li>
                            <li><a href="elements.html">Elements</a></li>-->
                        </ul>
                    </nav>

            </header>

        <!-- Banner -->
            <section id="banner">
                <div class="inner">
                    <span><img width="40%" src="{{ asset('img/front/logo.png') }}" alt=""></span>
                    <h1>FREE Vaccine Reminder and Safe Medical Record keeping app</h1> 
		<span><a href="https://play.google.com/store/apps/details?id=com.kidiatric&hl=en"><img src="https://i.ibb.co/xXn75T1/google-play.png" </a>
			<a href="https://apps.apple.com/in/app/kidiatric-my-vaccine-reminder/id1488453972"><img src"https://imgbb.com/"><img src="https://i.ibb.co/0VzHTQ3/apple-app-store-icon-e1485370451143.png" alt="apple-app-store-icon-e1485370451143" border="0"></a> </a> </span>
                    <ul class="actions special">
                        <li><a href="#one" class="button wide scrolly">Get Started</a></li>
                    </ul>
                </div>
            </section>

        <!-- One -->
            <section id="one" class="wrapper">
                <div class="inner">
                    <div class="split style1 centered reversed">
                        <section>
				<iframe width="450" height="300" src="https://www.youtube.com/embed/eiPa-ND8Hz8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>                                                    </section>
                        <section>
                            <h2 style="text-align: center;font-style: bold">This is the story of a mom who was afraid of missing out on her baby's 
                                vaccine date but then found enough courage to use tech to solve it for Her! </h2>
                            <div class="features">
                                <article class="icon fa-calendar">
                                    <h3>Vaccine Schedule</h3>
                                    <p style="color:black;font-size:16px;">Vaccine due dates may be forgotten easily and can prove costly in long run. Knowledge about vaccines is meagre in even educated parents. Know the WHEN and WHY about Vaccines and Check if your Vaccinations are UP-TO-Date.</p>
                                </article>
                                <article class="icon fa-file">
                                    <h3>Medical Records</h3>
                                    <p style="color:black;font-size:16px;">Records get accumulated over years and keeping them at one place and tracking them is not easy. Retrieval when needed is also a challenge. </p>
                                </article>
                                <article class="icon fa-comments">
                                    <h3>Communication and Health Tips</h3>
                                    <p style="color:black;font-size:16px;">Communication between Pediatrician and Parents lack a personal touch. Updates and Notifications regarding health tips and 'Good to have' vaccines from your Paediatrician is always a service one would appreciate.
                                    </p>
                                </article>
                                <article class="icon fa-times-circle">
                                    <h3>Not Easy</h3>
                                    <p style="color:black;font-size:16px;">No Easy Way exists to outfit above problems in India. </p>
                                </article>
                            </div>
                        </section>
                    </div>
                </div>
            </section>

        <!-- Two -->
            <section id="two" class="wrapper style1 special">
                <div class="inner">
                    <div class="stats">
                        <article>
                            <h3><span>15</span> Years of various Vaccinations</h3>
                            <p style="color:white;font-size:15px;">Vaccine Schedule for babies is spread over fifteen years. 
                                Keeping track of vaccination over years is tough.  
                                It gets missed easily. Kidiatric takes care of that for you.
                            </p>
                        </article>
                        <article>
                            <h3><span>25</span> Vaccine Types</h3>
                            <p style="color:white;font-size:15px;">There are over 25 vaccines that are give to your child. Its important to know
                                what each does and their importance. Kidiatric tells you all of that and more.
                            </p>
                        </article>
                        <article>
                            <h3><span>100s</span> Medical Records</h3>
                            <p style="color:white;font-size:15px;">In just 5 years of your parenting you'd have over a 100 medical Records.
                                How do you even manage that?!   
                            </p>
                        </article>
                        <article>
                            <h3><span>1</span> Solution </h3>
                            <p style="color:white;font-size:15px;">To do all of these, there is an app now!<b><p style="color:white;font-size:18px;"> Kidiatric! </b></p>
                        </article>
                    </div>
                </div>
            </section>

        <!-- Three -->
            <section id="three" class="wrapper">
                <div class="inner">
                    <ul class="tabs">
                        <li>
                            <h3>Vaccine Reminder</h3>
                            <div class="split reversed">
                                <div class="content">
                                    <h2>Automated Vaccine Reminder and Health tip notifications</h2>
                                    <p style="color:black;font-size:14px;">
                                        <icon class="fa fa-plus-square"></icon>
                                       <b> A simple app + sms reminder to ensure you don't miss your child's vaccines
                                        <br>
                                        <icon class="fa fa-plus-square"></icon>
                                        Changeable schedule
                                        <br>
                                        <icon class="fa fa-plus-square"></icon>
                                        Important notifications from Paediatrician and Dieticians
                                        <br>
                                        <icon class="fa fa-plus-square"></icon>
                                        Manage vaccination for multiple kids </b>

                                    </p>
                                    <ul class="actions">
                                        <!--<li><a href="#" class="button">Learn More</a></li>-->
                                    </ul>
                                </div>
                                <div class="image"><img src="{{ asset('img/front/pic05.jpg') }}" alt="" /></div>
                            </div>
                        </li>
                        <li>
                            <h3>Records</h3>
                            <div class="split reversed">
                                <div class="content">
                                    <h2>Records Keeping</h2>
                                    <p style="color:black;font-size:14px;">

                                        <icon class="fa fa-medkit"></icon>
                                       <b> Upload and collate the medical records of your baby at one place within an app
                                        <br>
                                        <icon class="fa fa-medkit"></icon>
                                        Electronically saved medical history for lifetime that facilitates hassle free handling and retrieval </b>
                                    </p>
                                    <ul class="actions">
                                        <!--<li><a href="#" class="button">Learn More</a></li>-->
                                    </ul>
                                </div>
                                <div class="image"><img src="{{ asset('img/front/pic05b.jpg') }}" alt="" /></div>
                            </div>
                        </li>
                        <li>
                            <h3>Built for India</h3>
                            <div class="split reversed">
                                <div class="content">
                                    <h2>Custom Vaccines Schedules specifically for India</h2>
                                    <p style="color:black;font-size:14px;">

                                        <icon class="fa fa-user-md"></icon>
                                      <b>  Finally an app is here that is built for Indian babies
                                        by an Indian company after talking to Indian doctors
                                        <br>
                                        <icon class="fa fa-user-md"></icon>
                                        Vaccine schedule as per IAP recommended immunization schedule </b>
                                    </p>
                                    <ul class="actions">
                                        <!--<li><a href="#" class="button">Learn More</a></li>-->
                                    </ul>
                                </div>
                                <div class="image"><img src="{{ asset('img/front/pic05c.jpg') }}" alt="" /></div>
                            </div>
                        </li>
                    </ul>
                </div>
            </section>

        <!-- Four -->
            <section id="four" class="quotes">
                <article class="icon solid fa-quote-left">
                    <span class="image"><img src="{{ asset('img/front/pic03.jpg') }}" alt="" data-position="center" /></span>
                    <h2>"Keeping track of Vaccines and Medical records became very difficult when I gave birth to my second child when the elder one was only 2.5 years old. This is going to be VERY useful."<i class="fa fa-thumbs-up" aria-hidden="true"></i></h2><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i>
                    <div class="author">
                        <span class="name">Simoli Mehta</span>
                        <span class="title">Working Mother of Two</span>
                    </div>
                </article>
                <article class="icon solid fa-quote-left">
                    <span class="image"><img src="{{ asset('img/front/pic05.jpg') }}" alt="" data-position="left" /></span>
                    <h2>"Accessing the prescriptions on phone while traveling is a great functionality! Reminder system is a boon for working Mothers"</h2> <i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i>
                    <div class="author">
                        <span class="name">Riya</span>
                        <span class="title">Mother of a 3 year old</span>
                    </div>
                </article>
            </section>

        <!-- Five -->
            <section id="five" class="wrapper">
                <div class="inner">
                    <div class="split">
                        <section>
                            <span class="image fit"><img src="{{ asset('img/front/pic06.JPG') }}" alt="" /></span>
                            <!--<ul class="actions">
                                <li><a href="#" class="button">Learn More</a></li>
                            </ul>-->
                        </section>
                        <section>
                            <h2>Cant Wait to get you hands on Kidiatric App?</h2>
                            <p style="color:black;font-size:18px;"> DOWNLOAD the APP here! <br> <br>
<span><a href="https://play.google.com/store/apps/details?id=com.kidiatric&hl=en"><img src="https://i.ibb.co/xXn75T1/google-play.png" </a>
			<a href="https://apps.apple.com/in/app/kidiatric-my-vaccine-reminder/id1488453972"><img src"https://imgbb.com/"><img src="https://i.ibb.co/0VzHTQ3/apple-app-store-icon-e1485370451143.png" alt="apple-app-store-icon-e1485370451143" border="0"></a> </a> </span>
                    <ul class="actions special">

                                <p style="color:black;font-size:18px;">
				Also, just subscribe below & we will add you to our super parent list. <br> <br>OR <br><br> write to us at contact@kidiatric.app</p>
                            <!--<div class="ratings">
                                <article>
                                    <h3>Ipsum amet</h3>
                                    <div class="progress" data-progress="60">60%</div>
                                </article>
                                <article>
                                    <h3>Adipiscing feugiat</h3>
                                    <div class="progress" data-progress="90">90%</div>
                                </article>
                                <article>
                                    <h3>Dolor ligula</h3>
                                    <div class="progress" data-progress="75">75%</div>
                                </article>
                                <article>
                                    <h3>Libero magna</h3>
                                    <div class="progress" data-progress="45">45%</div>
                                </article>
                                <article>
                                    <h3>Nec lacinia</h3>
                                    <div class="progress" data-progress="55">55%</div>
                                </article>
                            </div>-->
                        </section>
                    </div>
                </div>
            </section>

        <!-- Footer -->
            <footer id="footer">
                <div class="inner">
                    <div class="split style1">
                        <div class="contact">
                            <h2>Contact</h2>
		
                            <ul class="contact-icons">

                                <li class="icon solid fa-home"><p style="color:white;font-size:16px;"><a href="#">Kidiatric - Tirupati House, Panjrapol<br> Ahmedabad 380015</a></li>
                                <li class="icon solid fa-phone"><p style="color:white;font-size:16px;"><a href="#">99987 23348</a></li>
                                <li class="icon solid fa-envelope"><p style="color:white;font-size:16px;"><a href="#">contact@kidiatric.app</a></li>
                            </ul>
                            <ul class="icons-bordered">
                                <li><a class="icon brands fa-facebook-f" href="https://www.facebook.com/kidiatric/">
                                    <span class="label">Facebook</span>
                                </a></li>
                                
                                <li><a class="icon brands fa-instagram" href="#">
                                    <span class="label">Instagram</span>
                                </a></li>
                                
                                <li><a class="icon brands fa-linkedin-in" href="#">
                                    <span class="label">LinkedIn</span>
                                </a></li>
                            </ul>
                        </div>
                        <form action="#" method="post">
                                                       <form action="#" method="post">
							<h2>Drop us a Hello! </h2>
							<div class="fields">
								<div class="field half">
									<input name="name" id="name" placeholder="Name" type="text" />
								</div>
								<div class="field half">
									<input name="email" id="email" placeholder="Email" type="email" />
								</div>
								<div class="field">
									<textarea name="message" id="message" rows="6" placeholder="Message"></textarea>
								</div>
							</div>
							<ul class="actions">
								<li><input value="Send Message" class="button" type="submit" /></li>
							</ul>
						</form>                                                   <div id="error_message" style="width:100%; height:100%; display:none; ">
                                                    <h4>Error</h4>
                                                    Sorry there was an error sending your form.
                                                    </div>
                                                    <div id="success_message" style="width:100%; height:100%; display:none; ">
                                                    <h2>Success! Your Message was Sent Successfully.</h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </form>
                    </div>
                    <div class="copyright">
                        <p>&copy; Kidiatric. All rights reserved.<br><a href="{{ route('privacyPolicy') }}" target="_blank">Privacy Policy</a> | <a href="{{ route('termsAndConditions') }}" target="_blank">Terms & Conditions</a></p>
                        <p></p>
                    </div>
                </div>
            </footer>

        <!-- Scripts -->
            <script src="{{ asset('js/home/jquery.min.js') }}"></script>
            <script src="{{ asset('js/home/jquery.dropotron.min.js') }}"></script>
            <script src="{{ asset('js/home/jquery.scrollex.min.js') }}"></script>
            <script src="{{ asset('js/home/jquery.scrolly.min.js') }}"></script>
            <script src="{{ asset('js/home/jquery.selectorr.min.js') }}"></script>
            <script src="{{ asset('js/home/browser.min.js') }}"></script>
            <script src="{{ asset('js/home/breakpoints.min.js') }}"></script>
            <script src="{{ asset('js/home/util.js') }}"></script>
            <script src="{{ asset('js/home/main.js') }}"></script>

    </body>
</html>