
<script src="{{ asset('assets/vendor/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/vendor/popper/popper.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/vendor/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-scrollbar/jquery.scrollbar.min.js') }}"   ></script>
<script src="{{ asset('assets/vendor/listjs/listjs.min.js') }}"></script>
<script src="{{ asset('assets/vendor/moment/moment.min.js') }}"></script>
<script src="{{ asset('assets/vendor/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap-notify/bootstrap-notify.min.js') }}"></script>

@if(route::is('doctor.login')  || route::is('doctor.postlogin'))
	<script type="text/javascript" src="{{ asset('js/pages/doc_login.js') }}"></script>
@endif
<script src="{{ asset('assets/vendor/DataTables/datatables.min.js') }}"></script>
<script src="{{ asset('js/front/datatable-data.js') }}"></script>

<script src="{{ asset('js/front/atmos.js')}}"></script>
<script src="{{ asset('assets/vendor/apexchart/apexcharts.min.js') }}"></script>

<!-- page level-->
@if(route::is('doctor.login')  || route::is('doctor.postlogin') || route::is('register') || route::is('doctor.register_mobile') || route::is('doctor.resendWebOtp') || route::is('doctor.check_otp'))
<script src="{{ asset('js/pages/login.js') }}"></script>
@endif

<!-- @if(route::is('register') || route::is('doctor.register_mobile') || route::is('doctor.resendWebOtp') || route::is('doctor.check_otp'))
<script src="{{ asset('js/pages/register.js') }}"></script>
@endif -->
<!-- dashboard chart -->
<script src="{{asset('js/front/dashboard-01.js')}}"></script>
<script src="{{ asset('js/pages/validation.js') }}"></script>
<!-- page level-->

<!--Additional Page includes-->
<script src="{{asset('assets/vendor/apexchart/apexcharts.min.js')}}"></script>


<script src="{{ asset('assets/vendor/dropify/js/dropify.js') }}" type="text/javascript"></script>

<script src="{{ asset('assets/vendor/ckeditor/ckeditor.js') }}" type="text/javascript"></script>

<script type="text/javascript">
	$('.dropify').dropify();
</script>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCG_sqbBhTzUacobqwsf3_QNbBaKu9dM_c&signed_in=true&libraries=places"></script>