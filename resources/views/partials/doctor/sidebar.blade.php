<aside class="admin-sidebar">
    <div class="admin-sidebar-brand">
        <!-- begin sidebar branding-->
        <a href="{{ route('doctor.dashboard') }}">
        <img class="admin-brand-logo" src="{{ asset('img/kiditric-logo.png') }}" width="40" alt="atmos Logo"></a>
        <!-- end sidebar branding-->
        <div class="ml-auto">
            <!-- sidebar pin-->
            <a href="#" class=""></a>
            <!-- sidebar close for mobile device-->
            <a href="#" class="admin-close-sidebar"></a>
        </div>
    </div>
    <div class="admin-sidebar-wrapper js-scrollbar">
        <ul class="menu">
            <li class="menu-item @if(route::is('doctor.dashboard')) active @endif ">
                <a href="{{ route('doctor.dashboard') }}" class="menu-link">
                    <span class="menu-label">
                        <span class="menu-name">Dashboard</span>
                    </span>
                    <span class="menu-icon">
                         <i class="icon-placeholder mdi mdi-chart-areaspline "></i>
                    </span>
                </a>
            </li>  

            <li class="menu-item @if(route::is('doctor.vaccinationList')) active opened @endif">
                <a href="{{ route('doctor.vaccinationList') }}" class="menu-link">
                    <span class="menu-label">
                        <span class="menu-name">Vaccination
                        </span>
                    </span>
                    <span class="menu-icon">
                        <i class="icon-placeholder mdi mdi-eyedropper "></i>
                    </span>
                </a>
            </li>

            <li class="menu-item @if(route::is('doctor.ParentsList')) opened @endif">
                <a href="{{ route('doctor.ParentsList') }}" class="menu-link">
                    <span class="menu-label">
                        <span class="menu-name">Parent Details
                        </span>
                    </span>
                    <span class="menu-icon">
                        <i class="icon-placeholder mdi mdi-account-child "></i>
                    </span>
                </a>
            </li>

            <li class="menu-item @if(route::is('doctor.wise.children')) active opened @endif">
                <a href="{{ route('doctor.wise.children') }}" class="menu-link">
                    <span class="menu-label">
                        <span class="menu-name">Children Details
                            <!-- <span class="menu-arrow"></span> -->
                        </span>
                    </span>
                    <span class="menu-icon">
                        <i class="icon-placeholder mdi mdi-emoticon-excited "></i>
                    </span>
                </a>
                
            </li>

            <li class="menu-item @if(route::is('doctor.createNotificationList') || route::is('doc.savedNotificationList')) active opened @endif">
                <a href="#" class="open-dropdown menu-link">
                    <span class="menu-label">
                        <span class="menu-name">Notifications
                            <span class="menu-arrow"></span>
                        </span>
                    </span>
                    <span class="menu-icon">
                        <i class="icon-placeholder mdi mdi-message-alert "></i>
                    </span>
                </a>
                <!--submenu-->

                <ul class="sub-menu" @if(route::is('doctor.createNotificationList') || route::is('doc.savedNotificationList')) style="display: block;" @endif>
                    <li class="menu-item @if(route::is('doctor.createNotificationList')) active @endif">
                        <a href="{{ route('doctor.createNotificationList') }}" class=" menu-link">
                            <span class="menu-label">
                                <span class="menu-name">Create Notification</span>
                            </span>
                            <span class="menu-icon">
                                <i class="icon-placeholder mdi mdi-message-draw "></i>
                            </span>
                        </a>
                    </li>
                    <li class="menu-item @if(route::is('doc.savedNotificationList')) opened @endif">
                        <a href="{{ route('doc.savedNotificationList') }}" class="menu-link">
                            <span class="menu-label">
                                <span class="menu-name">Notification List
                                    <!-- <span class="menu-arrow"></span> -->
                                </span>
                            </span>
                            <span class="menu-icon">
                                <i class="icon-placeholder mdi mdi-message-text "></i>
                            </span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="menu-item">
                <a href="javascript:void(0);" class="menu-link helpSupport">
                    <span class="menu-label">
                        <span class="menu-name">Help & Support
                            <!-- <span class="menu-arrow"></span> -->
                        </span>
                    </span>
                    <span class="menu-icon">
                        <i class="icon-placeholder mdi mdi-headset "></i>
                    </span>
                </a>
            </li>
            
        </ul>
    </div>

</aside>