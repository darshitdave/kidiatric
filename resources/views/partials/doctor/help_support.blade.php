<div class="modal fade"  id="example_04" tabindex="-1" role="dialog" aria-labelledby="example_02" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered modal-lg"  role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Help & Support</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="modalcontent">
                <div class="container-fluid ">
                    <div class="row ">
                        <div class="col-md-12">
                            <p>Please send your queries or suggestions from below, you can also contact Admin on +91-9998723348 or write on contact@kidiatric.app. Admin will contact you soon!</p>
                            <br>
                            <form class="needs-validation"  action="{{ route('doc.saveHelpSupportRequest') }}" method="post">
                                @csrf

                                <input type="hidden" value="{{ Auth::guard('doctor')->user()->id }}" name="doctor_id"> 
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <textarea class="form-control" name="message" rows="5" required></textarea> 
                                    </div>
                                    <div class="form-group col-md-12">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
