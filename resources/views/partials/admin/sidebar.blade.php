<aside class="admin-sidebar">
    <div class="admin-sidebar-brand">
        <!-- begin sidebar branding-->
        <a href="{{ route('admin.dashboard') }}">
            <img class="admin-brand-logo" src="{{ asset('img/kiditric-logo.png') }}" width="40" alt="atmos Logo"></a>
            <!-- end sidebar branding-->
            <div class="ml-auto">
                <!-- sidebar pin-->
                <a href="#" class=""></a>
                <!-- sidebar close for mobile device-->
                <a href="#" class="admin-close-sidebar"></a>
            </div>
        </div>
        <div class="admin-sidebar-wrapper js-scrollbar">
            <ul class="menu">
                
                <li class="menu-item @if(route::is('admin.dashboard')) active @endif ">
                    <a href="{{ route('admin.dashboard') }}" class="menu-link">
                        <span class="menu-label">
                            <span class="menu-name">Dashboard</span>
                        </span>
                        <span class="menu-icon">
                           <i class="icon-placeholder mdi mdi-chart-areaspline "></i>
                       </span>
                   </a>
                </li>  

                <li class="menu-item @if(route::is('admin.doctorList') || route::is('admin.addDoctor')) active opened @endif">
                    <a href="#" class="open-dropdown menu-link">
                        <span class="menu-label">
                            <span class="menu-name">Doctor
                                <span class="menu-arrow"></span>
                            </span>
                        </span>
                        <span class="menu-icon">
                            <i class="icon-placeholder mdi mdi-stethoscope "></i>
                        </span>
                    </a>

                    <ul class="sub-menu" @if(route::is('admin.doctorList') || route::is('admin.addDoctor')|| route::is('admin.viewDoctorDetails')|| route::is('admin.editDoctor')) style="display: block;" @endif>
                        <li class="menu-item @if(route::is('admin.addDoctor')) active @endif">
                            <a href="{{ route('admin.addDoctor') }}" class=" menu-link">
                                <span class="menu-label">
                                    <span class="menu-name">Add Doctor</span>
                                </span>
                                <span class="menu-icon">
                                    <i class="icon-placeholder mdi mdi-stethoscope "></i>
                                </span>
                            </a>
                        </li>
                        <li class="menu-item @if(route::is('admin.doctorList')) active @endif">
                            <a href="{{ route('admin.doctorList') }}" class=" menu-link">
                                <span class="menu-label">
                                    <span class="menu-name">Doctors List </span>
                                </span>
                                <span class="menu-icon">
                                    <i class="icon-placeholder mdi mdi-format-list-bulleted "></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="menu-item @if(route::is('admin.vaccinationList') || route::is('admin.addVaccination')) active opened @endif">
                    <a href="#" class="open-dropdown menu-link">
                        <span class="menu-label">
                            <span class="menu-name">Vaccination
                                <span class="menu-arrow"></span>
                            </span>
                        </span>
                        <span class="menu-icon">
                            <i class="icon-placeholder mdi mdi-eyedropper "></i>
                        </span>
                    </a>

                    <ul class="sub-menu" @if(route::is('admin.vaccinationList') || route::is('admin.addVaccination')|| route::is('admin.viewVaccinationDetails')|| route::is('admin.editVaccination')) style="display: block;" @endif>
                        <li class="menu-item @if(route::is('admin.addVaccination')) active @endif">
                            <a href="{{ route('admin.addVaccination') }}" class=" menu-link">
                                <span class="menu-label">
                                    <span class="menu-name">Add Vaccination</span>
                                </span>
                                <span class="menu-icon">
                                    <i class="icon-placeholder mdi mdi-eyedropper "></i>
                                </span>
                            </a>
                        </li>
                        <li class="menu-item @if(route::is('admin.vaccinationList')) active @endif">
                            <a href="{{ route('admin.vaccinationList') }}" class=" menu-link">
                                <span class="menu-label">
                                    <span class="menu-name">Vaccination List </span>
                                </span>
                                <span class="menu-icon">
                                    <i class="icon-placeholder mdi mdi-format-list-bulleted "></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="menu-item @if(route::is('admin.ParentsList')) opened @endif">
                    <a href="{{ route('admin.ParentsList') }}" class="menu-link">
                        <span class="menu-label">
                            <span class="menu-name">Parent Details
                                <!-- <span class="menu-arrow"></span> -->
                            </span>
                        </span>
                        <span class="menu-icon">
                            <i class="icon-placeholder mdi mdi-account-child "></i>
                        </span>
                    </a>
                </li>

                <li class="menu-item @if(route::is('admin.ChildrenList')) opened @endif">
                    <a href="{{ route('admin.ChildrenList') }}" class="menu-link">
                        <span class="menu-label">
                            <span class="menu-name">Children Details
                                <!-- <span class="menu-arrow"></span> -->
                            </span>
                        </span>
                        <span class="menu-icon">
                            <i class="icon-placeholder mdi mdi-emoticon-excited "></i>
                        </span>
                    </a>
                </li>

                <li class="menu-item @if(route::is('admin.savedNotificationList') || route::is('admin.doc.savedNotificationList')) active opened @endif">
                    <a href="#" class="open-dropdown menu-link">
                        <span class="menu-label">
                            <span class="menu-name">Notifications
                                <span class="menu-arrow"></span>
                            </span>
                        </span>
                        <span class="menu-icon">
                            <i class="icon-placeholder mdi mdi-message-alert "></i>
                        </span>
                    </a>

                    <ul class="sub-menu" @if(route::is('admin.createNotificationList') || route::is('admin.savedNotificationList')|| route::is('admin.doc.savedNotificationList')) style="display: block;" @endif>
                        <li class="menu-item @if(route::is('admin.createNotificationList')) active @endif">
                            <a href="{{ route('admin.createNotificationList') }}" class=" menu-link">
                                <span class="menu-label">
                                    <span class="menu-name">Create Notification</span>
                                </span>
                                <span class="menu-icon">
                                    <i class="icon-placeholder mdi mdi-message-draw "></i>
                                </span>
                            </a>
                        </li>
                        <li class="menu-item @if(route::is('admin.savedNotificationList')) active @endif">
                            <a href="{{ route('admin.savedNotificationList') }}" class=" menu-link">
                                <span class="menu-label">
                                    <span class="menu-name">Notification List</span>
                                </span>
                                <span class="menu-icon">
                                    <i class="icon-placeholder mdi mdi-message-text "></i>
                                </span>
                            </a>
                        </li>
                        <li class="menu-item @if(route::is('admin.doc.savedNotificationList')) active @endif">
                            <a href="{{ route('admin.doc.savedNotificationList') }}" class=" menu-link">
                                <span class="menu-label">
                                    <span class="menu-name">Doctor Notification List </span>
                                </span>
                                <span class="menu-icon">
                                    <i class="icon-placeholder mdi mdi-account-details "></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="menu-item @if(route::is('admin.helpSupport')) opened @endif">
                    <a href="{{ route('admin.helpSupport') }}" class="menu-link">
                        <span class="menu-label">
                            <span class="menu-name">Help & Support
                            </span>
                        </span>
                        <span class="badge badge-danger supportRequest" style="margin-left: -15px;"></span>
                    </a>
                </li>
            </ul>
        </div>
</aside>