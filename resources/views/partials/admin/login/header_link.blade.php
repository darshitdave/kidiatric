<link rel="icon" type="image/x-icon" href="{{ asset('img/kiditric-logo.png') }}"/>
<link rel="icon" href="{{ asset('img/kiditric-logo.png') }}" type="image/png" sizes="16x16">
<link rel="stylesheet" href="{{asset('assets/vendor/pace/pace.css')}}">
<script src="{{asset('assets/vendor/pace/pace.min.js')}}"></script>
<!--vendors-->
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendor/jquery-scrollbar/jquery.scrollbar.css')}}">
<link rel="stylesheet" href="{{asset('assets/vendor/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/vendor/jquery-ui/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{asset('css/developer.css')}}">
<link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
<link rel="stylesheet" href="{{asset('assets/vendor/timepicker/bootstrap-timepicker.min.css')}}">
<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,600" rel="stylesheet">

<link rel="stylesheet" href="{{asset('assets/vendor/DataTables/datatables.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/vendor/DataTables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css')}}">

<!--Material Icons-->
<link rel="stylesheet" type="text/css" href="{{asset('css/fonts/materialdesignicons/materialdesignicons.min.css')}}">
<!--Material Icons-->
<link rel="stylesheet" type="text/css" href="{{asset('css/fonts/feather/feather-icons.css')}}">
<!--Bootstrap + atmos Admin CSS-->
<link rel="stylesheet" type="text/css" href="{{asset('css/front/atmos.min.css')}}">
<!-- Additional library for page -->
<script src="{{ asset('assets/vendor/jquery/jquery.min.js') }}"></script>
<link href="{{ asset('assets/vendor/dropify/css/dropify.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
