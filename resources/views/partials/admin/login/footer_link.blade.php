
<script src="{{ asset('assets/vendor/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/vendor/popper/popper.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/vendor/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-scrollbar/jquery.scrollbar.min.js') }}"   ></script>
<script src="{{ asset('assets/vendor/listjs/listjs.min.js') }}"></script>
<script src="{{ asset('assets/vendor/moment/moment.min.js') }}"></script>
<script src="{{ asset('assets/vendor/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap-notify/bootstrap-notify.min.js') }}"></script>

<script src="{{ asset('assets/vendor/DataTables/datatables.min.js') }}"></script>
<script src="{{ asset('js/front/datatable-data.js') }}"></script>

<script src="{{ asset('js/front/atmos.min.js')}}"></script>
<script src="{{ asset('assets/vendor/apexchart/apexcharts.min.js') }}"></script>

<!-- page level-->
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>

<script src="{{ asset('js/pages/notification.js') }}"></script>
<script src="{{ asset('js/pages/login.js') }}"></script>
<script src="{{ asset('js/pages/validation.js') }}"></script>
<script src="{{ asset('js/pages/admin_vaccination.js') }}"></script>


<!-- dashboard chart -->
<script src="{{asset('js/front/dashboard-01.js')}}"></script>
<!-- page level-->
<!--Additional Page includes-->
<script src="{{asset('assets/vendor/apexchart/apexcharts.min.js')}}"></script>
<script src="{{ asset('assets/vendor/dropify/js/dropify.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendor/ckeditor/ckeditor.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/pages/developer.js') }}"></script>
	


<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCG_sqbBhTzUacobqwsf3_QNbBaKu9dM_c&signed_in=true&libraries=places"></script>
<script src="{{ asset('js/pages/doctor.js') }}"></script>

@if(route::is('admin.login'))
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

  <script>
    $(document).ready(function() {
      App.init();
    });
  </script>
  <script type="text/javascript">
    @if(Session::has('messages'))
      
       jQuery(document).ready(function() {
         @foreach(Session::get('messages') AS $msg) 
            toastr['{{$msg["type"]}}']('{{$msg["message"]}}');
         @endforeach

       });
       
     @endif
     @if (count($errors) > 0) 
      
      jQuery(document).ready(function() {
      
          @foreach($errors->all() AS $error)
           toastr.error('{{$error}}');
       @endforeach     
        });
       
  @endif
  </script>
@endif