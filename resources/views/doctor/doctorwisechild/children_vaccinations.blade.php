@extends('layouts.doctor.doctor')
@section('title','My Vaccinations | Kidiatric')
@section('content')
<section class="admin-content">
    <div class="bg-dark">
        <div class="container  m-b-30">
            <div class="row">
                <div class="col-12 text-white p-t-40 p-b-90">
                    <h4>My Vaccinations</h4>
                </div>
            </div>
        </div>
    </div>

     <div class="container pull-up">
        <div class="row m-b-30">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive p-t-10">
                            <table id="" class="table" style="width:100%">
                                <thead>
                                <tr>
                                    
                                    <th>Profile Image</th>
                                    <th>Name</th>
                                    <th>DOB</th>
                                    <th>Gender</th>
                                    <th>Doctor</th>
                                    <th>City Name</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        @if(!is_null($child_details->profile_image))
                                            <td><img src="https://kidiatric.s3.ap-south-1.amazonaws.com/uploads/child_image/{{$child_details->uuid}}/profile_image/{{$child_details->profile_image}}" class="css-class" style="height: 68px;"></img></td>
                                        @else
                                            @if($child_details->gender == 'Male')
                                                <td><img src="{{asset('img/boy.png')}}" class="css-class" style="height: 68px;"></img></td>
                                            @else
                                                <td><img src="{{asset('img/girl.png')}}" class="css-class" style="height: 68px;"></img></td>
                                            @endif
                                        @endif
                                        <td>{{ $child_details->child_name }}</td>
                                            <?php $dob = date('d-m-Y',strtotime($child_details->date_of_birth)); ?>
                                        <td>{{ $dob }}</td>
                                        <td>{{ $child_details->gender }}</td>
                                        @if(!is_null($child_details->doctor) && $child_details->doctor->doctor_id != '')
                                            <td>{{ $child_details->doctor->doctordetail->doctor_name }} <b>({{ $child_details->doctor->doctordetail->doctor_id }})</b></td>
                                        @else
                                            <td>Council</td>
                                        @endif
                                        @if(!is_null($child_details->location) && !is_null($child_details->location->city))
                                            <td>{{ $child_details->location->city->city_name }}</td>
                                        @else
                                            <td>-----</td>
                                        @endif

                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive p-t-10">
                            <table id="example-multi" class="table" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Sr.no</th>
                                    <th>Vaccination Name</th>
                                    <th>Vaccination Duration (Months)</th>
                                    <th>Status</th>
                                    <th>Due Date</th>
                                    <th>Taken Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!is_null($vaccination_data))
                                    @foreach($vaccination_data as $vk => $vv)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{$vv['name']}}</td>
                                            
                                            <?php $taken_at = $vv['taken_at']/30;
                                                $taken = number_format($taken_at,1);
                                             ?>
                                            <td>{{ $taken }}</td>

                                            <td>{{$vv['status']}}</td>
                                            <td>{{$vv['due_date']}}</td>
                                            <td>{{$vv['taken_date']}}</td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>		

@endsection
	
@section('js')

@endsection