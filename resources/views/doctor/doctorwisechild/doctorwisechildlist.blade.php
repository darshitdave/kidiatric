@extends('layouts.doctor.doctor')
@section('title','Patient List | Kidiatric')
@section('content')
<section class="admin-content">
    <div class="bg-dark">
        <div class="container  m-b-30">
            <div class="row">
                <div class="col-12 text-white p-t-40 p-b-90">
                    <h4 class="">
                        Patient List
                    </h4>
                </div>
            </div>
        </div>
    </div>  

        <div class="container pull-up">
        <div class="row m-b-30">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('doctor.wise.children') }}" method="post">
                            @csrf
                            <div class="form-row">

                                <div class="col-md-3 mb-3">
                                    <label for=""> Select Gender</label>
                                    <select class="form-control" id="gender" name="gender">
                                        <option value="">Select Gender</option>
                                        <option value="Male" @if($gender == 'Male') selected="selected" @endif>Male</option>
                                        <option value="Female" @if($gender == 'Female') selected="selected" @endif>Female</option>
                                    </select>
                                </div>

                                <div class="col-md-3 mb-3">
                                    <label for="">Min Age (months)</label>
                                    <input type="text" class="form-control" name="min_age" placeholder="Min Age Range" value="{{ $min_range }}">
                                </div>

                                <div class="col-md-3 mb-3">
                                    <label for="">Max Age (months)</label>
                                    <input type="text" class="form-control" name="max_age" placeholder="Max Age Range" value="{{ $max_range }}">
                                </div>

                                <div class="col-md-3 mb-3">
                                    <label for="">City</label>
                                    <select class="form-control" id="city_id" name="city_id">
                                        <option value="">Select City</option>
                                        @if(!is_null($cityList))
                                            @foreach($cityList as $ck => $cv)
                                                <option value="{{ $cv->id }}" @if($city_id == $cv->id) selected="selected" @endif>{{ $cv->city_name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                
                                <div class="col-md-2">
                                    <button type="submit" class="btn btn-primary mt-1" id="filter" name="save_and_list" value="save_and_list" >Filter Childern List</button>
                                </div>
                                @if($filter == 1)
                                    <div class="col-md-2">
                                        <a href="{{ route('doctor.wise.children') }}" class="btn btn-danger mt-1" id="filter" name="save_and_list" value="save_and_list" style="margin-left:-15px">Reset Filter</a>
                                    </div>
                                @endif
                            </div>                               
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form action="{{ route('doctor.childrenExcelData')}}" method="post" id="doctorForm">
        @csrf
        <input type="hidden" name="children_id" value="{{ json_encode($id_json) }}">
    </form>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <a href="javascript:void(0);" class="btn btn-primary mb-3" id="createChildDataCSV" name="save_and_list" value="save_and_list" style="float:right;margin-left: 10px;">Export Excel</a>
                            <form action="{{ route('doc.sendUserNotification')}}" method="post" id="notificationForm">
                            @csrf
                            <!-- <a href="javascript:void(0);" class="btn btn-primary mb-3" id="sendNotification" name="save_and_list" value="save_and_list" style="float:right;">Send Notification</a><br/><br/> -->
                            <div class="table-responsive p-t-10">
                                <table id="example-multi" class="table" style="width:100%">
                                    <thead>
                                    <tr>
                                        <!-- <th><input type="checkbox" name="checkbox" class="checkall"></th> -->
                                        <th>Sr.no</th>
                                        <th>Profile Image</th>
                                        <th>Name</th>
                                        <th>DOB</th>
                                        <th>Age(months)</th>
                                        <th>Gender</th>
                                        <th>City Name</th>
                                        <th>Vaccination List</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(!is_null($child_details))
                                        @foreach($child_details as $ck => $cv)

                                            <tr>
                                                <!-- <td><input type="checkbox" name="checkbox[]" class="checkbox" value="{{ $cv->id }}"></td> -->
                                                <td>{{ $loop->iteration }}</td>
                                                @if(!is_null($cv->profile_image))
                                                    <td><img src="https://kidiatric.s3.ap-south-1.amazonaws.com/uploads/child_image/{{$cv->uuid}}/profile_image/{{$cv->profile_image}}" class="css-class" style="height: 68px;"></img></td>
                                                @else
                                                    @if($cv->gender == 'Male')
                                                        <td><img src="{{asset('img/boy.png')}}" class="css-class" style="height: 68px;"></img></td>
                                                    @else
                                                        <td><img src="{{asset('img/girl.png')}}" class="css-class" style="height: 68px;"></img></td>
                                                    @endif
                                                @endif
                                                <td>{{ $cv->child_name }} </td>
                                                <td>{{ date('d-m-Y',strtotime($cv->date_of_birth)) }}</td>
                                                <td>{{ intval($cv->age / 30,0) }}</td>
                                                <td>{{ $cv->gender }}</td>
                                                @if(!is_null($cv->location) && !is_null($cv->location->city))
                                                    <td>{{ $cv->location->city->city_name }}</td>
                                                @else
                                                    <td>-----</td>
                                                @endif
                                                <td>
                                                    <a href="" class="btn m-b-15 ml-2 mr-2 btn-dark parents" data-id="{{$cv->id}}" onclick="{{$cv->id}}" data-toggle="modal" title="Parents Details" data-target="#example_02"><i class="fe fe-eye"></i></a>
                                                    <a href="{{ route('doctor.ChildrenVaccination',$cv->uuid) }}" target="_black"  class="btn m-b-15 ml-2 mr-2 btn-dark" title="Vaccination Details"><i class="fe fe-list"></i></a>
                                                     <a href="{{ route('doc.doctorWiseChildDocument',$cv->uuid) }}" target="_black"  class="btn m-b-15 ml-2 mr-2 btn-dark" title="Child Document"><i class="mdi mdi-file-document"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>

<!-- Modal -->
<div class="modal fade"  id="example_02" tabindex="-1" role="dialog" aria-labelledby="example_02" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md"  role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Parents Details</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body" id="modalcontent">
        
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                Close
            </button>
        </div>
      </div>
    </div>
</div>
@endsection
@section('js')

<script>

$(document).on('click', '.parents', function() {

    var id = $(this).data('id');
    
    $.ajax({
        type: 'post',
        url: "{{ route('doctor.ChildrenParentDetails') }}",
        data: {
            'id': id
        },
        success: function(data) {
           $('#modalcontent').html(data);
        }
    });
});

$(document).on('change','.checkall',function(){
        if(this.checked){
            $('.checkbox').prop('checked',true);
        } else {
            $('.checkbox').prop('checked',false);
        }
    });

$(document).on('click','#sendNotification',function(){
    var selected = [];
    $(".checkbox:checked").each(function(){
        selected.push($(this).val());
    });

    if(selected.length){
        $('#notificationForm').submit();
    } else {
        notification('Please Select Atleast One Parents','danger');
    }
});

function notification(message,type){

    $.notify({
    title: '',
        message: message
    }, {
        placement: {
            align: "right",
            from: "top"
        },
        timer: 500,
        type: type,
    });   
}

$(document).on('click','#createChildDataCSV',function(e){
    e.preventDefault();
    $('#doctorForm').submit();    
});

</script>
@endsection