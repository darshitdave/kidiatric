@extends('layouts.doctor.doctor')
@section('title','Patient List | Kidiatric')
@section('content')
<section class="admin-content">
    <div class="bg-dark">
        <div class="container  m-b-30">
            <div class="row">
                <div class="col-12 text-white p-t-40 p-b-90">
                    <h4 class="">
                        Create Notification
                    </h4>
                </div>
            </div>
        </div>
    </div>

    <form action="{{ route('doc.sendUserNotification')}}" method="post" id="notificationForm">
    @csrf
        <div class="container pull-up">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="alert alert-dark" role="alert">
                                <center>Step ( 1 / 2 )</center>
                            </div>
                            <a href="javascript:void(0);" class="btn btn-primary mb-3" id="sendNotification" name="save_and_list" value="save_and_list" style="float:right;">Send Notification</a><br/><br/>
                            <div class="table-responsive p-t-10">
                                <table id="example-multi" class="table" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th><input type="checkbox" name="checkbox" class="checkall"></th>
                                        <th>Sr.no</th>
                                        <th>Profile Image</th>
                                        <th>Name</th>
                                        <th>Date Of Birth</th>
                                        <th>Gender</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(!is_null($child_details))
                                        @foreach($child_details as $ck => $cv)

                                            <tr>
                                                <td><input type="checkbox" name="checkbox[]" class="checkbox" value="{{ $cv->id }}"></td>
                                                <td>{{ $loop->iteration }}</td>
                                                @if(!is_null($cv->profile_image))
                                                    <td><img src="https://kidiatric.s3.ap-south-1.amazonaws.com/uploads/child_image/{{ $cv->uuid }}/profile_image/{{$cv->profile_image}}" class="css-class" style="height: 68px;"></img></td>
                                                @else
                                                    <td><img src="{{asset('uploads/noimage/not-available.jpg')}}" class="css-class" style="height: 68px;"></img></td>
                                                @endif
                                                <td>{{ $cv->child_name }} </td>
                                                <?php $dob = date('d-m-Y',strtotime($cv->date_of_birth)); ?>
                                                <td>{{ $dob }}</td>
                                                <td>{{ $cv->gender }}</td>
                                                <td>
                                                    <a href="" class="btn m-b-15 ml-2 mr-2 btn-dark parents" data-id="{{$cv->id}}" onclick="{{$cv->id}}" data-toggle="modal" data-target="#example_02"><i class="fe fe-eye"></i></a>
                                                    <a href="{{ route('doctor.ChildrenVaccination',$cv->id) }}" target="_black"  class="btn m-b-15 ml-2 mr-2 btn-dark"><i class="fe fe-list"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>

<!-- Modal -->
<div class="modal fade"  id="example_02" tabindex="-1" role="dialog" aria-labelledby="example_02" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md"  role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Parents Details</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body" id="modalcontent">
        
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                Close
            </button>
        </div>
      </div>
    </div>
</div>
@endsection
@section('js')

<script>
$(document).on('click', '.parents', function() {

    var id = $(this).data('id');
    
    $.ajax({
        type: 'post',
        url: 'doctor-children-parentdetails',
        data: {
            'id': id
        },
        success: function(data) {
           $('#modalcontent').html(data);
        }
    });
});

$(document).on('change','.checkall',function(){
        if(this.checked){
            $('.checkbox').prop('checked',true);
        } else {
            $('.checkbox').prop('checked',false);
        }
    });

$(document).on('click','#sendNotification',function(){
    var selected = [];
    $(".checkbox:checked").each(function(){
        selected.push($(this).val());
    });

    if(selected.length){
        $('#notificationForm').submit();
    } else {
        notification('Please Select Atleast One Parents','danger');
    }
});

function notification(message,type){

    $.notify({
    title: '',
        message: message
    }, {
        placement: {
            align: "right",
            from: "top"
        },
        timer: 500,
        type: type,
    });   
}
</script>
@endsection