@extends('layouts.doctor.doctor')
@section('title','Child Document List | Kidiatric')
@section('content')
<section class="admin-content">
    <div class="bg-dark">
        <div class="container  m-b-30">
            <div class="row">
                <div class="col-12 text-white p-t-40 p-b-90">
                    <h4 class="">
                        Child Document List
                    </h4>
                </div>
            </div>
        </div>
    </div>

     <div class="container pull-up">
        <div class="row m-b-30">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        
                            <div class="table-responsive p-t-10">
                                <table id="" class="table" style="width:100%">
                                    <thead>
                                    <tr>
                                        
                                        <th>Profile Image</th>
                                        <th>Name</th>
                                        <th>DOB</th>
                                        <th>Gender</th>
                                        <th>Doctor</th>
                                        <th>City Name</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            @if(!is_null($child_details->profile_image))
                                                <td><img src="https://kidiatric.s3.ap-south-1.amazonaws.com/uploads/child_image/{{$child_details->uuid}}/profile_image/{{$child_details->profile_image}}" class="css-class" style="height: 68px;"></img></td>
                                            @else
                                                @if($child_details->gender == 'Male')
                                                    <td><img src="{{asset('img/boy.png')}}" class="css-class" style="height: 68px;"></img></td>
                                                @else
                                                    <td><img src="{{asset('img/girl.png')}}" class="css-class" style="height: 68px;"></img></td>
                                                @endif
                                            @endif
                                            <td>{{ $child_details->child_name }}</td>
                                                <?php $dob = date('d-m-Y',strtotime($child_details->date_of_birth)); ?>
                                            <td>{{ $dob }}</td>
                                            <td>{{ $child_details->gender }}</td>
                                            @if(!is_null($child_details->doctor) && $child_details->doctor->doctor_id != '')
                                                <td>{{ $child_details->doctor->doctordetail->doctor_name }} <b>({{ $child_details->doctor->doctordetail->doctor_id }})</b></td>
                                            @else
                                                <td>Council</td>
                                            @endif
                                            @if(!is_null($child_details->location) && !is_null($child_details->location->city))
                                                <td>{{ $child_details->location->city->city_name }}</td>
                                            @else
                                                <td>-----</td>
                                            @endif

                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <a href="{{ route('doc.downloadAllDocument',$child_details->id) }}" class="btn btn-primary mb-3" style="float:right;">Download All Documents</a><br/><br/>
                        <div class="table-responsive p-t-10">
                            <table id="example-multi" class="table" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Sr.no</th>
                                    <th>Title</th>
                                    <th>Prescription Date</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!is_null($getPrescription))
                                    @foreach($getPrescription as $nk => $nv)
                                        
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $nv->title }}</td>
                                            <td>{{ $nv->prescription_date}}</td>
                                            <td>
                                                <a href="{{ route('doc.authImage',$nv->image_token) }}" class="btn m-b-15 ml-2 mr-2 btn-dark" title="Download" target="_blank"><i class="mdi mdi-arrow-down-bold-box"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>  

<div class="modal fade"  id="example_02" tabindex="-1" role="dialog" aria-labelledby="example_02" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg"  role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body" id="modalcontent">
        
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                Close
            </button>
        </div>
      </div>
    </div>
</div>

@endsection
    
@section('js')


<script>

var today = new Date();
$('.js-datepicker').daterangepicker({ 
    maxDate: new Date(),
    locale: {
        format: 'DD-MM-Y'
    } 
});

$(document).on('click', '.nf', function() {

    if($(this).hasClass('modal_count')){

        var id = $(this).data('id');
        
        $.ajax({
            type: 'post',
            url: 'admin-notification-target-user',
            data: {
                'id': id
            },
            success: function(data) {
               $('#modalcontent').html(data);
               $('#exampleModalLabel').text('Targeted Users');
            }
        });
        
    } else{
        var id = $(this).data('id');

        $.ajax({
            type: 'post',
            url: 'admin-notification-image',
            data: {
                'id': id
            },
            success: function(data) {
               $('#modalcontent').html(data);
               $('#exampleModalLabel').text('Notification Message');
            }
        });
    }
});
</script>
@endsection
