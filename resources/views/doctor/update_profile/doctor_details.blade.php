@extends('layouts.doctor.doctor')
@section('title','Doctor Details | Kidiatric')
@section('content')
<section class="admin-content">
    <div class="bg-dark">
        <div class="container  m-b-30">
            <div class="row">
                <div class="col-12 text-white p-t-40 p-b-90">
                    <h4 class="">Doctor Details</h4>
                </div>
            </div>
        </div>
    </div>

    <div class="container  pull-up">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">

                <!--widget card begin-->
                <div class="card m-b-30">
                    <div class="card-header">
                        <h5 class="m-b-0">
                            Doctor Details
                        </h5>
                    </div>
                    <div class="card-body ">
                        <form method="post" action="{{ route('doctor.updateDoctor.details') }}" id="doctorForms">
                            @csrf
                            <div class="form-group">
                                <label for="inputDoctorName">Full Name<span class="mendetory">*</span></label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">Dr.</span>
                                    </div>
                                    <input type="text" class="form-control doctor_name" name="doctor_name" id="inputDoctorName" placeholder="Enter Full Name" value="{{Auth::guard('doctor')->user()->doctor_name}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputMoblileNumber">Doctor ID<span class="mendetory">*</span></label>
                                <input type="text" class="form-control" name="doctor_id"  placeholder="Enter Doctor Id" readonly="readonly" value="{{Auth::guard('doctor')->user()->doctor_id}}">
                                
                            </div>

                            <div class="form-group">
                                <label for="inputMoblileNumber">Personal Mobile Number<span class="mendetory">*</span></label>
                                <input type="tel" class="form-control mobile_check" name="mobile_number" maxlength="10" pattern="^\d{10}$" oninvalid="this.setCustomValidity('Please Enter valid Mobile Number')" oninput="setCustomValidity('')" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" id="inputMoblileNumber"  placeholder="Enter Personal Mobile Number" readonly="readonly" value="{{Auth::guard('doctor')->user()->mobile_number}}">
                                
                            </div>
                            
                            <div class="form-group">
                                <label for="inputEmail">Email ID<span class="mendetory">*</span></label>
                                <input type="email" class="form-control" name="email"  id="inputEmail"  placeholder="Enter Email ID" required @if($doctor_profile->email != '') value="{{$doctor_profile->email}}" @endif>
                            </div>

                            <input type="hidden" name="id" value="{{Auth::guard('doctor')->user()->id}}">
                            <div class="form-group">
                                <label for="inputYear">Practicing Since<span class="mendetory">*</span></label>
                                <input type="tel" class="form-control year_check" name="practicing_since" maxlength="4" pattern="^\d{4}$" oninvalid="this.setCustomValidity('Please Enter valid Year')" oninput="setCustomValidity('')" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" id="inputYear"  placeholder="Enter Practicing Since" required @if($doctor_profile->practicing_since != '') value="{{$doctor_profile->practicing_since}}" @endif>
                            </div>

                            <div class="form-group">
                                <label for="pacinput">City Name<span class="mendetory">*</span></label>
                                <input type="text" class="form-control" name="city" id="pacinput" placeholder="Enter City Name" required  @if($doctor_profile->ref_city_name != '') value="{{$doctor_profile->ref_city_name}}" @endif>
                                <input type="hidden" id="city" name="city_name"/>
                            </div>

                            <div class="form-group">
                                <label for="inputMoblileNumber">Pincode<span class="mendetory">*</span></label>
                                <input type="tel" class="form-control" name="pincode" maxlength="6" pattern="^\d{6}$" oninvalid="this.setCustomValidity('Please Enter valid Pincode')" oninput="setCustomValidity('')" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" id="inputMoblileNumber"  placeholder="Enter Pincode" required @if($doctor_profile->pincode != '') value="{{$doctor_profile->pincode}}" @endif>
                            </div>

                            <div class="form-group">
                                <label for="inputAddress">Address<span class="mendetory">*</span></label>
                                <textarea type="text" class="form-control" name="address" placeholder="Enter Address" id="inputAddress" required="" spellcheck="false"> @if($doctor_profile->address != '') {{$doctor_profile->address}} @endif </textarea>
                            </div>

                            <div class="form-group">
                                <label for="inputSpecialization">Specialization<span class="mendetory">*</span></label>
                                <textarea type="text" class="form-control" name="specialization" placeholder="Enter Specialization" id="inputSpecialization" spellcheck="false" required> @if($doctor_profile->specialization != '') {{$doctor_profile->specialization}} @endif </textarea>
                            </div>

                            <div class="contact_no">
                                @if(count($doctor_profile->clinic_number) > 0)
                                    @foreach($doctor_profile->clinic_number as $ck => $cv)

                                        <div class="form-group remove">
                                            <label for="inputContact">Clinic Phone Number<span class="mendetory">*</label>
                                            <input type="tel" class="form-control number" name="clinic_phone_number[]" id="inputContact"  placeholder="Enter Clinic Phone Number" value="{{ $cv->clinic_phone_number }}" style="width:90%" data-msg="Enter Clinic Phone Number" required="required">
                                            @if($ck != 0)
                                                <a href="javascript:void(0);" class="btn btn-danger removeInput" style="float:right;margin-top: -36px;"><i class="fe fe-trash"></i></a>
                                            @endif
                                        </div>
                                    @endforeach
                                @else
                                
                                    <div class="form-group">
                                        <label for="inputContact">Clinic Phone Number<span class="mendetory">*</span></label>
                                        <input type="tel" class="form-control number" name="clinic_phone_number[]" oninvalid="this.setCustomValidity('Please Enter valid Mobile Number')" oninput="setCustomValidity('')" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" id="inputContact" placeholder="Enter Clinic Phone Number" value=""  style="width:90%" data-msg="Please Enter Clinic Phone Number" required="required">
                                    </div>
                                @endif
                            </div>

                            <a href="javascript:void(0);" class="btn btn-info mt-2 addContactNo"><i class="fe fe-plus"></i></a><br><br><br>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary button_submit">Submit</button>
                                <a href="{{route('doctor.dashboard')}}"  class="btn btn-danger">Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
<script type="text/javascript">
    $(document).on('click','.addContactNo',function(){

        var html = '<div class="form-group remove"><label for="inputContact">Clinic Phone Number</label><input type="tel" class="form-control number" name="clinic_phone_number[]" id="inputContact" placeholder="Enter Clinic Phone Number" style="width:90%" data-msg="Please Enter Clinic Phone Number"><a href="javascript:void(0);" class="btn btn-danger removeInput" style="float:right;margin-top: -36px;"><i class="fe fe-trash"></i></a></div>';

        $('.contact_no').append(html);
    });

    $(document).on('click','.removeInput',function(){
        $(this).closest('.remove').remove();
    });

    $(document).on('keyup paste','.number',function(){
        this.value = this.value.replace(/[^0-9]/g, '');
    });

$("input.year_check").keyup(function(){
    $(".year_check").parent().next(".validation").remove();
});

$(document).on('focusout','.year_check',function(){

    var year = $("input.year_check").val();

    var str = "01/05/"+year;

    var year = str.match(/\/(\d{4})/)[1];
    var currYear = new Date().toString().match(/(\d{4})/)[1];
    
    if (year > currYear) {

        $(':input[type="submit"]').prop('disabled', true);

        if ($(".year_check").parent().next(".validation").length == 0) // only add if not added
        {
           $(".year_check").parent().after("<div class='validation' style='color:red;'>Please Enter Valid Year</div>");
        }
    } else {

        $(':input[type="submit"]').prop('disabled',false);
        $(".year_check").parent().next(".validation").remove();
    }
});

$(document).on('focusout', '.doctor_name', function() { 
    $(".doctor_name").parent().next(".validation").remove();
    var doctor_name = $('.doctor_name').val();
    if(doctor_name == ''){
        $(".doctor_name").parent().after("<div class='validation' style='color:red;margin:-10px 0 10px 6px'>Enter Full Name!</div>");
    }else{
        $(".doctor_name").parent().next(".validation").remove();
    }  
});

$(document).on('click','.button_submit',function(){
    $(".doctor_name").parent().next(".validation").remove();
    var doctor_name = $('.doctor_name').val();
    if(doctor_name == ''){
        $(".doctor_name").parent().after("<div class='validation' style='color:red;margin:-10px 0 10px 6px'>Enter Full Name!</div>");
    }else{
        $(".doctor_name").parent().next(".validation").remove();
    }
});

$(".doctor_name").keyup(function(){
    $(".doctor_name").parent().next(".validation").remove();
});

var isPlaceAuthentic = false;
var lastPlaceAuthenticated = '';

function initialize() {
    var input = (document.getElementById('pacinput'));
    var autocomplete = new google.maps.places.Autocomplete(input);
    var infowindow = new google.maps.InfoWindow();
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        infowindow.close();
        var place = autocomplete.getPlace();
        isPlaceAuthentic = true;
        lastPlaceAuthenticated = $('#pacinput').val();
        isPlaceAuthentic = false;
        var address = '';
        var toInsertData = place.adr_address;
        var latti = place.geometry.location.lat();
        var longi = place.geometry.location.lng();
        var addressToInsert = toInsertData.substr(0, toInsertData.indexOf('<')).trim();
        if (toInsertData.indexOf('<') !== 1) {
            toInsertData = toInsertData.substr(toInsertData.indexOf('<'));
        }
        var streetAddress = $(toInsertData).filter('.street-address').text().trim();
        var extendedAddress = $(toInsertData).filter('.extended-address').text().trim();
        var cityName = $(toInsertData).filter('.locality').text().trim();
        var stateName = $(toInsertData).filter('.region').text().trim();
        var countryName = $(toInsertData).filter('.country-name').text().trim();

        $("#latitude").val(latti);
        $("#longitude").val(longi);
        $("#city").val(cityName);
        $("#state").val(stateName);
        $("#country").val(countryName);
        
        var appenedAddress = addressToInsert.concat(streetAddress);
        appenedAddress = appenedAddress.concat(extendedAddress);
        if (place.address_components) {
            address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
        }
        infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
    });
}

google.maps.event.addDomListener(window, 'load', initialize);
$("#area_submit").on('click',function(e){
    if(($("#latitude").val() == "" || $("#longitude").val() == "")){
        alert("Enter Valid Address");
        e.preventDefault();
    }
});
</script>
@endsection