<div class="form-group floating-label col-md-12">
    <label>OTP</label>
    <input type="tel" required maxlength="20"  pattern="^\d{20}$"  oninvalid="this.setCustomValidity('Please Enter valid OTP')" oninput="setCustomValidity('')" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" class="form-control input-lg" name="otp" id="otp_verification" placeholder="Enter OTP">
    <input type="hidden" name="mobile_number" id="new_mobile_number" value="{{$mobile_number}}">
    
</div>