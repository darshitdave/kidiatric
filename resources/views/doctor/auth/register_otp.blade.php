<div class="form-group floating-label col-md-12">
    <label>OTP</label>
    <input type="tel" required maxlength="20"    class="form-control input-lg" name="otp" id="otp_verification" placeholder="Enter OTP">
    <input type="hidden" name="mobile_number" id="new_mobile_number" value="{{$mobile_number}}">
    <input type="hidden" name="doctor_name" value="{{$doctor_name}}">
</div>