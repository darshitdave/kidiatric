@extends('layouts.doctor.login.login')
@section('title','Kidiatric | Doctor Login')
@section('content')
<main class="admin-main  ">
    <div class="container-fluid">
        <div class="row ">
            <div class="col-lg-4  bg-white">
                <div class="row align-items-center m-h-100 card-ui">
                    <div class="mx-auto col-md-8">
                        <div class="p-b-20 text-center">
                            <p>
                                <img src="{{ asset('img/kiditric-logo.png') }}" width="80" alt="">

                            </p>
                            <p class="admin-brand-content">
                                Kidiatric
                            </p>
                        </div>
                        <h3 class="text-center p-b-20 fw-400">Doctor Login</h3>
                        <form class="needs-validation doctor_login" action="{{ route('doctor.postlogin') }}" id="doc_loginForm" method="post">
                          @csrf
                          <div class="mobile">
                            <div class="form-row">
                                <div class="form-group floating-label col-md-12">
                                    <label>Mobile Number</label>
                                    <input type="tel" maxlength="10"  pattern="^\d{10}$"  oninvalid="this.setCustomValidity('Please Enter valid Mobile Number')" oninput="setCustomValidity('')" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" class="form-control input-lg mobile_number" name="mobile_number" id="mobile_number" placeholder="Enter Mobile Number" required>
                                </div>
                            </div>
                            <div class="spinner-border text-info spinner-border-sm spiner" role="status" style="display: none;">
                                <span class="sr-only">Loading...</span>
                            </div>
                            <a type="javascript:void(0);" class="btn btn-primary btn-block btn-lg text-white next">Get OTP</a>
                        </div>
                        <div class="otp" style="display: none;">
                            <div class="form-row" id="otp">
                                
                            </div>
                            <button type="submit" class="btn btn-primary btn-block btn-lg">Login</button>
                        </div>
                        </form>
                        <p class="text-right p-t-10">
                            <a href="{{ route('register') }}" class="text-underline">Don't Have An Account?</a><br>
                            <a href="javascript:void(0);" class="text-underline resendOtp"  style="display:none">Resend OTP</a>
                        </p>
                        <center><div class="timer" style="display:none"><span id="time">05:30</span></div></center>
                    </div>

                </div>
            </div>
            <div class="col-lg-8 d-none d-md-block bg-cover" style="background-image: url('{{asset('img/dashboard-login-image.jpg')}}');">

            </div>
        </div>
    </div>
</main>

<div class="modal modal-slide-left  fade" id="siteSearchModal" tabindex="-1" role="dialog" aria-labelledby="siteSearchModal"
     aria-hidden="true">
    
</div>
@endsection

