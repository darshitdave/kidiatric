@extends('layouts.doctor.login.login')
@section('title','Kidiatric | Doctor Register')
@section('content')
<main class="admin-main  ">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4  bg-white">
                <div class="row align-items-center m-h-100 card-ui">
                    <div class="mx-auto col-md-8">
                        <div class="p-b-20 text-center">
                            <p>
                                <img src="{{asset('img/kiditric-logo.png')}}" width="80" alt="">

                            </p>
                            <p class="admin-brand-content">
                                Kidiatric
                            </p>
                        </div>
                        <h3 class="text-center p-b-20 fw-400">Doctor Register</h3>
                        <form class="needs-validation doctor_login" action="{{ route('doctor.postregister') }}" id="registerDoctor" method="post">
                          @csrf
                          <div class="mobile">

                            <div class="form-row">
                                <div class="form-group floating-label col-md-12">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Dr.</span>
                                        </div>
                                        <input type="text" class="form-control doctor_name" name="doctor_name" placeholder="Please Enter Name" required>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group floating-label col-md-12">
                                    <label>Mobile Number</label>
                                    <input type="tel" maxlength="10"  pattern="^\d{10}$"  oninvalid="this.setCustomValidity('Please Enter valid Mobile Number')" oninput="setCustomValidity('')" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" class="form-control input-lg" name="mobile_number" id="mobile_number" placeholder="Please Enter Mobile Number" required>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group floating-label col-md-12">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck1">
                                        <label class="custom-control-label" for="customCheck1">Accept</label>
                                        <a href="{{ route('termsAndConditions') }}" target="_blank" style="color: dodgerblue">Terms and Conditions.</a><br>
                                        <span id="term" style="color: red;"></span>
                                    </div>
                                </div>
                            </div>
                            
                            <a type="javascript:void(0);" class="btn btn-primary btn-block btn-lg text-white next">Get OTP</a>
                        </div>
                        <div class="otp" style="display: none;">
                            <div class="form-row" id="otp">
                                
                            </div>
                            <button type="submit" class="btn btn-primary btn-block btn-lg submit_next">Register</button>
                        </div>
                        </form>
                        <p class="text-right p-t-10">
                            <a href="{{ route('doctor.login') }}" class="text-underline">Already Have An Account?</a><br>
                            <a href="javascript:void(0);" class="text-underline resendOtp"  style="display:none">Resend OTP</a>
                        </p>
                        <center><div class="timer" style="display:none"><span id="time">05:00</span></div></center>
                    </div>

                </div>
            </div>
            <div class="col-lg-8 d-none d-md-block bg-cover" style="background-image: url('{{asset('img/dashboard-login-image.jpg')}}');">

            </div>
        </div>
    </div>
</main>

<div class="modal modal-slide-left  fade" id="siteSearchModal" tabindex="-1" role="dialog" aria-labelledby="siteSearchModal"
     aria-hidden="true">
    
</div>


@endsection

@section('js')
<script>
$(document).on('click', '.next', function() {

    $(".doctor_name").parent().next(".validation").remove();
    $("#mobile_number").parent().next(".validation").remove();

    var mobile_number = $('#mobile_number').val();
    var doctor_name = $('.doctor_name').val();
    var check_length = $('#mobile_number').val().length;

        if(doctor_name != ''){
            if(mobile_number != ''){
                if(check_length == 10){
                    $("#mobile_number").parent().next(".validation").remove();
                    $(".doctor_name").parent().next(".validation").remove();
                    if($('input[type="checkbox"]').prop("checked") == true){
                        $("#term").text('');
                    
                    $.ajax({
                        type: 'post',
                        url: '{{ route("doctor.register_mobile") }}',
                        data: {
                            'mobile_number': mobile_number,
                            'doctor_name': doctor_name,
                            '_token' : '{{csrf_token()}}'
                        },
                        success: function(data) {
                            if(data == 'false'){
                                
                                $("#mobile_number").parent().after("<div class='validation' style='color:red;margin:-10px 0 10px 6px'>Mobile Number Already Exists!</div>");

                            }else if(data == 'otp_wrong'){

                                $("#mobile_number").parent().after("<div class='validation' style='color:red;margin:-10px 0 10px 6px'>Entered OTP Is Wrong!</div>");

                            }else{

                                $("#mobile_number").parent().next(".validation").remove();
                                $('.mobile').remove();
                                $('.otp').show();
                                $('#otp').html(data);
                                $('#otp_verification').focus();
                                var fiveMinutes = 300;
                                display = $('#time');
                                startTimer(fiveMinutes,display);
                                $('.timer').show();
                                var interval = setInterval(function() {
                                    $('.timer').hide();
                                    $('.resendOtp').show();
                                    clearInterval(interval);
                                }, 300000);
                            }
                        }
                    });
                    }else{
                        $("#term").text('Please Check T&C');
                    }
                }else{

                    $("#mobile_number").parent().after("<div class='validation' style='color:red;margin:-10px 0 10px 6px'>Please Enter 10 Digit Mobile Number!</div>");
                }
            }else{
                $("#registerDoctor").find("#mobile_number-error").remove(); 
                $("#mobile_number").parent().after("<div class='validation' style='color:red;margin:-10px 0 10px 6px'>Please Enter Mobile Number!</div>");
            }
        }else{
            $("#registerDoctor").find("#mobile_number-error").remove(); 
            $("#registerDoctor").find("#doctor_name-error").remove(); 
            $("#mobile_number").parent().after("<div class='validation' style='color:red;margin:-10px 0 10px 6px'>Please Enter Mobile Number!</div>");
            $(".doctor_name").parent().after("<div class='validation' style='color:red;margin:-10px 0 10px 6px'>Please Enter Name!</div>");
        }
    
});

$(document).on('click', '.submit_next', function(e) {
    e.preventDefault();
    var mobile_number = $('#new_mobile_number').val();
    var otp = $('#otp_verification').val();
    if(otp != ''){
        $.ajax({
            type: 'post',
            url: '{{ route("doctor.check_otp") }}',
            data: {
                'mobile_number': mobile_number,
                'otp': otp,
                '_token' : '{{csrf_token()}}'
            },
            success: function(data) {
                if(data == 'false'){
                    $("#otp_verification").parent().next(".validation").remove();
                    $("#otp_verification").parent().after("<div class='validation' style='color:red;margin:-10px 0 10px 6px'>Invalid OTP!</div>");
                }else{

                    $("#otp_verification").parent().next(".validation").remove();
                    $("#registerDoctor").submit();
                }
            }
        });
    } else {
        $("#otp_verification").parent().next(".validation").remove();
        $("#otp_verification").parent().after("<div class='validation' style='color:red;margin:-10px 0 10px 6px'>Please Enter OTP!</div>");
    }

});

$("input#mobile_number").keyup(function(){
    $("#mobile_number").parent().next(".validation").remove();
});


$("input.doctor_name").keyup(function(){
    $(".doctor_name").parent().next(".validation").remove();
});

$('.next').click(function(){
    if($('input[type="checkbox"]').prop("checked") == false){
        $("#term").text('Please Check T&C');
    }

});

$('input[type="checkbox"]').click(function(){
    if($('input[type="checkbox"]').prop("checked") == true){
        $("#term").text('');
    }
});

function startTimer(duration) {
    var timer = duration, minutes, seconds;
    var timerInterval = setInterval(function () {
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.text(minutes + ":" + seconds);

        if (--timer < 0) {
            timer = duration;
            clearInterval(timerInterval);
        }
    }, 1000);
}

$(document).on('click','.resendOtp',function(){

    $(this).closest('.card-ui').append("<div class='loading-container'></div> ");
    $('.resendOtp').hide();
    $.ajax({
        type: 'post',
        url: 'resend-web-otp',
        data: {
            'mobile_number': $('#new_mobile_number').val(),
        },
        success: function(data) {
            $('.loading-container').remove();
            $('.timer').show();
            var fiveMinutes = 300,
            display = $('#time');
            startTimer(fiveMinutes, display);
            $('.timer').show();
            setInterval(function() {
                $('.timer').hide();
                $('.resendOtp').show();
            }, 300000);
        }
    });
});
</script>
@endsection
