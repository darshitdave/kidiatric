@extends('layouts.doctor.doctor')
@section('title','Doctor | Doctor Dashboard')
@section('content')
<section class="admin-content">
    <div class="container p-t-20">
        <div class="row">
            <div class="col-12 m-b-30">
                <h3>Today</h3>
            </div>
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-4">
                        <a href="{{ route('doctor.ParentsList',1) }}">
                            <!--widget card begin-->
                            <div class="card m-b-30">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col my-auto">
                                            <div class="h6 text-muted ">Parents </div>
                                        </div>

                                        <div class="col-auto my-auto">
                                            <div class="avatar">
                                                <div class="avatar-title rounded-circle badge-soft-primary"><i
                                                            class="mdi mdi-account-child"></i></div>

                                            </div>
                                        </div>
                                    </div>
                                    <h1 class="display-4 fw-600">{{$today_users}}</h1>
                                    <div class="h6">
                                    </div>
                                </div>
                            </div>  
                        </a>
                    </div>

                    <div class="col-lg-4">
                        <a href="{{ route('doctor.wise.children',1) }}">
                            <!--widget card begin-->
                            <div class="card m-b-30">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col my-auto">
                                            <div class="h6 text-muted ">Children </div>
                                        </div>

                                        <div class="col-auto my-auto">
                                            <div class="avatar">
                                                <div class="avatar-title rounded-circle badge-soft-primary"><i
                                                            class="mdi mdi-emoticon-excited"></i></div>

                                            </div>
                                        </div>
                                    </div>
                                    <h1 class="display-4 fw-600">{{$today_children}}</h1>
                                    <div class="h6">
                                    </div>
                                </div>
                            </div>
                            <!--widget card ends-->
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="row">
                    <div class="col-12 m-b-30">
                        <h3>This Week</h3>
                    </div>

                    <div class="col-lg-4">
                        <a href="{{ route('doctor.ParentsList',2) }}">
                            <!--widget card begin-->
                            <div class="card m-b-30">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col my-auto">
                                            <div class="h6 text-muted ">Parents </div>
                                        </div>

                                        <div class="col-auto my-auto">
                                            <div class="avatar">
                                                <div class="avatar-title rounded-circle badge-soft-primary"><i
                                                            class="mdi mdi-account-child"></i></div>

                                            </div>
                                        </div>
                                    </div>
                                    <h1 class="display-4 fw-600">{{$weekly_user}}</h1>
                                    <div class="h6">
                                        <!-- <span class="text-danger"> <i class="mdi mdi-arrow-bottom-right"></i> +0.65% </span>
                                        Less activity than usual. -->
                                    </div>
                                </div>
                            </div>
                            <!--widget card ends-->
                        </a>
                    </div>

                    <div class="col-lg-4">
                        <a href="{{ route('doctor.wise.children',2) }}">
                            <!--widget card begin-->
                            <div class="card m-b-30">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col my-auto">
                                            <div class="h6 text-muted ">Children </div>
                                        </div>

                                        <div class="col-auto my-auto">
                                            <div class="avatar">
                                                <div class="avatar-title rounded-circle badge-soft-primary"><i
                                                            class="mdi mdi-emoticon-excited"></i></div>

                                            </div>
                                        </div>
                                    </div>
                                    <h1 class="display-4 fw-600">{{$weekly_children}}</h1>
                                    <div class="h6">
                                        <!-- <span class="text-danger"> <i class="mdi mdi-arrow-bottom-right"></i> +0.65% </span>
                                        Less activity than usual. -->
                                    </div>
                                </div>
                            </div>
                            <!--widget card ends-->
                        </a>
                    </div>

                </div>
            </div>

            <div class="col-lg-12">
                <div class="row">
                    <div class="col-12 m-b-30">
                        <h3>This Month</h3>
                    </div>
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-4">
                                <a href="{{ route('doctor.ParentsList',3) }}">
                                    <!--widget card begin-->
                                    <div class="card m-b-30">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col my-auto">
                                                    <div class="h6 text-muted ">Parents </div>
                                                </div>

                                                <div class="col-auto my-auto">
                                                    <div class="avatar">
                                                        <div class="avatar-title rounded-circle badge-soft-primary"><i
                                                                    class="mdi mdi-account-child"></i></div>

                                                    </div>
                                                </div>
                                            </div>
                                            <h1 class="display-4 fw-600">{{$monthly_user}}</h1>
                                            <div class="h6">
                                            </div>
                                        </div>
                                    </div>
                                    <!--widget card ends-->
                                </a>
                            </div>
                            <div class="col-lg-4">
                                <a href="{{ route('doctor.wise.children',3) }}">
                                    <!--widget card begin-->
                                    <div class="card m-b-30">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col my-auto">
                                                    <div class="h6 text-muted ">Children </div>
                                                </div>

                                                <div class="col-auto my-auto">
                                                    <div class="avatar">
                                                        <div class="avatar-title rounded-circle badge-soft-primary"><i
                                                                    class="mdi mdi-emoticon-excited"></i></div>

                                                    </div>
                                                </div>
                                            </div>
                                            <h1 class="display-4 fw-600">{{$monthly_children}}</h1>
                                            <div class="h6">
                                            </div>
                                        </div>
                                    </div>
                                    <!--widget card ends-->
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="row">
                    <div class="col-12 m-b-30">
                        <h3>All Time</h3>
                    </div>
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-4">
                                <a href="{{ route('doctor.ParentsList') }}">
                                    <!--widget card begin-->
                                    <div class="card m-b-30">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col my-auto">
                                                    <div class="h6 text-muted ">Parents </div>
                                                </div>

                                                <div class="col-auto my-auto">
                                                    <div class="avatar">
                                                        <div class="avatar-title rounded-circle badge-soft-primary"><i
                                                                    class="mdi mdi-account-child"></i></div>

                                                    </div>
                                                </div>
                                            </div>
                                            <h1 class="display-4 fw-600">{{$all_user}}</h1>
                                            <div class="h6">
                                            </div>
                                        </div>
                                    </div>
                                    <!--widget card ends-->
                                </a>
                            </div>
                            <div class="col-lg-4">
                                <a href="{{ route('doctor.wise.children') }}">
                                    <!--widget card begin-->
                                    <div class="card m-b-30">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col my-auto">
                                                    <div class="h6 text-muted ">Children </div>
                                                </div>

                                                <div class="col-auto my-auto">
                                                    <div class="avatar">
                                                        <div class="avatar-title rounded-circle badge-soft-primary"><i
                                                                    class="mdi mdi-emoticon-excited"></i></div>

                                                    </div>
                                                </div>
                                            </div>
                                            <h1 class="display-4 fw-600">{{$all_children}}</h1>
                                            <div class="h6">
                                            </div>
                                        </div>
                                    </div>
                                    <!--widget card ends-->
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card">

                            <div class="card-body">
                                <div class="card-header">
                                    <h3 class=" m-b-0">Parents</h3>
                                </div>
                                <div id="chart-25"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="card">

                            <div class="card-body">
                                <div class="card-header">
                                    <h3 class=" m-b-0">Patients</h3>
                                </div>
                                <div id="chart-26"></div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


        </div>
    </div>
</section>

@endsection  

@section('js')
<script type="text/javascript">
if($("#chart-25").length ){
    var options = {
        chart: {
            height: 350,
            type: 'bar',
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    position: 'top', // top, center, bottom
                },
            }
        },
        
        series: [{
            name: 'Order',
            data: {{ json_encode($monthly_user_data) }}
        }],
        xaxis: {
            categories: {!! json_encode($user_month) !!},
            position: 'top',
            labels: {
                offsetY: -18,

            },
            axisBorder: {
                show: false
            },
            axisTicks: {
                show: false
            },
            crosshairs: {
                fill: {
                    type: 'gradient',
                    gradient: {
                        colorFrom: '#D8E3F0',
                        colorTo: '#BED1E6',
                        stops: [0, 100],
                        opacityFrom: 0.4,
                        opacityTo: 0.5,
                    }
                }
            },
            tooltip: {
                enabled: true,
                offsetY: -35,

            }
        },
        fill: {
            gradient: {
                shade: 'light',
                type: "horizontal",
                shadeIntensity: 0.25,
                gradientToColors: undefined,
                inverseColors: true,
                opacityFrom: 1,
                opacityTo: 1,
                stops: [50, 0, 100, 100]
            },
        },
        yaxis: {
            axisBorder: {
                show: false
            },
            axisTicks: {
                show: false,
            },
            labels: {
                show: false,
                formatter: function (val) {
                    return val;
                }
            }

        },
        title: {
            text: 'Month Wise Parents',
            floating: true,
            offsetY: 320,
            align: 'center',
            style: {
                color: '#444'
            }
        },
    }

    var chart = new ApexCharts(
        document.querySelector("#chart-25"),
        options
    );

    chart.render();

}

if($("#chart-26").length ){
    var options = {
        chart: {
            height: 350,
            type: 'bar',
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    position: 'top', // top, center, bottom
                },
            }
        },
        
        series: [{
            name: 'Order',
            data: {{ json_encode($monthly_child_data) }}
        }],
        xaxis: {
            categories: {!! json_encode($child_month) !!},
            position: 'top',
            labels: {
                offsetY: -18,

            },
            axisBorder: {
                show: false
            },
            axisTicks: {
                show: false
            },
            crosshairs: {
                fill: {
                    type: 'gradient',
                    gradient: {
                        colorFrom: '#D8E3F0',
                        colorTo: '#BED1E6',
                        stops: [0, 100],
                        opacityFrom: 0.4,
                        opacityTo: 0.5,
                    }
                }
            },
            tooltip: {
                enabled: true,
                offsetY: -35,

            }
        },
        fill: {
            gradient: {
                shade: 'light',
                type: "horizontal",
                shadeIntensity: 0.25,
                gradientToColors: undefined,
                inverseColors: true,
                opacityFrom: 1,
                opacityTo: 1,
                stops: [50, 0, 100, 100]
            },
        },
        yaxis: {
            axisBorder: {
                show: false
            },
            axisTicks: {
                show: false,
            },
            labels: {
                show: false,
                formatter: function (val) {
                    return val;
                }
            }

        },
        title: {
            text: 'Month Wise Patients',
            floating: true,
            offsetY: 320,
            align: 'center',
            style: {
                color: '#444'
            }
        },
    }

    var chart = new ApexCharts(
        document.querySelector("#chart-26"),
        options
    );

    chart.render();

}
</script>
@endsection 