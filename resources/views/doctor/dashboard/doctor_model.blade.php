<div class="container-fluid ">
    <div class="row ">
        <div class="col-md-12">
            <form class="needs-validation"  method="post">
                @csrf

                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label>Verification of your profile is pending, please contact admin if verification is not done within 48 hours.</label>
                        
                    </div>
                    
                </div>
            </form>
        </div>
    </div>
</div>