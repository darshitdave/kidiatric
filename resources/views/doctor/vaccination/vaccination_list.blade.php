@extends('layouts.doctor.doctor')
@section('title','Vaccination List | Kidiatric')
@section('content')
<section class="admin-content">
    <div class="bg-dark">
        <div class="container  m-b-30">
            <div class="row">
                <div class="col-12 text-white p-t-40 p-b-90">
                    <h4 class="">
                        Vaccination List
                    </h4>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="container pull-up">
        <div class="row m-b-30">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                    <form action="{{ route('admin.doctorList') }}" method="post">
                        @csrf
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->

    <div class="container pull-up">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive p-t-10">
                            <table id="example-multi" class="table" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Sr.no</th>
                                    <th>Name</th>
                                    <th>Taken At (Months)</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!is_null($doc_vac))
                                    @foreach($doc_vac as $dk => $dv)

                                        <tr style="background-color:{{ $final_array[$dv->taken_at] }}">
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $dv['vaccination']['vaccination_name'] }}<b>@if($dv->vaccination_type == '0') (c) @elseif ($dv->vaccination_type == '1') (G) @endif </b></td>
                                            <?php
                                                $value = $dv->taken_at;
                                                $final_val = $value/30;
                                            ?>
                                            <td>
                                                @if(isset($doctor_check))
                                                <input type="text" name="taken_at" data-id="{{$dv->id}}" class="taken at{{$dv->id}} close_{{$dv->id}} form-control" value="{{$final_val}}">&nbsp;

                                                <input class="edit{{$dv->id}} btn btn-success"  type="button" data-id="{{$dv->id}}"  value="✓" style="display: none" >

                                                <input class="close{{$dv->id}} btn btn-danger delete" type="button" value="x" data-id="{{$dv->id}}" data-value="{{$final_val}}" style="display: none">

                                                @else
                                                <input type="text" name="taken_at"  class="test taken form-control" readonly="readonly" value="{{$final_val}}" data-target="#example_3">&nbsp;
                                                @endif
                                                
                                            </td>
                                            <td>
                                                @php $checked = ''; @endphp
                                                @if($dv->is_active == 1)
                                                  @php $checked = 'checked'; @endphp
                                                @endif
                                                @if(isset($doctor_check))
                                                    <label class="cstm-switch ">
                                                        <input type="checkbox" name="option" value="1" class="cstm-switch-input changeStatus" data-id="{{ $dv->id }}" {{ $checked }}>
                                                        <span class="cstm-switch-indicator size-md "></span>
                                                        <span class="cstm-switch-description"></span>
                                                    </label>
                                                @else
                                                    <label class="cstm-switch ">
                                                        <input type="checkbox" name="option" value="1" class="cstm-switch-input changeStatus test" data-id="{{ $dv->id }}" {{ $checked }} data-target="#example_3">
                                                        <span class="cstm-switch-indicator size-md "></span>
                                                        <span class="cstm-switch-description"></span>
                                                    </label>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade"  id="example_3" tabindex="-1" role="dialog" aria-labelledby="example_02" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg"  role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Thank You For Registering</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body" id="modalcontent">
        
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                Close
            </button>
        </div>
      </div>
    </div>
</div>
@endsection
@section('js')
<script>
$(document).ready(function(){
    $('.edit').hide();
});
</script>
<script type="text/javascript">
$(document).on("change",'.changeStatus',function(){

    if(!$(this).hasClass('test')){

        if(this.checked){
            var status = "1";
        } else {
            var status = "0";
        }
        var vac_id = $(this).data('id');

        $.ajax({
            type: "post",
            url: '{{ route("doctor.vaccination.status") }}',
            data:{ 'status' : status,'id' : vac_id},
            success:function(data){
                if(data == 'true'){
                    if(status == 1){
                        msg = 'Status Enabled!';
                    } else {
                        msg = 'Status Disabled!';
                    }
                } else {        
                   msg = 'Something Went Wrong';
                }

                $.notify({ title: '', message: msg},{
                placement: {
                    align: "right",
                    from: "top"
                },
                    timer: 500,
                    type: 'success',
                });
            }
        });

    }else{
        $(this).prop('checked',true);
        var type = 'type';

        $.ajax({
            type: 'post',
            url: '{{ route("doctor.disable.model") }}',
            data: {

                'type': type
            },
            success: function(data) {
                $('#example_3').modal('show');
                $('#modalcontent').html(data);
            }
        });

    }
});

$(document).on('click', '.taken', function() {

    if(!$(this).hasClass('test')){
    
    var id = $(this).data('id');
    
    $('.edit'+id).show();
    $('.close'+id).show();
    
    }else{

        var type = 'type';

        $.ajax({
            type: 'post',
            url: '{{ route("doctor.disable.model") }}',
            data: {

                'type': type
            },
            success: function(data) {
               $('#example_3').modal('show'); 
               $('#modalcontent').html(data);
            }
        });
    }
    $(document).on('click','.edit'+id, function() {

        var taken_at = $("input.at"+id).val();

        $.ajax({
            type: 'post',
            url: '{{ route("doctor.saveTaken") }}',
            data: {
                'taken_at': taken_at,
                'id' : id
            },
            success: function(data) {
                if(data == 'true'){
                    location.reload();
                    $('.edit'+id).hide();
                    $('.close'+id).hide();
                    msg = 'Taken At Successfully Updated!';            
                }

                $.notify({ title: '', message: msg},{
                placement: {
                    align: "right",
                    from: "top"
                },
                    timer: 500,
                    type: 'success',
                });
            }
        });

    });    

    $(document).on('click','.close'+id, function() {
        $('.edit'+id).hide();
        $('.close'+id).hide();
    });

    $(document).on('click','.delete', function() {
        $('.close_'+$(this).data('id')).val($(this).data('value'));
        
    });
});


</script>

@endsection