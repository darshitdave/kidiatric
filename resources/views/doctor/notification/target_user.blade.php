<style type="text/css">
.message img{
    width: 850px !important;
    height: 330px !important;
}
</style>
<div class="container-fluid ">
    <div class="row ">
        <div class="col-md-12">
            <form class="needs-validation"  method="post">
                @csrf
                @if(count($target_user) > 0)
                 <table class="table" style="width:100%">
                        <thead>
                        <tr>
                            <th>Sr.no</th>
                            <th>Targeted User Name</th>
                            <th>Mobile Number</th>
                            <th>Email</th>
                        </tr>
                        </thead>
                            <tbody>
                            <?php $parent_id = array(); ?>                                
                            @if(!is_null($target_user))
                                @foreach($target_user as $tk => $tv)
                                    @if(!in_array($tv->user_id, $parent_id))
                                        <?php $parent_id[] = $tv->user_id;?>
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                                <?php $user_name = App\Model\User::where('id',$tv->user_id)->first(); ?>
                                            <td>{{ $user_name->name }}</td>
                                            <td>{{ $user_name->mobile_number }}</td>
                                            @if($user_name->email != '')
                                                <td>{{ $user_name->email }}</td>
                                            @else
                                                <td> --------- </td>
                                            @endif
                                        </tr>
                                    @endif
                                @endforeach
                            @endif
                            </tbody>
                    </table>
                @else
                    <h5>No Data Found</h5>
                @endif
            </form>
        </div>
    </div>
</div>