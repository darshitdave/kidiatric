@extends('layouts.admin.admin')
@section('title','View Doctor Details | Kidiatric')
@section('content')
<section class="admin-content">
    <div class="bg-dark">
        <div class="container  m-b-30">
            <div class="row">
                <div class="col-12 text-white p-t-40 p-b-90">
                    <h4 class="">View Doctor Details</h4>
                </div>
            </div>
        </div>
    </div>

    <div class="container  pull-up">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">

                <!--widget card begin-->
                <div class="card m-b-30">
                    <div class="card-header">
                        <h5 class="m-b-0">
                            View Doctor Details
                        </h5>
                    </div>
                    <div class="card-body ">
                        <form method="post" action="{{ route('admin.doctor.approval') }}" id="doctorForm">
                            @csrf
                            <div class="form-group">
                                <label for="inputDoctorName">Full Name<span class="mendetory">*</span></label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">Dr.</span>
                                    </div>
                                    <input type="text" class="form-control" name="doctor_name" id="inputDoctorName" placeholder="Full Name" required value="{{$doctor_list->doctor_name}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputMoblileNumber">Personal Mobile Number<span class="mendetory">*</span></label>
                                <input type="tel" class="form-control" name="mobile_number" maxlength="10" pattern="^\d{10}$" oninvalid="this.setCustomValidity('Please Enter valid Mobile Number')" oninput="setCustomValidity('')" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" id="inputMoblileNumber"  placeholder="Enter Mobile Number" required value="{{$doctor_list->mobile_number}}">
                                <input type="hidden" name="id" value="{{$doctor_list->id}}">
                            </div>
                            
                            <div class="form-group">
                                <label for="inputEmail">Email ID<span class="mendetory">*</span></label>
                                <input type="email" class="form-control" name="email"  id="inputEmail"  placeholder="Email ID" required value="{{$doctor_list->email}}">
                            </div>

                            <div class="form-group">
                                <label for="inputYear">Practicing Since<span class="mendetory">*</span></label>
                                <input type="tel" class="form-control year_check" name="practicing_since" maxlength="4" pattern="^\d{4}$" oninvalid="this.setCustomValidity('Please Enter valid Year')" oninput="setCustomValidity('')" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" id="inputYear"  placeholder="Enter Year" required value="{{$doctor_list->practicing_since}}">
                            </div>

                            <div class="form-group">
                                <label for="pacinput">City Name<span class="mendetory">*</span></label>
                                <input type="text" class="form-control" name="city" id="pacinput" placeholder="Location Search" required value="{{$doctor_list->ref_city_name}}">
                                <input type="hidden" id="city" name="city_name"/>
                            </div>

                            <div class="form-group">
                                <label for="inputMoblileNumber">Pincode<span class="mendetory">*</span></label>
                                <input type="tel" class="form-control" name="pincode" maxlength="6" pattern="^\d{6}$" oninvalid="this.setCustomValidity('Please Enter valid Pincode')" oninput="setCustomValidity('')" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" id="inputMoblileNumber"  placeholder="Enter Pincode" required value="{{$doctor_list->pincode}}">
                            </div>

                            <div class="form-group">
                                <label for="inputAddress">Address<span class="mendetory">*</span></label>
                                <textarea type="text" class="form-control" name="address" placeholder="Address" id="inputAddress" required="" spellcheck="false">{{$doctor_list->address}}</textarea>
                            </div>

                            <div class="form-group">
                                <label for="inputSpecialization">Specialization<span class="mendetory">*</span></label>
                                <textarea type="text" class="form-control" name="specialization" placeholder="Specialization" id="inputSpecialization"  spellcheck="false">{{$doctor_list->specialization}}</textarea>
                            </div>

                             <div class="contact_no">
                                @if(count($doctor_list->clinic_number) > 0)
                                    @foreach($doctor_list->clinic_number as $ck => $cv)
                                        <div class="form-group remove">
                                            <label for="inputContact">Clinic Phone Number<span class="mendetory">*</label>
                                            <input type="tel" class="form-control number" maxLength="8" name="clinic_phone_number[]" id="inputContact"  placeholder="Clinic Phone Number" value="{{ $cv->clinic_phone_number }}" style="width:90%" data-msg="Please Enter Clinic Phone Number">
                                            @if($ck != 0)
                                                <a href="javascript:void(0);" class="btn btn-danger removeInput" style="float:right;margin-top: -36px;"><i class="fe fe-trash"></i></a>
                                            @endif
                                        </div>
                                    @endforeach
                                @else
                                
                                    <div class="form-group">
                                        <label for="inputContact">Clinic Phone Number<span class="mendetory">*</span></label>
                                        <input type="tel" class="form-control number" name="clinic_phone_number[]" maxLength="8" pattern="^\d{8}$" oninvalid="this.setCustomValidity('Please Enter valid Mobile Number')" oninput="setCustomValidity('')" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" id="inputContact" placeholder="Clinic Phone Number" value=""  style="width:90%" data-msg="Please Enter Clinic Phone Number" >
                                    </div>
                                @endif
                            </div>

                            <a href="javascript:void(0);" class="btn btn-info mt-2 addContactNo"><i class="fe fe-plus"></i></a><br><br><br>

                            <div class="form-group">
                                <button type="submit" name="approval" value="1" class="btn btn-success">Approve</button>
                                <button type="submit" name="approval" value="2" class="btn btn-info">Disapprove</button>
                                <a  href="{{route('admin.doctorList')}}"  class="btn btn-danger">Cancel</a>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

