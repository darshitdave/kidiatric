@extends('layouts.admin.admin')
@section('title','Doctor List | Kidiatric')
@section('content')
<section class="admin-content">
    <div class="bg-dark">
        <div class="container  m-b-30">
            <div class="row">
                <div class="col-12 text-white p-t-40 p-b-90">
                    <h4 class="">
                        Doctor List
                    </h4>
                </div>
            </div>
        </div>
    </div>
    <div class="container pull-up">
        <div class="row m-b-30">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('admin.doctorList') }}" method="post">
                        @csrf
                            <div class="form-row">

                                <div class="col-md-4 mb-3">
                                    <label for=""> Select Status</label>
                                    <select class="form-control" id="approval" name="approval_status">
                                        <option value="4" @if($approval_id == 4) selected="selected" @endif >All</option>
                                        <option value="0" @if($approval_id == 0) selected="selected" @endif >Pending</option>
                                        <option value="1" @if($approval_id == 1) selected="selected" @endif >Approved</option>
                                        <option value="2" @if($approval_id == 2) selected="selected" @endif >Disapproved</option>
                                        <option value="3" @if($approval_id == 3) selected="selected" @endif >Soft Delete</option>
                                    </select>
                                </div>

                                <div class="col-md-4 mb-3">
                                    <label for=""> Select city</label>
                                    <select class="form-control" id="paper_type" name="city_id">
                                        <option value="">Select City</option>
                                        @if(!is_null($city_list))
                                            @foreach($city_list as $ck => $cv)
                                                <option value="{{ $cv->id }}" @if($city == $cv->id) selected="selected" @endif>{{ $cv->city_name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                
                                <div class="col-md-2 mt-4">
                                    <button type="submit" class="btn btn-primary mt-1" id="filter" name="save_and_list" value="save_and_list" >Filter Doctor List</button>
                                </div>
                                
                                <div class="col-md-2 mt-4">
                                    <a href="{{ route('admin.doctorList') }}" class="btn btn-danger mt-1" id="filter" name="save_and_list" value="save_and_list">Reset Filter</a>
                                </div>
                               
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form action="{{ route('admin.doctor_csv')}}" method="post" id="doctorForm">
    @csrf
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <a href="javascript:void(0);" class="btn btn-primary mb-3" id="createDoctorCSV" name="save_and_list" value="save_and_list" style="float:right;">Export Excel</a><br/><br/>
                        <div class="table-responsive p-t-10">
                            <table id="example-multi" class="table" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Sr.no</th>
                                    <th>Name</th>
                                    <th>City</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>Approved Status</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!is_null($doctor_list))
                                    @foreach($doctor_list as $dk => $dv)
                                    
                                        <tr>
                                            <input type="hidden" name="doctor_id" value="{{$id_json}}">
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $dv->doctor_name }}</td>
                                            @if(!is_null($dv->city_name))
                                                <td>{{ $dv->city_name->city_name}}</td>
                                            @else
                                                <td>-----------</td>
                                            @endif
                                            <td>{{ $dv->email }}</td>
                                            <td>{{ $dv->mobile_number }}</td>
                                            <td>
                                                @if($dv->is_approve == 1)
                                                    <span class="badge badge-primary">Approved</span>
                                                @elseif($dv->is_approve == 2)
                                                    <span class="badge badge-danger">Disapproved</span>
                                                @else
                                                    <span class="badge badge-warning">Pending</span>
                                                @endif
                                            </td>
                                            <td>
                                                @php $checked = ''; @endphp
                                                @if($dv->is_active == 1)
                                                  @php $checked = 'checked'; @endphp
                                                @endif
                                                <label class="cstm-switch ">
                                                    <input type="checkbox" name="option" value="1" title="Status" class="cstm-switch-input changeCategory" data-id="{{ $dv->id }}" {{ $checked }}>
                                                    <span class="cstm-switch-indicator size-md "></span>
                                                    <span class="cstm-switch-description"></span>
                                                </label>
                                            </td>
                                            <td>
                                                <a href="{{ route('admin.editDoctor',$dv->id) }}" class="btn m-b-15 ml-2 mr-2 btn-dark" title="Edit"><i class="fe fe-edit"></i></a>
                                                <a href="{{ route('admin.deleteDoctor',$dv->id) }}" title="Delete" class="btn m-b-15 ml-2 mr-2 btn-dark" onclick="return confirm('Are your sure want to remove this information ?')"><i class="fe fe-trash"></i></a>
                                                <a href="{{ route('admin.doctor.vaccination',$dv->id) }}" title="Vaccination List" class="btn m-b-15 ml-2 mr-2 btn-dark"><i class="fe fe-list"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</section>
@endsection
