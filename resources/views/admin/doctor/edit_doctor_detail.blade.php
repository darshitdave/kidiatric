@extends('layouts.admin.admin')
@section('title','Update Doctor Details | Kidiatric')
@section('content')
<section class="admin-content">
    <div class="bg-dark">
        <div class="container  m-b-30">
            <div class="row">
                <div class="col-12 text-white p-t-40 p-b-90">
                    <h4 class="">Update Doctor Details</h4>
                </div>
            </div>
        </div>
    </div>

    <div class="container  pull-up">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">

                <!--widget card begin-->
                <div class="card m-b-30">
                    <div class="card-header">
                        <h5 class="m-b-0">
                            Update Doctor Details
                        </h5>
                    </div>
                    <div class="card-body ">
                        <form method="post" action="{{ route('admin.doctor.approval') }}" id="doctorForms">
                            @csrf
                            <div class="form-group">
                                <label for="inputDoctorName">Full Name<span class="mendetory">*</span></label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">Dr.</span>
                                    </div>
                                    <input type="text" class="form-control doctor_name" name="doctor_name" id="inputDoctorName" placeholder="Enter Full Name" value="{{$doctor_list->doctor_name}}">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="inputMoblileNumber">Doctor ID<span class="mendetory">*</span></label>
                                <input type="text" class="form-control" name="doctor_id" placeholder="Enter Doctor ID"  value="{{$doctor_list->doctor_id}}" readonly="readonly">
                            </div>

                            <div class="form-group">
                                <label for="inputMoblileNumber">Personal Mobile Number<span class="mendetory">*</span></label>
                                <input type="tel" class="form-control" name="mobile_number" maxlength="10" pattern="^\d{10}$" oninvalid="this.setCustomValidity('Please Enter valid Mobile Number')" oninput="setCustomValidity('')" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" id="inputMoblileNumber"  placeholder="Enter Personal Mobile Number" required value="{{$doctor_list->mobile_number}}">
                                <input type="hidden" name="id" value="{{$doctor_list->id}}">
                            </div>
                            
                            <div class="form-group">
                                <label for="inputEmail">Email ID<span class="mendetory">*</span></label>
                                <input type="email" class="form-control" name="email"  id="inputEmail"  placeholder="Enter Email ID" required value="{{$doctor_list->email}}">
                            </div>

                            <div class="form-group">
                                <label for="inputYear">Practicing Since<span class="mendetory">*</span></label>
                                <input type="tel" class="form-control year_check" name="practicing_since" maxlength="4" pattern="^\d{4}$" oninvalid="this.setCustomValidity('Please Enter valid Year')" oninput="setCustomValidity('')" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" id="inputYear"  placeholder="Enter Practicing Since" required value="{{$doctor_list->practicing_since}}">
                            </div>

                            <div class="form-group">
                                <label for="pacinput">City Name<span class="mendetory">*</span></label>
                                <input type="text" class="form-control" name="city" id="pacinput" placeholder="Enter City Name" required value="{{$doctor_list->ref_city_name}}">
                                <input type="hidden" id="city" name="city_name"/>
                            </div>

                            <div class="form-group">
                                <label for="inputMoblileNumber">Pincode<span class="mendetory">*</span></label>
                                <input type="tel" class="form-control" name="pincode" maxlength="6" pattern="^\d{6}$" oninvalid="this.setCustomValidity('Please Enter valid Pincode')" oninput="setCustomValidity('')" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" id="inputMoblileNumber"  placeholder="Enter Pincode" required value="{{$doctor_list->pincode}}">
                            </div>

                            <div class="form-group">
                                <label for="inputAddress">Address<span class="mendetory">*</span></label>
                                <textarea type="text" class="form-control" name="address" placeholder="Enter Address" id="inputAddress" required="" spellcheck="false">{{$doctor_list->address}}</textarea>
                            </div>

                            <div class="form-group">
                                <label for="inputSpecialization">Specialization<span class="mendetory">*</span></label>
                                <textarea type="text" class="form-control" required="required" name="specialization" placeholder="Enter Specialization" id="inputSpecialization"  spellcheck="false">{{$doctor_list->specialization}}</textarea>
                            </div>

                            <div class="contact_no">
                                @if(count($doctor_list->clinic_number) > 0)
                                    @foreach($doctor_list->clinic_number as $ck => $cv)

                                        <div class="form-group remove">
                                            <label for="inputContact">Clinic Phone Number<span class="mendetory">*</label>
                                            <input type="tel" class="form-control number" name="clinic_phone_number[]" id="inputContact"  placeholder="Enter Clinic Phone Number" value="{{ $cv->clinic_phone_number }}" style="width:90%" data-msg="Please Enter Clinic Phone Number" required>
                                            @if($ck != 0)
                                                <a href="javascript:void(0);" class="btn btn-danger removeInput" style="float:right;margin-top: -36px;"><i class="fe fe-trash"></i></a>
                                            @endif
                                        </div>
                                    @endforeach
                                @else
                                
                                    <div class="form-group">
                                        <label for="inputContact">Clinic Phone Number<span class="mendetory">*</span></label>
                                        <input type="tel" class="form-control number" name="clinic_phone_number[]" oninvalid="this.setCustomValidity('Please Enter valid Mobile Number')" oninput="setCustomValidity('')" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" id="inputContact" placeholder="Enter Clinic Phone Number" value=""  style="width:90%" data-msg="Please Enter Clinic Phone Number" required>
                                    </div>
                                @endif
                            </div>

                            <a href="javascript:void(0);" class="btn btn-info mt-2 addContactNo"><i class="fe fe-plus"></i></a><br><br>

                            <div class="form-group">
                                <label for=""> Select Status</label>
                                <select class="form-control" id="approval" name="approval">
                                    <option value="">Select Approval Status</option>
                                    <option value="0" @if($doctor_list->is_approve == 0) selected="selected" @endif>Pending</option>
                                    <option value="1" @if($doctor_list->is_approve == 1) selected="selected" @endif>Approve</option>
                                    <option value="2" @if($doctor_list->is_approve == 2) selected="selected" @endif>Disapprove</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <button type="submit" name="submit" class="btn btn-success button_submit">Submit</button>
                                <a  href="{{route('admin.doctorList')}}"   class="btn btn-danger">Cancel</a>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
<script>
$(document).on('click','.addContactNo',function(){

    var html = '<div class="form-group remove"><label for="inputContact">Clinic Phone Number</label><input type="tel" class="form-control number" name="clinic_phone_number[]" id="inputContact" placeholder="Enter Clinic Phone Number" style="width:90%" data-msg="Please Enter Clinic Phone Number" ><a href="javascript:void(0);" class="btn btn-danger removeInput" style="float:right;margin-top: -36px;"><i class="fe fe-trash"></i></a></div>';

    $('.contact_no').append(html);
    $('.number').attr('required',true);
});

$(document).on('focusout', '.doctor_name', function() { 
    $(".doctor_name").parent().next(".validation").remove();
    var doctor_name = $('.doctor_name').val();
    if(doctor_name == ''){
        $(".doctor_name").parent().after("<div class='validation' style='color:red;margin:-10px 0 10px 6px'>Enter Full Name!</div>");
    }else{
        $(".doctor_name").parent().next(".validation").remove();
    }  
});

$(document).on('click','.button_submit',function(){
    $(".doctor_name").parent().next(".validation").remove();
    var doctor_name = $('.doctor_name').val();
    if(doctor_name == ''){
        $(".doctor_name").parent().after("<div class='validation' style='color:red;margin:-10px 0 10px 6px'>Enter Full Name!</div>");
    }else{
        $(".doctor_name").parent().next(".validation").remove();
    }
});

$(".doctor_name").keyup(function(){
    $(".doctor_name").parent().next(".validation").remove();
});
</script>
@endsection