@extends('layouts.admin.admin')
@section('title','Vaccination List | Kidiatric')
@section('content')
<section class="admin-content">
    <div class="bg-dark">
        <div class="container  m-b-30">
            <div class="row">
                <div class="col-12 text-white p-t-40 p-b-90">
                    <h4 class="">
                        Vaccination List
                    </h4>
                </div>
            </div>
        </div>
    </div>
    <div class="container pull-up">
        <div class="row m-b-30">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('admin.doctor.vaccinationList') }}" method="post">
                        @csrf
                            <div class="form-row">

                                <div class="col-md-4 mb-3">
                                    <label for=""> Select Doctor</label>
                                    <select class="form-control" id="paper_type" name="doctor_id">
                                        <option value="">Select Doctor</option>
                                        @if(!is_null($doctor_name))
                                            @foreach($doctor_name as $dk => $dv)
                                                <option value="{{ $dv->id }}" @if($doc_name == $dv->id) selected="selected" @endif>{{ $dv->doctor_name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                
                                <div class="col-md-4 mb-3">
                                    <label for=""> Select Vaccination</label>
                                    <select class="form-control" id="paper_type" name="vacc_id">
                                        <option value="">Select Vaccination</option>
                                        @if(!is_null($vac_name))
                                            @foreach($vac_name as $vk => $vv)
                                                <option value="{{ $vv->id }}" @if($vacc_name == $vv->id) selected="selected" @endif>{{ $vv->vaccination_name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>

                                <div class="col-md-2 mt-4">
                                    <button type="submit" class="btn btn-primary mt-1" id="filter" name="save_and_list" value="save_and_list" >Filter List</button>
                                </div>
                                
                                <div class="col-md-2 mt-4">
                                    <a href="{{ route('admin.doctor.vaccinationList') }}" class="btn btn-danger mt-1" id="filter" name="save_and_list" value="save_and_list">Reset Filter</a>
                                </div>
                            </div>
                        </form>   
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive p-t-10">
                            <table id="example-multi" class="table" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Sr.no</th>
                                    <th>Doctor Name</th>
                                    <th>Vac Name</th>
                                    <th>Taken At (Months)</th>
                                    <th>Updated At</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                
                                @if(!is_null($doc_vac))
                                    @foreach($doc_vac as $dk => $dv)
                                        <tr style="background-color:{{ $final_array[$dv->taken_at] }}" class="{{ $final_array[$dv->taken_at] }}asdfasfsadh;sdkldfhkl;sdafh">

                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $dv->doctors->doctor_name}}</td>
                                            <td>{{ $dv->vaccination_name }}</td>
                                            <?php
                                                $value = $dv->taken_at;
                                                $final_val = $value/30;
                                            ?>
                                            <td>
                                                <input type="text" name="taken_at" data-id="{{$dv->id}}" class="taken at{{$dv->id}} form-control" value="{{$final_val}}" disabled="disabled">&nbsp;

                                                <input class="edit{{$dv->id}} btn btn-success"  type="button" data-id="{{$dv->id}}"  value="✓" style="display: none" >

                                                <input class="close{{$dv->id}} btn btn-danger" type="button" value="x" data-id="{{$dv->id}}" style="display: none"></td>

                                            <td>{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $dv->updated_at)->format('d-m-Y H:i:s') }}</td>

                                            <td>
                                                @php $checked = ''; @endphp
                                                @if($dv->is_active == 1)
                                                  @php $checked = 'checked'; @endphp
                                                @endif
                                                <label class="cstm-switch ">
                                                    <input type="checkbox" name="option" value="1" class="cstm-switch-input changeStatus" data-id="{{ $dv->id }}" {{ $checked }}>
                                                    <span class="cstm-switch-indicator size-md "></span>
                                                    <span class="cstm-switch-description"></span>
                                                </label>
                                            </td>

                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
