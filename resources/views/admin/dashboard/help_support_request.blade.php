@extends('layouts.admin.admin')
@section('title','Help & Support List | Kidiatric')
@section('content')
<section class="admin-content">
    <div class="bg-dark">
        <div class="container  m-b-30">
            <div class="row">
                <div class="col-12 text-white p-t-40 p-b-90">
                    <h4 class="">
                        Help & Support List
                    </h4>
                </div>
            </div>
        </div>
    </div>

    <div class="container pull-up">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive p-t-10">
                            <table id="example-multi" class="table" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Sr.no</th>
                                    <th>Doctor Name</th>
                                    <th>Doctor Id</th>
                                    <th>Doctor Number</th>
                                    <th>Status</th>
                                    <th>Message / Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!is_null($getRequest))
                                    @foreach($getRequest as $nk => $nv)
                                    
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $nv->doctor->doctor_name }}</td>
                                            <td>{{ $nv->doctor->id }}</td>
                                            <td>{{ $nv->doctor->mobile_number }}</td>
                                            <td>{{ $nv->status == 1 ? 'Not Attended' : 'Attended' }}</td>
                                            <td>
                                                <a href="javascript:void(0);" class="btn m-b-15 ml-2 mr-2 btn-dark getMessage" data-id="{{ $nv->id }}"  title="Message Detail"><i class="fe fe-eye"></i></a>
                                                @if($nv->status == 1)
                                                    <a href="{{ route('admin.attendSupportRequest',$nv->id) }}" class="btn m-b-15 ml-2 mr-2 btn-dark" title="Attend">Attend</a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade"  id="example06" tabindex="-1" role="dialog" aria-labelledby="example_02" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered modal-lg"  role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="modalcontent">
                <div class="container-fluid ">
                    <div class="row ">
                        <div class="col-md-12">
                            <p id="modalData"></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
    