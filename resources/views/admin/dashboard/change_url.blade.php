@extends('layouts.admin.admin')
@section('title','Kidiatric | YouTube Link')
@section('content')
<section class="admin-content">
    <div class="bg-dark">
        <div class="container  m-b-30">
            <div class="row">
                <div class="col-12 text-white p-t-40 p-b-90">
                    <h4 class="">  Set YouTube Link</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="container pull-up">
        <div class="row">
            <div class="col-lg-6 offset-3">
                <!--widget card begin-->
                <div class="card m-b-30">
                    <div class="card-header">
                        <h5 class="m-b-0">
                           Set YouTube Link
                        </h5>
                        <p class="m-b-0 text-muted">
                        </p>
                    </div>
                    <div class="card-body">
                        <form class="needs-validation" method="POST" action="{{ route('admin.updateSocialMediaLink') }}">
                            @csrf   
                            <input type="hidden" name="id" value="{{ Auth::guard('admin')->user()->id }}"> 
                            
                            <div class="form-group">
                                <label for="inputOldpass">Enter URL</label>
                                <input type="text" name="link" class="form-control" placeholder="Enter Link" @if(isset($updated_link->link) && $updated_link->link != '') value = "{{$updated_link->link}}" @endif >
                            </div>

                            <div class="form-group">
                                <button class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection  