@extends('layouts.admin.admin')
@section('title','Doctor Wise Vaccination | Kidiatric')
@section('content')
<section class="admin-content">
    <div class="bg-dark">
        <div class="container  m-b-30">
            <div class="row">
                <div class="col-12 text-white p-t-40 p-b-90">
                    <h4 class="">
                        Doctor Wise Vaccination
                    </h4>
                </div>
            </div>
        </div>
    </div>
<!--     <div class="container ">
        <div class="row m-b-30">
            <div class="col-lg-12">
                <div class="card">
                    
                </div>
            </div>
        </div>
    </div> -->

    <div class="container pull-up">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive p-t-10">
                            <table id="example-multi" class="table" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Sr.no</th>
                                    <th>Doctor Name</th>
                                    <th>Vac Name</th>
                                    <th>Taken At</th>
                                    <th>Updated At</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                
                                @if(!is_null($doctor_hist))
                                    @foreach($doctor_hist as $dk => $dv)
                                        <tr style="background-color:{{ $final_array[$dv->taken_at] }}" class="{{ $final_array[$dv->taken_at] }}asdfasfsadh;sdkldfhkl;sdafh">

                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $dv->doctors->doctor_name}}</td>
                                            <td>{{ $dv->vaccination_name }}</td>
                                            <?php
                                                $value = $dv->taken_at;
                                                $final_val = $value/30;
                                            ?>
                                            <td>
                                                <input type="text" name="taken_at" data-id="{{$dv->id}}" class="taken at{{$dv->id}} form-control" value="{{$final_val}}">&nbsp;

                                                <input class="edit{{$dv->id}} btn btn-success"  type="button" data-id="{{$dv->id}}"  value="✓" style="display: none" >

                                                <input class="close{{$dv->id}} btn btn-danger" type="button" value="x" data-id="{{$dv->id}}" style="display: none"></td>

                                            <td>{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $dv->updated_at)->format('d-m-Y H:i:s') }}</td>

                                            <td>
                                                @php $checked = ''; @endphp
                                                @if($dv->is_active == 1)
                                                  @php $checked = 'checked'; @endphp
                                                @endif
                                                <label class="cstm-switch ">
                                                    <input type="checkbox" name="option" value="1" class="cstm-switch-input changeStatus" data-id="{{ $dv->id }}" {{ $checked }}>
                                                    <span class="cstm-switch-indicator size-md "></span>
                                                    <span class="cstm-switch-description"></span>
                                                </label>
                                            </td>

                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('js')
<script>
$(document).ready(function(){
    $('.edit').hide();
});
</script>
<script type="text/javascript">
$(document).on("change",'.changeStatus',function(){
    if(this.checked){
        var status = "1";
    } else {
        var status = "0";
    }
    var vac_id = $(this).data('id');

    $.ajax({
        type: "post",
        url: '{{ route("admin.vaccination.status") }}',
        data:{ 'status' : status,'id' : vac_id},
        success:function(data){
            if(data == 'true'){
                if(status == 1){
                    msg = 'Status Enabled!';
                } else {
                    msg = 'Status Disabled!';
                }
            } else {        
               msg = 'Something Went Wrong';
            }

            $.notify({ title: '', message: msg},{
            placement: {
                align: "right",
                from: "top"
            },
                timer: 500,
                type: 'success',
            });
        }
    });
});

</script>

<script>
$(document).on('click', '.taken', function() {

    var id = $(this).data('id');

    $('.edit'+id).show();
    $('.close'+id).show();
    
    $(document).on('click','.edit'+id, function() {

        var taken_at = $("input.at"+id).val();

        $.ajax({
            type: 'post',
            url: '{{ route("admin.doctor.saveTaken") }}',
            data: {
                'taken_at': taken_at,
                'id' : id
            },
            success: function(data) {
                if(data == 'true'){
                    $('.edit'+id).hide();
                    $('.close'+id).hide();

                    msg = 'Taken At Successfully Updated!';            
                }

                $.notify({ title: '', message: msg},{
                placement: {
                    align: "right",
                    from: "top"
                },
                    timer: 500,
                    type: 'success',
                });
            }
        });

    });    

    $(document).on('click','.close'+id, function() {
        $('.edit'+id).hide();
        $('.close'+id).hide();
    });

});
</script>

@endsection