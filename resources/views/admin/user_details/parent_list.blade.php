@extends('layouts.admin.admin')
@section('title','Parents Details | Kidiatric')
@section('content')
<section class="admin-content">
    <div class="bg-dark">
        <div class="container  m-b-30">
            <div class="row">
                <div class="col-12 text-white p-t-40 p-b-90">
                    <h4 class="">
                        Parents Details
                    </h4>
                </div>
            </div>
        </div>
    </div>

    <div class="container pull-up">
        <div class="row m-b-30">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('admin.ParentsList',$id) }}" method="post">
                            @csrf
                            <div class="form-row">

                                <div class="col-md-4 mb-4">
                                    <label for=""> Select Role</label>
                                    <select class="form-control" id="role" name="role">
                                        <option value="">Select Role</option>
                                        <option value="0" @if($role == '0') selected="selected" @endif>Mother</option>
                                        <option value="1" @if($role == '1') selected="selected" @endif>Father</option>
                                        <option value="2" @if($role == '2') selected="selected" @endif>Guardian</option>
                                    </select>
                                </div>

                                <div class="col-md-4 mb-4">
                                    <label for="">City</label>
                                    <select class="form-control" id="city_id" name="city_id">
                                        <option value="">Select City</option>
                                        @if(!is_null($cityList))
                                            @foreach($cityList as $ck => $cv)
                                                <option value="{{ $cv->id }}" @if($city_id == $cv->id) selected="selected" @endif>{{ $cv->city_name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>

                                <div class="col-md-4 mb-4">
                                    <label for=""> Select Child Gender</label>
                                    <select class="form-control" id="gender" name="gender">
                                        <option value="">Select Child Gender</option>
                                        <option value="Male" @if($gender == 'Male') selected="selected" @endif >Male</option>
                                        <option value="Female" @if($gender == 'Female') selected="selected" @endif>Female</option>
                                    </select>
                                </div>
                                
                                <div class="col-md-3 mb-3">
                                    <label for="">Child Min Age (months)</label>
                                    <input type="text" class="form-control" name="min_age" placeholder="Min Age Range" value="{{ $min_range }}">
                                </div>

                                <div class="col-md-3 mb-3">
                                    <label for="">Child Max Age (months)</label>
                                    <input type="text" class="form-control" name="max_age" placeholder="Max Age Range" value="{{ $max_range }}">
                                </div>

                                <div class="col-md-2 mt-4">
                                    <button type="submit" class="btn btn-primary mt-1" id="filter" name="save_and_list" value="save_and_list" >Filter Parents List</button>
                                </div>
                                @if($filter == 1)
                                    <div class="col-md-2 mt-4">
                                        <a href="{{ route('admin.ParentsList') }}" class="btn btn-danger mt-1" id="filter" name="save_and_list" value="save_and_list" style="margin-left: -20px;">Reset Filter</a>
                                    </div>
                                @endif
                            </div>                               
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form action="{{ route('admin.parentExcelData')}}" method="post" id="generateExcel">
        @csrf
        <input type="hidden" name="parent_id" value="{{ json_encode($id_json) }}">
    </form>
    
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <a href="javascript:void(0);" class="btn btn-primary mb-3 " id="exportExcel" name="save_and_list" value="save_and_list" style="float:right">Export Excel</a>
                        <form action="{{ route('admin.sendUserNotification')}}" method="post" id="notificationForm">
                            @csrf
                            <a href="javascript:void(0);" class="btn btn-primary mb-3" id="sendNotification" name="save_and_list" value="save_and_list" style="float:right;;margin-right: 10px;">Send Notification</a>
                            <br/><br/>
                            <div class="table-responsive p-t-10">
                                <table id="example-multi" class="table" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th><input type="checkbox" name="checkbox" class="checkall"></th>
                                        <th>Sr.no</th>
                                        <th>Name</th>
                                        <th>Mobile</th>
                                        <th>Email</th>
                                        <th>Relation</th>
                                        <th>No Of Children</th>
                                        <th>City Name</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(!is_null($parent_list))
                                        @foreach($parent_list as $pk => $pv)
                                        
                                            <tr>
                                                <td><input type="checkbox" name="checkbox[]" class="checkbox" value="{{ $pv->id }}"></td>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $pv->name }}</td>
                                                @if(!is_null($pv->mobile_number))
                                                    <td>{{ $pv->mobile_number}}</td>
                                                @else
                                                    <td>-----------</td>
                                                @endif
                                                @if(!is_null($pv->email))
                                                    <td>{{ $pv->email }}</td>
                                                @else
                                                    <td>-----------</td>
                                                @endif
                                                @if(array_key_exists($pv->relation,$realtion))
                                                    <td>{{ $realtion[$pv->relation] }}</td>
                                                @else
                                                    <td>-----------</td>
                                                @endif
                                                @if($pv->type == 0)
                                                    <?php $child_count = App\Model\ChildrenDetail::where('user_id',$pv->id)->where('is_delete','0')->count(); ?>
                                                @else
                                                    <?php $child_count = App\Model\ChildrenDetail::where('user_id',$pv->parent_id)->where('is_delete','0')->count(); ?>
                                                @endif
                                                @if($pv->type == 0)
                                                <td class="parents" data-id="{{$pv->id}}" onclick="{{$pv->id}}" data-toggle="modal" data-target="#example_02" title="Child Details"><u style="color: blue;cursor: pointer;">{{$child_count}}</u></td>
                                                @else
                                                <td class="parents" data-id="{{$pv->parent_id}}" onclick="{{$pv->parent_id}}" data-toggle="modal" data-target="#example_02" title="Child Details"><u style="color: blue;cursor: pointer;">{{$child_count}}</u></td>
                                                @endif
                                                @if(!is_null($pv->city))
                                                    <td>{{ $pv->city->city_name }}</td>
                                                @else
                                                    <td>-----------</td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>		


<!-- Modal -->
<div class="modal fade"  id="example_02" tabindex="-1" role="dialog" aria-labelledby="example_02" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg"  role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Children details</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body" id="modalcontent">
        
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                Close
            </button>
        </div>
      </div>
    </div>
</div>
@endsection
	
@section('js')

<script>

$(".js-datepicker-list").datepicker({
    endDate: "today"
});

$(document).on('click', '.parents', function() {

    var id = $(this).data('id');
    
    $.ajax({
        type: 'post',
        url: "{{ route('admin.parentsChildDetails') }}",
        data: {
            'id': id
        },
        success: function(data) {
           $('#modalcontent').html(data);
        }
    });
});

$(document).on('change','.checkall',function(){
        if(this.checked){
            $('.checkbox').prop('checked',true);
        } else {
            $('.checkbox').prop('checked',false);
        }
    });

$(document).on('click','#sendNotification',function(){
    var selected = [];
    $(".checkbox:checked").each(function(){
        selected.push($(this).val());
    });

    if(selected.length){
        $('#notificationForm').submit();
    } else {
        notification('Please Select Atleast One Parents','danger');
    }
});

$(document).on('click','#exportExcel',function(){
    $('#generateExcel').submit();    
});

function notification(message,type){

    $.notify({
    title: '',
        message: message
    }, {
        placement: {
            align: "right",
            from: "top"
        },
        timer: 500,
        type: type,
    });   
}
</script>


@endsection