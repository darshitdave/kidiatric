<div class="container-fluid ">
    <div class="row ">
        <div class="col-md-12">
            <form class="needs-validation"  method="post">
                @csrf
                @if(count($child_Details) > 0)
                    <table class="table" style="width:100%">
                        <thead>
                        <tr>
                            <th>Sr.no</th>
                            <th>Profile Image</th>
                            <th>Name</th>
                            <th>Date Of Birth</th>
                            <th>Doctor Name</th>
                            <th>Age(months)</th>
                            <th>Gender</th>
                            <th>Medical Information</th>
                        </tr>
                        </thead>
                            <tbody>
                            @if(!is_null($child_Details))
                                @foreach($child_Details as $ck => $cv)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        @if(!is_null($cv->profile_image))
                                            <td><img src="https://kidiatric.s3.ap-south-1.amazonaws.com/uploads/child_image/{{$cv->uuid}}/profile_image/{{$cv->profile_image}}" class="css-class" style="height: 68px;"></img></td>
                                        @else
                                            @if($cv->gender == 'Male')
                                                <td><img src="{{asset('img/boy.png')}}" class="css-class" style="height: 68px;"></img></td>
                                            @else
                                                <td><img src="{{asset('img/girl.png')}}" class="css-class" style="height: 68px;"></img></td>
                                            @endif
                                        @endif
                                        <td>{{ $cv->child_name }}</td>
                                        <td>{{ date('d-m-Y',strtotime($cv->date_of_birth)) }}</td>
                                        @if(!is_null($cv->doctor) && !is_null($cv->doctor->doctordetail))
                                            <td>{{ $cv->doctor->doctordetail->doctor_name }} <b>({{ $cv->doctor->doctordetail->doctor_id }})</b></td>
                                        @else
                                            <td>Council</td>
                                        @endif
                                        <td>{{ intval($cv->age / 30,0) }}</td>
                                        <td>{{ $cv->gender }}</td>
                                        <td>
                                            <a href="{{ route('admin.ChildrenVaccination',$cv->id) }}" target="_blank"  class="btn m-b-15 ml-2 mr-2 btn-dark" title="Vaccination Details"><i class="fe fe-list"></i></a>
                                            <a href="{{ route('admin.childDocuments',$cv->uuid) }}" target="_blank"  class="btn m-b-15 ml-2 mr-2 btn-dark" title="Child Document"><i class="mdi mdi-file-document"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                    </table>
                @else
                    <h5>Child is not added</h5>
                @endif
            </form>
        </div>
    </div>
</div>