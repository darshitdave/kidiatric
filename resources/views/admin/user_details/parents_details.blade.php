<div class="container-fluid ">
    <div class="row ">
        <div class="col-md-12">
            <form class="needs-validation"  method="post">
                @csrf

                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label>Parent Name</label>
                        <input type="text" class="form-control" name="name" placeholder="name"  autocomplete="off" value="{{$parent_list->name}}" required="" aria-invalid="false" readonly="readonly">
                    </div>
                    
                    <div class="form-group col-md-12">
                        <label>Mobile Number</label>
                        <input type="text" class="form-control" name="mobile_number" placeholder="mobile_number"  autocomplete="off" value="{{$parent_list->mobile_number}}" required="" aria-invalid="false" readonly="readonly">
                    </div>
                </div>
                @if(count($link_parent) > 0)
                <h5>Other Parents with access</h5>
                    @foreach($link_parent as $lk => $lv)
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label>Parent name</label>
                            <input type="text" class="form-control" name="name" placeholder="name"  autocomplete="off" value="{{$lv['user']['name']}}" required="" aria-invalid="false" readonly="readonly">
                        </div>
                        
                        <div class="form-group col-md-12">
                            <label>Mobile Number</label>
                            <input type="text" class="form-control" name="mobile_number" placeholder="mobile_number"  autocomplete="off" value="{{$lv['user']['mobile_number']}}" required="" aria-invalid="false" readonly="readonly">
                        </div>
                    </div>
                    @endforeach
                @endif
            </form>
        </div>
    </div>
</div>