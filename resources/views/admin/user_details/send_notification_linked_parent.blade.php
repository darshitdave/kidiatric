@extends('layouts.admin.admin')
@section('title','Notification | Kidiatric')
@section('content')
<section class="admin-content">
    <div class="bg-dark">
        <div class="container  m-b-30">
            <div class="row">
                <div class="col-12 text-white p-t-40 p-b-90">
                    <h4 class="">
                        Send Notification
                    </h4>
                </div>
            </div>
        </div>
    </div>
    <div class="container pull-up">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive p-t-10">
                            <table id="example-multi" class="table" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Sr.no</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Mobile</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!is_null($userData))
                                    @foreach($userData as $ck => $cv)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $cv['name'] }}</td>
                                        <td>{{ $cv['email'] }}</td>
                                        @if($cv['mobile_number'] != '')
                                        <td>{{ $cv['mobile_number'] }}</td>
                                        @else
                                        <td>--------</td>
                                        @endif
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><br>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!--widget card begin-->
                <div class="card m-b-30">
                    <div class="card-header">
                        <h5 class="m-b-0">
                            Send Notification
                        </h5>
                    </div>
                    <div class="card-body ">

                        <label class="cstm-switch ">
                            <h5>SMS</h5>&nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="checkbox" name="option" value="sms" class="cstm-switch-input message" data-id="sms">
                            <span class="cstm-switch-indicator size-md "></span>
                            <span class="cstm-switch-description"></span>
                        </label>

                        <label class="cstm-switch" style="margin-left: 58px;">
                            <h5>Push Notification</h5>&nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="checkbox" name="option" value="notification" class="cstm-switch-input message" data-id="nf">
                            <span class="cstm-switch-indicator size-md "></span>
                            <span class="cstm-switch-description"></span>
                        </label>

                        <label class="cstm-switch" style="margin-left: 58px;">
                            <h5>Email</h5>&nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="checkbox" name="option" value="email" class="cstm-switch-input message" data-id="email">
                            <span class="cstm-switch-indicator size-md "></span>
                            <span class="cstm-switch-description"></span>
                        </label><br><br><br>

                        <form method="post" action="{{ route('admin.sendChildNotificationuser') }}" enctype="multipart/form-data" novalidate>
                            @csrf
                            <input type="hidden" value="{{ json_encode($user_id) }}" name="user_id">

                            <div class="form-group" id="smsDiv" style="display: none;">
                                <label for="inputSMS"><b>SMS</b></label>
                                <input type="text" class="form-control sms" name="sms" id="inputSMS" placeholder="SMS">
                            </div>

                            <div class="form-group" id="notificationDiv" style="display: none;">
                                <label for="inputPush"><b>Push Notification</b></label>
                                <input type="text" class="form-control push" name="push_notification_title" placeholder="Title" id="inputPush"><br>
                                <textarea class="form-control push" name="push_notification_description" placeholder="Description"></textarea><br>
                                <input type="file" class="form-control dropify" name="image">
                            </div>

                            <div class="form-group"  id="emailDiv" style="display: none;">
                                <label for="inputSubject"><b>Email</b></label><br>
                                <label for="inputSubject"><b>Subject</b></label>
                                <input type="text" class="form-control email" name="subject" placeholder="Email Subject" id="inputSubject"><br>
                                <label for="inputSubject"><b>Description</b></label>
                                <textarea type="text" class="form-control ckeditor email" name="email" placeholder="Email"></textarea>
                            </div>

                            <div class="form-group" id='button' style="display: none;">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>
@endsection
