<div class="container-fluid ">
    <div class="row ">
        <div class="col-md-12">
            <form class="needs-validation"  method="post">
                @csrf
                @if(count($notification_details) > 0)
                 <table class="table" style="width:100%">
                        <thead>
                        <tr>
                            <th>Sr.no</th>
                            <th>Targeted User Name</th>
                            <th>Mobile Number</th>
                            <th>Email</th>
                        </tr>
                        </thead>
                            <tbody>
                            @if(!is_null($notification_details))
                                @foreach($notification_details as $nk => $nv)
                                    
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $nv['user']['name'] }}</td>
                                        <td>{{ $nv['user']['mobile_number'] }}</td>
                                        @if($nv['user']['email'] != '')
                                            <td>{{ $nv['user']['email'] }}</td>
                                        @else
                                            <td> --------- </td>
                                        @endif
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                    </table>
                @else
                    <h5>No Data Found</h5>
                @endif
            </form>
        </div>
    </div>
</div>