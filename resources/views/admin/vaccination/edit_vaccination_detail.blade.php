@extends('layouts.admin.admin')
@section('title','Update Vaccination Details | Kidiatric')
@section('content')
<section class="admin-content">
    <div class="bg-dark">
        <div class="container  m-b-30">
            <div class="row">
                <div class="col-12 text-white p-t-40 p-b-90">
                    <h4 class="">Update Vaccination Details</h4>
                </div>
            </div>
        </div>
    </div>

    <div class="container  pull-up">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">

                <!--widget card begin-->
                <div class="card m-b-30">
                    <div class="card-header">
                        <h5 class="m-b-0">
                            Update Vaccination Details
                        </h5>
                    </div>
                    <div class="card-body ">
                        <form method="post" action="{{ route('admin.updateVaccination') }}" id="vaccinationForm">
                            @csrf
                            <div class="form-group">
                                <label for="inputVacName">Vaccination Name<span class="mendetory">*</span></label>
                                    <input type="text" class="form-control" name="vaccination_name" id="inputVacName" placeholder="Full Name" required value="{{$vaccination_list->vaccination_name}}">
                            </div>

                            <div class="form-group">
                                <label for="inputVacDes">Vaccination Description<span class="mendetory">*</span></label>
                                <textarea type="text" class="form-control" name="vaccination_des" placeholder="Vaccination Description" id="inputVacDes" required="" spellcheck="false">{{$vaccination_list->vaccination_des}}</textarea>
                                <input type="hidden" name="id" value="{{$vaccination_list->id}}">   
                            </div>

                            <div class="form-group">
                                <label for="inputDuration">To Be Taken At (Months)<span class="mendetory">*</span></label>
                                <input type="number" class="form-control taken_at" name="taken_at" maxlength="4" min="0" oninvalid="this.setCustomValidity('Please Enter valid Duration')"  id="inputDuration"  placeholder="Enter Duration" required value="{{$months}}">
                                
                            </div>

                            <div class="form-group">
                                <label for="inputPrevent">Prevents From<span class="mendetory">*</span></label>
                                <textarea type="text" class="form-control" name="prevents" placeholder="Prevents From" id="inputPrevent" spellcheck="false" required>{{$vaccination_list->prevents}}</textarea>
                            </div>

                            <div class="form-group">
                                <label for="inputVacType">Type<span class="mendetory">*</span></label>
                                <select class="form-control" name="vacc_type" required="required">
                                    <option value="" selected="selected">Select Type</option>
                                    <option value="0" @if($vaccination_list->vacc_type == 0) selected="selected" @endif>Council</option>
                                    <option value="1" @if($vaccination_list->vacc_type == 1) selected="selected" @endif>Other</option>
                                </select>
                            </div>
                            
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <a href="{{route('admin.vaccinationList')}}"  class="btn btn-danger">Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
@endsection