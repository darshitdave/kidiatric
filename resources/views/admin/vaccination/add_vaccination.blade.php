@extends('layouts.admin.admin')
@section('title','Add Vaccination Details | Kidiatric')
@section('content')
<section class="admin-content">
    <div class="bg-dark">
        <div class="container  m-b-30">
            <div class="row">
                <div class="col-12 text-white p-t-40 p-b-90">
                    <h4 class="">Add Vaccination</h4>
                </div>
            </div>
        </div>
    </div>

    <div class="container  pull-up">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">

                <!--widget card begin-->
                <div class="card m-b-30">
                    <div class="card-header">
                        <h5 class="m-b-0">
                            Add Vaccination Details
                        </h5>
                    </div>
                    <div class="card-body ">
                        <form method="post" action="{{ route('admin.saveVaccination') }}" id="vaccinationForm">
                            @csrf
                            <div class="form-group">
                                <label for="inputVacName">Vaccination Name<span class="mendetory">*</span></label>
                                    <input type="text" class="form-control" name="vaccination_name" id="inputVacName" placeholder="Full Name" required>
                            </div>

                            <div class="form-group">
                                <label for="inputVacDes">Vaccination Description<span class="mendetory">*</span></label>
                                <textarea type="text" class="form-control" name="vaccination_des" placeholder="Vaccination Description" id="inputVacDes" required="" spellcheck="false"></textarea>
                            </div>

                            <div class="form-group">
                                <label for="inputDuration">To Be Taken At (Months)<span class="mendetory">*</span></label>
                                <input type="number" class="form-control taken_at" name="taken_at" maxlength="4" min="1" oninvalid="this.setCustomValidity('Please Enter valid Duration')" id="inputDuration"  placeholder="Enter Duration" required>
                                
                            </div>

                            <div class="form-group">
                                <label for="inputPrevent">Prevents From<span class="mendetory">*</span></label>
                                <textarea type="text" class="form-control" name="prevents" placeholder="Prevents From" id="inputPrevent" spellcheck="false" required></textarea>
                            </div>

                            <div class="form-group">
                                <label for="inputVacType">Vaccination Type<span class="mendetory">*</span></label>
                                <select class="form-control" name="vacc_type" required="required">
                                    <option value="" selected="selected">Select Vaccination Type</option>
                                    <option value="0">Council</option>
                                    <option value="1">Other</option>
                                </select>
                            </div>
                            
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <a href="{{route('admin.vaccinationList')}}"  class="btn btn-danger">Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
@endsection