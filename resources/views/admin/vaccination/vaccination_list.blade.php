@extends('layouts.admin.admin')
@section('title','Vaccination List | Kidiatric')
@section('content')
<section class="admin-content">
    <div class="bg-dark">
        <div class="container  m-b-30">
            <div class="row">
                <div class="col-12 text-white p-t-40 p-b-90">
                    <h4 class="">
                        Vaccination List
                    </h4>
                </div>
            </div>
        </div>
    </div>

<form action="{{ route('admin.vaccination_csv')}}" method="post" id="vaccinationForm">
@csrf
    <div class="container pull-up">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <a href="javascript:void(0);" class="btn btn-primary mb-3" id="createVaccinationCSV" name="save_and_list" value="save_and_list" style="float:right;">Export Excel</a><br/><br/>
                        <div class="table-responsive p-t-10">
                            <table id="example-multi" class="table" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Sr.no</th>
                                    <th>Name</th>
                                    <th>Taken At (Months)</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!is_null($vaccination_list))
                                    @foreach($vaccination_list as $vk => $vv)

                                        <tr style="background-color:{{ $final_array[$vv->taken_at] }}">
                                            <input type="hidden" name="vaccination" value="{{$id_json}}">
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $vv->vaccination_name }}<b>@if($vv->vaccination_type == '0') (c) @elseif ($vv->vaccination_type == '1') (G) @endif </b></td>
                                            <?php
                                                $value = $vv->taken_at;
                                                $final_val = $value/30;
                                            ?>
                                            <td>
                                                <input type="text" name="taken_at" data-id="{{$vv->id}}" class="taken at{{$vv->id}} close_{{$vv->id}} form-control" value="{{$final_val}}">&nbsp;

                                                <input class="edit{{$vv->id}} btn btn-success"  type="button" data-id="{{$vv->id}}"  value="✓" style="display: none" >

                                                <input class="close{{$vv->id}} btn btn-danger delete" type="button" value="x" data-id="{{$vv->id}}" data-value="{{$final_val}}" style="display: none">
                                            </td>

                                            <td>
                                                @php $checked = ''; @endphp
                                                @if($vv->is_active == 1)
                                                  @php $checked = 'checked'; @endphp
                                                @endif
                                                <label class="cstm-switch ">
                                                    <input type="checkbox" name="option" value="1" class="cstm-switch-input changeStatus" data-id="{{ $vv->id }}" {{ $checked }}>
                                                    <span class="cstm-switch-indicator size-md "></span>
                                                    <span class="cstm-switch-description"></span>
                                                </label>
                                            </td>
                                            <td>
                                                <a href="{{ route('admin.editVaccination',$vv->id) }}" class="btn m-b-15 ml-2 mr-2 btn-dark" title="Edit"><i class="fe fe-edit"></i></a>
                                                <a href="{{ route('admin.deleteVaccination',$vv->id) }}" class="btn m-b-15 ml-2 mr-2 btn-dark" onclick="return confirm('Are your sure want to delete this Vaccination ?')" title="Delete"><i class="fe fe-trash"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</section>
@endsection
