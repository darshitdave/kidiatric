@extends('layouts.admin.admin')
@section('title','Notification List | Kidiatric')
@section('content')
<section class="admin-content">
    <div class="bg-dark">
        <div class="container  m-b-30">
            <div class="row">
                <div class="col-12 text-white p-t-40 p-b-90">
                    <h4 class="">
                       Doctor Notification List
                    </h4>
                </div>
            </div>
        </div>
    </div>
    <div class="container pull-up">
        <div class="row m-b-30">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('admin.doc.savedNotificationList') }}" method="post">
                            @csrf
                            <div class="form-row">

                                <div class="col-md-3 m-b-3">
                                    <label for=""> Date Range </label>
                                    <input type="text" class="js-datepicker form-control" name="date_range" placeholder="Select a Date" value="{{$range}}" autocomplete="off">
                                </div>

                                <div class="col-md-3 mb-3">
                                    <label for=""> Doctor Name</label>
                                    <input type="text" name="doctor_name" class="form-control doctor_name"  placeholder="Enter Doctor Name" autocomplete="off" value="{{$doctor_name}}">
                                    <input type="hidden" name="doctor_name_id" id="doctor_name_id" value="{{ $doctor_name_id }}">
                                </div>

                                <div class="col-md-3 mb-3">
                                    <label for=""> Doctor Id</label>
                                    <input type="text" name="doctor_id" class="form-control"  placeholder="Enter Doctor Id" autocomplete="off" value="{{ $doctor_id }}">
                                </div>
                                
                                <div class="col-md-3 mb-3">
                                    <label for=""> Select Notification Type</label>
                                    <select class="form-control" id="notification" name="notification">
                                        <option value="">Select Notification Type</option>
                                        <option value="EMAIL" @if($notification == "EMAIL") selected="selected" @endif >EMAIL</option>
                                        <option value="SMS" @if($notification == "SMS") selected="selected" @endif >SMS</option>
                                        <option value="PUSH_NOTIFICATION" @if($notification == 'PUSH_NOTIFICATION') selected="selected" @endif >PUSH NOTIFICATION</option>
                                        
                                    </select>
                                </div>

                               
                                <div class="col-md-1">
                                    <button type="submit" class="btn btn-primary mt-1" id="filter" name="save_and_list" value="save_and_list" >Filter</button>
                                </div>
                                
                                @if($filter == 1)
                                    <div class="col-md-2">
                                        <a href="{{ route('admin.doc.savedNotificationList') }}" class="btn btn-danger mt-1" id="filter" name="save_and_list" value="save_and_list">Reset Filter</a>
                                    </div>
                                @endif
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form action="{{ route('admin.exportDoctorNotificationData')}}" method="post" id="doctorForm">
        @csrf
        <input type="hidden" name="notification_id" value="{{ json_encode($id_json) }}">
    </form>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <a href="javascript:void(0);" class="btn btn-primary mb-3" id="createNotificationDataCSV" name="save_and_list" value="save_and_list" style="float:right;">Export Excel</a><br><br>
                        <div class="table-responsive p-t-10">
                            <table id="example-multi" class="table" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Sr.no</th>
                                    <th>Doctor ID</th>
                                    <th>Doctor Name</th>
                                    <th>Type</th>
                                    <th>Targeted User</th>
                                    <th>Interaction</th>
                                    <th>Date</th>
                                    <th>Message</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!is_null($notification_list))
                                    @foreach($notification_list as $nk => $nv)
                                    
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            @if(!is_null($nv->notificationuser))
                                                <td>{{ $nv->notificationuser->doctor_id}}</td>
                                            @else
                                                <td>-----</td>
                                            @endif

                                            @if(!is_null($nv->notificationuser) && !is_null($nv->notificationuser->doctor))
                                                <td>{{ $nv->notificationuser->doctor->doctor_name}}</td>
                                            @else
                                                <td>-----</td>
                                            @endif

                                            <td>{{ $nv->type }}</td>
                                            <td>{{$nv->user_count}}</td>
                                            @if($nv->type == 'PUSH_NOTIFICATION')
                                            <td class="avatar avatar-xs">
                                                <!-- <button class="avatar-title bg-dark rounded-circle" data-id="{{$nv->id}}" data-value={{$nv->notificationuser->doctor_id}} data-toggle="modal" data-target="#example_02"  title="Interaction">i</button> -->
                                                <a href="#" class="avatar-title bg-dark rounded-circle" data-id="{{$nv->id}}" data-value={{$nv->notificationuser->doctor_id}} data-toggle="modal" data-target="#example_02"  title="Interaction" style="width: 86%;"><span style="color: white";>i</span></a>
                                            </td>
                                            @else
                                            <td> ------------ </td>
                                            @endif
                                            <td>{{ date('d-m-Y h:i A',strtotime($nv->created_at)) }}</td>
                                            <td>
                                                <a href="{{ route('admin.viewDoctorNotification',$nv->id) }}" class="btn m-b-15 ml-2 mr-2 btn-dark" target="_blank"  title="Notification Detail"><i class="fe fe-eye"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>	


<div class="modal fade"  id="example_02" tabindex="-1" role="dialog" aria-labelledby="example_02" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg"  role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body" id="modalcontent">
        
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                Close
            </button>
        </div>
      </div>
    </div>
</div>

@endsection
    
@section('js')
<script>
$('.js-datepicker').daterangepicker({ 
    maxDate: new Date(),
    locale: {
        format: 'DD-MM-Y'
    } 
});

@if($range == '')   
    $(document).ready(function(){
        $('.js-datepicker').val('');
    });
@endif

$(document).on('keyup','.doctor_name',function(){
    $.ajax({
        url: "{{ route('getAutoSuggestDoctorName') }}",
        type: "POST",
        dataType: "JSON",
        data:{ 
            doctor_name: $(this).val(),
        },
        success: function(data){
            autocompletedatalist = data;
            $('.doctor_name').autocomplete({ 
                source: autocompletedatalist,
                select: function(event, ui) {
                    $('.doctor_name').val(ui.item.label);
                    $('#doctor_name_id').val(ui.item.value);
                    return false;
                }
            });
        }
    });
});

$(document).on('click','#createNotificationDataCSV',function(e){
    e.preventDefault();
    $('#doctorForm').submit();    
});

$(document).on('click', '.rounded-circle', function() {

    var id = $(this).data('id');
    var doctor_id = $(this).data('value');

    $.ajax({
        type: 'post',
        url: 'admin-doctor-notification-user',
        data: {
            'id': id,
            'doctor_id' : doctor_id
        },
        success: function(data) {
           $('#modalcontent').html(data);
           $('#exampleModalLabel').text('Interected Users');
        }
    });
    
});
</script>
@endsection