@extends('layouts.admin.admin')
@section('title','Notification | Kidiatric')
@section('content')
<section class="admin-content">
    <div class="bg-dark">
        <div class="container  m-b-30">
            <div class="row">
                <div class="col-12 text-white p-t-40 p-b-90">
                    <h4 class="">
                        Send Notification
                    </h4>
                </div>
            </div>
        </div>
    </div>
  
    <div class="container pull-up">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive p-t-10">
                            <table id="example-multi" class="table" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Sr.no</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $parent_id = array(); ?>
                                @if(!is_null($getNotificationUser))
                                    @foreach($getNotificationUser as $ck => $cv)
                                        <tr>
                                            @if(!in_array($cv->user_id, $parent_id))
                                                <?php $parent_id[] = $cv->user_id;?>
                                                <td>{{ $loop->iteration }}</td>
                                                @if(!is_null($cv->user))
                                                    <td>{{ $cv->user->name }}</td>
                                                    <td>{{ $cv->user->email }}</td>
                                                    <td>{{ $cv->user->mobile_number }}</td>
                                                @else
                                                    <td>-------</td>
                                                    <td>-------</td>
                                                    <td>-------</td>
                                                @endif
                                            @endif
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <br>

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!--widget card begin-->
                <div class="card m-b-30">
                    <div class="card-header">
                        <h5 class="m-b-0">
                            Send Notification
                        </h5>
                    </div>
                    <div class="card-body ">
                    
                    <form method="post" action="{{ route('admin.sendChildNotificationuser') }}" enctype="multipart/form-data" novalidate>
                        @csrf

                        @if($findNotificationData->type == "SMS")
                            <div class="form-group">
                                <label for="inputSMS"><b>SMS</b></label>
                                <input type="text" class="form-control" name="sms" id="inputSMS" placeholder="SMS" value="{{ $findNotificationData->message }}" readonly="true">
                            </div>
                        @endif

                        @if($findNotificationData->type == "PUSH_NOTIFICATION")
                            <div class="form-group">
                                <label for="inputPush"><b>Push Notification</b></label>
                                <input type="text" class="form-control" name="push_notification_title" placeholder="Title" id="inputPush" value="{{ $findNotificationData->title }}" readonly="true">
                                <br>
                                <textarea class="form-control" name="push_notification_description" placeholder="Description" readonly="true">{{ $findNotificationData->message }}</textarea> 
                                <br>
                                <input type="file" class="form-control dropify" name="image" @if($findNotificationData->url != '') data-default-file="https://kidiatric.s3.ap-south-1.amazonaws.com/uploads/admin_notification/notification_image/{{ $findNotificationData->url }}" @else data-default-file=""  @endif data-show-remove="false" style="z-index: -1">
                            </div>
                        @endif
                        
                        @if($findNotificationData->type == "EMAIL")
                            <div class="form-group">
                                <label for="inputSubject"><b>Email</b></label><br>
                                <label for="inputSubject"><b>Subject</b></label>
                                <input type="text" class="form-control" name="subject" placeholder="Email Subject" id="inputSubject" value="{{ $findNotificationData->title }}" readonly="true">
                                <br>
                                <label for="inputSubject"><b>Description</b></label>
                                <textarea type="text" class="form-control ckeditor" name="email" placeholder="Email" disabled> {{ $findNotificationData->message }}</textarea>
                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
