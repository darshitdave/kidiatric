@extends('layouts.admin.admin')
@section('title','Children Details | Kidiatric')
@section('content')
<section class="admin-content">
    <div class="bg-dark">
        <div class="container  m-b-30">
            <div class="row">
                <div class="col-12 text-white p-t-40 p-b-90">
                    <h4 class="">
                        User List
                    </h4>
                </div>
            </div>
        </div>
    </div>

    <div class="container pull-up">
        <div class="row m-b-30">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                    <form action="{{ route('admin.UserList') }}" method="post">
                        @csrf
                        <div class="form-row">

                            <div class="col-md-4 mb-3">
                                <label for=""> Date Range</label>
                                <input type="text" class="form-control input-daterange" id="date_range" name="date_range" placeholder="Date Range" autocomplete="off" value="">
                            </div>

                            
                            <div class="col-md-2 mt-2">
                                <button type="submit" class="btn btn-primary mt-1" id="filter" name="save_and_list" value="save_and_list" >Filter User List</button>
                            </div>

                            
                            <div class="col-md-2 mt-2">
                                <a href="{{ route('admin.UserList') }}" class="btn btn-danger mt-1" id="filter" name="save_and_list" value="save_and_list" style="margin-left: -50px;">Reset Filter</a>
                            </div>
                            
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive p-t-10">
                            <table id="example-multi" class="table" style="width:100%">
                                <thead>
                                <tr>
                                    <th><input type="checkbox" name="checkbox" class="checkall"></th>
                                    <th>Sr.no</th>
                                    <th>Name</th>
                                    <th>Mobile Number</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!is_null($getUserList))
                                    @foreach($getUserList as $uk => $uv)
                                    
                                        <tr>
                                            <td><input type="checkbox" name="checkbox[]" class="checkbox" value="{{ $uv->id }}"></td>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $uv->name }}</td>
                                            @if($uv->relation != '')
                                                <td>{{ $uv->relation }}</td>
                                            @else
                                                <td> --------- </td>
                                            @endif
                                            
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>		


@endsection
	
@section('js')
<script type="text/javascript">
     $(document).on('change','.checkall',function(){
        if(this.checked){
            $('.checkbox').prop('checked',true);
        } else {
            $('.checkbox').prop('checked',false);
        }
    });

    function notification(message,type){

        $.notify({
        title: '',
            message: message
        }, {
            placement: {
                align: "right",
                from: "top"
            },
            timer: 500,
            type: type,
        });   
    }
</script>
@endsection