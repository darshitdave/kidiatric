<!DOCTYPE HTML>
<html>
    <head>
        <title>Kidiatric - Pocket Paediatric. Vaccine and Health Data Recording App.</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="icon" href="https://kidiatric.app/img/kiditric-logo.png" type="image/png" sizes="16x16">
        <link rel="stylesheet" href="{{ asset('css/front/main.css') }}" />
    </head>
    <body class="is-preload">

        <section id="one" class="wrapper">
            <div class="inner">
            	<center>
            		<img src="https://kidiatric.app/img/front/logo.png" style="width:15%;margin-top: -50px;"><br><br>
            		<h2><b>Privacy Policy</h2></b>
            	</center>
	           	<p>Kidiatric.app and the Kidiatric App is a property of Kidiatric, 106, Sarita Complex, Off C.G. Road, Jain Derasar Lane, Navrangpura, Ahmedabad 380009, Gujarat.</p>

				<p>We recognise the importance of maintaining your privacy. We value your privacy and appreciate your trust in us. This Policy describes how we treat user information we collect on http://www.kidiatric.app and other sources including the App. By visiting and/or using our website and App, you agree to this Privacy Policy.</p>

				<p>Kidiatric app as a Free app. This SERVICE is provided at no cost and is intended for use as is.</p>

				<p>This page is used to inform visitors regarding my policies with the collection, use, and disclosure of Personal Information if anyone decided to use my Service.</p>

				<p>If you choose to use my Service, then you agree to the collection and use of information in relation to this policy. The Personal Information that we collect is used for providing and improving the Service. We will not use or share your information with anyone except as described in this Privacy Policy.</p>

				<p>The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which is accessible at Kidiatric unless otherwise defined in this Privacy Policy.</p>

				<p><strong>Information Collection and Use</strong></p>

				<p>For a better experience, while using our Service, We may require you to provide us with certain personally identifiable information, including but not limited to First Name, Last Name, Child&#39;s personal information, contact number, City, email ID.&nbsp;</p>

				<p>The app does use third party services that may collect information used to identify you.</p>

				<p>We might also collect the type of mobile device you are using, or the version of the operating system your computer or device is running.</p>

				<p>Information you post. We collect information you post in a public space on our website or on a third-party social media site belonging to kidiatric.app</p>

				<p>Demographic information. We may collect demographic information about you, events you like, events you intend to participate in, tickets you buy, or any other information provided by your during the use of our website. We might collect this as a part of a survey also.</p>

				<p>We collect information from you passively. We use tracking tools like Google Analytics, Google Webmaster, browser cookies and web beacons for collecting information about your usage of our website.</p>

				<p><strong>Log Data</strong></p>

				<p>We want to inform you that whenever you use my Service, in a case of an error in the app We collect data and information (through third party products) on your phone called Log Data. This Log Data may include information such as your device Internet Protocol (&ldquo;IP&rdquo;) address, device name, operating system version, the configuration of the app when utilizing my Service, the time and date of your use of the Service, and other statistics.</p>

				<p><strong>Cookies</strong></p>

				<p>Cookies are files with a small amount of data that are commonly used as anonymous unique identifiers. These are sent to your browser from the websites that you visit and are stored on your device&#39;s internal memory.</p>

				<p>This Service does not use these &ldquo;cookies&rdquo; explicitly. However, the app may use third party code and libraries that use &ldquo;cookies&rdquo; to collect information and improve their services. You have the option to either accept or refuse these cookies and know when a cookie is being sent to your device. If you choose to refuse our cookies, you may not be able to use some portions of this Service.</p>

				<p><strong>Service Providers</strong></p>

				<p>We may employ third-party companies and individuals due to the following reasons:</p>

				<ul>
					<li>To facilitate our Service;</li>
					<li>To provide the Service on our behalf;</li>
					<li>To perform Service-related services; or</li>
					<li>To assist us in analyzing how our Service is used.</li>
				</ul>

				<p>We want to inform users of this Service that these third parties have access to your Personal Information. The reason is to perform the tasks assigned to them on our behalf. However, they are obligated not to disclose or use the information for any other purpose.</p>

				<p><strong>Payment and billing information:</strong></p>

				<p>We might collect your billing name, billing address and payment method when you buy a ticket. We NEVER collect your credit card number or credit card expiry date or other details pertaining to your credit card on our website. Credit card information will be obtained and processed by our online payment partner CC Avenue.</p>

				<p><strong>Security</strong></p>

				<p>We value your trust in providing us with your Personal Information, thus we are striving to use commercially acceptable means of protecting it. But remember that no method of transmission over the Internet, or method of electronic storage is 100% secure and reliable, and we cannot guarantee its absolute security.</p>

				<p><strong>Links to Other Sites</strong></p>

				<p>This Service may contain links to other sites. If you click on a third-party link, you will be directed to that site. Note that these external sites are not operated by me. Therefore, we strongly advise you to review the Privacy Policy of these websites. We have no control over and assume no responsibility for the content, privacy policies, or practices of any third-party sites or services.</p>

				<p><strong>Children&rsquo;s Privacy</strong></p>

				<p>These Services do not address anyone under the age of 13. We do not knowingly collect personally identifiable information from children under 13. In the case we discover that a child under 13 has provided me with personal information, we immediately delete this from our servers. If you are a parent or guardian and you are aware that your child has provided us with personal information, please contact me so that we will be able to take necessary actions.</p>

				<p><strong>Changes to This Privacy Policy</strong></p>

				<p>We may update our Privacy Policy from time to time. Thus, you are advised to review this page periodically for any changes. We will notify you of any changes by posting the new Privacy Policy on this page. These changes are effective immediately after they are posted on this page.</p>

				<p><strong>Contact Us</strong></p>

				<p>If you have any questions or suggestions about our Privacy Policy, do not hesitate to contact us at contact@kidiatric.app</p>
			</div>

        </section>

        <script src="{{ asset('js/home/jquery.min.js') }}"></script>
        <script src="{{ asset('js/home/jquery.dropotron.min.js') }}"></script>
        <script src="{{ asset('js/home/jquery.scrollex.min.js') }}"></script>
        <script src="{{ asset('js/home/jquery.scrolly.min.js') }}"></script>
        <script src="{{ asset('js/home/jquery.selectorr.min.js') }}"></script>
        <script src="{{ asset('js/home/browser.min.js') }}"></script>
        <script src="{{ asset('js/home/breakpoints.min.js') }}"></script>
        <script src="{{ asset('js/home/util.js') }}"></script>
        <script src="{{ asset('js/home/main.js') }}"></script>

    </body>
</html>