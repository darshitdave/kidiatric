$(document).on('click', '.next', function() {
    $("#mobile_number").parent().next(".validation").remove();
    var mobile_number = $('#mobile_number').val();
    var check_length = $('#mobile_number').val().length;

    if(check_length == 10){
        if(mobile_number != ''){
            $("#mobile_number").parent().next(".validation").remove();
            $(this).closest('.card-ui').append("<div class='loading-container'></div> ");
            $.ajax({
                type: 'post',
                url: 'check-mobile',
                data: {
                    'mobile_number': mobile_number,
                },
                success: function(data) {
                    $('.loading-container').remove();
                    if(data == 'false'){
                        $("#mobile_number").parent().after("<div class='validation' style='color:red;margin:-10px 0 10px 6px'>Please Enter Your Registered Mobile Number!</div>");
                    } else if(data == 'deactivate'){
                        $("#mobile_number").parent().after("<div class='validation' style='color:red;margin:-10px 0 10px 6px'>Your profile has been deactivated, please contact admin</div>");
                    } else if(data == 'delete'){
                        $("#mobile_number").parent().after("<div class='validation' style='color:red;margin:-10px 0 10px 6px'>Your profile has been deactivated, please contact admin</div>");
                    } else if(data == 'disapprove'){
                        $("#mobile_number").parent().after("<div class='validation' style='color:red;margin:-10px 0 10px 6px'>Please contact admin, your profile has been disapproved</div>");
                    } else {
                        $('.mobile').remove();
                        $('.otp').show();
                        $('.spiner').hide();
                        $('#otp').html(data);
                        $('#otp_verification').focus();
                        var fiveMinutes = 90;
                        display = $('#time');
                        startTimer(fiveMinutes,display);
                        $('.timer').show();
                        var interval = setInterval(function() {
                            $('.timer').hide();
                            $('.resendOtp').show();
                            clearInterval(interval);
                        }, 90000);
                    }
                   
                }
            });
        } else {
             $("#mobile_number").parent().after("<div class='validation' style='color:red;margin:-10px 0 10px 6px'>Please Enter Mobile Number!</div>");
        }
    }
});

$("input#mobile_number").keyup(function(){
    $("#mobile_number").parent().next(".validation").remove();
});

function startTimer(duration) {
    var timer = duration, minutes, seconds;
    var timerInterval = setInterval(function () {
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.text(minutes + ":" + seconds);

        if (--timer < 0) {
            timer = duration;
            clearInterval(timerInterval);
        }
    }, 1000);
}

$(document).on('click','.resendOtp',function(){

    $(this).closest('.card-ui').append("<div class='loading-container'></div> ");
    $('.resendOtp').hide();
    $.ajax({
        type: 'post',
        url: 'resend-web-otp',
        data: {
            'mobile_number': $('#new_mobile_number').val(),
        },
        success: function(data) {
            $('.loading-container').remove();
            $('.timer').show();
            var fiveMinutes = 90,
            display = $('#time');
            startTimer(fiveMinutes, display);
            $('.timer').show();
            setInterval(function() {
                $('.timer').hide();
                $('.resendOtp').show();
            }, 90000);
        }
    });
});
    