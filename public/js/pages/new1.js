
$(document).on('click', '.next', function() {
   
    var mobile_number = $('#mobile_number').val();
    var doctor_name = $('.doctor_name').val();

    if(doctor_name != '' && mobile_number != ''){
        $("#mobile_number").parent().next(".validation").remove();
        $(".doctor_name").parent().next(".validation").remove();
        if($('input[type="checkbox"]').prop("checked") == true){
            $("#term").text('');
        }
        $.ajax({
            type: 'post',
            url: '{{ route("doctor.register_mobile") }}',
            data: {
                'mobile_number': mobile_number,
                'doctor_name': doctor_name,
                '_token' : '{{csrf_token()}}'
            },
            success: function(data) {
                if(data == 'false'){
                    $("#mobile_number").parent().after("<div class='validation' style='color:red;margin-bottom: 10px;'>Mobile Number Already Exists!</div>");
                }else{

                    $("#mobile_number").parent().next(".validation").remove();
                    $('.mobile').remove();
                    $('.otp').show();
                    $('#otp').html(data);
                }
               
            }
        });
    }else{
        $(".doctor_name").parent().after("<div class='validation' style='color:red;margin-bottom: 10px;'>Enter Name!</div>");
        $("#mobile_number").parent().after("<div class='validation' style='color:red;margin-bottom: 10px;'>Enter Doctor Number!</div>");
    }
    
});



$("input#mobile_number").keyup(function(){
    $("#mobile_number").parent().next(".validation").remove();
});



$("input.doctor_name").keyup(function(){
    $(".doctor_name").parent().next(".validation").remove();
});

$('.next').click(function(){
    if($('input[type="checkbox"]').prop("checked") == false){
        $("#term").text('Please Check T&C');
    }

});
