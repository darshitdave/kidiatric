$(document).ready(function(){
    $('.edit').hide();
});

$(document).on("change",'.changeStatus',function(){
    if(this.checked){
        var status = "1";
    } else {
        var status = "0";
    }
    var vac_id = $(this).data('id');

    $.ajax({
        type: "post",
        url: 'admin-doc-vac-status',
        data:{ 'status' : status,'id' : vac_id},
        success:function(data){
            if(data == 'true'){
                if(status == 1){
                    msg = 'Status Enabled!';
                } else {
                    msg = 'Status Disabled!';
                }
            } else {        
               msg = 'Something Went Wrong';
            }

            $.notify({ title: '', message: msg},{
            placement: {
                align: "right",
                from: "top"
            },
                timer: 500,
                type: 'success',
            });
        }
    });
});

$(document).on('click', '.taken', function() {

    var id = $(this).data('id');

    $('.edit'+id).show();
    $('.close'+id).show();
    
    $(document).on('click','.edit'+id, function() {
        
        var taken_at = $("input.at"+id).val();

        $.ajax({
            type: 'post',
            url: 'admin-doctor-save-taken',
            data: {
                'taken_at': taken_at,
                'id' : id
            },
            success: function(data) {
                if(data == 'true'){
                    $('.edit'+id).hide();
                    $('.close'+id).hide();

                    msg = 'Taken At Successfully Updated!';            
                }

                $.notify({ title: '', message: msg},{
                placement: {
                    align: "right",
                    from: "top"
                },
                    timer: 500,
                    type: 'success',
                });
            }
        });

    });    

     $(document).on('click','.close'+id, function() {
        $('.edit'+id).hide();
        $('.close'+id).hide();
    });

    $(document).on('click','.delete', function() {
        $('.close_'+$(this).data('id')).val($(this).data('value'));
        
    });
});

$(document).on('click','#createVaccinationCSV',function(){
    $('#vaccinationForm').submit();    
});