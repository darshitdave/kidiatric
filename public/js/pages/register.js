$(document).on('click', '.next', function() {

    $("#mobile_number").parent().next(".validation").remove();
    $(".doctor_name").parent().next(".validation").remove();

    var mobile_number = $('#mobile_number').val();
    var doctor_name = $('.doctor_name').val();
    
    if(doctor_name != ''){
        if(mobile_number != ''){
            $("#mobile_number").parent().next(".validation").remove();
            $(".doctor_name").parent().next(".validation").remove();
            if($('input[type="checkbox"]').prop("checked") == true){
                $("#term").text('');
            
            $.ajax({
                type: 'post',
                url: 'register-mobile',
                data: {
                    'mobile_number': mobile_number,
                    'doctor_name': doctor_name,
                    '_token' : '{{csrf_token()}}'
                },
                success: function(data) {
                    if(data == 'false'){
                        
                        $("#mobile_number").parent().after("<div class='validation' style='color:red;margin:-10px 0 10px 6px'>Mobile Number Already Exists!</div>");

                    }else if(data == 'otp_wrong'){

                        $("#mobile_number").parent().after("<div class='validation' style='color:red;margin:-10px 0 10px 6px'>Entered OTP Is Wrong!</div>");

                    }else{

                        $("#mobile_number").parent().next(".validation").remove();
                        $('.mobile').remove();
                        $('.otp').show();
                        $('#otp').html(data);
                        $('#otp_verification').focus();
                        var fiveMinutes = 90;
                        display = $('#time');
                        startTimer(fiveMinutes,display);
                        $('.timer').show();
                        var interval = setInterval(function() {
                            $('.timer').hide();
                            $('.resendOtp').show();
                            clearInterval(interval);
                        }, 90000);
                    }
                }
            });
            }else{
                $("#term").text('Please Check T&C');
            }
        }else{

            $("#mobile_number").parent().after("<div class='validation' style='color:red;margin:-10px 0 10px 6px'>Please Enter Number!</div>");
        }
    }else{
        $("#mobile_number").parent().after("<div class='validation' style='color:red;margin:-10px 0 10px 6px'>Please Enter Number!</div>");
        $(".doctor_name").parent().after("<div class='validation' style='color:red;margin:-10px 0 10px 6px'>Please Enter Name!</div>");
    }
    
});

$(document).on('click', '.submit_next', function(e) {
    e.preventDefault();
    var mobile_number = $('#new_mobile_number').val();
    var otp = $('#otp_verification').val();
    if(otp != ''){
        $.ajax({
            type: 'post',
            url: 'register-check-otp',
            data: {
                'mobile_number': mobile_number,
                'otp': otp,
                '_token' : '{{csrf_token()}}'
            },
            success: function(data) {
                if(data == 'false'){
                    $("#otp_verification").parent().next(".validation").remove();
                    $("#otp_verification").parent().after("<div class='validation' style='color:red;margin:-10px 0 10px 6px'>Invalid OTP!</div>");
                }else{

                    $("#otp_verification").parent().next(".validation").remove();
                    $("#registerDoctor").submit();
                }
            }
        });
    } else {
        $("#otp_verification").parent().next(".validation").remove();
        $("#otp_verification").parent().after("<div class='validation' style='color:red;margin:-10px 0 10px 6px'>Please Enter OTP!</div>");
    }

});

$("input#mobile_number").keyup(function(){
    $("#mobile_number").parent().next(".validation").remove();
});

$("input.doctor_name").keyup(function(){
    $(".doctor_name").parent().next(".validation").remove();
});

$('.next').click(function(){
    if($('input[type="checkbox"]').prop("checked") == false){
        $("#term").text('Please Check T&C');
    }

});

$('input[type="checkbox"]').click(function(){
    if($('input[type="checkbox"]').prop("checked") == true){
        $("#term").text('');
    }
});

function startTimer(duration) {
    var timer = duration, minutes, seconds;
    var timerInterval = setInterval(function () {
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.text(minutes + ":" + seconds);

        if (--timer < 0) {
            timer = duration;
            clearInterval(timerInterval);
        }
    }, 1000);
}

$(document).on('click','.resendOtp',function(){

    $(this).closest('.card-ui').append("<div class='loading-container'></div> ");
    $('.resendOtp').hide();
    $.ajax({
        type: 'post',
        url: 'resend-web-otp',
        data: {
            'mobile_number': $('#new_mobile_number').val(),
        },
        success: function(data) {
            $('.loading-container').remove();
            $('.timer').show();
            var fiveMinutes = 90,
            display = $('#time');
            startTimer(fiveMinutes, display);
            $('.timer').show();
            setInterval(function() {
                $('.timer').hide();
                $('.resendOtp').show();
            }, 90000);
        }
    });
});