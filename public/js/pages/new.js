
$(document).on('click', '.next', function() {
   
    var mobile_number = $('#mobile_number').val();
        
        $.ajax({
            type: 'post',
            url: 'check-mobile',
            data: {
                'mobile_number': mobile_number,
                '_token' : '{{csrf_token()}}'
            },
            success: function(data) {
                if(data == 'false'){
                    $('<span id="mobile_number-error" class="error">Mobile Number Not Exists!</span>').insertAfter('#mobile_number');
                }else{
                    $('.mobile').remove();
                    $('.otp').show();
                    $('#otp').html(data);
                }
               
            }
        });
});
