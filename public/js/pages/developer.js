$(document).ready(function(){
	$.ajax({
        type: 'get',
        url: '/administrator-panel/get-notification-count/',
        success: function(data) {
           $('.supportRequest').text(data);
        }
    });
});

$('.dropify').dropify();

$(document).on('click','.getMessage',function(){

	var getid = $(this).data('id');

	$.ajax({
        type: 'post',
        url: 'get-message-details',
        data:{
        	id : getid
        },
        success: function(data) {
           $('#modalData').html(data);
           $('#example06').modal('show');
        }
    });

});