$(document).ready(function() {
    $('#smsDiv').hide();
    $('#notificationDiv').hide();
    $('#emailDiv').hide();
    $('#button').hide();
});    
$(document).on("change",'.message',function(){
    var nf = $(this).data('id');
    $('#button').show();
    if(this.checked){
        if(nf == 'sms'){
            $('#smsDiv').show();
            $('.sms').attr('required',true);
        } else if(nf == 'nf'){
            $('#notificationDiv').show();
            $('.push').attr('required',true);
        } else if(nf == 'email') {
            $('#emailDiv').show();
            $('.email').attr('required',true);
        }
    } else {
        if(nf == 'sms'){
            $('#smsDiv').hide();
            $('.sms').attr('required',false);
        } else if(nf == 'nf') {
            $('#notificationDiv').hide();
            $('.push').attr('required',false);
        } else if(nf == 'email') {
            $('#emailDiv').hide();
            $('.email').attr('required',false);
        }
    }
});
