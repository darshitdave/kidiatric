$(document).ready(function() {
	//custom validation method
    $.validator.addMethod("customemail", 
        function(value, element) {
            return /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(value);
        }, 
        "Please enter email id along with domain name."
    );

    // validate doctor form
    $("#doctorForms").validate({
        errorElement: 'span',
        rules: {
            email: {
                customemail : true,
            },
            practicing_since: {
                minlength: 4
            },
            "clinic_phone_number[]":{
                required:true
            }
        },
        messages: {
            email:"Please enter valid email address",
            practicing_since:"Enter Valid 4 Digit Year",
        },
    });

    // validate doctor form
    $("#vaccinationForm").validate({
        errorElement: 'span',
        rules: {
            vacc_type: {
                maxlength: 4
            },
        },
        messages: {
            vacc_type:{
               maxlength:"Please Valid Number",
            },
        },
    });
});