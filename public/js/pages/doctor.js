// $(document).on('click','.addContactNo',function(){

//     var html = '<div class="form-group remove"><label for="inputContact">Clinic Phone Number<span class="mendetory">*</span></label><input type="tel" class="form-control number" name="clinic_phone_number[]" id="inputContact" placeholder="Clinic Phone Number" style="width:90%" data-msg="Please Enter Clinic Phone Number" required ><a href="javascript:void(0);" class="btn btn-danger removeInput" style="float:right;margin-top: -36px;"><i class="fe fe-trash"></i></a></div>';
//     $('.number').attr('required',true);
//     $('.contact_no').append(html);
// });

$(document).on('click','.removeInput',function(){
    $(this).closest('.remove').remove();
});

$(document).on('keyup paste','.number',function(){
    this.value = this.value.replace(/[^0-9]/g, '');
});

$(document).on('focusout','.mobile_check',function(){
   var mobile_number = $("input.mobile_check").val();
   $.ajax({
        type: 'post',
        url: 'doctor-check-mobile',
        data: {
            'mobile_number': mobile_number
        },
        success: function(data) {
            if(data == 'true'){
                $(':input[type="submit"]').prop('disabled', true);
                if ($(".mobile_check").parent().next(".validation").length == 0) // only add if not added
                {
                    $(".mobile_check").parent().after("<div class='validation' style='color:red;'>Mobile Number Already Exists</div>");
                }
                
            } else {
                $(':input[type="submit"]').prop('disabled',false);
                $(".mobile_check").parent().next(".validation").remove();
            }
        }
    });

});

$("input.year_check").keyup(function(){
    $(".year_check").parent().next(".validation").remove();
});


$(document).on('focusout','.year_check',function(){

    var year = $("input.year_check").val();
    var str = "01/05/"+year;
    var year = str.match(/\/(\d{4})/)[1];
    var currYear = new Date().toString().match(/(\d{4})/)[1];
    
    if (year > currYear) {

        $(':input[type="submit"]').prop('disabled', true);

        if ($(".year_check").parent().next(".validation").length == 0) // only add if not added
        {
           $(".year_check").parent().after("<div class='validation' style='color:red;'>Please Enter Valid Year</div>");
        }
    } else {

        $(':input[type="submit"]').prop('disabled',false);
        $(".year_check").parent().next(".validation").remove();
    }
});
    
var isPlaceAuthentic = false;
var lastPlaceAuthenticated = '';

function initialize() {
    var input = (document.getElementById('pacinput'));
    var autocomplete = new google.maps.places.Autocomplete(input);
    var infowindow = new google.maps.InfoWindow();
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        infowindow.close();
        var place = autocomplete.getPlace();
        isPlaceAuthentic = true;
        lastPlaceAuthenticated = $('#pacinput').val();
        isPlaceAuthentic = false;
        var address = '';
        var toInsertData = place.adr_address;
        var latti = place.geometry.location.lat();
        var longi = place.geometry.location.lng();
        var addressToInsert = toInsertData.substr(0, toInsertData.indexOf('<')).trim();
        if (toInsertData.indexOf('<') !== 1) {
            toInsertData = toInsertData.substr(toInsertData.indexOf('<'));
        }
        var streetAddress = $(toInsertData).filter('.street-address').text().trim();
        var extendedAddress = $(toInsertData).filter('.extended-address').text().trim();
        var cityName = $(toInsertData).filter('.locality').text().trim();
        var stateName = $(toInsertData).filter('.region').text().trim();
        var countryName = $(toInsertData).filter('.country-name').text().trim();

        $("#latitude").val(latti);
        $("#longitude").val(longi);
        $("#city").val(cityName);
        $("#state").val(stateName);
        $("#country").val(countryName);
        
        var appenedAddress = addressToInsert.concat(streetAddress);
        appenedAddress = appenedAddress.concat(extendedAddress);
        if (place.address_components) {
            address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
        }
        infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
    });
}


google.maps.event.addDomListener(window, 'load', initialize);
$("#area_submit").on('click',function(e){
    if(($("#latitude").val() == "" || $("#longitude").val() == "")){
        alert("Please Enter Valid Address");
        e.preventDefault();
    }
});

$(document).on("change",'.changeCategory',function(){
    if(this.checked){
        var status = "1";
    } else {
        var status = "0";
    }
    var city_id = $(this).data('id');

    $.ajax({
        type: "post",
        url: 'change-doctor-status',
        data:{ 'status' : status,'id' : city_id},
        success:function(data){
            if(data == 'true'){
                if(status == 1){
                    msg = 'Status Enabled!';
                } else {
                    msg = 'Status Disabled!';
                }
            } else {        
               msg = 'Something Went Wrong!';
            }

            $.notify({ title: '', message: msg},{
            placement: {
                align: "right",
                from: "top"
            },
                timer: 500,
                type: 'success',
            });
        }
    });
});

$(document).on('click','#createDoctorCSV',function(){
    $('#doctorForm').submit();    
});
